<?php

namespace app\helpers;

use app\models\Account;
use app\models\AgreementAct;
use app\models\AgreementAnnex;
use app\models\AgreementAnnexA;
use app\models\AgreementStatus;
use app\models\Bank;
use app\models\Entity;
use app\models\Pattern;
use app\models\Person;
use app\models\Staff;
use app\models\Tools;
use Yii;
use yii\base\DynamicModel;
use yii\base\InvalidConfigException;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;

class ProgramsHelper
{

    CONST INFINITY_TRIES_AMOUNT = 500;

    private static $_SERVICE_TRIES_INFINITELY = [
        105,
        108,
    ];


    public static function serviceNonLimitedTestTries($svcId)
    {
        return in_array($svcId, self::$_SERVICE_TRIES_INFINITELY);
    }

}
