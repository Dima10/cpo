<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TrainingProgModuleSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="training-prog-module-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'trpl_id') ?>

    <?= $form->field($model, 'trpl_trm_id') ?>

    <?= $form->field($model, 'trpl_trp_id') ?>

    <?= $form->field($model, 'trpl_create_user') ?>

    <?= $form->field($model, 'trpl_create_time') ?>

    <?php // echo $form->field($model, 'trpl_create_ip') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
