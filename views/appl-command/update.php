<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ApplCommand */
/* @var $ab array */
/* @var $comp array */
/* @var $searchModel app\models\ApplCommandContentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $trt array */
/* @var $svdt array */
/* @var $pat array */
/* @var $reestr array */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => Yii::t('app', 'Appl Command'),
]) . $model->applcmd_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Appl Commands'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->applcmd_id, 'url' => ['view', 'id' => $model->applcmd_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');




$js = '
        $("#request_s").change(function() {
            var val = $("#request_s").val();
            $.get("'.\yii\helpers\Url::to(['/appl-request/ajax-search']).'",
                {
                    id: "request_id",
                    text : val
                },
                function (data) {
                    $("#request_id").html(data);
                }
            );
        });
';

$this->registerJs($js, yii\web\View::POS_READY);


$js = '
        $("#request_id").change(function() {
            var val = $("#request_id").val();
            $.get("'.\yii\helpers\Url::to(['/appl-request/ajax-person']).'",
                {
                    id: "request_prs",
                    applr : val
                },
                function (data) {
                    $("#request_prs").html(data);
                }
            );
        });
';

$this->registerJs($js, yii\web\View::POS_READY);

$js = '
        $("#btn_add").click(function() {
            var v1 = $("#request_id").val();
            var v2 = $("#request_prs").val();
            var v3 = '.$model->applcmd_id.';
            $.get("'.\yii\helpers\Url::to(['/appl-command/ajax-add-person']).'",
                {
                    req: v1,
                    prs: v2,
                    cmd: v3
                },
                function (data) {
                    $("#applcmdc").html(data);
                }
            );
        });
';

$this->registerJs($js, yii\web\View::POS_READY);

?>

<div class="appl-command-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        //'ab' => $ab,
        'comp' => $comp,

        'trt' => $trt,
        'svdt' => $svdt,
        'pat' => $pat,
        'reestr' => $reestr,
    ]) ?>

    <hr>
    <div class="container">
        <div class="row">
            <div class="col-sm-2">
                <?= Html::label('Поиск заявки', ['request_s']) ?>
            </div>
            <div class="col-sm-4">
                <?= Html::textInput('request_s', '', ['id' => 'request_s', 'class' => 'form-control']) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-2">
                <?= Html::label('Выбор заявки', ['request_id']) ?>
            </div>
            <div class="col-sm-4">
                <?= Html::dropDownList('request_id', null, [], ['id' => 'request_id', 'class' => 'form-control']) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-2">
                <?= Html::label('Выбор обучаемого', ['request_prs']) ?>
            </div>
            <div class="col-sm-4">
                <?= Html::dropDownList('request_prs', null, [], ['id' => 'request_prs', 'class' => 'form-control']) ?>
            </div>
        </div>
    </div>

    <form class="form-group">

    </form>
    <?= Html::a('Добавить', '#', ['id' => 'btn_add', 'class' => 'btn btn-success']) ?>
    <hr>



    <?= $this->render('_grid_content', [
        'dataProvider' => $dataProvider,
        'searchModel' => $searchModel,
    ]) ?>

</div>
