<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\ApplXxxContentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<div class="appl-xxx-content-index-grid">

<?php

Pjax::begin();

    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            ['class' => 'yii\grid\ActionColumn'],

            'applxxxc_id',
            'applxxxcApplxxx.applxxx_number',
            'applxxxcPrs.prs_full_name',
            'applxxxcTrp.trp_name',
            [
                'attribute' => 'applxxxc_position',
                'visible' => !\app\models\User::isSpecAdmin(),
            ],
            'applxxxcAb.ab_name',
            'applxxxc_passed',

            [
                'attribute' => 'applxxxc_create_user',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'applxxxc_create_time',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'applxxxc_create_ip',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'applxxxc_update_user',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'applxxxc_update_time',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'applxxxc_update_ip',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],
            
        ],
    ]);

Pjax::end();


?>

</div>
