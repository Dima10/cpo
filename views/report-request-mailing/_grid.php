<?php

use app\models\ApplRequestContent;
use app\models\SentEmailsFromRequest;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\ApplRequestContentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<div class="report-listener-index-grid">

<?php

Pjax::begin();

try {
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'layout'=>"{pager}\n{summary}\n{items}\n{summary}\n{pager}",
        'pager' => [
            'firstPageLabel' => Yii::t('app', 'First'),
            'lastPageLabel' => Yii::t('app', 'Last'),
            'maxButtonCount' => 20,

        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'applr_id',
                'label' => Yii::t('app', 'Appl Request'),
                'value' => function (app\models\SentEmailsFromRequest $data) {
                    return $data->applr_id;
                },
            ],
            [
                'attribute' => 'applrc_prs_id',
                'label' => Yii::t('app', 'Prs ID'),
                'value' => function (app\models\SentEmailsFromRequest $data) {
                    return $data->applrc_prs_id ?? '';
                },
                'visible' => !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'login',
                'label' => Yii::t('app', 'Prs Connect User'),
                'value' => function (app\models\SentEmailsFromRequest $data) {
                    return $data->login ?? '';
                },
            ],
            [
                'attribute' => 'fio',
                'label' => Yii::t('app', 'Prs Full Name'),
                'value' => function (app\models\SentEmailsFromRequest $data) {
                    return $data->fio ?? '';
                },
            ],
            [
                'attribute' => 'trp_name',
                'label' => Yii::t('app', 'Sprog Trp ID'),
                'value' => function (app\models\SentEmailsFromRequest $data) {
                    return $data->applrcTrp->trp_name ?? '';
                },
            ],
            [
                'attribute' => 'ab_id',
                'label' => Yii::t('app', 'Ab ID Code'),
                'value' => function (app\models\SentEmailsFromRequest $data) {
                    return $data->ab->ab_id ?? '';
                },
                'visible' => !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'ab_name',
                'label' => Yii::t('app', 'Ab Name UR'),
                'value' => function (app\models\SentEmailsFromRequest $data) {
                    return $data->ab->ab_name ?? '';
                },
            ],
            [
                'attribute' => 'manager_ab_name',
                'label' => Yii::t('app', 'Applr Manager ID'),
                'value' => function (app\models\SentEmailsFromRequest $data) {
                    return $data->manager->ab_name ?? '';
                },
                'visible' => !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'recipient_email',
                'label' => Yii::t('app', 'Recipient Email'),
                'value' => function (app\models\SentEmailsFromRequest $data) {
                    return $data->recipient_email ?? '';
                },
            ],
            [
                'attribute' => 'sender_email',
                'label' => Yii::t('app', 'Sender Email'),
                'value' => function (app\models\SentEmailsFromRequest $data) {
                    return $data->sender_email ?? '';
                },
            ],
            [
                'attribute' => 'status',
                'label' => Yii::t('app', 'Status'),
                'value' => function (app\models\SentEmailsFromRequest $data) {
                    return ($data->status == 1) ? 'Отправлено' : 'Не отправлено';
                },
                'filter' => [0 => Yii::t('app', 'Не отправлено'), 1 => Yii::t('app', 'Отправлено')],
            ],
            [
                'attribute' => 'send_date',
                'label' => Yii::t('app', 'Send Date'),
                'value' => function (app\models\SentEmailsFromRequest $data) {
                    return $data->send_date;
                },
            ],
            [
                'attribute' => 'applr_reestr',
                'label' => Yii::t('app', 'Applr Reestr'),
                'value' => function (app\models\SentEmailsFromRequest $data) {
                    $reestrsArray = \app\models\Constant::reestr_val();
                    return isset($reestrsArray[$data->applr_reestr]) ? $reestrsArray[$data->applr_reestr] : $reestrsArray[0];
                },
                'filter' => \app\models\Constant::reestr_val(true),
                'visible' => !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'applr_flag',
                'label' => Yii::t('app', 'Applr Flag'),
                'value' => function (app\models\SentEmailsFromRequest $data) {
                    return \app\models\Constant::YES_NO[$data->applr_flag ?? ''];
                },
                'filter' => \app\models\Constant::yes_no(true),
                'visible' => !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'portal',
                'label' => Yii::t('app', 'Время первого входа на портал после отправки'),
                'value' => function (app\models\SentEmailsFromRequest $data) {
                    return $data->portal;
                },
                'visible' => !\app\models\User::isSpecAdmin(),
//                'filter' => false,
            ],




            [
                'attribute' => 'appls_sh_passed',
                'label' => Yii::t('app', 'Appl Sh Test'),
                'value' => function (app\models\SentEmailsFromRequest $data) {
                    return (isset($data->applSh) && $data->applSh->appls_passed == 1) ? 'Сдано' : 'Не сдано';
                },
                'filter' => [0 => Yii::t('app', 'Не сдано'), 1 => Yii::t('app', 'Сдано')],
                'visible' => !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'appl_sh_date_end',
                'label' => Yii::t('app', 'Appl Sh Test Date End'),
                'value' => function (app\models\SentEmailsFromRequest $data) {
                    return $data->applSh->appls_date ?? '';
                },
                'visible' => !\app\models\User::isSpecAdmin(),
            ],

            [
                'attribute' => 'appls_sheet_passed',
                'label' => Yii::t('app', 'Appl Sheet Test'),
                'value' => function (app\models\SentEmailsFromRequest $data) {
                    return (isset($data->applSheet) && $data->applSheet->appls_passed == 1) ? 'Сдано' : 'Не сдано';
                },
                'filter' => [0 => Yii::t('app', 'Не сдано'), 1 => Yii::t('app', 'Сдано')],
                'visible' => !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'appl_sheet_date_end',
                'label' => Yii::t('app', 'Appl Sheet Test Date End'),
                'value' => function (app\models\SentEmailsFromRequest $data) {
                    return $data->applSheet->appls_date ?? '';
                },
                'visible' => !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'applf_file_name',
                'label' => Yii::t('app', 'Finish education'),
                'value' => function (app\models\SentEmailsFromRequest $data) {
                    return (!empty($data->applFinal->applf_file_name)) ? 'Завершено' : 'Не завершено';
                },
                'filter' => [0 => Yii::t('app', 'Не завершено'), 1 => Yii::t('app', 'Завершено')],
                'visible' => !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'applf_end_date',
                'label' => Yii::t('app', 'Applf End Date'),
                'value' => function (app\models\SentEmailsFromRequest $data) {
                    return $data->applFinal->applf_end_date ?? '';
                },
                'visible' => !\app\models\User::isSpecAdmin(),
            ],



        ],
    ]);
} catch (Exception $e) {
    echo $e->getMessage();
}

Pjax::end();


?>

</div>
