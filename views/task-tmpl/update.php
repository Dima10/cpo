<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TaskTmpl */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => Yii::t('app', 'Task Tmpl'),
]) . $model->tasktp_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Task Tmpls'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->tasktp_id, 'url' => ['view', 'id' => $model->tasktp_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="task-tmpl-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
