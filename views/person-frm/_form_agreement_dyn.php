<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model yii\base\DynamicModel */
/* @var $modelA app\models\Agreement */
/* @var $form yii\widgets\ActiveForm */
/* @var $person array */
/* @var $company array */
/* @var $pattern array */

?>

<div class="person-frm-agreement-form">

    <?php
        $form = ActiveForm::begin([
                'action' => ['create-agreement', 'prs_id' => array_keys($person)[0]],
                'fieldConfig' => [
                    'template' => '<div class="row"><div class="col-sm-2">{label}</div><div class="col-sm-6">{input}</div><div class="col-sm-4">{error}</div></div>',
                ],        
            ]);

        echo isset($modelA) ? $form->errorSummary($modelA) : '';

    ?>

    <?php //  $form->field($model, 'number')->textInput(['ReadOnly' => true])->label(Yii::t('app', 'Agr Number'))  ?>

    <?=  $form->field($model, 'date')->widget(\yii\jui\DatePicker::class, [
                //'language' => 'ru',
                'dateFormat' => 'yyyy-MM-dd',
        ])->label(Yii::t('app', 'Agr Date'))
    ?>

    <?=  $form->field($model, 'sum')->textInput()->label(Yii::t('app', 'Agr Sum'))  ?>

    <?=  $form->field($model, 'tax')->textInput()->label(Yii::t('app', 'Agr Tax'))  ?>

    <?= $form->field($model, 'prs_id')->dropDownList($person, ['ReadOnly' => true])->label(Yii::t('app', 'Person')) ?>

    <?= $form->field($model, 'comp_id')->dropDownList($company)->label(Yii::t('app', 'Company')) ?>

    <?= $form->field($model, 'pat_id')->dropDownList($pattern)->label(Yii::t('app', 'Pattern')) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Create'), ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
