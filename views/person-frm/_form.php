<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Person */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="person-frm-form">

    <?php
        $form = ActiveForm::begin([
                'fieldConfig' => [
                    'template' => '<div class="row"><div class="col-sm-2">{label}</div><div class="col-sm-6">{input}</div><div class="col-sm-4">{error}</div></div>',
                ],        
            ]);
    ?>

    <?= $form->field($model, 'prs_last_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'prs_first_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'prs_middle_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'prs_pass_sex')->radioList([0 => 'м', 1 => 'ж']) ?>

    <?= (\app\models\User::isSpecAdmin())
        ? $form->field($model, 'prs_birth_date')->hiddenInput()->label(false)
        : $form->field($model, 'prs_birth_date')->textInput()
    ?>

    <?= (\app\models\User::isSpecAdmin())
        ? $form->field($model, 'prs_inn')->hiddenInput(['maxlength' => true])->label(false)
        : $form->field($model, 'prs_inn')->textInput(['maxlength' => true])
    ?>

    <?= (\app\models\User::isSpecAdmin())
        ? $form->field($model, 'prs_pass_serial')->hiddenInput(['maxlength' => true])->label(false)
        : $form->field($model, 'prs_pass_serial')->textInput(['maxlength' => true])
    ?>

    <?= (\app\models\User::isSpecAdmin())
        ? $form->field($model, 'prs_pass_number')->hiddenInput(['maxlength' => true])->label(false)
        : $form->field($model, 'prs_pass_number')->textInput(['maxlength' => true])
    ?>

    <?= (\app\models\User::isSpecAdmin())
        ? $form->field($model, 'prs_pass_issued_by')->hiddenInput(['maxlength' => true])->label(false)
        : $form->field($model, 'prs_pass_issued_by')->textInput(['maxlength' => true])
    ?>

    <?= (\app\models\User::isSpecAdmin())
        ? $form->field($model, 'prs_pass_date')->hiddenInput(['maxlength' => true])->label(false)
        : $form->field($model, 'prs_pass_date')->textInput(['maxlength' => true])
    ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
