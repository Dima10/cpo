<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Contact */
/* @var $contactType array */
/* @var $person array */

$this->title = Yii::t('app', 'Create Contact');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Frm'), 'url' => ['site/frm']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Persons'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Person'), 'url' => ['view', 'id' => array_keys($person)[0]]];
$this->params['breadcrumbs'][] = ['label' => array_keys($person)[0], 'url' => ['view', 'id' => array_keys($person)[0]]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="person-frm-contact-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form_contact', [
        'model' => $model,
        'person' => $person,
        'contactType' => $contactType,
    ]) ?>

</div>
