<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model yii\base\DynamicModel */
/* @var $modelA app\models\AgreementAnnex */
/* @var $agreement array */
/* @var $account array */
/* @var $person array */
/* @var $pattern array */
/* @var $svc array */
/* @var $dataProvider yii\data\ArrayDataProvider */

$this->title = Yii::t('app', 'Create Agreement Annex');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Frm'), 'url' => ['site/frm']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Persons'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Person'), 'url' => ['view', 'id' => array_keys($person)[0]]];
$this->params['breadcrumbs'][] = ['label' => array_keys($person)[0], 'url' => ['view', 'id' => array_keys($person)[0]]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="person-frm-agreement-annex-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form_agreement_annex_dyn', [
        'model' => $model,
        'modelA' => $modelA,
        'person' => $person,
        'agreement' => $agreement,
        'account' => $account,
        'pattern' => $pattern,
        'svc' => $svc,
        'dataProvider' => $dataProvider,

    ]) ?>


</div>
