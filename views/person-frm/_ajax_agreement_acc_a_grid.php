<?php

use yii\helpers\Html;
use yii\grid\GridView;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ArrayDataProvider */

?>

<?php
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'options' => ['id' => 'grid_acc'],
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{delete}',
                    'buttons' => [
                        'delete' =>
                            function($url, $model) {
                                return Html::a('', '#',
                                    [
                                        'class' => 'glyphicon glyphicon-trash',
                                        'onclick' => '$.post("'.\yii\helpers\Url::toRoute(['/person-frm/ajax-agreement-acc-a-delete', 'id'=>$model['ana_id']]).'",
                                                                            function(data) {
                                                                                $( \'#grid_acc\' ).html(data);
                                                                                $.get("'.\yii\helpers\Url::to(['/person-frm/ajax-agreement-acc-a-sum']).'",
                                                                                    {
                                                                                    },
                                                                                    function (data) {
                                                                                        $("#sum").val(data);
                                                                                        $.get("'.\yii\helpers\Url::to(['/person-frm/ajax-agreement-acc-a-tax']).'",
                                                                                            {
                                                                                            },
                                                                                            function (data) {
                                                                                                $("#tax").val(data);
                                                                                            }
                                                                                        );
                                                                                     }
                                                                                 );
                                                                               
                                                                                
                                                                            });',
                                    ]
                                );
                            }

                    ],
                ],
                [
                    'label' => Yii::t('app', 'Ana ID'),
                    'value' => 'ana_id',
                ],
                [
                    'label' => Yii::t('app', 'Aca Ana ID'),
                    'value' => 'ana_name',
                ],
                [
                    'label' => Yii::t('app', 'Aca Qty'),
                    'value' => 'qty',
                ],
                [
                    'label' => Yii::t('app', 'Aca Price'),
                    'value' => 'price',
                ],
                [
                    'label' => Yii::t('app', 'Aca Tax'),
                    'value' => function ($data) { return $data['tax']==0 ? Yii::t('app', 'Without Tax') : $data['tax'];  },
                ],

            ],
        ]);


?>

