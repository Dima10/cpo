<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\ContactSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>


<?php

try {
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => null,
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn',
                'options' => ['width' => Yii::$app->params['yii\grid\SerialColumn']['width']],
            ],
            [
                'attribute' => 'applrc_applr_id',
                'label' => Yii::t('app', 'Applrc Applr ID'),
                'format' => 'raw',
                'value' => function (\app\models\ApplRequestContent $data) {
                    return  Html::a($data->applrcApplr->applrAb->ab_name, \yii\helpers\Url::to(['entity-frm/view', 'id' => $data->applrcApplr->applr_ab_id]), ['target' => '_blank']);
                },
            ],
            [
                'attribute' => 'applrc_applr_id',
                'label' => Yii::t('app', 'Applrc Applr ID'),
                'value' => function ($data) {
                    return $data->applrcApplr->applr_number;
                },
            ],
            [
                'attribute' => 'applrc_applr_id',
                'label' => Yii::t('app', 'Applr Date'),
                'value' => function ($data) {
                    return $data->applrcApplr->applr_date;
                },
            ],
            [
                'attribute' => 'applrc_applr_id',
                'label' => Yii::t('app', 'Applr Ast ID'),
                'value' => function ($data) {
                    return $data->applrcApplr->applrAst->ast_name;
                },
            ],
            [
                'attribute' => 'applrc_applr_id',
                'label' => Yii::t('app', 'Applr Reestr'),
                'value' => function ($data) {
                    $reestrsArray = \app\models\Constant::reestr_val();
                    return isset($reestrsArray[$data->applrcApplr->applr_reestr]) ? $reestrsArray[$data->applrcApplr->applr_reestr] : $reestrsArray[0];
                    return \app\models\Constant::reestr_val()[$data->applrcApplr->applr_reestr];
                },
            ],
            [
                'attribute' => 'applrc_svc_id',
                'label' => Yii::t('app', 'Applrc Svc ID'),
                'value' => function ($data) {
                    return $data->applrcSvc->svc_name;
                },
            ],
            [
                'attribute' => 'applrc_trp_id',
                'label' => Yii::t('app', 'Applrc Trp ID'),
                'value' => function ($data) {
                    return $data->applrcTrp->trp_name;
                },
            ],
            [
                'attribute' => 'applrc_applr_id',
                'label' => Yii::t('app', 'Applr Flag'),
                'value' =>
                    function ($data) {
                        try {
                            return \app\models\Constant::YES_NO[$data->applrcApplr->applr_flag];
                        } catch (Exception $e){
                            return $data->applrcApplr->applr_flag;
                        }
                    },
            ],


            [
                'attribute' => 'applrc_create_user',
                'visible' => Yii::$app->user->identity->level >= 80,
            ],
            [
                'attribute' => 'applrc_create_time',
                'visible' => Yii::$app->user->identity->level >= 80,
            ],
            [
                'attribute' => 'applrc_create_ip',
                'visible' => Yii::$app->user->identity->level >= 80,
            ],
            [
                'attribute' => 'applrc_update_user',
                'visible' => Yii::$app->user->identity->level >= 80,
            ],
            [
                'attribute' => 'applrc_update_time',
                'visible' => Yii::$app->user->identity->level >= 80,
            ],
            [
                'attribute' => 'applrc_update_ip',
                'visible' => Yii::$app->user->identity->level >= 80,
            ],
        ],
    ]);
} catch (Exception $e) {
    echo $e->getMessage();
}

