<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ContactSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="contact-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'con_id') ?>

    <?= $form->field($model, 'con_ab_id') ?>

    <?= $form->field($model, 'con_cont_id') ?>

    <?= $form->field($model, 'con_text') ?>

    <?= $form->field($model, 'con_create_user') ?>

    <?php // echo $form->field($model, 'con_create_time') ?>

    <?php // echo $form->field($model, 'con_create_ip') ?>

    <?php // echo $form->field($model, 'con_update_user') ?>

    <?php // echo $form->field($model, 'con_update_time') ?>

    <?php // echo $form->field($model, 'con_update_ip') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
