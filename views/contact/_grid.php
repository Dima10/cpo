<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\ContactSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $addrbook array */
/* @var $contactType array */


Pjax::begin(); 
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn',
                'options' => ['width' => Yii::$app->params['yii\grid\SerialColumn']['width']],
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'options' => ['width' => Yii::$app->params['yii\grid\ActionColumn']['width']],
            ],

            [
                'attribute' => 'con_id',
                'options' => ['width' => '100'],
            ],
            [
                'attribute' => 'contragent',
                'label' => Yii::t('app', 'Con Ab ID'),
                'value' => function ($data) { return $data->conAb->ab_name; },
//                'filter' => $addrbook,
            ],
            [
                'attribute' => 'con_cont_id',
                'label' => Yii::t('app', 'Con Cont ID'),
                'value' => function ($data) { return $data->conCont->cont_name; },
                'filter' => $contactType,
            ],
            
            'con_text:ntext',

            [
                'attribute' => 'con_create_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'con_create_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'con_create_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'con_update_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'con_update_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'con_update_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
        ],
    ]);
Pjax::end();

