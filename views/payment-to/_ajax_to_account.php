<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PaymentTo */
/* @var $account array */
/* @var $id string */

?>

<?= Html::dropDownList($model->payt_id, $model->payt_to_acc_id, $account, ['id' => $id, 'payt_id' => $model->payt_id,]) ?>


