<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AgreementAnnexSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="agreement-annex-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'agra_id') ?>

    <?= $form->field($model, 'agra_number') ?>

    <?= $form->field($model, 'agra_agr_id') ?>

    <?= $form->field($model, 'agra_sum') ?>

    <?= $form->field($model, 'agra_data') ?>

    <?php // echo $form->field($model, 'agra_data_sign') ?>

    <?php // echo $form->field($model, 'agra_create_user') ?>

    <?php // echo $form->field($model, 'agra_create_time') ?>

    <?php // echo $form->field($model, 'agra_create_ip') ?>

    <?php // echo $form->field($model, 'agra_update_user') ?>

    <?php // echo $form->field($model, 'agra_update_time') ?>

    <?php // echo $form->field($model, 'agra_update_ip') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
