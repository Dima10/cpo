<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $module array */

?>

<?= Html::dropDownList('module', isset($module[0]) ? $module[0] : '', $module, ['id' => 'module_id', 'class' => 'form-control']) ?>

