<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\TrainingProgModuleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $model app\models\TrainingProg */
/* @var $prog array */
/* @var $module array */

?>
<div class="training-prog-module-index-grid">


<p>
    <?= Html::a(Yii::t('app', 'Create Training Prog Module'), ['create-prog-module', 'trp_id' => $model->trp_id], ['class' => 'btn btn-success']) ?>
</p>

<?php

Pjax::begin();

    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'filterPosition' => '',
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'class' => 'yii\grid\ActionColumn',
                'options' => ['width' => Yii::$app->params['yii\grid\ActionColumn']['width']],
                'template' => '{delete}',
                'buttons' => [
                    'delete' =>
                        function($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url,
                                [
                                    'title' => Yii::t('yii', 'Delete'),
                                    'data' => [
                                        'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                        'method' => 'post',
                                    ],
                                ]
                            );
                        }

                ],
                'urlCreator' =>
                    function ($action, $model, $key, $index) {
                        if ($action === 'delete') {
                            $url = yii\helpers\Url::to(['delete-prog-module', 'id' => $model->trpl_id]);
                            return $url;
                        }
                    }
            ],

            'trpl_id',
            'trplTrm.trm_code',
            [
                'attribute' => 'trpl_trm_id',
                'label' => Yii::t('app', 'Trpl Trm ID'),
                'value' => function ($data) { return $data->trplTrm->trm_name; },
                'filter' => $module,
            ],
            [
                'attribute' => 'trplTrm.trmTrm.trm_name',
                'label' => Yii::t('app', 'Training Module'),
                'visible' => !\app\models\User::isSpecAdmin(),
            ],
            /*
            [
                'attribute' => 'trpl_trp_id',
                'label' => Yii::t('app', 'Trpl Trp ID'),
                'value' => function ($data) { return $data->trplTrp->trp_name; },
                'filter' => $prog,
            ],
            */

            [
                'attribute' => 'trpl_create_user',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'trpl_create_time',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'trpl_create_ip',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],

        ],
    ]);

Pjax::end();


?>

</div>
