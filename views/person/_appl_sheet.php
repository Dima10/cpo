<?php

use app\helpers\ProgramsHelper;
use yii\helpers\Html;
use yii\helpers\Url;
//use yii\grid\GridView;
//use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\ApplSheetSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $sheet app\models\ApplSh */
/* @var $trp_id integer */
/* @var $svcNonLimitedTest boolean */

?>

<h4>
Итоговое тестирование проводится по завершению освоения всего курса лекционного материала и успешного прохождения промежуточного тестирования.<br><br>
Количество попыток прохождения итогового тестирования – <?= isset($sheet) ? $sheet->appls_try : ($svcNonLimitedTest) ? ProgramsHelper::INFINITY_TRIES_AMOUNT :3 ?>.<br><br>
При повторном прохождения тестирования рекомендуем повторно ознакомиться с лекционными материалами.<br><br>
Компьютерное итоговое тестирование считается пройденным с положительным результатом в случае, когда процент правильных ответов за каждый тест составляет 60 и более процентов (Согласно Положению об итоговой аттестации слушателей ООО "ЦПО"<br><br>

В случае не прохождения итогового тестирования, слушателю выдается справка об обучении или о периоде обучения лицам, не прошедшим аттестации и производится отчисление с курса.<br><br>
</h4>

<?php


echo Html::a(Yii::t('app', 'Начать тестирование'), Url::toRoute(['appl-sheet', 'trp_id' => $trp_id]), ['class' => 'btn btn-primary'])

/*
Pjax::begin();

    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            ['class' => 'yii\grid\ActionColumn'],

            'appls_id',
            'appls_number',
            [
                'attribute' => 'appls_number',
                'label' => Yii::t('app', 'Link'),
                'format' => 'raw',
                'value' => function ($data) {
                    if (isset($data->appls_magic)) {
                        return Html::a(Yii::$app->urlManager->createAbsoluteUrl(['appl-sheet/test', 'uniq' => $data->appls_magic]), Yii::$app->urlManager->createAbsoluteUrl(['appl-sheet/test', 'uniq' => $data->appls_magic]), ['target' => '_blank']);
                    } else {
                        return '';
                    }
                },
            ],
            'appls_date',
            [
                'attribute' => 'applsPrs.prs_full_name',
                'label' => Yii::t('app', 'Appls Prs ID'),
            ],
            [
                'attribute' => 'applsTrp.trp_name',
                'label' => Yii::t('app', 'Appls Trp ID'),
            ],

            'appls_score_max',
            'appls_score',
            'appls_passed',
            [
                'attribute' => 'applsTrt.trt_name',
                'label' => Yii::t('app', 'Appls Trt ID'),
            ],

            [
                'attribute' => 'appls_file_name',
                'label' => Yii::t('app', 'Appls File Name'),
                'format' => 'raw',
                'value' => function ($data) {
                    return Html::a($data->appls_file_name, yii\helpers\Url::toRoute(['download', 'id' => $data->appls_id]), ['target' => '_blank']);
                },
            ],

            [
                'attribute' => 'appls_create_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'appls_create_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'appls_create_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'appls_update_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'appls_update_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'appls_update_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],

        ],
    ]);

Pjax::end();
*/

?>

