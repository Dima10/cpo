<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model yii\base\DynamicModel */

$this->title = Yii::t('app', 'Connect');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="person-connect">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form_connect', [
        'model' => $model,
    ]) ?>

</div>
