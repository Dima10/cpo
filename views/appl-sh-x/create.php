<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ApplShX */

$this->title = Yii::t('app', 'Create Appl Sheet X');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Appl Sh Xes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="appl-sh-x-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
