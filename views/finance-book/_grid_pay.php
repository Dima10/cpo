<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

if (!isset($payArray)){
    $payArray = [];
}

$link1 = yii::$app->urlManager->createUrl(['finance-book/update-pay-sum']);
$link2 = yii::$app->urlManager->createUrl(['finance-book/update-payment']);
$link3 = yii::$app->urlManager->createUrl(['finance-book/update-comment']);
$link4 = yii::$app->urlManager->createUrl(['entity-frm/update-pay-sum']);

$js = <<<EOL
function onChangeEvent (obj, v) {

    if (v==1) {
        $.get(
            '$link1',
            {
                id: obj.id,
                value: obj.value
            },
            function (data) {
                obj.value = data; 
            }
        );
    }

    if (v==2) {
        $.get(
            '$link2',
            {
                id: obj.id,
                value: obj.value
            },
            function (data) {
                obj.value = data; 
            }
        );
    }

    if (v==3) {
        $.get(
            '$link3',
            {
                id: obj.id,
                value: obj.value
            },
            function (data) {
                obj.value = data; 
            }
        );
    }
    
    if (v==4) {
        $.get(
            '$link4',
            {
                id: obj.id,
                value: obj.value
            },
            function (data) {
                obj.value = data; 
            }
        );
    }

}
EOL;

$this->registerJs($js, \yii\web\View::POS_END);



    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'id' => 'grid',
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],


            'fb_id',
            'svc_name',
            //'fbSvc.svc_name',
            [
                'attribute' => 'fbFbt.fbt_name',
                'label' => Yii::t('app', 'Fb Fbt ID'),
            ],
            'ab_name',
            //'fbAb.ab_name',
            [
                'attribute' => 'fbComp.comp_name',
                'label' => Yii::t('app', 'Fb Comp ID'),
            ],
            'fb_sum',
            [
                'attribute' => 'fbFbs.fbs_name',
                'label' => Yii::t('app', 'Fb Fbs ID'),
            ],
            'fb_tax',

            [
                'attribute' => 'fb_pay',
                'label' => Yii::t('app', 'Fb Pay'),
                'format' => 'raw',
                'value' =>
                function ($data) use ($payArray) {
                    $s = Html::textInput($data->fb_aca_id, (isset($payArray[$data->fb_aca_id]) && $data->fb_pay == null) ? $payArray[$data->fb_aca_id] : '', ['id' => $data->fb_aca_id, 'class' => 'fb_sum_row_pay_class', 'onChange' => 'onChangeEvent(this, 1)', 'size' => '6']);
                    if (!$data->fb_pay){
                        return $s;
                    }
                    return ' -----------';
                },
            ],


            'aga_number',
            //'fbAga.aga_number',
            'fbAcc.acc_number',


            'fb_date',

            [
                'attribute' => 'fb_payment',
                'label' => Yii::t('app', 'Fb Payment'),
                'format' => 'raw',
                'value' =>
                    function ($data) {
                        $s = Html::textInput($data->fb_id, $data->fb_payment, ['id' => $data->fb_id, 'class' => 'fb_payment_row_pay_class', 'onChange' => 'onChangeEvent(this, 2)', 'size' => '24']);
                        return $s;
                    },
            ],
            [
                'attribute' => 'fb_comment',
                'label' => Yii::t('app', 'Fb Comment'),
                'format' => 'raw',
                'value' =>
                    function ($data) {
                        $s = Html::textInput($data->fb_id, $data->fb_comment, ['id' => $data->fb_id, 'onChange' => 'onChangeEvent(this, 3)', 'size' => '48']);
                        return $s;
                    },
            ],
            
        ],

    ]);

?>
