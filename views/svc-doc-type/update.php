<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SvcDocType */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => Yii::t('app', 'Svc Doc Type'),
]) . $model->svdt_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Core'), 'url' => ['site/core']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Svc Doc Types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->svdt_id, 'url' => ['view', 'id' => $model->svdt_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="svc-doc-type-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
