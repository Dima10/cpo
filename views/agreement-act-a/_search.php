<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AgreementActASearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="agreement-act-a-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'acta_id') ?>

    <?= $form->field($model, 'acta_act_id') ?>

    <?= $form->field($model, 'acta_ana_id') ?>

    <?= $form->field($model, 'acta_qty') ?>

    <?= $form->field($model, 'acta_price') ?>

    <?php // echo $form->field($model, 'acta_tax') ?>

    <?php // echo $form->field($model, 'acta_create_user') ?>

    <?php // echo $form->field($model, 'acta_create_time') ?>

    <?php // echo $form->field($model, 'acta_create_ip') ?>

    <?php // echo $form->field($model, 'acta_update_user') ?>

    <?php // echo $form->field($model, 'acta_update_time') ?>

    <?php // echo $form->field($model, 'acta_update_ip') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
