<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AgreementActA */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => Yii::t('app', 'Agreement Act A'),
]) . $model->acta_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Agreement Act As'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->acta_id, 'url' => ['view', 'id' => $model->acta_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="agreement-act-a-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
