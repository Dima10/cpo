<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'FinanceBook Update');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'FinanceBook Update'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tools-financebook-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_grid_agreement_acc_a', [
        'dataProvider' => $dataProvider,
    ]) ?>

</div>
