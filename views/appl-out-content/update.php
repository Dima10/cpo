<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ApplOutContent */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => Yii::t('app', 'Appl Out Content'),
]) . $model->apploutc_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Appl Out Contents'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->apploutc_id, 'url' => ['view', 'id' => $model->apploutc_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="appl-out-content-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
