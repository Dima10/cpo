<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\FinanceBookType */

$this->title = Yii::t('app', 'Create Finance Book Type');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Finance Book Types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="finance-book-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
