<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SystemMailSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="system-mail-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'sm_id') ?>

    <?= $form->field($model, 'sm_name') ?>

    <?= $form->field($model, 'sm_server') ?>

    <?= $form->field($model, 'sm_user') ?>

    <?= $form->field($model, 'sm_password') ?>

    <?php // echo $form->field($model, 'sm_create_user') ?>

    <?php // echo $form->field($model, 'sm_create_time') ?>

    <?php // echo $form->field($model, 'sm_create_ip') ?>

    <?php // echo $form->field($model, 'sm_update_user') ?>

    <?php // echo $form->field($model, 'sm_update_time') ?>

    <?php // echo $form->field($model, 'sm_update_ip') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
