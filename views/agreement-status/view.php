<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\AgreementStatus */

$this->title = $model->ast_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Finance'), 'url' => ['site/finance']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Agreement Statuses'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="agreement-status-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->ast_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->ast_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?php
    try {
        echo DetailView::widget([
            'model' => $model,
            'attributes' => [
                'ast_id',
                'ast_name',
                'ast_sys',
                'ast_create_time',
                'ast_create_ip',
                'ast_update_user',
                'ast_update_time',
                'ast_update_ip',
                'ast_create_user',
            ],
        ]);
    } catch (Exception $e) {
        echo $e->getMessage();
    } ?>

</div>
