<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AgreementStatusSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Agreement Statuses');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Finance'), 'url' => ['site/finance']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="agreement-status-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Agreement Status'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= $this->render('_grid', [
        'dataProvider' => $dataProvider,
        'searchModel' => $searchModel,
    ]) ?>

</div>
