<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\UserMail */
/* @var $sysmail array */
/* @var $reestr array */
/* @var $type array */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => Yii::t('app', 'User Mail'),
]) . $model->um_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'User Mails'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->um_id, 'url' => ['view', 'id' => $model->um_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="user-mail-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'sysmail' => $sysmail,
        'reestr' => $reestr,
        'type' => $type,
    ]) ?>

</div>
