<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserMailSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $sysmail array */
/* @var $reestr array */
/* @var $type array */

$this->title = Yii::t('app', 'User Mails');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-mail-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create User Mail'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= $this->render('_grid', [
        'dataProvider' => $dataProvider,
        'searchModel' => $searchModel,
        'sysmail' => $sysmail,
        'reestr' => $reestr,
        'type' => $type,
    ]) ?>

</div>
