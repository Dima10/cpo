<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\UserMail */
/* @var $form yii\widgets\ActiveForm */
/* @var $sysmail array */
/* @var $reestr array */
/* @var $type array */

?>

<div class="user-mail-form">

    <?php $form = ActiveForm::begin(
        [
            'fieldConfig' => [
                'template' => '<div class="row"><div class="col-sm-2">{label}</div><div class="col-sm-6">{input}</div><div class="col-sm-4">{error}</div></div>',
            ],        
        ]
    
    ); 

        echo $form->errorSummary($model);
    ?>

    <?php echo $form->field($model, 'um_name')->textInput(['maxlength' => true]); ?>

    <?php echo $form->field($model, 'um_umt_id')->dropDownList($type); ?>

    <?php echo $form->field($model, 'um_subj')->textInput(['maxlength' => true]); ?>

    <?php echo $form->field($model, 'um_text')->textarea(['rows' => 6]); ?>

    <?php echo $form->field($model, 'um_sm_id')->dropDownList($sysmail); ?>

    <?php echo $form->field($model, 'um_reestr')->dropDownList($reestr); ?>

    <?php echo $form->field($model, 'um_flag')->checkbox(); ?>

    <?php echo $form->field($model, 'um_to')->textInput(['maxlength' => true]); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
