<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AgreementAddSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $agreement_status array */

$this->title = Yii::t('app', 'Agreement Adds');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="agreement-add-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Agreement Add'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= $this->render('_grid', [
        'dataProvider' => $dataProvider,
        'searchModel' => $searchModel,
        'agreement_status' => $agreement_status,
    ]) ?>

</div>
