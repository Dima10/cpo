<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AgreementAdd */
/* @var $form yii\widgets\ActiveForm */
/* @var $agreement array */
/* @var $agreement_status array */

?>

<div class="agreement-add-form">

    <?php $form = ActiveForm::begin(
        [
            'fieldConfig' => [
                'template' => '<div class="row"><div class="col-sm-2">{label}</div><div class="col-sm-6">{input}</div><div class="col-sm-4">{error}</div></div>',
            ],        
        ]
    
    ); 

        echo $form->errorSummary($model);
    ?>

    <?php echo $form->field($model, 'agadd_number')->textInput(['maxlength' => true]); ?>

    <?=  $form->field($model, 'agadd_date')->widget(\yii\jui\DatePicker::class, [
        //'language' => 'ru',
        'dateFormat' => 'yyyy-MM-dd',
    ])
    ?>

    <?php echo $form->field($model, 'agadd_agr_id')->dropDownList($agreement); ?>

    <?php echo $form->field($model, 'agadd_ast_id')->dropDownList($agreement_status); ?>

    <?php echo $form->field($model, 'agadd_file_data')->fileInput(); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
