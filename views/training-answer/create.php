<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TrainingAnswer */
/* @var $question array */

$this->title = Yii::t('app', 'Create Training Answer');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Service'), 'url' => ['site/svc']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Training Answers'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="training-answer-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'question' => $question,
    ]) ?>

</div>
