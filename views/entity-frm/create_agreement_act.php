<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model yii\base\DynamicModel */
/* @var $modelA app\models\AgreementAct */
/* @var $entity array */
/* @var $company array */
/* @var $agreement array */
/* @var $agreement_annex array */
/* @var $pattern array */
/* @var $dataProvider yii\data\ArrayDataProvider */
/* @var $address            array */
/* @var $account            array */
/* @var $staff              array */

$this->title = Yii::t('app', 'Create Agreement Act');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Frm'), 'url' => ['site/frm']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Entities'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Entity'), 'url' => ['view', 'id' => array_keys($entity)[0]]];
$this->params['breadcrumbs'][] = ['label' => array_keys($entity)[0], 'url' => ['view', 'id' => array_keys($entity)[0]]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="entity-frm-agreement-act-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form_agreement_act_dyn', [
        'model' => $model,
        'modelA' => $modelA,
        'entity' => $entity,
        'company' => $company,
        'agreement' => $agreement,
        'agreement_annex' => $agreement_annex,
        'pattern' => $pattern,
        'dataProvider' => $dataProvider,
        'account' => $account,
        'address' => $address,
        'staff' => $staff,
    ]) ?>

</div>
