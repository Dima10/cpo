<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $bank array */
?>

<?= Html::dropDownList('bank_id', isset($bank[0]) ? $bank[0] : '', $bank, ['id' => 'bank_id']) ?>
