<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\EntitySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $entityType array */
/* @var $entityClass array */
/* @var $agent array */

$this->title = Yii::t('app', 'Entities');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Frm'), 'url' => ['site/frm']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="entity-frm-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Entity'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= $this->render('_grid', [
        'dataProvider' => $dataProvider,
        'searchModel' => $searchModel,
        'entityType' => $entityType,
        'entityClass' => $entityClass,
        'agent' => $agent,
    ]) ?>

</div>
