<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Address */
/* @var $form yii\widgets\ActiveForm */
/* @var $addressType array */
/* @var $country array */
/* @var $region array */
/* @var $city array */
/* @var $entity array */

$script  = <<< 'EOD'
function getStateRegion(val) {
    $.ajax({
    type: "POST",
    url: "index.php?r=address/ajax-region&id="+val,
    data:'id='+val,
    success: function(data){
        $("#region").html(data);
        $("#city").html('');
    }
    });
}

function getStateCity(val) {
    $.ajax({
    type: "POST",
    url: "index.php?r=address/ajax-city&id="+val,
    data:'id='+val,
    success: function(data){
        $("#city").html(data);
    }
    });
}

EOD;

$this->registerJs($script, \yii\web\View::POS_END);

?>

<div class="entity-frm-address-form">

    <?php
        $form = ActiveForm::begin([
                'action' => ($model->isNewRecord ?
                                        ['create-address', 'ent_id' => array_keys($entity)[0]]
                                        :
                                        ['update-address', 'ent_id' => array_keys($entity)[0], 'id' => $model->add_id ]
                            ),
                'fieldConfig' => [
                    'template' => '<div class="row"><div class="col-sm-2">{label}</div><div class="col-sm-6">{input}</div><div class="col-sm-4">{error}</div></div>',
                ],        
            ]);
        echo $form->errorSummary($model);
    ?>

    <?= $form->field($model, 'add_ab_id')->dropDownList($entity, ['ReadOnly' => true]) ?>

    <?= $form->field($model, 'add_active')->checkbox() ?>

    <?= $form->field($model, 'add_addt_id')->dropDownList($addressType) ?>

    <?= $form->field($model, 'add_cou_id')->dropDownList($country, ['id' => 'country', 'onChange' => 'getStateRegion(this.value);']) ?>

    <?= $form->field($model, 'add_reg_id')->dropDownList($region, ['id' => 'region', 'onChange' => 'getStateCity(this.value);']) ?>

    <?= $form->field($model, 'add_city_id')->dropDownList($city, ['id' => 'city']) ?>

    <?= $form->field($model, 'add_index')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'add_data')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
