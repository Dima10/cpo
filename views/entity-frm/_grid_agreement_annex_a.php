<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<div class="entity-frm-agreement-annex-a-index-grid">

<?php

Pjax::begin();

try {
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions' => [
            'id' => 'servicesRows',
            'class' => 'table table-striped table-bordered',
        ],
        'rowOptions' => function ($data, $key, $index, $grid) {
            return [
                'id' => "tr_".$data->ana_id,
            ];
        },
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            //['class' => 'yii\grid\ActionColumn'],

            [
                'visible' => ($model->agra_ast_id == 1) ? true : false,
                'class' => 'yii\grid\ActionColumn',
                'options' => ['width' => Yii::$app->params['yii\grid\ActionColumn']['width']],
                'template' => '{view} {update} {delete}',
                'buttons' => [
                    'view' =>
                        function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, ['target'=>'_blank', 'title' => Yii::t('yii', 'View'),
                            ]);
                        },
                    'update' =>
                        function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url,
                                    [
                                        'title' => Yii::t('yii', 'Update'),
                                        'id' => $model->ana_id.'_updateAnnexARow',
                                        'class' => $model->ana_id.'_processUpdate processUpdate',
                                        'data-ana_id' => $model->ana_id,
                                    ]
                            ).Html::a('<span class="glyphicon glyphicon-floppy-save"></span>', $url,
                                    [
                                        'title' => Yii::t('yii', 'Save'),
                                        'id' => $model->ana_id.'_saveAnnexARow',
                                        'class' => 'hidden '.$model->ana_id.'_processUpdate procesSave',
                                        'data-ana_id' => $model->ana_id,
                                    ]
                                );
                        },
                    'delete' =>
                        function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url,
                                [
                                    'title' => Yii::t('yii', 'Delete'),
                                    'class' => 'deleteService',
                                    'data-ana_id' => $model->ana_id,
                                ]);
                        },
                ],
                'urlCreator' =>
                    function ($action, $model, $key, $index) {
                        if ($action === 'view') {
                            $url = yii\helpers\Url::toRoute(['svc/view', 'id' => $model->ana_svc_id]);
                            return $url;
                        }
                        if ($action === 'delete') {
                            $url = yii\helpers\Url::to(['delete', 'id' => $key]);
                            return $url;
                        }
                        return '#';
                    },
            ],

            'ana_id',
            //'ana_agra_id',
            [
                'attribute' => 'ana_svc_id',
                'label' => Yii::t('app', 'Ana Svc ID'),
                'value' => function ($data) {
                    return isset($data->anaSvc) ? $data->anaSvc->svc_name : '';
                },
            ],
            [
                'attribute' => 'ana_trp_id',
                'label' => Yii::t('app', 'Ana Trp ID'),
                'value' => function ($data) {
                    return isset($data->anaTrp) ? $data->anaTrp->trp_name : '';
                },
            ],

            'ana_cost_a',
            'ana_price_a',
            [
                'attribute' => 'ana_qty',
                'label' => Yii::t('app', 'Ana Qty'),
                'format' => 'raw',
                'value' => function ($data) {

                    $span = '<span id="'.$data->ana_id.'_priceQty" class="'.$data->ana_id.'_processUpdate">'.$data->ana_qty.'</span>';
                    $input = Html::input('text','ana_qty', $data->ana_qty, ['class'=>'hidden '.$data->ana_id.'_processUpdate processUpdateIput inputQty', 'id'=>$data->ana_id.'_ana_qty', 'data-relate-anaid'=>$data->ana_id, 'size'=>5, 'style'=>'text-align:center']);

                    return isset($data->ana_qty) ? $span.$input : 0;
                },
            ],
            [
                'attribute' => 'ana_price',
                'label' => Yii::t('app', 'Ana Price'),
                'format' => 'raw',
                'value' => function ($data) {

                    $span = '<span id="'.$data->ana_id.'_priceLabel" class="'.$data->ana_id.'_processUpdate">'.$data->ana_price.'</span>';
                    $input = Html::input('text','ana_price', $data->ana_price, ['class'=>'hidden '.$data->ana_id.'_processUpdate processUpdateIput', 'id'=>$data->ana_id.'_ana_price', 'size'=>8, 'style'=>'text-align:center']);

                    return isset($data->ana_price) ? $span.$input : 0;
                },
            ],
            [
                'attribute' => 'ana_tax',
                'label' => Yii::t('app', 'Ana Tax'),
                'format' => 'raw',
                'value' => function ($data) {

                    $span = '<span id="'.$data->ana_id.'_ana_tax_span" class="'.$data->ana_id.'_processTax">'.$data->ana_tax.'</span>';
                    $input = Html::input('text','ana_tax', $data->ana_tax, ['class'=>'hidden '.$data->ana_id.'_processTax', 'readonly'=>'readonly', 'id'=>$data->ana_id.'_ana_tax', 'size'=>8, 'style'=>'text-align:center']);

                    return isset($data->ana_tax) && $data->anaSvc->svc_tax_flag==1 ? $span.$input : Yii::t('app', 'Without Tax');
                },
            ],


            [
                'attribute' => 'ana_create_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'ana_create_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'ana_create_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            /*
            [
                'attribute' => 'ana_update_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'ana_update_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'ana_update_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            */

        ],
    ]);
} catch (Exception $e) {
    echo $e->getMessage();
}

Pjax::end();


?>

</div>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <button type="button" id="exampleModalBtn" class="hidden" data-toggle="modal" data-target="#exampleModal"></button>
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="border: none !important;">
                <h5 class="modal-title" id="exampleModalLabel">Приложение обновлено</h5>
            </div>
        </div>
    </div>
</div>