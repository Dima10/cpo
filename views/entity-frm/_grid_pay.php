<?php

use yii\helpers\Html;
use yii\grid\GridView;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $payArray array */

if (!isset($payArray)){
    $payArray = [];
}

?>
<div class="entity-frm-agreement-acc-a-index-grid">

<?php

    echo GridView::widget([
        'id' => 'grid',
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            //['class' => 'yii\grid\ActionColumn'],

            'aca_id',
            //'aca_aga_id',
            [
                'attribute' => 'aca_ana_id',
                'label' => Yii::t('app', 'Aca Ana ID'),
                'value' =>  function ($data) {
                                if (!is_null($data->acaAna)) {
                                    return
                                        (!is_null($data->acaAna->anaAgra) ? $data->acaAna->anaAgra->agra_number : '')
                                            .' / '.
                                        (!is_null($data->acaAna->anaSvc) ? $data->acaAna->anaSvc->svc_name : '')
                                            .' / '.
                                        (!is_null($data->acaAna->anaTrp) ? $data->acaAna->anaTrp->trp_name : '');
                                }
                                return '';
                            },
            ],

            'aca_qty',
            'aca_price',
            [
                'attribute' => 'aca_tax',
                'label' => Yii::t('app', 'Aca Tax'),
                'value' => function ($data) { return isset($data->aca_tax) ? $data->aca_tax : Yii::t('app', 'Without Tax'); },
            ],

            'sum',

            [
                'attribute' => 'paySum',
                'label' => Yii::t('app', 'Fb Pay Prepare'),
                'format' => 'raw',
                'value' =>
                    function ($data) use ($payArray) {
                        $s = Html::textInput($data->aca_id, isset($payArray[$data->aca_id]) ? $payArray[$data->aca_id] : '', ['id' => $data->aca_id, 'onChange' => 'onChangeEvent(this, 1)', 'size' => '6', 'ReadOnly' => $data->paySum >= $data->sum]);
                        return $s;
                    },
            ],
            
            [
                'attribute' => 'paySum',
                'label' => Yii::t('app', 'Fb Pay'),
                'format' => 'raw',
                'value' =>
                    function ($data) {
                        return Html::label($data->paySum, [], ['style' => "color:red"]);
                    },
            ],

            [
                'attribute' => 'aca_create_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'aca_create_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'aca_create_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            /*
            [
                'attribute' => 'aca_update_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'aca_update_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'aca_update_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            */

        ],
    ]);


?>

</div>


<?php


$link1 = yii::$app->urlManager->createUrl(['entity-frm/update-pay-sum']);

$js = <<<EOL
function onChangeEvent (obj, v) {

    if (v==1) {
        $.get(
            '$link1',
            {
                id: obj.id,
                value: obj.value
            },
            function (data) {
                obj.value = data; 
            }
        );
    }

}
EOL;

$this->registerJs($js, \yii\web\View::POS_END);


?>