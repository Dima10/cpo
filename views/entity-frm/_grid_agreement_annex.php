<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\AgreementAnnexSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $entity app\models\Entity */
/* @var $comp array */
/* @var $agreement_status array */
/* @var $agreement array */


echo Html::a(Yii::t('app', 'Create Agreement Annex'), ['create-agreement-annex', 'ent_id' => $entity->ent_id], ['class' => 'btn btn-success']);

Pjax::begin();
try {
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'layout' => "{pager}{summary}{items}{pager}",
        'pager' => [
            'firstPageLabel' => Yii::t('app', 'First'),
            'lastPageLabel' => Yii::t('app', 'Last'),
            'maxButtonCount' => 100,

        ],
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn',
                'options' => ['width' => Yii::$app->params['yii\grid\SerialColumn']['width']],
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'options' => ['width' => Yii::$app->params['yii\grid\ActionColumn']['width']],
                'template' => '{update} {delete}',
                'buttons' => [
                    'update' =>
                        function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, ['title' => Yii::t('yii', 'Update'),
                            ]);
                        },
                    'delete' =>
                        function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url,
                                [
                                    'title' => Yii::t('yii', 'Delete'),
                                    'data' => [
                                        'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                        'method' => 'post',
                                    ],
                                ]);
                        },
                ],
                'urlCreator' =>
                    function ($action, $model, $key, $index) use ($entity) {
                        if ($action === 'update') {
                            $url = yii\helpers\Url::to(['update-agreement-annex', 'ent_id' => $entity->ent_id, 'id' => $model->agra_id]);
                            return $url;
                        }
                        if ($action === 'delete') {
                            $url = yii\helpers\Url::to(['delete-agreement-annex', 'id' => $model->agra_id]);
                            return $url;
                        }
                        return '#';
                    },
            ],

            [
                'attribute' => 'agra_id',
                'options' => ['width' => '100'],
            ],

            'agra_number',
            'agra_date',

            [
                'attribute' => 'agra_ast_id',
                'label' => Yii::t('app', 'Agra Ast ID'),
                'value' => function ($data) {
                    return $data->agraAst->ast_name;
                },
                'filter' => $agreement_status,
            ],

            /*
            [
                'attribute' => 'agra_ab_id',
                'label' => Yii::t('app', 'Agr Ab ID'),
                'value' => function ($data) {
                    return $data->agraAgr->agrAb->entity->entEntt->entt_name_short . ' ' . $data->agraAgr->agrAb->ab_name;
                },
                'filter' => $entity,
            ],
            */
            [
                'attribute' => 'agra_comp_id',
                'label' => Yii::t('app', 'Agr Comp ID'),
                'value' => function ($data) {
                    return $data->agraAgr->agrComp->comp_name;
                },
                'filter' => $comp,
            ],

            [
                'attribute' => 'agra_agr_id',
                'label' => Yii::t('app', 'Basis (agreement)'),
                'format' => 'raw',
                'value' =>
                    function ($data) {
                        return $data->agraAgr->agr_number;
                    },
                'filter' => $agreement,
                /*
                function ($data) {
                return Html::a($data->agra_fdata, yii\helpers\Url::toRoute(['download', 'id' => $data->agra_id]), ['target' => '_blank']);
            },
                */
            ],


            [
                'attribute' => 'agra_fdata',
                'label' => Yii::t('app', 'Agra Fdata'),
                'format' => 'raw',
                'value' => function ($data) {
                    return Html::a($data->agra_fdata, yii\helpers\Url::toRoute(['download-agreement-annex', 'id' => $data->agra_id]), ['target' => '_blank']);
                },
            ],

            [
                'attribute' => 'agra_fdata_sign',
                'label' => Yii::t('app', 'Agra Fdata Sign'),
                'format' => 'raw',
                'value' => function ($data) {
                    return Html::a($data->agra_fdata_sign, yii\helpers\Url::toRoute(['download-agreement-annex-sign', 'id' => $data->agra_id]), ['target' => '_blank']);
                },
            ],

            'agra_sum',
            'agra_tax',
            'agra_comment',


            [
                'attribute' => 'agra_create_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'agra_create_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'agra_create_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'agra_update_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'agra_update_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'agra_update_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],

        ],
    ]);
} catch (Exception $e) {
    echo $e->getMessage();
}
Pjax::end();
