<?php

use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<div class="entity-frm-agreement-acc-a-index-grid">

<?php

try {
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions' => [
            'id' => 'servicesRows',
            'class' => 'table table-striped table-bordered',
        ],
        'rowOptions' => function ($model, $key, $index, $grid) {
            return [
                'id' => "tr_".$model->aca_id,
            ];
        },
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            //['class' => 'yii\grid\ActionColumn'],

            [
                'visible' => ($model->aga_ast_id == 1) ? true : false,
                'class' => 'yii\grid\ActionColumn',
                'options' => ['width' => Yii::$app->params['yii\grid\ActionColumn']['width']],
                'template' => '{delete}',
                'buttons' => [
                    'delete' =>
                        function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url,
                                [
                                    'title' => Yii::t('yii', 'Delete'),
                                    'class' => 'deleteService',
                                    'data-aca_id' => $model->aca_id,
                                ]);
                        },
                ],
                'urlCreator' =>
                    function ($action, $model, $key, $index) {
                        if ($action === 'view') {
                            $url = yii\helpers\Url::toRoute(['svc/view', 'id' => $model->acaAna->anaSvc->svc_id]);
                            return $url;
                        }
                        if ($action === 'delete') {
                            $url = yii\helpers\Url::to(['delete', 'id' => $key]);
                            return $url;
                        }
                        return '#';
                    },
            ],

            'aca_id',
            //'aca_aga_id',
            [
                'attribute' => 'aca_ana_id',
                'label' => Yii::t('app', 'Aca Ana ID'),
                'value' => function ($data) {
                    if (isset($data->acaAna)) {
                        return (isset($data->acaAna->anaAgra) ? $data->acaAna->anaAgra->agra_number : '') . ' / ' .
                            (isset($data->acaAna->anaSvc) ? $data->acaAna->anaSvc->svc_name : '') . ' / ' .
                            (isset($data->acaAna->anaTrp) ? $data->acaAna->anaTrp->trp_name : '');
                    }
                    return '';
                },
            ],

            [
                'attribute' => 'aca_qty',
                'label' => Yii::t('app', 'Ana Qty'),
                'format' => 'raw',
                'value' => function ($data) {

                    $span = '<span id="'.$data->aca_id.'_priceQty" class="'.$data->aca_id.'_processUpdate">'.$data->aca_qty.'</span>';
                    $input = Html::input('text','aca_qty', $data->aca_qty, ['class'=>'hidden '.$data->aca_id.'_processUpdate processUpdateIput inputQty', 'id'=>$data->aca_id.'_aca_qty', 'data-relate-acaid'=>$data->aca_id, 'size'=>5, 'style'=>'text-align:center']);

                    return isset($data->aca_qty) ? $span.$input : 0;
                },
            ],
            [
                'attribute' => 'aca_price',
                'label' => Yii::t('app', 'Aca Price'),
                'format' => 'raw',
                'value' => function ($data) {

                    $span = '<span id="'.$data->aca_id.'_priceLabel" class="'.$data->aca_id.'_processUpdate">'.$data->aca_price.'</span>';
                    $input = Html::input('text','aca_price', $data->aca_price, ['class'=>'hidden '.$data->aca_id.'_processUpdate processUpdateIput', 'id'=>$data->aca_id.'_aca_price', 'size'=>8, 'style'=>'text-align:center']);

                    return isset($data->aca_price) ? $span.$input : 0;
                },
            ],
            [
                'attribute' => 'aca_tax',
                'format' => 'raw',
                'label' => Yii::t('app', 'Aca Tax'),
                'value' => function ($data) {


                    $span = '<span id="'.$data->aca_id.'_aca_tax_span" class="'.$data->aca_id.'_processTax">'.$data->aca_tax.'</span>';
                    $input = Html::input('text','aca_tax', $data->aca_tax, ['class'=>'hidden '.$data->aca_id.'_processTax', 'readonly'=>'readonly', 'id'=>$data->aca_id.'_aca_tax', 'size'=>8, 'style'=>'text-align:center']);

                    return isset($data->aca_tax) ? $span.$input : Yii::t('app', 'Without Tax');
                },
            ],


            [
                'attribute' => 'aca_create_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'aca_create_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'aca_create_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            /*
            [
                'attribute' => 'aca_update_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'aca_update_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'aca_update_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            */

        ],
    ]);
} catch (Exception $e) {
    echo $e->getMessage();
}


?>

</div>



<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <button type="button" id="exampleModalBtn" class="hidden" data-toggle="modal" data-target="#exampleModal"></button>
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="border: none !important;">
                <h5 class="modal-title" id="exampleModalLabel">Счет обновлен</h5>
            </div>
        </div>
    </div>
</div>