<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Entity */
/* @var $form yii\widgets\ActiveForm */
/* @var $agent array */
/* @var $agent_m array */
/* @var $entityClass array */

$js = '
        $("#abSearch").click(
            function() {
                var abVal = $("#abText").val();
                $.get("'.\yii\helpers\Url::to(['/ab/ajax-search']).'",
                    {
                      id: "agent_id", 
                      text : abVal
                    },
                    function (data) {
                        $("#agent_id").html(data);
                    }
                );
            }
        );
        $("#abSearch1").click(
            function() {
                var abVal = $("#abText1").val();
                $.get("'.\yii\helpers\Url::to(['/ab/ajax-search']).'",
                    {
                      id: "agent_id", 
                      text : abVal
                    },
                    function (data) {
                        $("#agent_id1").html(data);
                    }
                );
            }
        );
';

$this->registerJs($js, yii\web\View::POS_READY);

?>

<div class="entity-frm-form">

    <?php
        $form = ActiveForm::begin([
                'action' => $model->isNewRecord ? ['create'] : ['update', 'id' => $model->ent_id],
                'fieldConfig' => [
                    'template' => '<div class="row"><div class="col-sm-2">{label}</div><div class="col-sm-6">{input}</div><div class="col-sm-4">{error}</div></div>',
                ],
            ]);
        echo $form->errorSummary($model);

    ?>

    <div class="row">
        <div class="col-sm-2">
            <?php //= Html::label(Yii::t('app', 'Ent Agent ID')) ?>
        </div>
        <div class="col-sm-6">
            <?= Html::input('text', 'abText', '', ['id' => 'abText']) ?>
            <?= Html::button(Yii::t('app', 'Search'), ['id' => 'abSearch']) ?>
        </div>
        <div class="col-sm-4">
        </div>
    </div>


    <?= $form->field($model, 'ent_agent_id')->dropDownList($agent, ['id' => 'agent_id']) ?>


    <div class="row">
        <div class="col-sm-2">
            <?php //= Html::label(Yii::t('app', 'Ent Agent ID')) ?>
        </div>
        <div class="col-sm-6">
            <?= Html::input('text', 'abText', '', ['id' => 'abText1']) ?>
            <?= Html::button(Yii::t('app', 'Search'), ['id' => 'abSearch1']) ?>
        </div>
        <div class="col-sm-4">
        </div>
    </div>


    <?= $form->field($model, 'ent_manager_id')->dropDownList($agent_m, ['id' => 'agent_id1']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>


    <?= $this->render('_ajax_class_link', [
        'model' => $model,
        'entityClass' => $entityClass,
    ]) ?>



</div>
