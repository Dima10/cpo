<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TaskSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="task-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'task_id') ?>

    <?= $form->field($model, 'task_ab_id') ?>

    <?= $form->field($model, 'task_taskt_id') ?>

    <?= $form->field($model, 'task_date_end') ?>

    <?= $form->field($model, 'task_tasks_id') ?>

    <?php // echo $form->field($model, 'task_user_id') ?>

    <?php // echo $form->field($model, 'task_note') ?>

    <?php // echo $form->field($model, 'task_event_id') ?>

    <?php // echo $form->field($model, 'task_create_user') ?>

    <?php // echo $form->field($model, 'task_create_time') ?>

    <?php // echo $form->field($model, 'task_create_ip') ?>

    <?php // echo $form->field($model, 'task_update_user') ?>

    <?php // echo $form->field($model, 'task_update_time') ?>

    <?php // echo $form->field($model, 'task_update_ip') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
