<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ContactType */
/* @var $contact_type array */

$this->title = Yii::t('app', 'Create Contact Type');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Core'), 'url' => ['site/core']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Contact Types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contact-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'contact_type' => $contact_type,
    ]) ?>

</div>
