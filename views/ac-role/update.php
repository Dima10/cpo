<?php

use app\models\AcRole;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AcRole */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => Yii::t('app', 'Ac Role'),
]) . ' ' . $model->acr_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'acl'), 'url' => ['site/acl']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Ac Roles'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->acr_id, 'url' => ['view', 'id' => $model->acr_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="ac-role-update" id="forAcRoleController">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>


    <hr />
    <h3>Доступы роли</h3>
    <button id="updateAccess" class="btn btn-info">Обновить / Сохранить таблицу доступов</button>
    <input type="hidden" value="<?php echo $model->acr_id; ?>" name="roleId" id="roleId">
    <table class="table table-striped">
        <thead>
            <tr>
                <th style="width: 300px">
                    Код
                </th>
                <th>
                    Список доступов
                    (
                    <a href="#" id="selectAllAccess" >выбрать все</a> /
                    <a href="#" id="unselectAllAccess" >снять выделение</a>
                    )
                </th>
                <th>

                </th>
                <th></th>
            </tr>
        </thead>
        <?php
        /** @var AcRole [] $acFunctions */
        /** @var AcRole $acFunction */
        $tmtArrayToStructure = [];
        $tmpArrayGeneral = [];
        $generalSetting = [773]; //add here acf_id to add checkbox with label to main <TD>
        foreach ($acFunctions as $acFunction){
            $x = explode('->', $acFunction->acf_name);

            if (in_array($acFunction->acf_id, $generalSetting)) {
                $tmpArrayGeneral[trim($x[0])] = '<u style="color: #9e0505"><b>'.$x[1]. '</b></u> <input '. ((in_array($acFunction->acf_id, $functionsInRole)) ? 'checked' : '').' class="accessCheckBox" value="'. $acFunction->acf_id .'" type="checkbox" name="access_'. $acFunction->acf_id. '" id="access_'. $acFunction->acf_id .'" style="cursor: pointer"> ';
            } else {
                $tmtArrayToStructure[trim($x[0])][] = $x[1]. ' <input '. ((in_array($acFunction->acf_id, $functionsInRole)) ? 'checked' : '').' class="accessCheckBox" value="'. $acFunction->acf_id .'" type="checkbox" name="access_'. $acFunction->acf_id. '" id="access_'. $acFunction->acf_id .'" style="cursor: pointer"> ';
            }
        }

        echo '<pre>';

        $resultString = [];
        foreach ($tmtArrayToStructure as $structureIndex => $structureValue){
            $resultString[$structureIndex] = implode(array_map(function ($value){
                return '<div class="col-md-3" style="margin-bottom: 10px">'.$value.'</div>';
            }, $structureValue));
        }

        foreach ($resultString as $item => $value){ ?>
            <tr>
                <td>
                    <b><?php echo $item?></b>
                    <br />
                    <?php echo (isset($tmpArrayGeneral[$item]) && !empty($tmpArrayGeneral[$item])) ? $tmpArrayGeneral[$item] : ''; ?>
                </td>
                <td>
                    <?php echo $value?>
                </td>
                <td>

                </td>
            </tr>
        <?php } ?>
    </table>
</div>
