<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ApplOut */

$this->title = $model->applout_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Appl Outs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="appl-out-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->applout_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->applout_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'applout_id',
            'applout_number',
            'applout_date',
            [
                'attribute' => 'applout_pat_id',
                'visible' => !\app\models\User::isSpecAdmin(),
            ],
            'applout_file_name',
            [
                'attribute' => 'applout_file_data',
                'visible' => !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'applout_create_user',
                'visible' => !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'applout_create_time',
                'visible' => !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'applout_create_ip',
                'visible' => !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'applout_update_user',
                'visible' => !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'applout_update_time',
                'visible' => !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'applout_update_ip',
                'visible' => !\app\models\User::isSpecAdmin(),
            ],
        ],
    ]) ?>

</div>
