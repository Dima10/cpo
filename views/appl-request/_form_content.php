<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ApplRequestContent */
/* @var $form yii\widgets\ActiveForm */
/* @var $person array */
/* @var $svc array */
/* @var $prog array */
/* @var $applRequests array */

$js = '
        $("#prsSearch").click(
            function() {
                var prsVal = $("#prsText").val();
                $.get("'.\yii\helpers\Url::to(['/person/ajax-search']).'",
                    {
                      id: "person_id", 
                      text : prsVal
                    },
                    function (data) {
                        $("#person_id").html(data);
                    }
                );
            }
        );
';

$this->registerJs($js, yii\web\View::POS_READY);

$js = '
        $("#prsSearchN").click(
            function() {
                var prsVal = $("#prsTextN").val();
                $.get("'.\yii\helpers\Url::to(['/person/ajax-search-number']).'",
                    {
                      id: "person_id", 
                      text : prsVal
                    },
                    function (data) {
                        $("#person_id").html(data);
                    }
                );
            }
        );
';

$this->registerJs($js, yii\web\View::POS_READY);


$js = '
        $("#svcSearch").click(
            function() {
                $.get("'.\yii\helpers\Url::to(['/svc/ajax-search']).'",
                    {
                      id: "svc_id", 
                      text : $("#svcText").val()
                    },
                    function (data) {
                        $("#svc_id").html(data);
                    }
                );
            }
        );
';

$this->registerJs($js, yii\web\View::POS_READY);

$js = '
        $("#svcSearch").click(
            function() {
                $.get("'.\yii\helpers\Url::to(['/svc/ajax-search']).'",
                    {
                      id: "svc_id", 
                      text : $("#svcText").val()
                    },
                    function (data) {
                        $("#svc_id").html(data);
                    }
                );
            }
        );
';

$this->registerJs($js, yii\web\View::POS_READY);


$js = '
function getTrainingProg(val) {

                $.get("'.\yii\helpers\Url::to(['/svc/ajax-prog-search']).'",
                    {
                      id: "prog_id", 
                      svc_id: val
                    },
                    function (data) {
                        $("#prog_id").html(data);
                    }
                );
}';
$this->registerJs($js, \yii\web\View::POS_END);

$js = '
        $("#progSearch").click(
            function() {
                var progVal = $("#progText").val();
                $.get("'.\yii\helpers\Url::to(['/svc/ajax-svc-prog-search-a']).'",
                    {
                      svc_id : $("#svc_id").val(),
                      text : progVal
                    },
                    function (data) {
                        $("#prog_id").html(data);
                    }
                );
            }
        );
';
$this->registerJs($js, \yii\web\View::POS_READY);



$js = '
function getTrProg(val) {

                $.get("'.\yii\helpers\Url::to(['/appl-request/ajax-prog-search']).'",
                    {
                      id: "prog_id", 
                      svc_id: val,
                      applr_id: '.$model->applrc_applr_id.'
                    },
                    function (data) {
                        $("#prog_id").html(data);
                    }
                );
}';

$this->registerJs($js, \yii\web\View::POS_END);

?>

<div class="appl-request-content-form">

    <?php $form = ActiveForm::begin(
        [
            'fieldConfig' => [
                'template' => '<div class="row"><div class="col-sm-2">{label}</div><div class="col-sm-6">{input}</div><div class="col-sm-4">{error}</div></div>',
            ],        
        ]
    
    ); 
        echo $form->errorSummary($model);
    ?>

    <?php echo $form->field($model, 'applrc_applr_id')->dropDownList($applRequests, ['ReadOnly' => true]); ?>

    <div class="row">
        <div class="col-sm-2">
            <?php//= Html::label(Yii::t('app', 'Stf Prs ID')) ?>
        </div>
        <div class="col-sm-6">
            <?= Html::input('text', 'prsText', '', ['id' => 'prsText']) ?>
            <?= Html::button(Yii::t('app', 'Search'), ['id' => 'prsSearch']) ?>
        </div>
        <div class="col-sm-4">
        </div>
    </div>
    <div class="row">
        <div class="col-sm-2">
            <?php//= Html::label(Yii::t('app', 'Stf Prs ID')) ?>
        </div>
        <div class="col-sm-6">
            <?= Html::input('text', 'prsTextN', '', ['id' => 'prsTextN']) ?>
            <?= Html::button(Yii::t('app', 'Search Number'), ['id' => 'prsSearchN']) ?>
        </div>
        <div class="col-sm-4">
        </div>
    </div>
    <?php echo $form->field($model, 'applrc_prs_id')->dropDownList($person, ['id' => 'person_id']); ?>
    <?php echo $form->field($model, 'applrc_position')->textInput(); ?>

    <div class="row">
        <!--
        <div class="col-sm-2">
            <?php//= Html::label(Yii::t('app', 'Ent Agent ID')) ?>
        </div>
        <div class="col-sm-6">
            <?= Html::input('text', 'svcText', '', ['id' => 'svcText']) ?>
            <?= Html::button(Yii::t('app', 'Search'), ['id' => 'svcSearch']) ?>
        </div>
        <div class="col-sm-4">
        </div>
        -->
    </div>
    <?php echo $form->field($model, 'applrc_svc_id')->dropDownList($svc, ['id' => 'svc_id', 'onChange' => 'getTrProg(this.value);']); ?>

    <?php echo $form->field($model, 'applrc_notify')->checkbox()->label(Yii::t('app', 'Applrc Notify')); ?>

    <div class="row">
        <!--
        <div class="col-sm-2">
            <?php//= Html::label(Yii::t('app', 'Ent Agent ID')) ?>
        </div>
        <div class="col-sm-6">
            <?= Html::input('text', 'progText', '', ['id' => 'progText']) ?>
            <?= Html::button(Yii::t('app', 'Search'), ['id' => 'progSearch']) ?>
        </div>
        <div class="col-sm-4">
        </div>
        -->
    </div>
    <?php echo $form->field($model, 'applrc_trp_id')->dropDownList($prog, ['id' => 'prog_id']); ?>

    <?php
    if ($model->applrcApplr->applr_flag > 0) {
        echo $form->field($model, 'applrc_date_upk')->widget(\yii\jui\DatePicker::class, [
            //'language' => 'ru',
            'dateFormat' => 'yyyy-MM-dd',
        ]);
    }
    ?>

    <?php //echo $form->field($model, 'applrc_flag')->checkbox(); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
