<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\PersonVisitSearch */
/* @var $searchModelTPS app\models\TrainingProgSessionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $dataProviderTPS yii\data\ActiveDataProvider */

$script  = <<<'EOD'
function _view_session(sid) {
    $.ajax({
    type: "POST",
    url: "index.php?r=person-visit/ajax-session&id="+sid,
    data:'id='+sid,
    success: function(data){
        $("#session").html(data);
    }
    });
}
EOD;

$this->registerJs($script, \yii\web\View::POS_BEGIN);


?>
<div class="person-visit-index-grid">

<?php

Pjax::begin();

try {
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}',
                'buttons' => [
                    'view' =>
                        function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, ['onclick' => "_view_session('{$model->pv_session_id}');", 'title' => Yii::t('yii', 'View')]);
                        }
                ],
                'urlCreator' =>
                    function ($action, $model, $key, $index) {
                        if ($action === 'view') {
                            $url = '#';
                            return $url;
                        }
                        return '#';
                    }
            ],
            'pv_id',
            'pv_prs_id',
            [
                'attribute' => 'prs_full_name',
                'value' => function ($data) {return $data->pvPrs->prs_full_name ?? '';},
                'label' => Yii::t('app', 'Prs Full Name'),
            ],
            [
                'attribute' => 'prs_connect_user',
                'value' => function ($data) {return $data->pvPrs->prs_connect_user ?? '';},
                'label' => Yii::t('app', 'Prs Connect User'),
            ],
            'pv_addr',
            'pv_time',
            'pv_create_time',
        ],
    ]);
} catch (Exception $e) {
    echo $e->getMessage();
}

Pjax::end();
?>

<div class="training-prog-session-index-grid">

<?php

Pjax::begin();
try {
    echo GridView::widget([
        'dataProvider' => $dataProviderTPS,
        'filterModel' => null,
        'id' => 'session',
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'tps_id',
            [
                'attribute' => 'tps_session_id',
            ],
            [
                'attribute' => 'trp_name',
                'label' => Yii::t('app', 'Tps Trp ID'),
                'value' => 'tpsTrp.trp_name',
            ],
            [
                'attribute' => 'prs_full_name',
                'label' => Yii::t('app', 'Tps Prs ID'),
                'value' => 'tpsPrs.prs_full_name',
            ],
            [
                'attribute' => 'tps_create_user',
                'label' => Yii::t('app', 'Prs Connect User'),
            ],
            'tps_create_time',
            'tps_create_ip',
        ]
    ]);
} catch (Exception $e) {
    echo $e->getMessage();
}
Pjax::end();

?>

</div>

</div>
