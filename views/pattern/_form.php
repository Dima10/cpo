<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\Alert;

/* @var $this yii\web\View */
/* @var $model app\models\Pattern */
/* @var $form yii\widgets\ActiveForm */
/* @var $pattern_type array */
/* @var $pattern_svdt array */
/* @var $pattern_trt array */

?>

<div class="pattern-form">

    <?=
        Alert::widget([
            'options' => [
                'class' => 'alert-info',
            ],
            'body' => 'Документ должен быть приведён к одному языку.<br>Если шаблон сделан в MS Word, то выделите весь текст в документе и укажите один язык.<br>Тэги рекомендуется вводить вручную за один раз.',
        ]);
    ?>

    <?php
        $form = ActiveForm::begin([
                'fieldConfig' => [
                    'template' => '<div class="row"><div class="col-sm-2">{label}</div><div class="col-sm-6">{input}</div><div class="col-sm-4">{error}</div></div>',
                ],
            ]);

        $form->errorSummary($model);

    ?>

    <?= $form->field($model, 'pat_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'pat_code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'pat_patt_id')->dropDownList($pattern_type) ?>

    <?= $form->field($model, 'pat_svdt_id')->dropDownList($pattern_svdt) ?>

    <?= $form->field($model, 'pat_trt_id')->dropDownList($pattern_trt) ?>

    <?= $form->field($model, 'pat_fdata')->fileInput() ?>

    <?= $form->field($model, 'pat_note')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
