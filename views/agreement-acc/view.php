<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\AgreementAcc */

$this->title = $model->aga_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Finance'), 'url' => ['site/finance']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Agreement Accs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="agreement-acc-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->aga_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->aga_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'aga_id',
            'aga_number',
            'aga_date',
            'agaAb.ab_name',
            'agaComp.comp_name',
            'agaAgr.agr_number',
            'agaAgra.agra_number',
            'aga_sum',
            'aga_tax',
            'aga_fdata',
            'aga_fdata_sign',
            'aga_create_user',
            'aga_create_time',
            'aga_create_ip',
            'aga_update_user',
            'aga_update_time',
            'aga_update_ip',
        ],
    ]) ?>

</div>
