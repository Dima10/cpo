<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\TrainingQuestionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $module array */

?>
<div class="training-question-index-grid">

<?php Pjax::begin(); ?>

<?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'layout' => '{pager}{summary}{items}{summary}{pager}',
        'pager' => [
            'firstPageLabel'=>Yii::t('app', 'First'),
            'lastPageLabel'=>Yii::t('app', 'Last'),
        ],

        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'class' => 'yii\grid\ActionColumn',
                'options' => ['width' => Yii::$app->params['yii\grid\ActionColumn']['width']],
                'template' => '{update}{delete}',
            ],

            'trq_id',
            'trq_question:ntext',

            [
                'attribute' => 'trm_name',
                'label' =>  Yii::t('app', 'Trq Trm ID'),
            ],

            [
                'attribute' => 'trm_code',
                'label' =>  Yii::t('app', 'Trm Code'),
            ],

            [
                'attribute' => 'trq_fname',
                'label' => Yii::t('app', 'Trq Fname'),
                'format' => 'raw',
                'value' => function ($data) {
                    if (isset($data->trq_fname)) {
                        return Html::img(yii\helpers\Url::to(['training-question/image', 'id' => $data->trq_id]), ['width' => '270px']);
                    } else {
                        return null;
                    }

                    //return Html::a($data->trq_fname, yii\helpers\Url::toRoute(['download', 'id' => $data->trq_id]), ['target' => '_blank']);
                },
                'visible' => !\app\models\User::isSpecAdmin(),
            ],

            /*
            [
                'attribute' => 'trq_trm_id',
                'value' =>  function ($data) {
                                return $data->trqTrm->trm_name;
                            },
                'filter' => $module,
            ],
            */

            [
                'attribute' => 'trq_create_user',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'trq_create_time',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'trq_create_ip',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'trq_update_user',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'trq_update_time',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'trq_update_ip',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],

        ],
    ]); ?>
<?php Pjax::end(); ?></div>
