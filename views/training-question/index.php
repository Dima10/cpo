<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\TrainingQuestionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $module array */

$this->title = Yii::t('app', 'Training Questions');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Service'), 'url' => ['site/svc']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="training-question-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Training Question'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= $this->render('_grid', [
        'dataProvider' => $dataProvider,
        'searchModel' => $searchModel,
        'module' => $module,
    ]) ?>

</div>
