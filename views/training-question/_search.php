<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TrainingQuestionSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="training-question-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'trq_id') ?>

    <?= $form->field($model, 'trq_question') ?>

    <?= $form->field($model, 'trq_trm_id') ?>

    <?= $form->field($model, 'trq_create_user') ?>

    <?= $form->field($model, 'trq_create_time') ?>

    <?php // echo $form->field($model, 'trq_create_ip') ?>

    <?php // echo $form->field($model, 'trq_update_user') ?>

    <?php // echo $form->field($model, 'trq_update_time') ?>

    <?php // echo $form->field($model, 'trq_update_ip') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
