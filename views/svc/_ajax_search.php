<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $id string */
/* @var $svc array */
?>

<?= Html::dropDownList($id, isset($svc[0]) ? $svc[0] : '', $svc, ['id' => $id]) ?>
