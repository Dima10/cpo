<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PersonVisit1 */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Person Visit1',
]) . $model->pv1_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Person Visit1s'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->pv1_id, 'url' => ['view', 'id' => $model->pv1_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="person-visit1-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
