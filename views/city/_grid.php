<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CitySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $country array */

Pjax::begin();
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn',
                'options' => ['width' => Yii::$app->params['yii\grid\SerialColumn']['width']],
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'options' => ['width' => Yii::$app->params['yii\grid\ActionColumn']['width']],
            ],

            [
                'attribute' => 'city_id',
                'options' => ['width' => '100']
            ],

            [
                'attribute' => 'city_cou_id',
                'label' => Yii::t('app', 'City Cou ID'),
                'value' => function ($data) { return $data->cityCou->cou_name; },
                'filter' => $country,
            ],

            [
                'attribute' => 'city_reg_id',
                'label' => Yii::t('app', 'City Reg ID'),
                'value' => function ($data) { return $data->cityReg->reg_name; },
                'filter' => $region,
            ],
            'city_name',

            [
                'attribute' => 'city_create_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'city_create_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'city_create_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'city_update_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'city_update_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'city_update_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],

        ],
    ]);
Pjax::end();
