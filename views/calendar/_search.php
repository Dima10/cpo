<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CalendarSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="calendar-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'cal_id') ?>

    <?= $form->field($model, 'cal_date') ?>

    <?= $form->field($model, 'cal_holiday') ?>

    <?= $form->field($model, 'cal_create_user') ?>

    <?= $form->field($model, 'cal_create_time') ?>

    <?php // echo $form->field($model, 'cal_create_ip') ?>

    <?php // echo $form->field($model, 'cal_update_user') ?>

    <?php // echo $form->field($model, 'cal_update_time') ?>

    <?php // echo $form->field($model, 'cal_update_ip') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
