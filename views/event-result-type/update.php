<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\EventResultType */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Event Result Type',
]) . $model->evrt_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Event Result Types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->evrt_id, 'url' => ['view', 'id' => $model->evrt_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="event-result-type-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
