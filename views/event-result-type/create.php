<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\EventResultType */

$this->title = Yii::t('app', 'Create Event Result Type');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Event Result Types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="event-result-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
