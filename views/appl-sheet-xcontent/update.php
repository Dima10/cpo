<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ApplSheetXContent */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => Yii::t('app', 'Appl Sheet Xcontent'),
]) . $model->applsxc_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Appl Sheet Xcontents'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->applsxc_id, 'url' => ['view', 'id' => $model->applsxc_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="appl-sheet-xcontent-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
