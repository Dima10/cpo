<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ApplSheetXContent */

$this->title = $model->applsxc_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Appl Sheet Xcontents'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="appl-sheet-xcontent-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->applsxc_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->applsxc_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'applsxc_id',
            'applsxc_applsx_id',
            'applsxc_prs_id',
            'applsxc_trp_id',
            'applsxc_score',
            'applsxc_passed',
            'applsxc_create_user',
            'applsxc_create_time',
            'applsxc_create_ip',
            'applsxc_update_user',
            'applsxc_update_time',
            'applsxc_update_ip',
        ],
    ]) ?>

</div>
