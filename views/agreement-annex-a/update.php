<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AgreementAnnexA */
/* @var $svc array */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => Yii::t('app', 'Agreement Annex A'),
]) . $model->ana_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Agreement Annex As'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ana_id, 'url' => ['view', 'id' => $model->ana_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="agreement-annex-a-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'svc' => $svc,
    ]) ?>

</div>
