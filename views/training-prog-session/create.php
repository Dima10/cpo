<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TrainingProgSession */

$this->title = Yii::t('app', 'Create Training Prog Session');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Training Prog Sessions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="training-prog-session-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
