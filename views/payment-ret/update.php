<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PaymentRet */
/* @var $ab array */
/* @var $comp array */
/* @var $aga array */
/* @var $acc array */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => Yii::t('app', 'Payment Ret'),
]) . $model->ptr_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Payment Rets'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ptr_id, 'url' => ['view', 'id' => $model->ptr_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="payment-ret-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'ab' => $ab,
        'comp' => $comp,
        'aga' => $aga,
        'acc' => $acc,
    ]) ?>

</div>
