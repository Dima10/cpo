<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PaymentRet */
/* @var $ab array */
/* @var $comp array */
/* @var $aga array */
/* @var $acc array */

$this->title = Yii::t('app', 'Create Payment Ret');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Payment Rets'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-ret-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'ab' => $ab,
        'comp' => $comp,
        'aga' => $aga,
        'acc' => $acc,
    ]) ?>

</div>
