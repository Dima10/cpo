<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\ApplSheetSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $ab array */
/* @var $trt array */
/* @var $trp array */
/* @var $reestr array */

?>
<div class="appl-sheet-index-grid">

<?php

Pjax::begin();

try {
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'layout' => "{pager}{summary}{items}{summary}{pager}",
        'pager' => [
            'firstPageLabel' => Yii::t('app', 'First'),
            'lastPageLabel' => Yii::t('app', 'Last'),
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            ['class' => 'yii\grid\ActionColumn'],

            'appls_id',
            [
                'attribute' => 'appls_reestr',
                'value' => function ($data) use ($reestr) {
                    if ($data->appls_reestr !== null){
                        return $reestr[$data->appls_reestr];
                    } else {
                        return '';
                    }

                },
                'label' => Yii::t('app', 'Applcmd Reestr'),
                'filter' => $reestr,
                'visible' => !\app\models\User::isSpecAdmin(),
            ],
            'appls_number',
            [
                'attribute' => 'appls_number1',
                'label' => Yii::t('app', 'Result'),
                'format' => 'raw',
                'value' => function ($data) {
                    if (isset($data->appls_magic)) {
                        return Html::a(Yii::$app->urlManager->createAbsoluteUrl(['appl-sheet/result', 'id' => $data->appls_id]), Yii::$app->urlManager->createAbsoluteUrl(['appl-sheet/result', 'id' => $data->appls_id, 'flag' => 1]), ['target' => '_blank']);
                    } else {
                        return '';
                    }
                },
            ],
            /*
            [
                'attribute' => 'appls_number1',
                'label' => Yii::t('app', 'Result'),
                'format' => 'raw',
                'value' => function ($data) {
                    return Html::a(Yii::t('app', 'Result'), Yii::$app->urlManager->createAbsoluteUrl(['appl-sheet/view', 'id' => $data->appls_id]), ['target' => '_blank']);
                },
            ],
            */

            'appls_date',
            [
                'attribute' => 'person_prs_full_name',
                'value' =>
                    function ($data) {
                        return $data->applsPrs->prs_full_name;
                    },
                'label' => Yii::t('app', 'Appls Prs ID'),
            ],
            [
                'attribute' => 'trp_name',
                'value' =>
                    function ($data) {
                        return $data->applsTrp->trp_name;
                    },
                'label' => Yii::t('app', 'Appls Trp ID'),
            ],

            'appls_score_max',
            'appls_score',
            [
                'attribute' => 'appls_passed',
                'value' =>
                    function ($data) {
                        return \app\models\Constant::YES_NO[$data->appls_passed ?? 0];
                    },
                'filter' => ['Нет', 'Да']
            ],
            [
                'attribute' => 'appls_trt_id',
                'label' => Yii::t('app', 'Appls Trt ID'),
                'value' =>
                    function ($data) {
                        return $data->applsTrt->trt_name;
                    },
                'filter' => $trt,
            ],

            [
                'attribute' => 'appls_file_name',
                'label' => Yii::t('app', 'Appls File Name'),
                'format' => 'raw',
                'value' => function ($data) {
                    return Html::a($data->appls_file_name, yii\helpers\Url::toRoute(['download', 'id' => $data->appls_id]), ['target' => '_blank']);
                },
            ],

            [
                'attribute' => 'appls_create_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'appls_create_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'appls_create_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'appls_update_user',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'appls_update_time',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'appls_update_ip',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],

        ],
    ]);
} catch (Exception $e) {
    echo $e->getMessage();
}

Pjax::end();


?>

</div>
