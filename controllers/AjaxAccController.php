<?php

namespace app\controllers;

use app\helpers\AgreementAccDocumentGenerator;
use app\helpers\AgreementActDocumentGenerator;
use app\helpers\DocumentGenerator;
use app\models\AgreementAcc;
use app\models\AgreementAccA;
use app\models\AgreementAct;
use app\models\AgreementActA;
use app\models\AgreementAnnex;
use app\models\AgreementAnnexA;
use Yii;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\UploadedFile;

class AjaxAccController extends Controller
{

    public function actionAjaxAgreementAccACreate($ana_id, $price, $qty, $aga_id, $date='2019-01-01') {
        $ana = AgreementAnnexA::findOne($ana_id);
        $acc = AgreementAcc::findOne($aga_id);

        $aca = AgreementAccA::find()->select(['aca_qty', 'aca_price'])->where('aca_ana_id = '.$ana_id)->all();

        $sum = 0;
        foreach ($aca as $acaItem) {
            $sum = $sum + $acaItem->aca_qty*$acaItem->aca_price;
        }

        if(($sum + ($price*$qty)) > ($ana->ana_price*$ana->ana_qty)){
            return json_encode(['row' => '', 'serviceId' => '', 'documentLink' => '' ]);
        }

        $serviceRow = new AgreementAccA();

        $serviceRow->setAttribute('aca_aga_id', $aga_id);
        $serviceRow->setAttribute('aca_ana_id', $ana->ana_id);
        $serviceRow->setAttribute('aca_qty', $qty);
        $serviceRow->setAttribute('aca_price', $price);
        $serviceRow->setAttribute('aca_tax', $ana->anaSvc->svc_tax_flag==1 ? ( (strtotime($date) >= strtotime(date('2019-01-01'))) ? ($price / 1.2 ) * 0.2 : ($price / 1.18 ) * 0.18) : null);

        $serviceRow->setAttribute('aca_create_user', Yii::$app->user->identity->username);
        $serviceRow->setAttribute('aca_create_time', date('Y-m-d H:i:s'));
        $serviceRow->setAttribute('aca_create_ip', Yii::$app->request->userIP);
        $serviceRow->setAttribute('aca_update_user', Yii::$app->user->identity->username);
        $serviceRow->setAttribute('aca_update_time', date('Y-m-d H:i:s'));
        $serviceRow->setAttribute('aca_update_ip', Yii::$app->request->userIP);

        $serviceRow->save();

        $acc->aga_sum += floatval($price) * intval($qty);
        $acc->aga_tax += floatval($serviceRow->aca_tax);
        $acc->save();

        $spanTax = null;
        $inputTax = null;
        if($serviceRow->aca_tax != null){
            $spanTax = '<span id="'.$serviceRow->aca_id.'_aca_tax_span" class="'.$serviceRow->aca_id.'_processTax">'.$serviceRow->aca_tax.'</span>';
            $inputTax = Html::input('text','aca_tax', $serviceRow->aca_tax, ['class'=>'hidden '.$serviceRow->aca_id.'_processTax', 'readonly'=>'readonly', 'id'=>$serviceRow->aca_id.'_aca_tax', 'size'=>8, 'style'=>'text-align:center']);
        }

        $row = '<tr id="tr_'.$serviceRow->aca_id.'" data-key="'.$serviceRow->aca_id.'">
                    <td> - </td>
                    <td>  
                        <a class="deleteService" href="/index.php?r=entity-frm/delete&id='.$serviceRow->aca_id.'" title="Удалить" data-aca_id="'.$serviceRow->aca_id.'"><span class="glyphicon glyphicon-trash"></span></a>
                    </td>
                    <td> '.$serviceRow->aca_id.' </td>

                    <td>'.($ana->agreementAccAs[0]->acaAga->aga_number ?? '').' *** '.
                            (isset($ana->anaAgra) ? $ana->anaAgra->agra_number : '') . ' / ' .
                            (isset($ana->anaSvc) ? $ana->anaSvc->svc_name : '') . ' / ' .
                            (isset($ana->anaTrp) ? $ana->anaTrp->trp_name : '').
                    '</td>

                    <td><span id="'.$serviceRow->aca_id.'_priceQty" class="'.$serviceRow->aca_id.'_processUpdate">'.$qty.'</span><input type="hidden" id="'.$serviceRow->aca_id.'_aca_qty" class="'.$serviceRow->aca_id.'_processUpdate processUpdateIput inputQty" name="aca_qty" value="'.$qty.'" size="5" data-relate-acaid="'.$serviceRow->aca_id.'" style="text-align:center"></td>
                    <td><span id="'.$serviceRow->aca_id.'_priceLabel" class="'.$serviceRow->aca_id.'_processUpdate">'.$price.'</span><input type="hidden" id="'.$serviceRow->aca_id.'_aca_price" class="'.$serviceRow->aca_id.'_processUpdate processUpdateIput" name="aca_price" value="'.$price.'" size="8" style="text-align:center"></td>
                    
                    <td>'.($serviceRow->aca_tax != null && $ana->anaSvc->svc_tax_flag==1 ? $spanTax.$inputTax : "Без НДС").'</td>
                    <td>'.Yii::$app->user->identity->username.'</td>
                    <td>'.date('Y-m-d H:i:s').'</td>
                    <td>'.Yii::$app->request->userIP.'</td>
                </tr>';


        $documentGenerator = new AgreementAccDocumentGenerator($aga_id);
        $documentLink = $documentGenerator->MSWordDocument();

        return json_encode(['row' => $row, 'serviceId' =>$serviceRow->aca_id,
            'documentLink' => $documentLink
        ]);
    }

    /**
     * @return false|string
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     * @throws \yii\web\HttpException
     */
    public function actionServiceDelete() {

        $acaId = $_POST['acaId'];
        $agaPrice = $_POST['agaPrice'];
        $agaTax = $_POST['agaTax'];

        $serviceRow = AgreementAccA::findOne($acaId);

        $acc = AgreementAcc::findOne($serviceRow->aca_aga_id);

        $acc->setAttribute('aga_sum', $agaPrice);
        $acc->setAttribute('aga_tax', $agaTax);
        $acc->save();

        $serviceRow->delete();

        $documentGenerator = new AgreementAccDocumentGenerator($acc->aga_id);
        $documentLink = $documentGenerator->MSWordDocument();

        return json_encode(
            [
                'result' => true,
                'documentLink' => $documentLink,
            ]
        );

    }

}
