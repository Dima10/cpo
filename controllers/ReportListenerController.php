<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2018 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\controllers;

use app\models\Ab;
use app\models\Agreement;
use app\models\AgreementAcc;
use app\models\AgreementAnnex;
use app\models\AgreementStatus;
use app\models\ApplFinal;
use app\models\ApplRequest;
use app\models\ApplRequestContent;
use app\models\ApplRequestContentReportSearch;
use app\models\Entity;
use app\models\EntityClass;
use app\models\EntityClassLnk;
use app\models\Excel_XML;
use app\models\Person;
use app\models\Staff;
use app\models\Svc;
use app\models\SvcDocType;
use app\models\TrainingProg;
use Yii;
use yii\base\DynamicModel;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

/**
 * ReportListenerController implements the R actions.
 */
class ReportListenerController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::class,
                'rules' => [
                    [
                        'allow' => false,
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' =>
                            function ($rule, $action) {
                                return \app\models\AcAccess::checkAction($action);
                            },
                    ],
                ],
            ],        
        ];
    }

    /**
     * Lists all Country models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ApplRequestContentReportSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        Yii::$app->session['ReportListenerParams'] = Yii::$app->request->queryParams;

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Finds the Country model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ApplRequestContentReportSearch the loaded model
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (($model = ApplRequestContentReportSearch::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }

    /**
     * Export to Excell
     * @see ApplRequestContentReportSearch->search()
     *
     * @throws \yii\web\RangeNotSatisfiableHttpException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionExportXml()
    {
        $query = (new \yii\db\Query())->select([
            'applr_number', 'applr_date', 'prs_id', 'prs_connect_user', 'prs_connect_pwd',
            '(SELECT GROUP_CONCAT(con_text) FROM contact AS cont WHERE cont.con_ab_id = prs_id AND cont.con_cont_id = 1) as contacts',
            'prs_full_name', 'prs_last_name', 'prs_first_name', 'prs_middle_name',
            '(SELECT GROUP_CONCAT(stf_position) FROM staff AS stff WHERE stff.stf_prs_id = prs_id) as stf_position',
            'ab1.ab_name as ab_name',
            '(SELECT GROUP_CONCAT(ec.entc_name) FROM entity_class_lnk ecl LEFT JOIN entity_class ec ON ecl.entcl_entc_id = ec.entc_id WHERE ecl.entcl_ent_id = ent_id) as entc_name',
            'agra_comment', 'ab2.ab_name as manager', 'agr_number', 'agra_number',
            '(SELECT GROUP_CONCAT(CONCAT(aga_number, " -", (SELECT ast_name FROM agreement_status AS agrsts WHERE ast_id = aga_ast_id)) ORDER BY aga_number) FROM agreement_acc AS agacc WHERE agacc.aga_agr_id = agr_id AND agacc.aga_prs_id = applrc_prs_id) as aga_number',
            'svdt_name', 'svc_name', 'trp_name',
            'svc_hour', 'applf_cmd_date', 'applf_end_date', 'applf_number', 'applr_reestr', 'applr_flag', 'ast_name' ])
            ->from('appl_request_content')
            ->leftJoin(Person::tableName(), 'applrc_prs_id = prs_id')
            ->leftJoin(Svc::tableName(), 'applrc_svc_id = svc_id')
            ->leftJoin(TrainingProg::tableName(), 'applrc_trp_id = trp_id')
            ->leftJoin(SvcDocType::tableName(), 'svc_svdt_id = svdt_id')
            ->leftJoin(ApplRequest::tableName(), 'applrc_applr_id = applr_id')
            ->leftJoin(AgreementAnnex::tableName(), 'applr_agra_id = agra_id')
            ->leftJoin(Agreement::tableName(), 'agr_id =agra_agr_id')
            ->leftJoin(Ab::tableName().' as ab1', 'applr_ab_id = ab1.ab_id')
            ->leftJoin(Entity::tableName(), 'ab_id = ent_id')
            ->leftJoin(AgreementStatus::tableName(), 'applr_ast_id = ast_id')
            ->leftJoin(ApplFinal::tableName(), 'applrc_applf_id = applf_id')
            ->leftJoin(Ab::tableName().' as ab2', 'applr_manager_id = ab2.ab_id')
        ;

        $searchModel = new ApplRequestContentReportSearch();
        $searchModel->load(Yii::$app->session['ReportListenerParams']);

        $query->andFilterWhere([
            'applrc_id' => $searchModel->applrc_id,
            'applrc_applr_id' => $searchModel->applrc_applr_id,
            'applrc_prs_id' => $searchModel->applrc_prs_id,
            'applrc_svc_id' => $searchModel->applrc_svc_id,
            'prs_id' => $searchModel->prs_id,
            'applr_flag' => $searchModel->applr_flag,
            'applr_reestr' => $searchModel->applr_reestr,
            'svdt_id' => $searchModel->svdt_name,
            'applr_ast_id' => $searchModel->ast_name
        ]);

        $query
            ->andFilterWhere(['like', 'applrc_date_upk', $searchModel->applrc_date_upk])
            ->andFilterWhere(['like', 'applrc_create_user', $searchModel->applrc_create_user])
            ->andFilterWhere(['like', 'applrc_create_time', $searchModel->applrc_create_time])
            ->andFilterWhere(['like', 'applrc_create_ip', $searchModel->applrc_create_ip])
            ->andFilterWhere(['like', 'applrc_update_user', $searchModel->applrc_update_user])
            ->andFilterWhere(['like', 'applrc_update_time', $searchModel->applrc_update_time])
            ->andFilterWhere(['like', 'applrc_update_ip', $searchModel->applrc_update_ip])
            ->andFilterWhere(['like', 'applr_number', $searchModel->applr_number])
            ->andFilterWhere(['like', 'applr_date', $searchModel->applr_date])
            ->andFilterHaving(['like', 'aga_number', $searchModel->aga_number])
            ->andFilterWhere(['like', 'agra_number', $searchModel->agra_number])
            ->andFilterWhere(['like', 'prs_connect_user', $searchModel->prs_connect_user])
            ->andFilterHaving(['like', 'contacts', $searchModel->contacts])
            ->andFilterWhere(['like', 'prs_full_name', $searchModel->prs_full_name])
            ->andFilterWhere(['like', 'prs_last_name', $searchModel->prs_last_name])
            ->andFilterWhere(['like', 'prs_first_name', $searchModel->prs_first_name])
            ->andFilterWhere(['like', 'prs_middle_name', $searchModel->prs_middle_name])
            ->andFilterHaving(['like', 'stf_position', $searchModel->stf_position])
            ->andFilterWhere(['like', 'svc_name', $searchModel->svc_name])
            ->andFilterWhere(['like', 'trp_name', $searchModel->trp_name])
            ->andFilterHaving(['like', 'ab_name', $searchModel->ab_name])
            ->andFilterWhere(['like', 'svc_hour', $searchModel->svc_hour])
            ->andFilterWhere(['like', 'svdt_name', $searchModel->svdt_name])
            ->andFilterWhere(['like', 'agr_number', $searchModel->agr_number])
            ->andFilterWhere(['like', 'ast_name', $searchModel->ast_name])
            ->andFilterHaving(['like', 'manager', $searchModel->manager])
            ->andFilterWhere(['like', 'agra_comment', $searchModel->agra_comment])
            ->andFilterWhere(['like', 'applf_number', $searchModel->applf_number])
            ->andFilterWhere(['like', 'applf_end_date', $searchModel->applf_end_date])
            ->andFilterWhere(['like', 'applf_cmd_date', $searchModel->applf_cmd_date])
            ->andFilterHaving(['like', 'entc_name', $searchModel->entc_name]);

        $data[] = [
            Yii::t('app', 'Appl Request'),
            Yii::t('app', 'Applm Applr Date'),
            Yii::t('app', 'Prs ID'),
            Yii::t('app', 'Prs Connect User'),
            Yii::t('app', 'Prs Connect Pwd'),
            Yii::t('app', 'Contact'),
            Yii::t('app', 'Prs Full Name'),
            Yii::t('app', 'Prs Last Name'),
            Yii::t('app', 'Prs First Name'),
            Yii::t('app', 'Prs Middle Name'),
            Yii::t('app', 'Stf Position'),
            Yii::t('app', 'Agr Ab ID'),
            Yii::t('app', 'Entcl Entc ID'),
            Yii::t('app', 'Agra Comment'),
            Yii::t('app', 'Applr Manager ID'),
            Yii::t('app', 'Agreement'),
            Yii::t('app', 'Applr Agra ID'),
            Yii::t('app', 'Agreement Acc'),
            Yii::t('app', 'Svdt Name'),
            Yii::t('app', 'Sprog Svc ID'),
            Yii::t('app', 'Sprog Trp ID'),
            Yii::t('app', 'Svc Hour'),
            Yii::t('app', 'Applf Cmd Date'),
            Yii::t('app', 'Applf End Date'),
            Yii::t('app', 'Appl Final'),
            Yii::t('app', 'Applr Reestr'),
            Yii::t('app', 'Applr Flag'),
            Yii::t('app', 'Agreement Status'),
        ];

        $queryData = array_map(function(&$row) {
            $row['stf_position'] = implode(',', array_filter(explode(',', $row['stf_position'])));
            $row['applr_reestr'] = \app\models\Constant::reestr_val()[$row['applr_reestr'] ?? ''];
            $row['applr_flag'] = \app\models\Constant::YES_NO[$row['applr_flag'] ?? ''];
            foreach ($row as $key => $value) {
                $row[$key] = empty($value) ? '' : $value;
            }
            return $row;
        }, $query->all());
        $data = array_merge($data, $queryData);

        $xls = new Excel_XML();
        $xls->WorksheetTitle = 'ReportListener';
        $xls->addArray($data);
        //$xls->generateXML('AgreementAcc');

        return Yii::$app->response->sendContentAsFile($xls->generateXML0(), 'ReportListener.xls');

    }
}
