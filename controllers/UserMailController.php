<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2018 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\controllers;

use app\models\Constant;
use app\models\SystemMail;
use app\models\UserMailType;
use Yii;
use app\models\UserMail;
use app\models\UserMailSearch;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\HttpException;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

/**
 * UserMailController implements the CRUD actions for UserMail model.
 */
class UserMailController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::class,
                'rules' => [
                    [
                        'allow' => false,
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' =>
                            function ($rule, $action) {
                                return \app\models\AcAccess::checkAction($action);
                            },
                    ],
                ],
            ],        
        ];
    }

    /**
     * Lists all UserMail models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserMailSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'sysmail' => ArrayHelper::map(SystemMail::find()->all(), 'sm_id', 'sm_name'),
            'reestr' => Constant::reestr_val(),
            'type' => ArrayHelper::map(UserMailType::find()->all(), 'umt_id', 'umt_name'),
        ]);
    }

    /**
     * Displays a single UserMail model.
     * @param integer $id
     * @return mixed
     * @throws HttpException
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new UserMail model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new UserMail();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->um_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'sysmail' => ArrayHelper::map(SystemMail::find()->all(), 'sm_id', 'sm_name'),
                'type' => ArrayHelper::map(UserMailType::find()->all(), 'umt_id', 'umt_name'),
                'reestr' => Constant::reestr_val(),
            ]);
        }
    }

    /**
     * Updates an existing UserMail model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws HttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->um_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'sysmail' => ArrayHelper::map(SystemMail::find()->all(), 'sm_id', 'sm_name'),
                'type' => ArrayHelper::map(UserMailType::find()->all(), 'umt_id', 'umt_name'),
                'reestr' => Constant::reestr_val(),
            ]);
        }
    }

    /**
     * Deletes an existing UserMail model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws HttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the UserMail model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return UserMail the loaded model
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (($model = UserMail::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }

    /**
     * Test sendmail.
     * @param integer $id
     * @return mixed;
     */
    public function actionTest($id)
    {
        if ($s = UserMail::sendMail($id)) {
            return $s;
        }
        return 'ok';
    }

}
