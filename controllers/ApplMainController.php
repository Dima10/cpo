<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\controllers;

use app\models\Ab;
use app\models\Excel_XML;
use app\models\SvcDocType;
use app\models\TrainingProg;
use Yii;
use app\models\ApplMain;
use app\models\ApplMainSearch;
use app\models\Company;
use app\models\Entity;
use app\models\Person;
use app\models\TrainingType;
use app\models\Svc;

use yii\web\Controller;
use yii\web\HttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

/**
 * ApplMainController implements the CRUD actions for ApplMain model.
 */
class ApplMainController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::class,
                'rules' => [
                    [
                        'allow' => false,
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' =>
                            function ($rule, $action) {
                                return \app\models\AcAccess::checkAction($action);
                            },
                    ],
                ],
            ],        
        ];
    }

    /**
     * Lists all ApplMain models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ApplMainSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        Yii::$app->session['ApplMainSearchParams'] = Yii::$app->request->queryParams;

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ApplMain model.
     * @param integer $id
     * @return mixed
     * @throws HttpException
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new ApplMain model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ApplMain();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->applm_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'person' => ArrayHelper::map(Person::find()->all(), 'prs_id', 'prs_full_name'),
                'ab' => ArrayHelper::map(Entity::find()->all(), 'ent_id', 'ent_name'),
                'comp' => ArrayHelper::map(Company::find()->all(), 'comp_id', 'comp_name'),
                'svc' => ArrayHelper::map(Svc::find()->all(), 'svc_id', 'svc_name'),
                'training_type' => ArrayHelper::map(TrainingType::find()->all(), 'trt_id', 'trt_name'),
            ]);
        }
    }

    /**
     * Updates an existing ApplMain model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws HttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->applm_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'person' => ArrayHelper::map(Person::find()->all(), 'prs_id', 'prs_full_name'),
                'ab' => ArrayHelper::map(Entity::find()->all(), 'ent_id', 'ent_name'),
                'comp' => ArrayHelper::map(Company::find()->all(), 'comp_id', 'comp_name'),
                'svc' => ArrayHelper::map(Svc::find()->all(), 'svc_id', 'svc_name'),
                'training_type' => ArrayHelper::map(TrainingType::find()->all(), 'trt_id', 'trt_name'),
            ]);
        }
    }

    /**
     * Deletes an existing ApplMain model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws HttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ApplMain model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ApplMain the loaded model
     * @throws HttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ApplMain::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }

    /**
     * Export to Excell
     * @see ApplMainSearch->search()
     *
     * @throws \yii\web\RangeNotSatisfiableHttpException
     */
    public function actionExportXml()
    {
        $query =
            (new \yii\db\Query())->select([
                'applm_id', 'applm_reestr','applm_prs_id','prs_full_name','applm_position','svc_name','trp_name','ab_name','comp_name',
                'trt_name','svdt_name','applm_date_upk','applm_apple_date','applm_number_upk','applm_appls_date','applm_applsx_date',
                'applm_appls0_date','applm_appls0x_date','applm_applcmd_date','applm_applr_date'
            ])->from(ApplMain::tableName())
                ->leftJoin(Person::tableName(), 'applm_prs_id = prs_id')
                ->leftJoin(Svc::tableName(), 'applm_svc_id = svc_id')
                ->leftJoin(Ab::tableName(), 'applm_ab_id = ab_id')
                ->leftJoin(Company::tableName(), 'applm_comp_id = comp_id')
                ->leftJoin(TrainingType::tableName(), 'applm_trt_id = trt_id')
                ->leftJoin(TrainingProg::tableName(), 'applm_trp_id = trp_id')
                ->leftJoin(SvcDocType::tableName(), 'applm_svdt_id = svdt_id')
        ;

        $searchModel = new ApplMainSearch();
        $searchModel->load(Yii::$app->session['ApplMainSearchParams']);

        $query->andFilterWhere([
            'applm_id' => $searchModel->applm_id,
            'applm_reestr' => $searchModel->applm_reestr,
            'applm_prs_id' => $searchModel->applm_prs_id,
            'svdt_id' => $searchModel->svdt_name,
            'trt_id' => $searchModel->trt_name
        ]);

        $query
            ->andFilterWhere(['like', 'prs_full_name', $searchModel->prs_full_name])
            ->andFilterWhere(['like', 'svc_name', $searchModel->svc_name])
            ->andFilterWhere(['like', 'trp_name', $searchModel->trp_name])
            ->andFilterWhere(['like', 'ab_name', $searchModel->ab_name])
            ->andFilterWhere(['like', 'comp_name', $searchModel->comp_name])

            ->andFilterWhere(['like', 'applm_position', $searchModel->applm_position])
            ->andFilterWhere(['like', 'applm_number_upk', $searchModel->applm_number_upk])
            ->andFilterWhere(['like', 'applm_create_user', $searchModel->applm_create_user])
            ->andFilterWhere(['like', 'applm_create_time', $searchModel->applm_create_time])
            ->andFilterWhere(['like', 'applm_create_ip', $searchModel->applm_create_ip])
            ->andFilterWhere(['like', 'applm_update_user', $searchModel->applm_update_user])
            ->andFilterWhere(['like', 'applm_update_time', $searchModel->applm_update_time])
            ->andFilterWhere(['like', 'applm_update_ip', $searchModel->applm_update_ip])

            ->andFilterWhere(['like', 'applm_date_upk', $searchModel->applm_date_upk])
            ->andFilterWhere(['like', 'applm_apple_date', $searchModel->applm_apple_date])
            ->andFilterWhere(['like', 'applm_applsx_date', $searchModel->applm_applsx_date])
            ->andFilterWhere(['like', 'applm_appls0x_date', $searchModel->applm_appls0x_date])
            ->andFilterWhere(['like', 'applm_appls_date', $searchModel->applm_appls_date])
            ->andFilterWhere(['like', 'applm_appls0_date', $searchModel->applm_appls0_date])
            ->andFilterWhere(['like', 'applm_applcmd_date', $searchModel->applm_applcmd_date])
            ->andFilterWhere(['like', 'applm_applr_date', $searchModel->applm_applr_date])
            ->andFilterWhere(['like', 'applm_date_start', $searchModel->applm_date_start])
            ->andFilterWhere(['like', 'applm_agr_date', $searchModel->applm_agr_date])
            ->andFilterWhere(['like', 'applm_applxxx_date', $searchModel->applm_applxxx_date])
        ;

        $data[] = [
            Yii::t('app', 'Applm ID'),
            Yii::t('app', 'Applm Reestr'),
            Yii::t('app', 'Applm Prs ID'),
            Yii::t('app', 'Prs Full Name'),
            Yii::t('app', 'Applm Position'),
            Yii::t('app', 'Applm Svc ID'),
            Yii::t('app', 'Applm Trp ID'),
            Yii::t('app', 'Applm Ab ID'),
            Yii::t('app', 'Applm Comp ID'),
            Yii::t('app', 'Applm Trt ID'),
            Yii::t('app', 'Applm Svdt ID'),
            Yii::t('app', 'Applm Date Upk'),
            Yii::t('app', 'Applm Apple Date'),
            Yii::t('app', 'Applm Number Upk'),
            Yii::t('app', 'Applm Appls Date'),
            Yii::t('app', 'Applm Applsx Date'),
            Yii::t('app', 'Applm Appls0 Date'),
            Yii::t('app', 'Applm Appls0x Date'),
            Yii::t('app', 'Applm Applcmd Date'),
            Yii::t('app', 'Applm Applr Date'),
        ];

        $data = array_merge($data, $query->all());

        $xls = new Excel_XML();
        $xls->WorksheetTitle = 'ApplMain';
        $xls->addArray($data);
        //$xls->generateXML('AgreementAcc');

        return Yii::$app->response->sendContentAsFile($xls->generateXML0(), 'ApplMain.xls');
    }

    public function actionUpdateReestr()
    {
        return;
        $command = Yii::$app->db->createCommand('
                select  ar.applr_id, ar.applr_reestr, arc.applrc_id
                from appl_request ar
                inner join appl_request_content arc on (ar.applr_id = arc.applrc_applr_id)
                where ar.applr_date>=\'2020-01-01\';
            ');
        $contents = $command->queryAll();

        foreach ($contents as $content){
            $applrcId = $content['applrc_id'];
            $applrReestr = $content['applr_reestr'];

            $command = Yii::$app->db->createCommand('UPDATE appl_main set applm_reestr=:reestr where applm_applrc_id=:applm_applrc_id');
            $command->bindParam(':reestr', $applrReestr);
            $command->bindParam(':applm_applrc_id', $applrcId);
            $command->execute();
        }
    }

}
