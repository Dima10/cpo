<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\controllers;

use app\models\ApplCommandContent;
use app\models\ApplCommandContentSearch;
use app\models\ApplEnd;
use app\models\ApplRequestContent;
use app\models\ApplXxx;
use app\models\Constant;
use app\models\Pattern;
use app\models\PatternType;
use app\models\ReplaceFiles;
use app\models\SvcDocType;
use app\models\TrainingType;
use app\models\User;
use Yii;
use app\models\ApplCommand;
use app\models\ApplCommandSearch;
use app\models\Company;

use yii\base\Exception;
use yii\web\Controller;
use yii\web\HttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * ApplCommandController implements the CRUD actions for ApplCommand model.
 */
class ApplCommandController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::class,
                'rules' => [
                    [
                        'allow' => false,
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' =>
                            function ($rule, $action) {
                                return \app\models\AcAccess::checkAction($action);
                            },
                    ],
                ],
            ],        
        ];
    }

    /**
     * Lists all ApplCommand models.
     * @return mixed
     */
    public function actionIndex()
    {
        $this->getView()->registerJsFile('/js/ApplCommandController.js',  ['position' => yii\web\View::POS_END]);

        $searchModel = new ApplCommandSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'comp' => ArrayHelper::map(Company::find()->all(), 'comp_id', 'comp_name'),
            'trt' => ArrayHelper::map(TrainingType::find()->all(), 'trt_id', 'trt_name'),
            'svdt' => ArrayHelper::map(SvcDocType::find()->all(), 'svdt_id', 'svdt_name'),
            'reestr' => Constant::reestr_val(),
        ]);
    }

    /**
     * Displays a single ApplCommand model.
     * @param integer $id
     * @return mixed
     * @throws HttpException
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new ApplCommand model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ApplCommand();
        $model->applcmd_date = date('Y-m-d');

        if ($model->load(Yii::$app->request->post())) {

            $model->applcmd_number = \DateTime::createFromFormat('Y-m-d', $model->applcmd_date)->format('Ymd') . '-' . $model->applcmd_trt_id.'-'.$model->applcmd_svdt_id;

            if ($model->save()) {
                return $this->redirect(['update', 'id' => $model->applcmd_id]);
            }

        }

        if (User::isSpecAdmin()) {
            $model->applcmd_comp_id = 1; // osnova
        }

        return $this->render('create', [
            'model' => $model,
            //'ab' => ArrayHelper::map(Entity::find()->select(['ent_id', 'ent_name'])->all(), 'ent_id', 'ent_name'),
            'comp' => ArrayHelper::map(Company::find()->select(['comp_id', 'comp_name'])->all(), 'comp_id', 'comp_name'),

            'trt' => ArrayHelper::map(TrainingType::find()->select(['trt_id', 'trt_name'])->all(), 'trt_id', 'trt_name'),
            'svdt' => ArrayHelper::map(SvcDocType::find()->select(['svdt_id', 'svdt_name'])->all(), 'svdt_id', 'svdt_name'),
            'pat' => ArrayHelper::map(Pattern::find()->select(['pat_id', 'pat_name'])->where(['pat_svdt_id' => $model->applcmd_svdt_id, 'pat_patt_id' => 6, 'pat_trt_id' => $model->applcmd_trt_id])->all(), 'pat_id', 'pat_name'),
            'reestr' => Constant::reestr_val(),
        ]);

    }

    /**
     * Updates an existing ApplCommand model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws Exception if not found Pattern
     * @throws HttpException
     * @throws \PhpOffice\PhpWord\Exception\CopyFileException
     * @throws \PhpOffice\PhpWord\Exception\CreateTemporaryFileException
     * @throws \PhpOffice\PhpWord\Exception\Exception
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {

            $pattern = Pattern::find()
                ->innerJoin(PatternType::tableName(), 'pat_patt_id = patt_id')
                ->where(['pat_svdt_id' => $model->applcmd_svdt_id])
                ->andWhere(['patt_id' => 6])
                ->andWhere(['pat_trt_id' => $model->applcmd_trt_id])
                ->one();
            if ($pattern == null) {
                throw new HttpException(500, yii::t('app', '$pattern = Pattern::find() not found'));
            }

            $model->applcmd_number = \DateTime::createFromFormat('Y-m-d', $model->applcmd_date)->format('Ymd') . '-' . $pattern->pat_code;


            $tmpl_name = Yii::getAlias('@app') . '/storage/' . $pattern->pat_fname;
            $file_name = Yii::getAlias('@app') . '/storage/' . $model->applcmd_date . '_' . $model->applcmd_trt_id . '_' . $pattern->pat_fname;
            file_put_contents($tmpl_name, $pattern->pat_fdata);

            $document = new \PhpOffice\PhpWord\TemplateProcessor($tmpl_name);
            $document->cloneRow('num', count($model->applCommandContents));

            $document->setValue('DOC_NUMBER', $model->applcmd_number);

            $dateVal = \DateTime::createFromFormat('Y-m-d', $model->applcmd_date)->getTimestamp();
            $document->setValue('DOC_DATE_DD', strftime('%d', $dateVal));
            $document->setValue('DOC_DATE_MONTH', Constant::MONTHS[strftime('%B', $dateVal)]);
            $document->setValue('DOC_DATE_YYYY', strftime('%Y', $dateVal));

            $i = 1;
            foreach ($model->applCommandContents as $k1 => $obj) {
                $document->setValue('num#' . $i, $i);
                $document->setValue('PERSONA_FIO#' . $i, $obj->applcmdcPrs->prs_full_name);
                $document->setValue('PROGRAMMA#' . $i, $obj->applcmdcTrp->trp_name);
                $document->setValue('DOGOVOR_NUMBER#' . $i, $obj->applcmdc_agr_number);

                $i++;
            }
            $document->saveAs($file_name);

            if (count($model->applCommandContents) > 0) {
                $model->applcmd_file_name = $model->applcmd_date . '_' . $model->applcmd_trt_id . '_' . $pattern->pat_fname;
                $model->applcmd_file_data = file_get_contents($file_name);
                $model->applcmd_pat_id = $pattern->pat_id;
            } else {
                $model->applcmd_file_name = null;
                $model->applcmd_file_data = null;
                $model->applcmd_pat_id = null;
            }

            unlink($file_name);
            unlink($tmpl_name);

            if ($model->save())
                return $this->redirect(['update', 'id' => $model->applcmd_id]);

        }
        $searchModel = new ApplCommandContentSearch();
        $dataProvider = $searchModel->search(['ApplCommandContentSearch' => ['applcmdc_applcmd_id' => $id]]);

        return $this->render('update', [
            'model' => $model,
            //'ab' => ArrayHelper::map(Entity::find()->select(['ent_id', 'ent_name'])->all(), 'ent_id', 'ent_name'),
            'comp' => ArrayHelper::map(Company::find()->select(['comp_id', 'comp_name'])->all(), 'comp_id', 'comp_name'),

            'trt' => ArrayHelper::map(TrainingType::find()->select(['trt_id', 'trt_name'])->where(['trt_id' => $model->applcmd_trt_id])->all(), 'trt_id', 'trt_name'),
            'svdt' => ArrayHelper::map(SvcDocType::find()->select(['svdt_id', 'svdt_name'])->where(['svdt_id' => $model->applcmd_svdt_id])->all(), 'svdt_id', 'svdt_name'),
            'pat' => ArrayHelper::map(Pattern::find()->select(['pat_id', 'pat_name'])->where(['pat_svdt_id' => $model->applcmd_svdt_id, 'pat_patt_id' => 6, 'pat_trt_id' => $model->applcmd_trt_id])->all(), 'pat_id', 'pat_name'),

            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'reestr' => Constant::reestr_val(),
        ]);

    }

    /**
     * Deletes an existing ApplCommand model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws HttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ApplCommand model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ApplCommand the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ApplCommand::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }

    /**
     * Redirect to download page.
     * @param integer $id
     * @return mixed
     */
    public function actionDownload($id)
    {
        return $this->redirect(['download-f', 'id' => $id]);
    }

    /**
     * Download file from existing model.
     * @param integer $id
     * @return mixed
     * @throws \yii\web\RangeNotSatisfiableHttpException
     */
    public function actionDownloadF($id)
    {
        $model = ApplCommand::findOne($id);
        return Yii::$app->response->sendContentAsFile($model->applcmd_file_data, $model->applcmd_file_name);
    }


    /**
     * Add person to existing model.
     * @param integer $req
     * @param integer $prs
     * @param integer $cmd
     * @return mixed
     */
    public function actionAjaxAddPerson($req, $prs, $cmd)
    {

        $reqCnt = ApplRequestContent::find()->where(['applrc_applr_id' => $req, 'applrc_prs_id' => $prs, 'applrc_applcmd_id' => null])->all();

        foreach ($reqCnt as $k => $value) {

            $model = new ApplCommandContent();
            $model->applcmdc_applcmd_id = $cmd;
            $model->applcmdc_applrc_id = $value->applrc_id;
            $model->applcmdc_prs_id = $value->applrc_prs_id;
            $model->applcmdc_trp_id = $value->applrc_trp_id;
            $model->save(false);

            $value->applrc_applcmd_id = $cmd;
            $value->save(false);

        }

        $searchModel = new ApplCommandContentSearch();
        $dataProvider = $searchModel->search(['ApplCommandContentSearch' => ['applcmdc_applcmd_id' => $cmd]]);

        return $this->renderAjax('_grid_content', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Deletes an existing ApplCommandContent model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $cmd_id
     * @param integer $id
     * @return mixed
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDeleteContent($cmd_id, $id)
    {
        if ($model = ApplCommandContent::findOne($id)) {
            if ($ref = $model->applcmdcApplrc) {
                $ref->applrc_applcmd_id = null;
                $ref->save(false);
            }

            $model->delete();
        }

        return $this->redirect(['update', 'id' => $cmd_id]);
    }

    public function actionReplaceFile()
    {
        $modelReplace = new ReplaceFiles();

        if (Yii::$app->request->isPost) {

            $modelReplace->replaceTypeId = (int)$_POST['ReplaceFiles']['replaceTypeId'];
            $modelReplace->idDocument = (int)$_POST['ReplaceFiles']['idDocument'];
            $modelReplace->fileData = $_FILES['ReplaceFiles'];

            if (empty($modelReplace->replaceTypeId) || empty($modelReplace->idDocument) || empty($modelReplace->fileData)) {
                $modelReplace->fileData = '';
                return $this->render('replace', [
                    'model' => $modelReplace,
                    'error' => 'Все поля должны быть заполнены',
                    'replaceTypes' => ReplaceFiles::$_replaceTypes
                ]);
            }

            $file = UploadedFile::getInstance($modelReplace, 'fileData');
            $modelReplace->fileData = file_get_contents($file->tempName);

            switch ($modelReplace->replaceTypeId) {
                case ReplaceFiles::PRIKAZ__O_ZACHISLENII:
                    $model = ApplCommand::findOne($modelReplace->idDocument);
                    $model->applcmd_file_data = $modelReplace->fileData;
                    break;
                case ReplaceFiles::PRIKAZ__OB_OKONCHANII:
                    $model = ApplEnd::findOne($modelReplace->idDocument);
                    $model->apple_file_data = $modelReplace->fileData;
                    break;
                case ReplaceFiles::PROTOKOL__KOMISII:
                    $model = ApplXxx::findOne($modelReplace->idDocument);
                    $model->applxxx_file_data = $modelReplace->fileData;
                    break;
                }

            if (isset($model) && $model->save(false)) {
                return $this->render('replace', [
                    'model' => new ReplaceFiles(),
                    'success' => 'Замена прошла успешно',
                    'replaceTypes' => ReplaceFiles::$_replaceTypes
                ]);
            }
        } else {
            return $this->render('replace', [
                'model' => $modelReplace,
                'replaceTypes' => ReplaceFiles::$_replaceTypes
            ]);
        }
    }
}
