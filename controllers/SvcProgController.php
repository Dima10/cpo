<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */


namespace app\controllers;

use Yii;
use app\models\{
    SvcProg,
    SvcProgSearch,
    Svc,
    TrainingProg
};
use yii\web\{
    Controller,
    NotFoundHttpException
};
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
 * SvcProgController implements the CRUD actions for SvcProg model.
 */
class SvcProgController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::class,
                'rules' => [
                    [
                        'allow' => false,
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' =>
                            function ($rule, $action) {
                                return \app\models\AcAccess::checkAction($action);
                            },
                    ],
                ],
            ],        
        ];
    }

    /**
     * Lists all SvcProg models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SvcProgSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'svc' => ArrayHelper::map(Svc::find()->all(), 'svc_id', 'svc_name'),
            'prog' => ArrayHelper::map(TrainingProg::find()->all(), 'trp_id', 'trp_name'),
       ]);
    }

    /**
     * Displays a single SvcProg model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new SvcProg model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SvcProg();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->sprog_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'svc' => ArrayHelper::map(Svc::find()->all(), 'svc_id', 'svc_name'),
                'prog' => ArrayHelper::map(TrainingProg::find()->all(), 'trp_id', 'trp_name'),
            ]);
        }
    }

    /**
     * Updates an existing SvcProg model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->sprog_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'svc' => ArrayHelper::map(Svc::find()->all(), 'svc_id', 'svc_name'),
                'prog' => ArrayHelper::map(TrainingProg::find()->all(), 'trp_id', 'trp_name'),
            ]);
        }
    }

    /**
     * Deletes an existing SvcProg model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the SvcProg model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SvcProg the loaded model
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (($model = SvcProg::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }

    /**
     * List filtered TrainingProg models.
     * @param integer $id
     * @return mixed
     */
    public function actionAjaxSearch($id)
    {
        $svc = Svc::findOne($id);

        $data = ArrayHelper::map($svc->svcProgs, 'trp_id', 'trp_name');

        return $this->renderAjax('_ajax_search', [
            'id' => $id,
            'data' => [0 => '<>'] + $data,
        ]);
    }
}
