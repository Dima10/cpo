<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\controllers;

use Yii;
use app\models\{
    TrainingAnswer,
    TrainingAnswerSearch,
    TrainingQuestion,
    TrainingQuestionSearch,
    TrainingModule
};
use yii\web\{
    Controller,
    NotFoundHttpException,
    UploadedFile
};
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
 * TrainingQuestionController implements the CRUD actions for TrainingQuestion model.
 */
class TrainingQuestionController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::class,
                'rules' => [
                    [
                        'allow' => false,
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' =>
                            function ($rule, $action) {
                                return \app\models\AcAccess::checkAction($action);
                            },
                    ],
                ],
            ],        
        ];
    }

    /**
     * Lists all TrainingQuestion models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TrainingQuestionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'module' => ArrayHelper::map(TrainingModule::find()->select(['trm_id', 'trm_name'])->all(), 'trm_id', 'trm_name'),
        ]);
    }

    /**
     * Displays a single TrainingQuestion model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TrainingQuestion model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {

        $model = new TrainingQuestion();

        if (Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->post());

            if (($file = UploadedFile::getInstance($model, 'trq_fdata')) != null) {
                $model->trq_fname = $file->name;
                $model->trq_fdata = file_get_contents($file->tempName);
            }

            if ($model->save()) {
                return $this->redirect(['update', 'id' => $model->trq_id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                    'module' => [],
                ]);

            }
        } else {
            return $this->render('create', [
                'model' => $model,
                'module' => [], //ArrayHelper::map(TrainingModule::find()->select(['trm_id', 'trm_name'])->all(), 'trm_id', 'trm_name'),
            ]);
        }
    }

    /**
     * Return filtered list of  TrainingModule model.
     * @param integer $val
     * @return mixed
     */
    public function actionAjaxTrainingModule($val)
    {
        return $this->renderAjax('_ajax_training_module', [
            'module' => ArrayHelper::map(TrainingModule::find()->select(['trm_id', 'CONCAT(trm_code,\' \',trm_name) as trm_name'])->where(['like', 'trm_name', $val])->all(), 'trm_id', 'trm_name'),
        ]);
    }


    /**
     * Updates an existing TrainingQuestion model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelAnswer = new TrainingAnswer();

        $provider = (new TrainingAnswerSearch())->search(['TrainingAnswerSearch' => ['tra_trq_id' => $id]]);

        if (Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->post());

            if (($file = UploadedFile::getInstance($model, 'trq_fdata')) != null) {
                $model->trq_fname = $file->name;
                $model->trq_fdata = file_get_contents($file->tempName);
            }
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->trq_id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                    'modelAnswer' => $modelAnswer,
                    'module' => ArrayHelper::map(TrainingModule::find()->select(['trm_id', 'trm_name'])->where(['trm_id' => $model->trq_trm_id])->all(), 'trm_id', 'trm_name'),
                    'dataProvider' => $provider,
                    'question' => ArrayHelper::map(TrainingQuestion::find()->select(['trq_id', 'trq_question'])->where(['trq_id' => $id])->all(), 'trq_id', 'trq_question'),
                ]);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
                'modelAnswer' => $modelAnswer,
                'module' => ArrayHelper::map(TrainingModule::find()->select(['trm_id', 'trm_name'])->where(['trm_id' => $model->trq_trm_id])->all(), 'trm_id', 'trm_name'),
                'dataProvider' => $provider,
                'question' => ArrayHelper::map(TrainingQuestion::find()->select(['trq_id', 'trq_question'])->where(['trq_id' => $id])->all(), 'trq_id', 'trq_question'),
            ]);
        }

    }

    /**
     * Creates a new TrainingAnswer model.
     * @return mixed
     */
    public function actionSubmitAnswer()
    {
        $model = new TrainingAnswer();

        $model->load(Yii::$app->request->post());

        if (($file = UploadedFile::getInstance($model, 'tra_file_data')) != null) {
            $model->tra_file_name = $file->name;
            $model->tra_file_data = file_get_contents($file->tempName);
        }

        $model->save();
        return $this->renderAjax('_grid_tra.php', [
                'dataProvider' => (new TrainingAnswerSearch())->search(['TrainingAnswerSearch' => ['tra_trq_id' => $model->tra_trq_id ]]),
            ]);
    }

    /**
     * Deletes an existing TrainingAnswer model.
     * @param integer $trq_id
     * @param integer $id
     * @return mixed
     */
    public function actionDeleteAnswer($trq_id, $id)
    {
        TrainingAnswer::findOne($id)->delete();

        return $this->renderAjax('_grid_tra.php', [
            'dataProvider' => (new TrainingAnswerSearch())->search(['TrainingAnswerSearch' => ['tra_trq_id' => $trq_id]]),
        ]);
    }

    /**
     * Deletes an existing TrainingQuestion model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Send image from existing model.
     * @param integer $id
     * @return mixed
     */
    public function actionImage($id)
    {
        $model = $this->findModel($id);
        return Yii::$app->response->sendContentAsFile($model->trq_fdata, $model->trq_fname);
    }


    /**
     * Redirect to download page.
     * @param integer $id
     * @return mixed
     */
    public function actionDownload($id)
    {
        return $this->redirect(['download-f', 'id' => $id]);
    }

    /**
     * Download file from existing TrainingQuestion model.
     * @param integer $id
     * @return mixed
     */
    public function actionDownloadF($id)
    {
        $model = TrainingQuestion::findOne($id);
        return Yii::$app->response->sendContentAsFile($model->trq_fdata, $model->trq_fname);
    }


    /**
     * Finds the TrainingQuestion model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TrainingQuestion the loaded model
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (($model = TrainingQuestion::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }
}
