<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2019 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */


namespace app\controllers;

use app\helpers\DocumentGenerator;
use app\models\AgreementAddSearch;
use app\models\ApplFinalSearch;
use app\models\ApplRequestContentSearch;
use app\models\ApplSheetSearch;
use app\models\Excel_XML;
use app\models\FinanceBookSearch;
use app\models\ListenerSearch;
use app\models\TrainingType;
use http\Url;
use Yii;
use app\models\Ab;
use app\models\AgreementAccA;
use app\models\AgreementAccASearch;
use app\models\AgreementAccSearchEf;
use app\models\AgreementActSearch;
use app\models\AgreementAdd;
use app\models\AgreementAnnexSearch;
use app\models\AgreementSearch;
use app\models\ApplFinal;
use app\models\ApplMain;
use app\models\ApplRequest;
use app\models\ApplRequestContent;
use app\models\ApplRequestSearch;
use app\models\ApplShSearch;
use app\models\Constant;
use app\models\Entity;
use app\models\EntityClass;
use app\models\EntityClassLnk;
use app\models\EntitySearch;
use app\models\EntityType;
use app\models\Staff;
use app\models\Person;
use app\models\Contact;
use app\models\ContactType;
use app\models\Address;
use app\models\AddressType;
use app\models\Account;
use app\models\Bank;
use app\models\Agreement;
use app\models\AgreementAnnex;
use app\models\AgreementAnnexA;
use app\models\AgreementAnnexASearch;
use app\models\AgreementAct;
use app\models\AgreementActA;
use app\models\AgreementActASearch;
use app\models\AgreementAcc;
use app\models\AgreementStatus;
use app\models\Svc;
use app\models\SvcProg;
use app\models\Pattern;
use app\models\Company;
use app\models\Country;
use app\models\Region;
use app\models\City;
use app\models\Tools;
use app\models\TrainingProg;
use app\models\FinanceBook;
use app\models\PaymentTo;
use app\models\PaymentToSearch;
use app\models\PaymentRet;
use app\models\PaymentRetSearch;

use yii\base\Exception;
use yii\helpers\Html;
use yii\web\{
    Controller, HttpException, NotFoundHttpException, UploadedFile
};
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\data\
{
    ArrayDataProvider,
    ActiveDataProvider
};
use yii\base\DynamicModel;

/**
 * EntityFrmController implements the CRUD actions for Entity model.
 */
class EntityFrmController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::class,
                'rules' => [
                    [
                        'allow' => false,
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' =>
                            function ($rule, $action) {
                                return \app\models\AcAccess::checkAction($action);
                            },
                    ],
                ],
            ],        
        ];
    }

    /**
     * Lists all Entity models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new EntitySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'entityType' => ArrayHelper::map(EntityType::find()->select(['entt_id', 'entt_name_short'])->all(), 'entt_id', 'entt_name_short'),
            'entityClass' => ArrayHelper::map(EntityClass::find()->select(['entc_id', 'entc_name'])->all(), 'entc_id', 'entc_name'),
            'agent' => ArrayHelper::map(Ab::find()->innerJoin(Entity::tableName(), 'ab_id = ent_agent_id')->all(), 'ab_id', 'ab_name'),
        ]);
    }

    /**
     * Creates a new Entity model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Entity();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->ent_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'entityType' => ArrayHelper::map(EntityType::find()->select(['entt_id', 'entt_name_short'])->all(), 'entt_id', 'entt_name_short'),
            ]);
        }
    }



    /**
     * Displays a single Entity model.
     * @param integer $id
     * @return mixed
     * @throws
     */
    public function actionView($id)
    {
        $this->getView()->registerJsFile('/js/EntityFrmFinalController.js',  ['position' => yii\web\View::POS_END]);

        $model = $this->findModel($id);

        // Staff
        $dataProviderStaff = new ActiveDataProvider([
            'query' => Staff::find()->where(['stf_ent_id' => $model->ent_id]),
        ]);

        // Contact
        $dataProviderContact = new ActiveDataProvider([
            'query' => Contact::find()->where(['con_ab_id' => $model->ent_id]),
        ]);

        // Address
        $dataProviderAddress = new ActiveDataProvider([
            'query' => Address::find()->where(['add_ab_id' => $model->ent_id]),
        ]);

        // Account
        $dataProviderAccount = new ActiveDataProvider([
            'query' => Account::find()->where(['acc_ab_id' => $model->ent_id]),
        ]);

        // Agreement
        /*
        $dataProviderAgreement = new ActiveDataProvider([
            'query' => Agreement::find()->where(['agr_ab_id' => $model->ent_id]),
        ]);
        */
        $searchModelAgreement = new AgreementSearch();
        $dataProviderAgreement = $searchModelAgreement->search(Yii::$app->request->queryParams, $model->ent_id);

        // AgreementAnnex
        /*
        $dataProviderAgreementAnnex = new ActiveDataProvider([
            'query' => AgreementAnnex::find()->where(['agra_ab_id' => $model->ent_id]),
        ]);
        */
        $searchModelAgreementAnnex = new AgreementAnnexSearch();
        $dataProviderAgreementAnnex = $searchModelAgreementAnnex->search(Yii::$app->request->queryParams, $model->ent_id);

        // AgreementAcc
        /*
        $dataProviderAgreementAcc = new ActiveDataProvider([
            'query' => AgreementAcc::find()->where(['aga_ab_id' => $model->ent_id]),
        ]);
        */
        $searchModelAgreementAcc = new AgreementAccSearchEf();
        $dataProviderAgreementAcc = $searchModelAgreementAcc->search(Yii::$app->request->queryParams, $model->ent_id);

        // AgreementAct
        /*
        $dataProviderAgreementAct = new ActiveDataProvider([
            'query' => AgreementAct::find()->where(['act_ab_id' => $model->ent_id]),
        ]);
        */
        $searchModelAgreementAct = new AgreementActSearch();
        $dataProviderAgreementAct = $searchModelAgreementAct->search(Yii::$app->request->queryParams, 10, $model->ent_id);

        // AgreementAdd
        $searchModelAgreementAdd = new AgreementAddSearch();
        $dataProviderAgreementAdd = $searchModelAgreementAdd->search(Yii::$app->request->queryParams, $model->ent_id);

        // FinanceBook
        $searchModelFinanceBook = new FinanceBookSearch();
        $dataProviderFB = $searchModelFinanceBook->search(Yii::$app->request->queryParams, $model->ent_id);

        // PaymentTo
        $dataProviderPaymentTo = new ActiveDataProvider([
            'query' => PaymentTo::find()->where(['payt_to_ab_id' => $model->ent_id])->andWhere(['IS NOT', 'payt_fb_id', NULL]),
            'sort' => false,
        ]);

        // PaymentRet
        $dataProviderPaymentRet = new ActiveDataProvider([
            'query' => PaymentRet::find()->where(['ptr_ab_id' => $model->ent_id])->andWhere(['IS NOT', 'ptr_fb_id', NULL]),
            'sort' => false,
        ]);

        // list listener
        $searchModelListener = new ListenerSearch();
        $dataProviderListener = $searchModelListener->search(Yii::$app->request->queryParams, $model->ent_id);

        // ApplRequest
        $searchModelApplRequest = new ApplRequestSearch();
        $dataProviderApplRequest = $searchModelApplRequest->search(Yii::$app->request->queryParams, $model->ent_id);

        // ApplSh
        $searchModelApplSh = new ApplShSearch();
        $dataProviderApplSh = $searchModelApplSh->search(Yii::$app->request->queryParams, $model->ent_id);

        // ApplSheet
        $searchModelApplSheet = new ApplSheetSearch();
        $dataProviderApplSheet = $searchModelApplSheet->search(Yii::$app->request->queryParams, $model->ent_id);

        // ApplFinal
        $searchModelApplFinal = new ApplFinalSearch();
        $dataProviderApplFinal = $searchModelApplFinal->search(Yii::$app->request->queryParams, $model->ent_id);


        return $this->render('view', [
            // Entity Tab
            'model' => $model,
            'entityType' => ArrayHelper::map(EntityType::find()->select(['entt_id', 'entt_name_short'])->all(), 'entt_id', 'entt_name_short'),

            // Staff Tab
            'searchModelStaff' => null, //$searchModelStaff,
            'dataProviderStaff' => $dataProviderStaff,

            // Contact Tab
            'searchModelContact' => null, //$searchModelContact,
            'dataProviderContact' => $dataProviderContact,


            // Address Tab
            'searchModelAddress' => null, //$searchModelAddress,
            'dataProviderAddress' => $dataProviderAddress,
            'addrbook' => null,
            'addressType' => null,
            'country' => null,
            'region' => null,
            'city' => null,

            // Account Tab
            'searchModelAccount' => null, //$searchModelAccount,
            'dataProviderAccount' => $dataProviderAccount,
            'bank' => [],

            // Agreement Tab
            'searchModelAgreement' => $searchModelAgreement,
            'dataProviderAgreement' => $dataProviderAgreement,
            'account' => ArrayHelper::map(Account::find()->select(['acc_id', 'acc_number'])->where(['acc_ab_id' => $model->ent_id])->all(), 'acc_id', 'acc_number'),
            'agreement_status' => ArrayHelper::map(AgreementStatus::find()->select(['ast_id', 'ast_name'])->all(), 'ast_id', 'ast_name'),
            'comp' => ArrayHelper::map(Company::find()->select(['comp_id', 'comp_name'])->all(), 'comp_id', 'comp_name'),

            // AgreementAnnex Tab
            'searchModelAgreementAnnex' => $searchModelAgreementAnnex,
            'dataProviderAgreementAnnex' => $dataProviderAgreementAnnex,
            'agreement' => ArrayHelper::map(Agreement::find()->select(['agr_id', 'agr_number'])->where(['agr_ab_id' => $model->ent_id])->all(), 'agr_id', 'agr_number'),

            // AgreementAcc Tab
            'searchModelAgreementAcc' => $searchModelAgreementAcc,
            'dataProviderAgreementAcc' => $dataProviderAgreementAcc,
            'agreement_annex' => ArrayHelper::map(AgreementAnnex::find()->select(['agra_id', 'agra_number'])->where(['agra_ab_id' => $model->ent_id])->all(), 'agra_id', 'agra_number'),

            // AgreementAct Tab
            'searchModelAgreementAct' => $searchModelAgreementAct,
            'dataProviderAgreementAct' => $dataProviderAgreementAct,

            // AgreementAdd Tab
            'searchModelAgreementAdd' => $searchModelAgreementAdd, //$searchModelAgreementAdd,
            'dataProviderAgreementAdd' => $dataProviderAgreementAdd,

            //Finance Tab
            'searchModelFB' => $searchModelFinanceBook, //$searchModelFinanceBook,
            'dataProviderFB' => $dataProviderFB,

            'agent' => [0 => '<>'] + ArrayHelper::map(Ab::find()->select(['ab_id', 'ab_name'])->where(['ab_id' => $model->ent_agent_id])->all(), 'ab_id', 'ab_name'),
            'agent_m' => [0 => '<>'] + ArrayHelper::map(Ab::find()->select(['ab_id', 'ab_name'])->where(['ab_id' => $model->ent_manager_id])->all(), 'ab_id', 'ab_name'),
            'entityClass' => ArrayHelper::map(EntityClass::find()->select(['entc_id', 'entc_name'])->all(), 'entc_id', 'entc_name'),

            //PaymentTo
            'searchModelPaymentTo' => null, //$searchModelPaymentTo,
            'dataProviderPaymentTo' => $dataProviderPaymentTo,

            //PaymentRet
            'searchModelPaymentRet' => null, //$searchModelPaymentRet,
            'dataProviderPaymentRet' => $dataProviderPaymentRet,

            // listener
            'searchModelListener' => $searchModelListener,
            'dataProviderListener' => $dataProviderListener,

            //ApplRequest
            'searchModelApplRequest' => $searchModelApplRequest,
            'dataProviderApplRequest' => $dataProviderApplRequest,
            'reestr' => Constant::reestr_val(),
            'training_type' => ArrayHelper::map(TrainingType::find()->all(), 'trt_id', 'trt_name'),

            //ApplSh
            'searchModelApplSh' => $searchModelApplSh,
            'dataProviderApplSh' => $dataProviderApplSh,
            'ab' => null,
            'trt' => null,
            'trp' => null,

            //ApplSheet
            'searchModelApplSheet' => $searchModelApplSheet,
            'dataProviderApplSheet' => $dataProviderApplSheet,

            'searchModelApplFinal' => $searchModelApplFinal,
            'dataProviderApplFinal' => $dataProviderApplFinal,


        ]);
    }



    /**
     * Creates a new Staff model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @param integer $ent_id
     * @return mixed
     * @throws
     */
    public function actionCreateStaff($ent_id)
    {
        $entity = $this->findModel($ent_id);

        $model = new Staff();

        if ($person = Person::objectExists()) {
            $model->stf_prs_id = $person->prs_id;
            Person::objectClear();
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $ent_id]);
        } else {
            return $this->render('create_staff', [
                'model' => $model,
                'entity' => ArrayHelper::map(Entity::find()->select(['ent_id', 'ent_name_short'])->where(['ent_id' => $entity->ent_id])->all(), 'ent_id', 'ent_name_short'),
                'person' => [],
            ]);
        }
    }

    /**
     * Updates an existing Staff model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $ent_id
     * @param integer $id
     * @return mixed
     * @throws
     */
    public function actionUpdateStaff($ent_id, $id)
    {
        $entity = $this->findModel($ent_id);

        $model = Staff::findOne($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $ent_id]);
        } else {
            return $this->render('update_staff', [
                'model' => $model,
                'entity' => ArrayHelper::map(Entity::find()->select(['ent_id', 'ent_name_short'])->where(['ent_id' => $entity->ent_id])->all(), 'ent_id', 'ent_name_short'),
                'person' => ArrayHelper::map(Person::find()->select(['prs_id', 'prs_full_name'])->innerJoin(Staff::tableName(), 'stf_prs_id = prs_id')->where(['stf_ent_id' => $entity->ent_id])->all(), 'prs_id', 'prs_full_name'),
            ]);
        }
    }


    /**
     * Creates a new Person model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @param integer $ent_id
     * @return mixed
     * @throws
     */
    public function actionCreatePerson($ent_id)
    {
        $entity = $this->findModel($ent_id);

        $model = new Person();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->objectSave();
            return $this->redirect(['create-staff', 'ent_id' => $ent_id]);
        } else {
            return $this->render('create_person', [
                'model' => $model,
                'entity' => ArrayHelper::map(Entity::find()->select(['ent_id', 'ent_name_short'])->where(['ent_id' => $entity->ent_id])->all(), 'ent_id', 'ent_name_short'),
            ]);
        }
    }

    /**
     * Creates a new Contact model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @param integer $ent_id
     * @return mixed
     * @throws
     */
    public function actionCreateContact($ent_id)
    {
        $entity = $this->findModel($ent_id);

        $model = new Contact();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $ent_id]);
        } else {
            return $this->render('create_contact', [
                'model' => $model,
                'entity' => ArrayHelper::map(Entity::find()->select(['ent_id', 'ent_name_short'])->where(['ent_id' => $entity->ent_id])->all(), 'ent_id', 'ent_name_short'),
                'addrbook' => ArrayHelper::map(Entity::find()->select(['ent_id', 'ent_name_short'])->where(['ent_id' => $entity->ent_id])->all(), 'ent_id', 'ent_name_short'),
                'contactType' => ArrayHelper::map(ContactType::find()->select(['cont_id', 'cont_name'])->all(), 'cont_id', 'cont_name'),
            ]);
        }
    }


    /**
     * Updates an existing Contact model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $ent_id
     * @param integer $id
     * @return mixed
     * @throws
     */
    public function actionUpdateContact($ent_id, $id)
    {
        $entity = $this->findModel($ent_id);

        $model = Contact::findOne($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $ent_id]);
        } else {
            return $this->render('update_contact', [
                'model' => $model,
                'entity' => ArrayHelper::map(Entity::find()->select(['ent_id', 'ent_name_short'])->where(['ent_id' => $entity->ent_id])->all(), 'ent_id', 'ent_name_short'),
                'contactType' => ArrayHelper::map(ContactType::find()->select(['cont_id', 'cont_name'])->all(), 'cont_id', 'cont_name'),
            ]);
        }
    }



    /**
     * Creates a new Address model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @param integer $ent_id
     * @return mixed
     * @throws
     */
    public function actionCreateAddress($ent_id)
    {
        $entity = $this->findModel($ent_id);

        $model = new Address();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $ent_id]);
        } else {
            return $this->render('create_address', [
                'model' => $model,
                'entity' => ArrayHelper::map(Entity::find()->select(['ent_id', 'ent_name_short'])->where(['ent_id' => $entity->ent_id])->all(), 'ent_id', 'ent_name_short'),
                'addressType' => ArrayHelper::map(AddressType::find()->select(['addt_id', 'addt_name'])->all(), 'addt_id', 'addt_name'),
                'country' => ArrayHelper::map(Country::find()->select(['cou_id', 'cou_name'])->all(), 'cou_id', 'cou_name'),
                'region' => [],
                'city' => [],
            ]);
        }
    }

    /**
     * Updates an existing Address model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $ent_id
     * @param integer $id
     * @return mixed
     * @throws
     */
    public function actionUpdateAddress($ent_id, $id)
    {
        $entity = $this->findModel($ent_id);

        $model = Address::findOne($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $ent_id]);
        } else {
            return $this->render('update_address', [
                'model' => $model,
                'addrbook' => ArrayHelper::map(Entity::find()->select(['ent_id', 'ent_name_short'])->where(['ent_id' => $entity->ent_id])->all(), 'ent_id', 'ent_name_short'),
                'entity' => ArrayHelper::map(Entity::find()->select(['ent_id', 'ent_name_short'])->where(['ent_id' => $entity->ent_id])->all(), 'ent_id', 'ent_name_short'),
                'addressType' => ArrayHelper::map(AddressType::find()->select(['addt_id', 'addt_name'])->all(), 'addt_id', 'addt_name'),
                'country' => ArrayHelper::map(Country::find()->select(['cou_id', 'cou_name'])->all(), 'cou_id', 'cou_name'),
                'region' => ArrayHelper::map(Region::find()->select(['reg_id', 'reg_name'])->where(['reg_cou_id' => $model->add_cou_id])->all(), 'reg_id', 'reg_name'),
                'city' => ArrayHelper::map(City::find()->select(['city_id', 'city_name'])->where(['city_reg_id' => $model->add_reg_id])->all(), 'city_id', 'city_name'),
            ]);
        }
    }


    /**
     * Creates a new Account model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @param integer $ent_id
     * @return mixed
     * @throws
     */
    public function actionCreateAccount($ent_id)
    {
        $entity = $this->findModel($ent_id);

        $model = new Account();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $ent_id]);
        } else {
            return $this->render('create_account', [
                'model' => $model,
                'entity' => ArrayHelper::map(Entity::find()->select(['ent_id', 'ent_name_short'])->where(['ent_id' => $entity->ent_id])->all(), 'ent_id', 'ent_name_short'),
                'bank' => [], //ArrayHelper::map(Bank::find()->all(), 'bank_id', 'bank_name'),
            ]);
        }
    }

    /**
     * Creates a list of Banks.
     * @param string $bic
     * @return mixed
     */
    public function actionAjaxBankSearch($bic)
    {
        return $this->renderAjax('_ajax_bank_search', [
            'bank' => ArrayHelper::map(Bank::find()->select(['bank_id', 'bank_name'])->where(['like', 'bank_bic', $bic])->orderBy('bank_name')->all(), 'bank_id', 'bank_name'),
        ]);
    }


    /**
     * Updates an existing Account model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $ent_id
     * @param integer $id
     * @return mixed
     * @throws
     */
    public function actionUpdateAccount($ent_id, $id)
    {
        $entity = $this->findModel($ent_id);

        $model = Account::findOne($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $ent_id]);
        } else {
            return $this->render('update_account', [
                'model' => $model,
                'entity' => ArrayHelper::map(Entity::find()->select(['ent_id', 'ent_name_short'])->where(['ent_id' => $entity->ent_id])->all(), 'ent_id', 'ent_name_short'),
                'bank' => ArrayHelper::map(Bank::find()->select(['bank_id', 'bank_name'])->all(), 'bank_id', 'bank_name'),
            ]);
        }
    }

    /**
     * Return Regions by Country.
     * @param integer $id
     * @return mixed
     */
    public function actionAjaxRegion($id)
    {
        return $this->renderAjax('_ajax_region', [
            'region' => ArrayHelper::map(Region::find()->select(['reg_id', 'reg_name'])->where(['reg_cou_id' => $id])->all(), 'reg_id', 'reg_name'),
        ]);
    }

    /**
     * Return City by Regions.
     * @param integer $id
     * @return mixed
     */
    public function actionAjaxCity($id)
    {
        return $this->renderAjax('_ajax_city', [
            'city' => [0 => '<>'] + ArrayHelper::map(City::find()->select(['city_id', 'city_name'])->where(['city_reg_id' => $id])->all(), 'city_id', 'city_name'),
        ]);
    }


    /**
     * Updates an existing Entity model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->ent_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'entityType' => ArrayHelper::map(EntityType::find()->select(['entt_id', 'entt_name_short'])->all(), 'entt_id', 'entt_name_short'),
            ]);
        }
    }

    /**
     * Deletes an existing Entity model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }

    /**
     * Finds the Entity model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Entity the loaded model
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (($model = Entity::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }



    /***********************************************************************************
     *
     *                                  Agreement
     *
     ***********************************************************************************/

    /**
     * Creates a new Agreement model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @param integer $ent_id
     * @return mixed
     * @throws
     */
    public function actionCreateAgreement($ent_id)
    {
        $entity = $this->findModel($ent_id);

        $dynModel = DynamicModel::validateData(['date', 'sum', 'tax', 'ent_id', 'comp_id', 'acc_id', 'pat_id', 'prs_id', 'add_id'],
            [
                [['ent_id', 'comp_id', 'acc_id', 'pat_id', 'prs_id', 'add_id'], 'integer'],
                [['date'], 'string'],
                [['sum', 'tax'], 'number']
            ]
        );
        $dynModel->ent_id = $ent_id;

        $model = new Agreement();

        if ($dynModel->load(Yii::$app->request->post())) {

            $model->agr_ab_id   = $dynModel->ent_id;
            $model->agr_comp_id = $dynModel->comp_id;
            $model->agr_pat_id  = $dynModel->pat_id;
            $model->agr_acc_id  = $dynModel->acc_id;
            $model->agr_prs_id  = $dynModel->prs_id;
            $model->agr_sum     = $dynModel->sum;
            $model->agr_tax     = $dynModel->tax;

            $model->agr_ast_id  =  AgreementStatus::find()->orderBy('ast_id')->one()->ast_id;
            $model->agr_date = $dynModel->date;

            $pattern = Pattern::findOne($model->agr_pat_id);

            $date_id = Agreement::find()->where(['agr_date' => $model->agr_date, 'agr_comp_id' => $model->agr_comp_id])->count('agr_date') + 1;
            $number = \DateTime::createFromFormat('Y-m-d', $model->agr_date)->format('ymd').'-'.$date_id.($pattern->pat_code ?? '');

            while (Agreement::find()->where(['agr_date' => $model->agr_date, 'agr_comp_id' => $model->agr_comp_id, 'agr_number' => $number])->exists()) {
                $date_id ++;
                $number = \DateTime::createFromFormat('Y-m-d', $model->agr_date)->format('ymd').'-'.$date_id.($pattern->pat_code ?? '');
            }

            $model->agr_number = \DateTime::createFromFormat('Y-m-d', $model->agr_date)->format('ymd').'-'.$date_id.($pattern->pat_code ?? '');
            $model->agr_fdata = str_replace('/', '-', $model->agr_number).' '.$pattern->pat_fname;



            // ------------- NEW -------------
            $tmpl_name = Yii::getAlias('@app') . '/storage/' . $pattern->pat_fname;
            $file_name = Yii::getAlias('@app') . '/storage/' . $model->agr_date . '_' . $pattern->pat_fname;
            file_put_contents($tmpl_name, $pattern->pat_fdata);

            $document = new \PhpOffice\PhpWord\TemplateProcessor($tmpl_name);
            //$document->cloneRow('num', count($grid));

            $tag_val = $model->agr_number;
            $document->setValue('DOC_NUMBER', $tag_val);

            $tag_val = \DateTime::createFromFormat('Y-m-d', $model->agr_date)->format('d-m-Y');
            $document->setValue('DOC_DATE', $tag_val);

            if ($obj = Entity::findOne($dynModel->ent_id)) {
                $tag_val = $obj->entEntt->entt_name;
                $document->setValue('ORGANISATION_TYPE_FULL', $tag_val);
            }
            if ($obj = Entity::findOne($dynModel->ent_id)) {
                $tag_val = $obj->ent_name;
                $document->setValue('ORGANISATION_NAME_FULL', $tag_val);
            }
            if ($obj = Address::find()->where(['add_id'=>$dynModel->add_id])->one()) {
                $tag_val =
                    (!empty($obj->add_index) ? $obj->add_index.', ' : '') .
                    (!empty($obj->addCou) ? $obj->addCou->cou_name.', ' : '') .
                    (!empty($obj->addReg)? $obj->addReg->reg_name.', ' : '') .
                    (!empty($obj->addCity) ? $obj->addCity->city_name.', ' : '') .
                    (!empty($obj->add_data) ? $obj->add_data : '');
                $document->setValue('ORGANISATION_U_ADDRESS', $tag_val);
            }
            if ($obj = Entity::findOne($dynModel->ent_id)) {
                $tag_val = $obj->ent_inn;
                $document->setValue('ORGANISATION_INN', $tag_val);
            }
            if ($obj = Entity::findOne($dynModel->ent_id)) {
                $tag_val = $obj->ent_kpp;
                $document->setValue('ORGANISATION_KPP', $tag_val);
            }
            if ($obj = Account::findOne(['acc_id' => $dynModel->acc_id])) {
                $tag_val = $obj->acc_number;
                $document->setValue('ORGANISATION_R_SCHET', $tag_val);
            }
            if ($acc = Account::findOne(['acc_id' => $dynModel->acc_id])) {
                if ($obj = Bank::findOne($acc->acc_bank_id)) {
                    $tag_val = $obj->bank_name;
                }
                $document->setValue('ORGANISATION_BANK_NAME', $tag_val);
            }
            if ($acc = Account::findOne(['acc_id' => $dynModel->acc_id])) {
                if ($obj = Bank::findOne($acc->acc_bank_id)) {
                    $tag_val = $obj->bank_account;
                }
                $document->setValue('ORGANISATION_BANK_K_SCHET', $tag_val);
            }
            if ($acc = Account::findOne(['acc_id' => $dynModel->acc_id])) {
                if ($obj = Bank::findOne($acc->acc_bank_id)) {
                    $tag_val = $obj->bank_bic;
                }
                $document->setValue('ORGANISATION_BANK_BIK', $tag_val);
            }
            if ($obj = Person::findOne($dynModel->prs_id)) {
                $tag_val = $obj->prs_full_name;
                $document->setValue('ORGANISATION_FIO', $tag_val);
            }
            if ($obj = Staff::find()->where(['stf_ent_id' => $dynModel->ent_id, 'stf_prs_id' => $dynModel->prs_id])->one()) {
                $tag_val = $obj->stf_position;
                $document->setValue('ORGANISATION_DOLGNOST', $tag_val);
            }

            $tag_val = $model->agr_sum > 0 ? number_format($model->agr_sum, 2) : '';
            $document->setValue('DOC_SUM', $tag_val);

            $tag_val = Tools::num2str($model->agr_sum);
            $document->setValue('DOC_SUM_TEXT', $tag_val);

            $tag_val = $model->agr_tax > 0 ? number_format($model->agr_tax, 2) : '';
            $document->setValue('DOC_TAX', $tag_val);

            $tag_val = Tools::num2str($model->agr_tax);
            $document->setValue('DOC_TAX_TEXT', $tag_val);

            $document->saveAs($file_name);

            $model->agr_fdata = str_replace('/', '-', $model->agr_number).' '.$pattern->pat_fname;
            $model->agr_data = file_get_contents($file_name);

            unlink($file_name);
            unlink($tmpl_name);

            if ($model->save()) {
                return $this->redirect(['view', 'id' => $entity->ent_id]);
            } else {
                $address = [];
                foreach (Address::find()->where(['add_ab_id'=>$dynModel->ent_id])->orderBy('add_id')->all() as $obj) {
                    $tag_val =
                        (!empty($obj->add_index) ? $obj->add_index.', ' : '') .
                        (!empty($obj->addCou) ? $obj->addCou->cou_name.', ' : '') .
                        (!empty($obj->addReg)? $obj->addReg->reg_name.', ' : '') .
                        (!empty($obj->addCity) ? $obj->addCity->city_name.', ' : '') .
                        (!empty($obj->add_data) ? $obj->add_data : '');
                    $address[$obj->add_id] = $tag_val;
                }

                return $this->render('create_agreement', [
                    'model' => $dynModel,
                    'modelA' => $model,
                    'entity' => ArrayHelper::map(Entity::find()->select(['ent_id', 'ent_name_short'])->where(['ent_id' => $entity->ent_id])->all(), 'ent_id', 'ent_name_short'),
                    'account' => ArrayHelper::map(Account::find()->select(['acc_id', 'acc_number'])->where(['acc_ab_id' => $entity->ent_id])->all(), 'acc_id', 'acc_number'),
                    'address' => $address,
                    'company' => ArrayHelper::map(Company::find()->select(['comp_id', 'comp_name'])->all(), 'comp_id', 'comp_name'),
                    'pattern' => ArrayHelper::map(Pattern::find()->select(['pat_id', 'pat_name'])->select(['pat_id', 'pat_name'])->where(['pat_patt_id'=>1])->all(), 'pat_id', 'pat_name'),
                    'staff' => ArrayHelper::map(Person::find()->select(['prs_id', 'prs_full_name'])->innerJoin(Staff::tableName(), 'prs_id=stf_prs_id')->where(['stf_ent_id' => $entity->ent_id])->all(), 'prs_id', 'prs_full_name'),
                ]);

            }
        } else {
            $dynModel->date = date('Y-m-d');
            $prs = Person::find()->innerJoin(Staff::tableName(), 'prs_id=stf_prs_id')->where(['stf_ent_id' => $entity->ent_id, 'stf_active' => 1])->one();
            $dynModel->prs_id = $prs->prs_id ?? null;
            $acc = Account::find()->where(['acc_ab_id' => $entity->ent_id, 'acc_active' => 1])->one();
            $dynModel->acc_id = $acc->acc_id ?? null;
            $addr = Address::find()->where(['add_ab_id'=>$entity->ent_id, 'add_active' => 1])->one();
            $dynModel->add_id = $addr->add_id ?? null;

            $address = [];
            foreach (Address::find()->where(['add_ab_id'=>$entity->ent_id])->orderBy('add_id')->all() as $obj) {
                $tag_val =
                    (!empty($obj->add_index) ? $obj->add_index.', ' : '') .
                    (!empty($obj->addCou) ? $obj->addCou->cou_name.', ' : '') .
                    (!empty($obj->addReg)? $obj->addReg->reg_name.', ' : '') .
                    (!empty($obj->addCity) ? $obj->addCity->city_name.', ' : '') .
                    (!empty($obj->add_data) ? $obj->add_data : '');
                $address[$obj->add_id] = $tag_val;
            }

            return $this->render('create_agreement', [
                'model' => $dynModel,
                'modelA' => $model,
                'entity' => ArrayHelper::map(Entity::find()->select(['ent_id', 'ent_name_short'])->where(['ent_id' => $entity->ent_id])->all(), 'ent_id', 'ent_name_short'),
                'account' => ArrayHelper::map(Account::find()->select(['acc_id', 'acc_number'])->where(['acc_ab_id' => $entity->ent_id])->all(), 'acc_id', 'acc_number'),
                'company' => ArrayHelper::map(Company::find()->select(['comp_id', 'comp_name'])->all(), 'comp_id', 'comp_name'),
                'pattern' => ArrayHelper::map(Pattern::find()->select(['pat_id', 'pat_name'])->select(['pat_id', 'pat_name'])->where(['pat_patt_id'=>1])->all(), 'pat_id', 'pat_name'),
                'staff' => ArrayHelper::map(Person::find()->select(['prs_id', 'prs_full_name'])->innerJoin(Staff::tableName(), 'prs_id=stf_prs_id')->where(['stf_ent_id' => $entity->ent_id])->all(), 'prs_id', 'prs_full_name'),
                'address' => $address,
            ]);
        }
    }

    /**
     * Updates an existing Agreement model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $ent_id
     * @param integer $id
     * @return mixed
     * @throws
     */
    public function actionUpdateAgreement($ent_id, $id)
    {
        $entity = $this->findModel($ent_id);

        $model = Agreement::findOne($id);

        if (Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->post());

            if (($file = UploadedFile::getInstance($model, 'agr_data')) != null) {
                $model->ag_fdata = $file->name;
                $model->agr_data = file_get_contents($file->tempName);
            } else {
                $model->agr_fdata = $model->oldAttributes['agr_fdata'];
                $model->agr_data = $model->oldAttributes['agr_data'];
            }

            if (($file = UploadedFile::getInstance($model, 'agr_data_sign')) != null) {
                $model->agr_fdata_sign = $file->name;
                $model->agr_data_sign = file_get_contents($file->tempName);
            } else {
                $model->agr_fdata_sign = $model->oldAttributes['agr_fdata_sign'];
                $model->agr_data_sign = $model->oldAttributes['agr_data_sign'];
            }


            if ($model->save()) {
                return $this->redirect(['view', 'id' => $ent_id]);
            } else {
                return $this->render('update_agreement', [
                    'model' => $model,
                    'entity' => ArrayHelper::map(Entity::find()->select(['ent_id', 'ent_name_short'])->where(['ent_id' => $entity->ent_id])->all(), 'ent_id', 'ent_name_short'),
                    'account' => ArrayHelper::map(Account::find()->select(['acc_id', 'acc_number'])->where(['acc_ab_id' => $entity->ent_id])->all(), 'acc_id', 'acc_number'),
                    'company' => ArrayHelper::map(Company::find()->select(['comp_id', 'comp_name'])->all(), 'comp_id', 'comp_name'),
                    'agreement_status' => ArrayHelper::map(AgreementStatus::find()->select(['ast_id', 'ast_name'])->orderBy('ast_id')->all(), 'ast_id', 'ast_name'),
                ]);
            }
        } else {
            return $this->render('update_agreement', [
                'model' => $model,
                'entity' => ArrayHelper::map(Entity::find()->select(['ent_id', 'ent_name_short'])->where(['ent_id' => $entity->ent_id])->all(), 'ent_id', 'ent_name_short'),
                'account' => ArrayHelper::map(Account::find()->select(['acc_id', 'acc_number'])->where(['acc_ab_id' => $entity->ent_id])->all(), 'acc_id', 'acc_number'),
                'company' => ArrayHelper::map(Company::find()->select(['comp_id', 'comp_name'])->all(), 'comp_id', 'comp_name'),
                'agreement_status' => ArrayHelper::map(AgreementStatus::find()->select(['ast_id', 'ast_name'])->orderBy('ast_id')->all(), 'ast_id', 'ast_name'),
            ]);
        }
    }

    /**
     * Delete an existing Agreement model.
     * If delete is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws
     */
    public function actionDeleteAgreement($id)
    {
        $model = Agreement::findOne($id);
        $entity = $this->findModel($model->agr_ab_id);
        $model->delete();
        return $this->redirect(['view', 'id' => $entity->ent_id]);
    }

    /**
     * Redirect to download page.
     * @param integer $id
     * @return mixed
     */
    public function actionDownloadAgreement($id)
    {
        return $this->redirect(['download-agreement-f', 'id' => $id]);
    }

    /**
     * Download file from existing Agreement model.
     * @param integer $id
     * @return mixed
     * @throws
     */
    public function actionDownloadAgreementF($id)
    {
        $model = Agreement::findOne($id);
        return Yii::$app->response->sendContentAsFile($model->agr_data, $model->agr_fdata);
    }

    /**
     * Redirect to download page.
     * @param integer $id
     * @return mixed
     */
    public function actionDownloadAgreementSign($id)
    {
        return $this->redirect(['download-agreement-sign-f', 'id' => $id]);
    }

    /**
     * Download file from existing Agreement model.
     * @param integer $id
     * @return mixed
     * @throws
     */
    public function actionDownloadAgreementSignF($id)
    {
        $model = Agreement::findOne($id);
        return Yii::$app->response->sendContentAsFile($model->agr_data_sign, $model->agr_fdata_sign);
    }




    /***********************************************************************************
     *
     *                                  AgreementAdd
     *
     ***********************************************************************************/

    /**
     * Creates a new AgreementAdd model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @param integer $ent_id
     * @return mixed
     * @throws
     */
    public function actionCreateAgreementAdd($ent_id)
    {
        $entity = $this->findModel($ent_id);

        $dynModel = DynamicModel::validateData(['date', 'ent_id', 'agr_id', 'pat_id', 'add_id', 'acc_id', 'prs_id', 'ast_id'],
            [
                [['ent_id', 'agr_id', 'pat_id', 'add_id', 'acc_id', 'prs_id', 'ast_id'], 'integer'],
                [['date'], 'string'],
            ]
        );
        $dynModel->ent_id = $ent_id;
        $model = new AgreementAdd();

        if ($dynModel->load(Yii::$app->request->post())) {

            $model->agadd_ab_id   = $dynModel->ent_id;
            $model->agadd_pat_id  = $dynModel->pat_id;
            $model->agadd_agr_id  = $dynModel->agr_id;
            $model->agadd_acc_id  = $dynModel->acc_id;
            $model->agadd_add_id  = $dynModel->add_id;
            $model->agadd_prs_id  = $dynModel->prs_id;
            $model->agadd_ast_id  = $dynModel->ast_id;

            $model->agadd_date = $dynModel->date;

            $pattern = Pattern::findOne($model->agadd_pat_id);

            /*
             Нумерация Дополнительных соглашений:
                номер договор
                + буквы из типа документа
                + порядковый номер в рамках договора...
             при этом в печатную форму идет только порядковый номер в рамках договора который...
            */

            $date_id = AgreementAdd::find()->where(['agadd_agr_id' => $model->agadd_agr_id])->count('agadd_date') + 1;
            $number = $model->agaddAgr->agr_number.($pattern->pat_code ?? '').str_pad($date_id, 2, STR_PAD_LEFT);

            while (AgreementAdd::find()->where(['agadd_agr_id' => $model->agadd_agr_id, 'agadd_number' => $number])->exists()) {
                $date_id ++;
                $number = $model->agaddAgr->agr_number.($pattern->pat_code ?? '').str_pad($date_id, 2, STR_PAD_LEFT);
            }

            $model->agadd_number = $number;
            $model->agadd_file_name = str_replace('/', '-', $model->agadd_number).' '.$pattern->pat_fname;



            // ------------- NEW -------------
            $tmpl_name = Yii::getAlias('@app') . '/storage/' . $pattern->pat_fname;
            $file_name = Yii::getAlias('@app') . '/storage/' . $model->agadd_date . '_' . $pattern->pat_fname;
            file_put_contents($tmpl_name, $pattern->pat_fdata);

            $document = new \PhpOffice\PhpWord\TemplateProcessor($tmpl_name);

            $tag_val = ltrim(substr($model->agadd_number, strlen($model->agadd_number)-2, 2), '0');
            $document->setValue('DOC_NUMBER', $tag_val);

            $tag_val = \DateTime::createFromFormat('Y-m-d', $model->agadd_date)->format('d-m-Y');
            $document->setValue('DOC_DATE', $tag_val);

            if ($obj = Entity::findOne($dynModel->ent_id)) {
                $tag_val = $obj->entEntt->entt_name;
                $document->setValue('ORGANISATION_TYPE_FULL', $tag_val);
            }
            if ($obj = Entity::findOne($dynModel->ent_id)) {
                $tag_val = $obj->ent_name;
                $document->setValue('ORGANISATION_NAME_FULL', $tag_val);
            }
            if ($obj = Address::find()->where(['add_ab_id'=>$dynModel->ent_id, 'add_addt_id'=>1])->orderBy('add_id')->one()) {
                $tag_val =
                    (!empty($obj->add_index) ? $obj->add_index.', ' : '') .
                    (!empty($obj->addCou) ? $obj->addCou->cou_name.', ' : '') .
                    (!empty($obj->addReg)? $obj->addReg->reg_name.', ' : '') .
                    (!empty($obj->addCity) ? $obj->addCity->city_name.', ' : '') .
                    (!empty($obj->add_data) ? $obj->add_data : '');
                $document->setValue('ORGANISATION_U_ADDRESS', $tag_val);
            }
            if ($obj = Entity::findOne($dynModel->ent_id)) {
                $tag_val = $obj->ent_inn;
                $document->setValue('ORGANISATION_INN', $tag_val);
            }
            if ($obj = Entity::findOne($dynModel->ent_id)) {
                $tag_val = $obj->ent_kpp;
                $document->setValue('ORGANISATION_KPP', $tag_val);
            }


            if ($obj = Entity::findOne($dynModel->ent_id)) {
                $tag_val = $obj->ent_inn;
                $document->setValue('ORGANISATION_INN', $tag_val);
            }
            if ($obj = Entity::findOne($dynModel->ent_id)) {
                $tag_val = $obj->ent_kpp;
                $document->setValue('ORGANISATION_KPP', $tag_val);
            }
            if ($obj = Account::findOne(['acc_id' => $model->agadd_acc_id])) {
                $tag_val = $obj->acc_number;
                $document->setValue('ORGANISATION_R_SCHET', $tag_val);
            }
            if ($acc = Account::findOne(['acc_id' => $model->agadd_acc_id])) {
                if ($obj = Bank::findOne($acc->acc_bank_id)) {
                    $tag_val = $obj->bank_name;
                    $document->setValue('ORGANISATION_BANK_NAME', $tag_val);
                }
            }
            if ($acc = Account::findOne(['acc_id' => $model->agadd_acc_id])) {
                if ($obj = Bank::findOne($acc->acc_bank_id)) {
                    $tag_val = $obj->bank_account;
                    $document->setValue('ORGANISATION_BANK_K_SCHET', $tag_val);
                }
            }
            if ($acc = Account::findOne(['acc_id' => $model->agadd_acc_id])) {
                if ($obj = Bank::findOne($acc->acc_bank_id)) {
                    $tag_val = $obj->bank_bic;
                    $document->setValue('ORGANISATION_BANK_BIK', $tag_val);
                }
            }
            if ($obj = Person::findOne($model->agadd_prs_id)) {
                $tag_val = $obj->prs_full_name;
                $document->setValue('ORGANISATION_FIO', $tag_val);
            }
            if ($obj = Staff::find()->where(['stf_ent_id' => $dynModel->ent_id, 'stf_prs_id' => $model->agadd_prs_id])->one()) {
                $tag_val = $obj->stf_position;
                $document->setValue('ORGANISATION_DOLGNOST', $tag_val);
            }


            $tag_val = isset($model->agaddAgr) ? $model->agaddAgr->agr_number : '';
            $document->setValue('AGR_NUMBER', $tag_val);

            $tag_val = isset($model->agaddAgr) ? \DateTime::createFromFormat('Y-m-d', $model->agaddAgr->agr_date)->format('d-m-Y') : '';
            $document->setValue('AGR_DATE', $tag_val);


            $dateVal = \DateTime::createFromFormat('Y-m-d', $model->agaddAgr->agr_date)->getTimestamp();
            $document->setValue('AGR_DATE_DD', strftime('%d', $dateVal));
            $document->setValue('AGR_DATE_MONTH', Constant::MONTHS[strftime('%B', $dateVal)]);
            $document->setValue('AGR_DATE_YYYY', strftime('%Y', $dateVal));

            $document->saveAs($file_name);

            $model->agadd_file_name = str_replace('/', '-', $model->agadd_number).' '.$pattern->pat_fname;
            $model->agadd_file_data = file_get_contents($file_name);

            unlink($file_name);
            unlink($tmpl_name);

            if ($model->save()) {
                return $this->redirect(['view', 'id' => $entity->ent_id]);
            } else {


                $address = [];
                foreach (Address::find()->where(['add_ab_id'=>$dynModel->ent_id])->orderBy('add_id')->all() as $obj) {
                    $tag_val =
                        (!empty($obj->add_index) ? $obj->add_index.', ' : '') .
                        (!empty($obj->addCou) ? $obj->addCou->cou_name.', ' : '') .
                        (!empty($obj->addReg)? $obj->addReg->reg_name.', ' : '') .
                        (!empty($obj->addCity) ? $obj->addCity->city_name.', ' : '') .
                        (!empty($obj->add_data) ? $obj->add_data : '');
                    $address[$obj->add_id] = $tag_val;
                }

                $account = [];
                foreach (Account::find()->where(['acc_ab_id' => $ent_id])->all() as $acc) {
                    if ($obj = Bank::findOne($acc->acc_bank_id)) {
                        $account[$acc->acc_id] = $obj->bank_name.' '.$acc->acc_number;
                    }
                }

                return $this->render('create_agreement_add', [
                    'modelAdd' => $model,
                    'model' => $dynModel,
                    'entity' => ArrayHelper::map(Entity::find()->select(['ent_id', 'ent_name_short'])->where(['ent_id' => $entity->ent_id])->all(), 'ent_id', 'ent_name_short'),
                    'pattern' => ArrayHelper::map(Pattern::find()->select(['pat_id', 'pat_name'])->select(['pat_id', 'pat_name'])->where(['pat_patt_id'=>17])->all(), 'pat_id', 'pat_name'),
                    'agreement' => ArrayHelper::map(Agreement::find()->select(['agr_id', 'agr_number'])->where(['agr_ab_id' => $model->agadd_ab_id])->all(), 'agr_id', 'agr_number'),
                    'address' => $address,
                    'account' => $account,
                    'staff' => ArrayHelper::map(Person::find()->select(['prs_id', 'prs_full_name'])->innerJoin(Staff::tableName(), 'prs_id=stf_prs_id')->where(['stf_ent_id' => $entity->ent_id])->all(), 'prs_id', 'prs_full_name'),
                    'agreement_status' => ArrayHelper::map(AgreementStatus::find()->orderBy('ast_id')->all(), 'ast_id', 'ast_name'),
                ]);

            }
        } else {
            $dynModel->date = date('Y-m-d');
            $addr = Address::find()->where(['add_ab_id'=>$entity->ent_id, 'add_active' => 1])->one();
            $dynModel->add_id = $addr->add_id ?? null;
            $acc = Account::find()->where(['acc_ab_id' => $ent_id, 'acc_active' => 1])->one();
            $dynModel->acc_id = $acc->acc_id ?? null;
            $prs = Person::find()->innerJoin(Staff::tableName(), 'prs_id=stf_prs_id')->where(['stf_ent_id' => $entity->ent_id, 'stf_active' => 1])->one();
            $dynModel->prs_id = $prs->prs_id ?? null;

            $address = [];
            foreach (Address::find()->where(['add_ab_id'=>$dynModel->ent_id])->orderBy('add_id')->all() as $obj) {
                $tag_val =
                    (!empty($obj->add_index) ? $obj->add_index.', ' : '') .
                    (!empty($obj->addCou) ? $obj->addCou->cou_name.', ' : '') .
                    (!empty($obj->addReg)? $obj->addReg->reg_name.', ' : '') .
                    (!empty($obj->addCity) ? $obj->addCity->city_name.', ' : '') .
                    (!empty($obj->add_data) ? $obj->add_data : '');
                $address[$obj->add_id] = $tag_val;
            }

            $account = [];
            foreach (Account::find()->where(['acc_ab_id' => $ent_id])->all() as $acc) {
                if ($obj = Bank::findOne($acc->acc_bank_id)) {
                    $account[$acc->acc_id] = $obj->bank_name.' '.$acc->acc_number;
                }
            }

            return $this->render('create_agreement_add', [
                'modelAdd' => $model,
                'model' => $dynModel,
                'entity' => ArrayHelper::map(Entity::find()->select(['ent_id', 'ent_name_short'])->where(['ent_id' => $entity->ent_id])->all(), 'ent_id', 'ent_name_short'),
                'pattern' => ArrayHelper::map(Pattern::find()->select(['pat_id', 'pat_name'])->select(['pat_id', 'pat_name'])->where(['pat_patt_id'=>17])->all(), 'pat_id', 'pat_name'),
                'agreement' => ArrayHelper::map(Agreement::find()->select(['agr_id', 'agr_number'])->where(['agr_ab_id' => $entity->ent_id])->all(), 'agr_id', 'agr_number'),
                'address' => $address,
                'account' => $account,
                'staff' => ArrayHelper::map(Person::find()->select(['prs_id', 'prs_full_name'])->innerJoin(Staff::tableName(), 'prs_id=stf_prs_id')->where(['stf_ent_id' => $entity->ent_id])->all(), 'prs_id', 'prs_full_name'),
                'agreement_status' => ArrayHelper::map(AgreementStatus::find()->orderBy('ast_id')->all(), 'ast_id', 'ast_name'),
            ]);
        }
    }

    /**
     * Updates an existing Agreement model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $ent_id
     * @param integer $id
     * @return mixed
     * @throws
     */
    public function actionUpdateAgreementAdd($ent_id, $id)
    {
        $entity = $this->findModel($ent_id);

        $model = AgreementAdd::findOne($id);

        if (Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->post());

            if (($file = UploadedFile::getInstance($model, 'agadd_file_data')) != null) {
                $model->agadd_file_name = $file->name;
                $model->agadd_file_data = file_get_contents($file->tempName);
            } else {
                $model->agadd_file_name = $model->oldAttributes['agadd_file_name'];
                $model->agadd_file_data = $model->oldAttributes['agadd_file_data'];
            }

            if ($model->save()) {
                return $this->redirect(['view', 'id' => $ent_id]);
            } else {
                return $this->render('update_agreement_add', [
                    'model' => $model,
                    'entity' => ArrayHelper::map(Entity::find()->select(['ent_id', 'ent_name_short'])->where(['ent_id' => $entity->ent_id])->all(), 'ent_id', 'ent_name_short'),
                    'company' => ArrayHelper::map(Company::find()->select(['comp_id', 'comp_name'])->all(), 'comp_id', 'comp_name'),
                    'agreement' => ArrayHelper::map(Agreement::find()->select(['agr_id', 'agr_number'])->where(['agr_ab_id' => $model->agadd_ab_id])->all(), 'agr_id', 'agr_number'),
                    'agreement_status' => ArrayHelper::map(AgreementStatus::find()->orderBy('ast_id')->all(), 'ast_id', 'ast_name'),
                ]);
            }
        } else {
            return $this->render('update_agreement_add', [
                'model' => $model,
                'entity' => ArrayHelper::map(Entity::find()->select(['ent_id', 'ent_name_short'])->where(['ent_id' => $entity->ent_id])->all(), 'ent_id', 'ent_name_short'),
                'company' => ArrayHelper::map(Company::find()->select(['comp_id', 'comp_name'])->all(), 'comp_id', 'comp_name'),
                'agreement' => ArrayHelper::map(Agreement::find()->select(['agr_id', 'agr_number'])->where(['agr_ab_id' => $model->agadd_ab_id])->all(), 'agr_id', 'agr_number'),
                'agreement_status' => ArrayHelper::map(AgreementStatus::find()->orderBy('ast_id')->all(), 'ast_id', 'ast_name'),
            ]);
        }
    }

    /**
     * Delete an existing Agreement model.
     * If delete is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws
     */
    public function actionDeleteAgreementAdd($id)
    {
        $model = AgreementAdd::findOne($id);
        $entity = $this->findModel($model->agadd_ab_id);
        $model->delete();
        return $this->redirect(['view', 'id' => $entity->ent_id]);
    }

    /**
     * Redirect to download page.
     * @param integer $id
     * @return mixed
     */
    public function actionDownloadAgreementAdd($id)
    {
        return $this->redirect(['download-agreement-add-f', 'id' => $id]);
    }

    /**
     * Download file from existing Agreement model.
     * @param integer $id
     * @return mixed
     * @throws
     */
    public function actionDownloadAgreementAddF($id)
    {
        $model = AgreementAdd::findOne($id);
        return Yii::$app->response->sendContentAsFile($model->agadd_file_data, $model->agadd_file_name);
    }



    /***********************************************************************************
     *
     *                                  AgreementAnnex
     *
     ***********************************************************************************/

    /**
     * Creates a new AgreementAnnex model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @param integer $ent_id
     * @return mixed
     * @throws
     */
    public function actionCreateAgreementAnnex($ent_id)
    {

        $entity = $this->findModel($ent_id);

        $dynModel = DynamicModel::validateData(['ent_id', 'date', 'agr_id', 'pat_id', 'add_id', 'acc_id', 'prs_id', 'sum', 'tax'],
            [
                [['ent_id', 'agr_id', 'pat_id', 'add_id', 'acc_id', 'prs_id'], 'integer'],
                [['date'], 'string'],
                [['sum', 'tax'], 'number']
            ]
        );

        $dynModel->ent_id = $ent_id;

        $model = new AgreementAnnex();

        if ($dynModel->load(Yii::$app->request->post())) {

            $model->agra_ab_id  = $dynModel->ent_id;
            $model->agra_date   = $dynModel->date;
            $model->agra_agr_id = $dynModel->agr_id;
            $model->agra_pat_id = $dynModel->pat_id;
            $model->agra_sum    = $dynModel->sum;
            $model->agra_tax    = $dynModel->tax;
            $model->agra_acc_id = $dynModel->acc_id;
            $model->agra_add_id = $dynModel->add_id;
            $model->agra_prs_id = $dynModel->prs_id;

            $model->agra_ast_id  =  AgreementStatus::find()->orderBy('ast_id')->one()->ast_id;

// ------------- NEW -------------
            $pattern = Pattern::findOne($model->agra_pat_id);

            $tmpl_name = Yii::getAlias('@app') . '/storage/' . $pattern->pat_fname;
            $file_name = Yii::getAlias('@app') . '/storage/' . $model->agra_date . '_' . $pattern->pat_fname;
            file_put_contents($tmpl_name, $pattern->pat_fdata);

            $document = new \PhpOffice\PhpWord\TemplateProcessor($tmpl_name);
            $grid = Yii::$app->session['grid_agreement_annex_a'];
            $vars = $document->getVariables();
            if (array_search('num', $vars) !== false) {
                $document->cloneRow('num', count($grid));
            }



            $date_id = AgreementAnnex::find()->where(['agra_agr_id' => $model->agra_agr_id])->count('agra_date') + 1;
            $number = $model->agraAgr->agr_number.'-'.$date_id;
            while (AgreementAnnex::find()->where(['agra_agr_id' => $model->agra_agr_id, 'agra_number' => $number])->exists()) {
                $date_id ++;
                $number = $model->agraAgr->agr_number.'-'.$date_id;
            }
            $model->agra_number = $model->agraAgr->agr_number.'-'.$date_id;
            $model->agra_fdata = str_replace('/', '-', $model->agra_number).' '.$pattern->pat_fname;

            $tag_val = $model->agra_number;
            $document->setValue('DOC_NUMBER', $tag_val);

            $tag_val = \DateTime::createFromFormat('Y-m-d', $model->agra_date)->format('d-m-Y');
            $document->setValue('DOC_DATE', $tag_val);


            $tag_val = isset($model->agraAgr) ? $model->agraAgr->agr_number : '';
            $document->setValue('AGR_NUMBER', $tag_val);

            $tag_val = isset($model->agraAgr) ? \DateTime::createFromFormat('Y-m-d', $model->agraAgr->agr_date)->format('d-m-Y') : '';
            $document->setValue('AGR_DATE', $tag_val);

            if ($obj = Entity::findOne($dynModel->ent_id)) {
                $tag_val = $obj->entEntt->entt_name;
                $document->setValue('ORGANISATION_TYPE_FULL', $tag_val);
            }
            if ($obj = Entity::findOne($dynModel->ent_id)) {
                $tag_val = $obj->ent_name;
                $document->setValue('ORGANISATION_NAME_FULL', $tag_val);
            }
            if ($obj = $model->agraAdd) {
                $tag_val =
                    (!empty($obj->add_index) ? $obj->add_index.', ' : '') .
                    (!empty($obj->addCou) ? $obj->addCou->cou_name.', ' : '') .
                    (!empty($obj->addReg)? $obj->addReg->reg_name.', ' : '') .
                    (!empty($obj->addCity) ? $obj->addCity->city_name.', ' : '') .
                    (!empty($obj->add_data) ? $obj->add_data : '');
                $document->setValue('ORGANISATION_U_ADDRESS', $tag_val);
            }
            if ($obj = Entity::findOne($dynModel->ent_id)) {
                $tag_val = $obj->ent_inn;
                $document->setValue('ORGANISATION_INN', $tag_val);
            }
            if ($obj = Entity::findOne($dynModel->ent_id)) {
                $tag_val = $obj->ent_kpp;
                $document->setValue('ORGANISATION_KPP', $tag_val);
            }
            if ($obj = Account::findOne(['acc_id' => $model->agra_acc_id])) {
                $tag_val = $obj->acc_number;
                $document->setValue('ORGANISATION_R_SCHET', $tag_val);
            }
            if ($acc = Account::findOne(['acc_id' => $model->agra_acc_id])) {
                if ($obj = Bank::findOne($acc->acc_bank_id)) {
                    $tag_val = $obj->bank_name;
                    $document->setValue('ORGANISATION_BANK_NAME', $tag_val);
                }
            }
            if ($acc = Account::findOne(['acc_id' => $model->agra_acc_id])) {
                if ($obj = Bank::findOne($acc->acc_bank_id)) {
                    $tag_val = $obj->bank_account;
                    $document->setValue('ORGANISATION_BANK_K_SCHET', $tag_val);
                }
            }
            if ($acc = Account::findOne(['acc_id' => $model->agra_acc_id])) {
                if ($obj = Bank::findOne($acc->acc_bank_id)) {
                    $tag_val = $obj->bank_bic;
                    $document->setValue('ORGANISATION_BANK_BIK', $tag_val);
                }
            }
            if ($obj = Person::findOne($model->agra_prs_id)) {
                $tag_val = $obj->prs_full_name;
                $document->setValue('ORGANISATION_FIO', $tag_val);
            }
            if ($obj = Staff::find()->where(['stf_ent_id' => $dynModel->ent_id, 'stf_prs_id' => $model->agra_prs_id])->one()) {
                $tag_val = $obj->stf_position;
                $document->setValue('ORGANISATION_DOLGNOST', $tag_val);
            }
            if (!empty($model->agra_sum)) {
                $tag_val = number_format($model->agra_sum, 2);
            } else {
                $tag_val = '';
            }
            $document->setValue('DOC_SUM', $tag_val);

            $tag_val = Tools::num2str($model->agra_sum);
            $document->setValue('DOC_SUM_TEXT', $tag_val);

            if (!empty($model->agra_tax)) {
                $tag_val = number_format($model->agra_tax, 2);
            } else {
                $tag_val = '';
            }
            $document->setValue('DOC_TAX', $tag_val);

            $tag_val = Tools::num2str($model->agra_tax);
            $document->setValue('DOC_TAX_TEXT', $tag_val);

            $grid = Yii::$app->session['grid_agreement_annex_a'];
            $i = 1;

            foreach ($grid as $val) {
                $tax = isset($val['tax']) && isset($val['qty']) ?  floatval($val['tax'])*floatval($val['qty']) : '';
                $sum = isset($val['price']) && isset($val['qty']) ? floatval($val['price'])*floatval($val['qty']) : '';

                $document->setValue('num#'.$i, $i);
                $document->setValue('SVC#'.$i, $val['svc_name']);
                $document->setValue('PROG#'.$i, $val['trp_name']);

                $document->setValue('DOC#'.$i,      $val['svc_doc']);
                $document->setValue('HOUR#'.$i,     $val['svc_hour']);
                $document->setValue('PRICE#'.$i,    number_format($val['price'], 2));
                $document->setValue('QTY#'.$i,      $val['qty']);
                $document->setValue('SUM_T#'.$i,    $tax==0 ? Yii::t('app', 'Without Tax') : number_format($tax, 2));
                $document->setValue('SUM#'.$i,      number_format($sum, 2));
                $i++;

            }

            $document->saveAs($file_name);

            $model->agra_fdata = str_replace('/', '-', $model->agra_number).' '.$pattern->pat_fname;
            $model->agra_data = file_get_contents($file_name);

            unlink($file_name);
            unlink($tmpl_name);

            if ($model->save()) {

                $grid = Yii::$app->session['grid_agreement_annex_a'];
                foreach ($grid as $value) {
                    $ana = new AgreementAnnexA();
                    $ana->ana_agra_id = $model->agra_id;
                    $ana->ana_svc_id = $value['svc_id'];
                    $ana->ana_trp_id = $value['trp_id'];
                    $ana->ana_cost_a = $value['cost_a'];
                    $ana->ana_price_a = $value['price_a'];
                    $ana->ana_price = $value['price'];
                    $ana->ana_qty = $value['qty'];
                    $ana->ana_tax = $value['tax'];
                    $ana->save();
                }

                return $this->redirect(['view', 'id' => $ent_id]);
            } else {

                $dynModel->date = date('Y-m-d');

                $addr = Address::find()->where(['add_ab_id'=>$entity->ent_id, 'add_active' => 1])->one();
                $dynModel->add_id = $addr->add_id ?? null;
                $acc = Account::find()->where(['acc_ab_id' => $ent_id, 'acc_active' => 1])->one();
                $dynModel->acc_id = $acc->acc_id ?? null;
                $prs = Person::find()->innerJoin(Staff::tableName(), 'prs_id=stf_prs_id')->where(['stf_ent_id' => $entity->ent_id, 'stf_active' => 1])->one();
                $dynModel->prs_id = $prs->prs_id ?? null;

                $address = [];
                foreach (Address::find()->where(['add_ab_id'=>$dynModel->ent_id])->orderBy('add_id')->all() as $obj) {
                    $tag_val =
                        (!empty($obj->add_index) ? $obj->add_index.', ' : '') .
                        (!empty($obj->addCou) ? $obj->addCou->cou_name.', ' : '') .
                        (!empty($obj->addReg)? $obj->addReg->reg_name.', ' : '') .
                        (!empty($obj->addCity) ? $obj->addCity->city_name.', ' : '') .
                        (!empty($obj->add_data) ? $obj->add_data : '');
                    $address[$obj->add_id] = $tag_val;
                }

                $account = [];
                foreach (Account::find()->where(['acc_ab_id' => $ent_id])->all() as $acc) {
                    if ($obj = Bank::findOne($acc->acc_bank_id)) {
                        $account[$acc->acc_id] = $obj->bank_name.' '.$acc->acc_number;
                    }
                }

                return $this->render('create_agreement_annex', [
                    'model' => $dynModel,
                    'modelA' => $model,
                    'entity' => ArrayHelper::map(Entity::find()->select(['ent_id', 'ent_name_short'])->where(['ent_id' => $entity->ent_id])->all(), 'ent_id', 'ent_name_short'),
                    'agreement' => ArrayHelper::map(Agreement::find()->select(['agr_id', 'agr_number'])->where(['agr_ab_id' => $model->agra_ab_id])->all(), 'agr_id', 'agr_number'),
                    'pattern' => ArrayHelper::map(Pattern::find()->select(['pat_id', 'pat_name'])->where(['pat_patt_id'=>3])->all(), 'pat_id', 'pat_name'),
                    'svc' => [0 => '<>'] + ArrayHelper::map(Svc::find()->select(['svc_id', 'svc_name'])->all(), 'svc_id', 'svc_name'),
                    'dataProvider' => new ArrayDataProvider([
                        'allModels' => Yii::$app->session['grid_agreement_annex_a'],
                        'pagination' => [
                            'pageSize' => 0,
                        ],

                    ]),
                    'address' => $address,
                    'account' => $account,
                    'staff' => ArrayHelper::map(Person::find()->select(['prs_id', 'prs_full_name'])->innerJoin(Staff::tableName(), 'prs_id=stf_prs_id')->where(['stf_ent_id' => $entity->ent_id])->all(), 'prs_id', 'prs_full_name'),
                ]);

            }

        } else {
            $dynModel->date = date('Y-m-d');

            $addr = Address::find()->where(['add_ab_id'=>$entity->ent_id, 'add_active' => 1])->one();
            $dynModel->add_id = $addr->add_id ?? null;
            $acc = Account::find()->where(['acc_ab_id' => $ent_id, 'acc_active' => 1])->one();
            $dynModel->acc_id = $acc->acc_id ?? null;
            $prs = Person::find()->innerJoin(Staff::tableName(), 'prs_id=stf_prs_id')->where(['stf_ent_id' => $entity->ent_id, 'stf_active' => 1])->one();
            $dynModel->prs_id = $prs->prs_id ?? null;

            $address = [];
            foreach (Address::find()->where(['add_ab_id'=>$dynModel->ent_id])->orderBy('add_id')->all() as $obj) {
                $tag_val =
                    (!empty($obj->add_index) ? $obj->add_index.', ' : '') .
                    (!empty($obj->addCou) ? $obj->addCou->cou_name.', ' : '') .
                    (!empty($obj->addReg)? $obj->addReg->reg_name.', ' : '') .
                    (!empty($obj->addCity) ? $obj->addCity->city_name.', ' : '') .
                    (!empty($obj->add_data) ? $obj->add_data : '');
                $address[$obj->add_id] = $tag_val;
            }

            $account = [];
            foreach (Account::find()->where(['acc_ab_id' => $ent_id])->all() as $acc) {
                if ($obj = Bank::findOne($acc->acc_bank_id)) {
                    $account[$acc->acc_id] = $obj->bank_name.' '.$acc->acc_number;
                }
            }

            Yii::$app->session['grid_agreement_annex_a'] = [];

            return $this->render('create_agreement_annex', [
                'model' => $dynModel,
                'modelA' => $model,
                'entity' => ArrayHelper::map(Entity::find()->select(['ent_id', 'ent_name_short'])->where(['ent_id' => $entity->ent_id])->all(), 'ent_id', 'ent_name_short'),
                'agreement' => ArrayHelper::map(Agreement::find()->select(['agr_id', 'agr_number'])->where(['agr_ab_id' => $entity->ent_id])->all(), 'agr_id', 'agr_number'),
                'pattern' => ArrayHelper::map(Pattern::find()->select(['pat_id', 'pat_name'])->where(['pat_patt_id'=>3])->all(), 'pat_id', 'pat_name'),
                'svc' => [0 => '<>'] + ArrayHelper::map(Svc::find()->select(['svc_id', 'svc_name'])->all(), 'svc_id', 'svc_name'),
                'dataProvider' => new ArrayDataProvider([
                    'allModels' => [],
                    'pagination' => [
                        'pageSize' => 0,
                    ],

                ]),
                'address' => $address,
                'account' => $account,
                'staff' => ArrayHelper::map(Person::find()->select(['prs_id', 'prs_full_name'])->innerJoin(Staff::tableName(), 'prs_id=stf_prs_id')->where(['stf_ent_id' => $entity->ent_id])->all(), 'prs_id', 'prs_full_name'),
            ]);
        }
    }

    /**
     * Add row and return grid.
     * @param integer $svc_id
     * @param integer $trp_id
     * @param string $price
     * @param integer $qty
     * @return mixed
     */
    public function actionAjaxAgreementACreate($svc_id, $trp_id, $price, $qty, $date='2019-01-01') {
        $svc = Svc::findOne($svc_id);
        $trp = TrainingProg::findOne($trp_id);
        $grid = Yii::$app->session['grid_agreement_annex_a'];
        $grid[] = [
            'svc_id' => $svc_id,
            'svc_name' => $svc->svc_name,
            'svc_doc' => $svc->svcSvdt->svdt_name,
            'svc_hour' => $svc->svc_hour,
            'trp_id' => $trp_id,
            'trp_name' => isset($trp) ?  $trp->trp_name : '',
            'cost_a' => $svc->svc_cost,
            'price_a' => $svc->svc_price,
            'price' => $price,
            'tax' => $svc->svc_tax_flag==1 ?
                (
                    (strtotime($date) >= strtotime(date('2019-01-01'))) ? ($price / 1.2 ) * 0.2 : ($price / 1.18 ) * 0.18
                )
                : null,
            'qty' => $qty,
        ];
        Yii::$app->session['grid_agreement_annex_a'] = $grid;

        $provider = new ArrayDataProvider([
            'allModels' => $grid,
            'pagination' => [
                'pageSize' => 0,
            ],

        ]);
        return $this->renderAjax('_ajax_agreement_a_grid', [
                'dataProvider' => $provider,
            ]
        );
    }

    /**
     * Add row and return grid.
     * @param integer $svc_id
     * @param integer $trp_id
     * @param string $price
     * @param integer $qty
     * @return mixed
     */
    public function actionAjaxAgreementARowCreate($svc_id, $trp_id, $price, $qty, $date='2019-01-01', $app_id) {
        $svc = Svc::findOne($svc_id);
        $trp = TrainingProg::findOne($trp_id);

        $grid[] = [
            'svc_id' => $svc_id,
            'svc_name' => $svc->svc_name,
            'svc_doc' => $svc->svcSvdt->svdt_name,
            'svc_hour' => $svc->svc_hour,
            'trp_id' => $trp_id,
            'trp_name' => isset($trp) ?  $trp->trp_name : '',
            'cost_a' => $svc->svc_cost,
            'price_a' => $svc->svc_price,
            'price' => $price,
            'tax' => $svc->svc_tax_flag==1 ?
                (
                    (strtotime($date) >= strtotime(date('2019-01-01'))) ? ($price / 1.2 ) * 0.2 : ($price / 1.18 ) * 0.18
                )
                : null,
            'qty' => $qty,
        ];

        $serviceRow = new AgreementAnnexA();
        $serviceRow->setAttribute('ana_agra_id', $app_id);
        $serviceRow->setAttribute('ana_svc_id', $svc_id);
        $serviceRow->setAttribute('ana_trp_id', $trp_id);
        $serviceRow->setAttribute('ana_cost_a', $svc->svc_cost);
        $serviceRow->setAttribute('ana_price_a', $svc->svc_price);
        $serviceRow->setAttribute('ana_qty', $qty);
        $serviceRow->setAttribute('ana_price', $price);
        $serviceRow->setAttribute('ana_tax', $svc->svc_tax_flag==1 ? ( (strtotime($date) >= strtotime(date('2019-01-01'))) ? ($price / 1.2 ) * 0.2 : ($price / 1.18 ) * 0.18) : null);

        $serviceRow->setAttribute('ana_create_user', Yii::$app->user->identity->username);
        $serviceRow->setAttribute('ana_create_time', date('Y-m-d H:i:s'));
        $serviceRow->setAttribute('ana_create_ip', Yii::$app->request->userIP);
        $serviceRow->setAttribute('ana_update_user', Yii::$app->user->identity->username);
        $serviceRow->setAttribute('ana_update_time', date('Y-m-d H:i:s'));
        $serviceRow->setAttribute('ana_update_ip', Yii::$app->request->userIP);

        $serviceRow->save();

        $spanTax = null;
        $inputTax = null;
        if($serviceRow->ana_tax != null){
            $spanTax = '<span id="'.$serviceRow->ana_id.'_ana_tax_span" class="'.$serviceRow->ana_id.'_processTax">'.$serviceRow->ana_tax.'</span>';
            $inputTax = Html::input('text','ana_tax', $serviceRow->ana_tax, ['class'=>'hidden '.$serviceRow->ana_id.'_processTax', 'readonly'=>'readonly', 'id'=>$serviceRow->ana_id.'_ana_tax', 'size'=>8, 'style'=>'text-align:center']);
        }

        $row = '<tr id="tr_'.$serviceRow->ana_id.'" data-key="'.$serviceRow->ana_id.'">
                    <td> - </td>
                    <td>
                        <a href="/index.php?r=svc%2Fview&id='.$serviceRow->ana_id.'" title="Просмотр" target="_blank"><span class="glyphicon glyphicon-eye-open"></span></a> 
                        <a id="'.$serviceRow->ana_id.'_updateAnnexARow" class="hidden '.$serviceRow->ana_id.'_processUpdate processUpdate" href="#" title="Редактировать" data-ana_id="'.$serviceRow->ana_id.'"><span class="glyphicon glyphicon-pencil"></span></a>
                        <a id="'.$serviceRow->ana_id.'_saveAnnexARow" class="'.$serviceRow->ana_id.'_processUpdate procesSave" href="#" title="Save" data-ana_id="'.$serviceRow->ana_id.'"><span class="glyphicon glyphicon-floppy-save"></span></a> 
                        <a class="deleteService" href="/index.php?r=entity-frm/delete&id='.$serviceRow->ana_id.'" title="Удалить" data-ana_id="'.$serviceRow->ana_id.'"><span class="glyphicon glyphicon-trash"></span></a>
                    </td>
                    <td>'.$serviceRow->ana_id.'</td>
                    <td>'.$svc->svc_name.'</td>
                    <td>'.(isset($trp) ?  $trp->trp_name : '').'</td>
                    <td>'.$svc->svc_cost.'</td>
                    <td>'.$svc->svc_price.'</td>
                    <td><span id="'.$serviceRow->ana_id.'_priceQty" class="hidden '.$serviceRow->ana_id.'_processUpdate">'.$qty.'</span><input type="text" id="'.$serviceRow->ana_id.'_ana_qty" class="'.$serviceRow->ana_id.'_processUpdate processUpdateIput inputQty" name="ana_qty" value="'.$qty.'" size="5" data-relate-anaid="'.$serviceRow->ana_id.'" style="text-align:center"></td>
                    <td><span id="'.$serviceRow->ana_id.'_priceLabel" class="hidden '.$serviceRow->ana_id.'_processUpdate">'.$price.'</span><input type="text" id="'.$serviceRow->ana_id.'_ana_price" class="'.$serviceRow->ana_id.'_processUpdate processUpdateIput" name="ana_price" value="'.$price.'" size="8" style="text-align:center"></td>
                    <td>'.($serviceRow->ana_tax != null && $svc->svc_tax_flag==1 ? $spanTax.$inputTax : "Без НДС").'</td>
                    <td>'.Yii::$app->user->identity->username.'</td>
                    <td>'.date('Y-m-d H:i:s').'</td>
                    <td>'.Yii::$app->request->userIP.'</td>
                </tr>';

        $documentGenerator = new DocumentGenerator();
        $documentLink = $documentGenerator->MSWordDocumentForAgreementAnnex($app_id);

        return json_encode(['row' => $row, 'serviceId' =>$serviceRow->ana_id, 'documentLink' => $documentLink ]);
    }

    /**
     * Add row and return grid.
     * @param integer $svc_id
     * @param integer $trp_id
     * @param string $price
     * @param integer $qty
     * @return mixed
     */
    public function actionAjaxActARowCreate($svc_id, $trp_id, $price, $qty, $date='2019-01-01', $app_id) {
        $svc = Svc::findOne($svc_id);
        $trp = TrainingProg::findOne($trp_id);

        $grid[] = [
            'svc_id' => $svc_id,
            'svc_name' => $svc->svc_name,
            'svc_doc' => $svc->svcSvdt->svdt_name,
            'svc_hour' => $svc->svc_hour,
            'trp_id' => $trp_id,
            'trp_name' => isset($trp) ?  $trp->trp_name : '',
            'cost_a' => $svc->svc_cost,
            'price_a' => $svc->svc_price,
            'price' => $price,
            'tax' => $svc->svc_tax_flag==1 ?
                (
                (strtotime($date) >= strtotime(date('2019-01-01'))) ? ($price / 1.2 ) * 0.2 : ($price / 1.18 ) * 0.18
                )
                : null,
            'qty' => $qty,
        ];

        $serviceRow = new AgreementAnnexA();
        $serviceRow->setAttribute('ana_agra_id', $app_id);
        $serviceRow->setAttribute('ana_svc_id', $svc_id);
        $serviceRow->setAttribute('ana_trp_id', $trp_id);
        $serviceRow->setAttribute('ana_cost_a', $svc->svc_cost);
        $serviceRow->setAttribute('ana_price_a', $svc->svc_price);
        $serviceRow->setAttribute('ana_qty', $qty);
        $serviceRow->setAttribute('ana_price', $price);
        $serviceRow->setAttribute('ana_tax', $svc->svc_tax_flag==1 ? ( (strtotime($date) >= strtotime(date('2019-01-01'))) ? ($price / 1.2 ) * 0.2 : ($price / 1.18 ) * 0.18) : null);

        $serviceRow->setAttribute('ana_create_user', Yii::$app->user->identity->username);
        $serviceRow->setAttribute('ana_create_time', date('Y-m-d H:i:s'));
        $serviceRow->setAttribute('ana_create_ip', Yii::$app->request->userIP);
        $serviceRow->setAttribute('ana_update_user', Yii::$app->user->identity->username);
        $serviceRow->setAttribute('ana_update_time', date('Y-m-d H:i:s'));
        $serviceRow->setAttribute('ana_update_ip', Yii::$app->request->userIP);

        $serviceRow->save();

        $spanTax = null;
        $inputTax = null;
        if($serviceRow->ana_tax != null){
            $spanTax = '<span id="'.$serviceRow->ana_id.'_ana_tax_span" class="'.$serviceRow->ana_id.'_processTax">'.$serviceRow->ana_tax.'</span>';
            $inputTax = Html::input('text','ana_tax', $serviceRow->ana_tax, ['class'=>'hidden '.$serviceRow->ana_id.'_processTax', 'readonly'=>'readonly', 'id'=>$serviceRow->ana_id.'_ana_tax', 'size'=>8, 'style'=>'text-align:center']);
        }

        $row = '<tr id="tr_'.$serviceRow->ana_id.'" data-key="'.$serviceRow->ana_id.'">
                    <td> - </td>
                    <td>
                        <a href="/index.php?r=svc%2Fview&id='.$serviceRow->ana_id.'" title="Просмотр" target="_blank"><span class="glyphicon glyphicon-eye-open"></span></a> 
                        <a id="'.$serviceRow->ana_id.'_updateAnnexARow" class="hidden '.$serviceRow->ana_id.'_processUpdate processUpdate" href="#" title="Редактировать" data-ana_id="'.$serviceRow->ana_id.'"><span class="glyphicon glyphicon-pencil"></span></a>
                        <a id="'.$serviceRow->ana_id.'_saveAnnexARow" class="'.$serviceRow->ana_id.'_processUpdate procesSave" href="#" title="Save" data-ana_id="'.$serviceRow->ana_id.'"><span class="glyphicon glyphicon-floppy-save"></span></a> 
                        <a class="deleteService" href="/index.php?r=entity-frm/delete&id='.$serviceRow->ana_id.'" title="Удалить" data-ana_id="'.$serviceRow->ana_id.'"><span class="glyphicon glyphicon-trash"></span></a>
                    </td>
                    <td>'.$serviceRow->ana_id.'</td>
                    <td>'.$svc->svc_name.'</td>
                    <td>'.(isset($trp) ?  $trp->trp_name : '').'</td>
                    <td>'.$svc->svc_cost.'</td>
                    <td>'.$svc->svc_price.'</td>
                    <td><span id="'.$serviceRow->ana_id.'_priceQty" class="hidden '.$serviceRow->ana_id.'_processUpdate">'.$qty.'</span><input type="text" id="'.$serviceRow->ana_id.'_ana_qty" class="'.$serviceRow->ana_id.'_processUpdate processUpdateIput inputQty" name="ana_qty" value="'.$qty.'" size="5" data-relate-anaid="'.$serviceRow->ana_id.'" style="text-align:center"></td>
                    <td><span id="'.$serviceRow->ana_id.'_priceLabel" class="hidden '.$serviceRow->ana_id.'_processUpdate">'.$price.'</span><input type="text" id="'.$serviceRow->ana_id.'_ana_price" class="'.$serviceRow->ana_id.'_processUpdate processUpdateIput" name="ana_price" value="'.$price.'" size="8" style="text-align:center"></td>
                    <td>'.($serviceRow->ana_tax != null && $svc->svc_tax_flag==1 ? $spanTax.$inputTax : "Без НДС").'</td>
                    <td>'.Yii::$app->user->identity->username.'</td>
                    <td>'.date('Y-m-d H:i:s').'</td>
                    <td>'.Yii::$app->request->userIP.'</td>
                </tr>';

        $documentGenerator = new DocumentGenerator();
        $documentLink = $documentGenerator->MSWordDocumentForAgreementAnnex($app_id);

        return json_encode(['row' => $row, 'serviceId' =>$serviceRow->ana_id, 'documentLink' => $documentLink ]);
    }

    /**
     * Delete row and return grid.
     * @param integer $id
     * @return mixed
     */
    public function actionAjaxAgreementADelete($id) {
        $grid = Yii::$app->session['grid_agreement_annex_a'];
        $key = -1;
        foreach ($grid as $k => $value) {
            if ($value['svc_id'] == $id) {
                $key = $k;
                break;
            }
        }
        if ($key >= 0)
            unset($grid[$key]);

        Yii::$app->session['grid_agreement_annex_a'] = $grid;

        $provider = new ArrayDataProvider([
            'allModels' => $grid,
            'pagination' => [
                'pageSize' => 0,
            ],

        ]);

        return $this->renderAjax('_ajax_agreement_a_grid', [
                'dataProvider' => $provider,
            ]
        );
    }


    /**
     * return AgreementA Sum.
     * @return mixed
     */
    public function actionAjaxAgreementASum() {
        $grid = Yii::$app->session['grid_agreement_annex_a'];
        $sum = 0.0;
        foreach ($grid as $k => $value) {
            $sum  += floatval($value['price']) * floatval($value['qty']);
        }

        return $this->renderAjax('_ajax_agreement_a_float', [
                'value' => $sum,
            ]
        );
    }

    /**
     * return AgreementA Tax.
     * @return mixed
     */
    public function actionAjaxAgreementATax() {
        $grid = Yii::$app->session['grid_agreement_annex_a'];
        $sum = 0.0;
        foreach ($grid as $k => $value) {
            $sum  += floatval($value['tax']) * floatval($value['qty']);
        }
        return $this->renderAjax('_ajax_agreement_a_float', [
                'value' => $sum,
            ]
        );
    }

    /**
     * Return cost from Svc model.
     * @param integer $svc_id
     * @return mixed
     */
    public function actionAjaxAgreementACost($svc_id) {
        $svc = Svc::findOne($svc_id);
        return $this->renderAjax('_ajax_agreement_a_cost', [
            'svc' => $svc,
            ]
        );

    }

    /**
     * Return price from Svc model.
     * @param integer $svc_id
     * @return mixed
     */
    public function actionAjaxAgreementAPrice($svc_id) {
        $svc = Svc::findOne($svc_id);
        return $this->renderAjax('_ajax_agreement_a_price', [
                'svc' => $svc,
            ]
        );

    }

    /**
     * Return list Svc model.
     * @param string $svc_name
     * @return mixed
     */
    public function actionAjaxAgreementASvc($svc_name) {
        $svc = ArrayHelper::map(
            Svc::find()
                ->where(['like', 'svc_name', $svc_name])
                ->all()
            , 'svc_id', 'svc_name');
        return $this->renderAjax('_ajax_agreement_a_svc', [
                'svc' => [0 => '<>'] + $svc,
            ]
        );

    }

    /**
     * Return list TraningProg Svc model.
     * @param integer $svc_id
     * @param string $trp_name default ''
     * @return mixed
     */
    public function actionAjaxAgreementATrp($svc_id, $trp_name = '') {
        $prog = ArrayHelper::map(
            TrainingProg::find()
                ->innerJoin(SvcProg::tableName(), 'trp_id = sprog_trp_id')
                ->where(['sprog_svc_id' => $svc_id])
                ->andWhere(['like', 'trp_name', $trp_name])
                ->orderBy(['trp_name'=>SORT_ASC])
                ->all()
            , 'trp_id', 'trp_name');
        return $this->renderAjax('_ajax_agreement_a_trp', [
                'prog' => $prog,
            ]
        );

    }

    /**
     * Updates an existing AgreementAnnex model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $ent_id
     * @param integer $id
     * @return mixed
     * @throws
     */
    public function actionUpdateAgreementAnnex($ent_id, $id)
    {

        $this->getView()->registerJsFile('/js/UpdateAgreementAnnexController.js',  ['position' => yii\web\View::POS_END]);

        $entity = $this->findModel($ent_id);

        $model = AgreementAnnex::findOne($id);

        $dataProvider = (new AgreementAnnexASearch())->search(['AgreementAnnexASearch' => ['ana_agra_id' => $id]]);

        if (Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->post());

            if (($file = UploadedFile::getInstance($model, 'agra_data')) != null) {
                $model->agra_fdata = $file->name;
                $model->agra_data = file_get_contents($file->tempName);
            } else {
                $model->agra_fdata = $model->oldAttributes['agra_fdata'];
                $model->agra_data = $model->oldAttributes['agra_data'];
            }

            if (($file = UploadedFile::getInstance($model, 'agra_data_sign')) != null) {
                $model->agra_fdata_sign = $file->name;
                $model->agra_data_sign = file_get_contents($file->tempName);
            } else {
                $model->agra_fdata_sign = $model->oldAttributes['agra_fdata_sign'];
                $model->agra_data_sign = $model->oldAttributes['agra_data_sign'];
            }

            if ($model->save()) {
                return $this->redirect(['view', 'id' => $entity->ent_id]);
            } else {
                return $this->render('update_agreement_annex', [
                    'model' => $model,
                    'entity' => ArrayHelper::map(Entity::find()->select(['ent_id', 'ent_name_short'])->where(['ent_id' => $entity->ent_id])->all(), 'ent_id', 'ent_name_short'),
                    'agreement' => ArrayHelper::map(Agreement::find()->select(['agr_id', 'agr_number'])->where(['agr_ab_id' => $model->agra_ab_id])->all(), 'agr_id', 'agr_number'),
                    'agreement_status' => ArrayHelper::map(AgreementStatus::find()->select(['ast_id', 'ast_name'])->orderBy('ast_id')->all(), 'ast_id', 'ast_name'),
                    'dataProvider' => $dataProvider,
                ]);
            }
        } else {
            return $this->render('update_agreement_annex', [
                'model' => $model,
                'entity' => ArrayHelper::map(Entity::find()->where(['ent_id' => $entity->ent_id])->all(), 'ent_id', 'ent_name_short'),
                'agreement' => ArrayHelper::map(Agreement::find()->where(['agr_ab_id' => $model->agra_ab_id])->all(), 'agr_id', 'agr_number'),
                'agreement_status' => ArrayHelper::map(AgreementStatus::find()->orderBy('ast_id')->all(), 'ast_id', 'ast_name'),
                'dataProvider' => $dataProvider,
            ]);
        }

    }


    /**
     * Delete an existing AgreementAnnex model.
     * If delete is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws
     */
    public function actionDeleteAgreementAnnex($id)
    {
        $model = AgreementAnnex::findOne($id);
        $entity = $this->findModel($model->agra_ab_id);
        $model->delete();
        return $this->redirect(['view', 'id' => $entity->ent_id]);
    }

    /**
     * Redirect to download page.
     * @param integer $id
     * @return mixed
     */
    public function actionDownloadAgreementAnnex($id)
    {
        return $this->redirect(['download-agreement-annex-f', 'id' => $id]);
    }

    /**
     * Download file from existing AgreementAnnex model.
     * @param integer $id
     * @return mixed
     * @throws
     */
    public function actionDownloadAgreementAnnexF($id)
    {
        $model = AgreementAnnex::findOne($id);
        return Yii::$app->response->sendContentAsFile($model->agra_data, $model->agra_fdata);
    }

    /**
     * Redirect to download page.
     * @param integer $id
     * @return mixed
     */
    public function actionDownloadAgreementAnnexSign($id)
    {
        return $this->redirect(['download-agreement-annex-sign-f', 'id' => $id]);
    }

    /**
     * Download file from existing AgreementAnnex model.
     * @param integer $id
     * @return mixed
     * @throws
     */
    public function actionDownloadAgreementAnnexSignF($id)
    {
        $model = AgreementAnnex::findOne($id);
        return Yii::$app->response->sendContentAsFile($model->agra_data_sign, $model->agra_fdata_sign);
    }


    /***********************************************************************************
     *
     *                                  AgreementAcc
     *
     ***********************************************************************************/


    /**
     * Creates a new AgreementAcc model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @param integer $ent_id
     * @return mixed
     * @throws
     */
    public function actionCreateAgreementAcc($ent_id)
    {
        $entity = $this->findModel($ent_id);

        $dynModel = DynamicModel::validateData(
            ['ent_id', 'date', 'agr_id', 'agra_id', 'pat_id', 'acc_id', 'add_id', 'prs_id', 'comp_id', 'sum', 'tax'],
            [
                [['ent_id', 'agr_id', 'agra_id',  'pat_id',  'comp_id', 'acc_id', 'add_id', 'prs_id'], 'integer'],
                [['date'], 'string'],
                [['sum', 'tax'], 'number']
            ]
        );

        $dynModel->ent_id = $ent_id;

        $model = new AgreementAcc();

        if ($dynModel->load(Yii::$app->request->post())) {

            $model->aga_ab_id   = $dynModel->ent_id;
            $model->aga_date    = $dynModel->date;
            $model->aga_agr_id  = $dynModel->agr_id;
            $model->aga_agra_id = $dynModel->agra_id;
            $model->aga_pat_id  = $dynModel->pat_id;
            $model->aga_acc_id  = $dynModel->acc_id;
            $model->aga_add_id  = $dynModel->add_id;
            $model->aga_prs_id  = $dynModel->prs_id;
            $model->aga_comp_id = $dynModel->comp_id;
            $model->aga_sum     = $dynModel->sum;
            $model->aga_tax     = $dynModel->tax;

            $model->aga_ast_id  =  AgreementStatus::find()->orderBy('ast_id')->one()->ast_id;

            if (isset($model->agaAgr)) {
                $date_id = AgreementAcc::find()->where(['aga_agr_id' => $model->aga_agr_id])->count('aga_date') + 1;
                $number = $model->agaAgr->agr_number.'/'.$date_id;

                while (AgreementAcc::find()->where(['aga_agr_id' => $model->aga_agr_id, 'aga_number' => $number])->exists()) {
                    $date_id ++;
                    $number = $model->agaAgr->agr_number.'/'.$date_id;
                }

                $model->aga_number = $model->agaAgr->agr_number.'/'.$date_id;
            }

            /*
            elseif (isset($model->agaAgra)) {
                $date_id = AgreementAcc::find()->where(['aga_agra_id' => $model->aga_agra_id])->count('aga_date') + 1;
                $model->aga_number = $model->agaAgra->agra_number.'/'.$date_id;
            } else {
                $date_id = AgreementAcc::find()->where(['aga_date' => $model->aga_date])->count('aga_date') + 1;
                $model->aga_number = date('Ymd').'/'.$date_id;
            }
            */

            $pattern = Pattern::findOne($model->aga_pat_id);
            if ($pattern == null) {
                throw new HttpException(500, yii::t('app', '$pattern = Pattern::find() not found'));
            }

            $tmpl_name = Yii::getAlias('@app') . '/storage/' . $pattern->pat_fname;

            $file_name = Yii::getAlias('@app') . '/storage/' . str_replace('/', '-', $model->aga_number).' '.$pattern->pat_fname;;
            file_put_contents($tmpl_name, $pattern->pat_fdata);

            $document = new \PhpOffice\PhpWord\TemplateProcessor($tmpl_name);

            $grid = Yii::$app->session['grid_agreement_acc_a'];
            $vars = $document->getVariables();
            if (array_search('num', $vars) !== false) {
                $document->cloneRow('num', count($grid));
            }

            $model->aga_fdata = str_replace('/', '-', $model->aga_number).' '.$pattern->pat_fname;
            //$model->aga_data = $str;


            $tag_val = $model->aga_number;
            $document->setValue('DOC_NUMBER', $tag_val);

            $tag_val = \DateTime::createFromFormat('Y-m-d', $model->aga_date)->format('d-m-Y');
            $document->setValue('DOC_DATE', $tag_val);

            if (!is_null($model->agaAgr)) {
                $tag_val = $model->agaAgr->agr_number;
                $document->setValue('AGR_NUMBER', $tag_val);
            }

            if (!is_null($model->agaAgr)) {
                $tag_val = \DateTime::createFromFormat('Y-m-d', $model->agaAgr->agr_date)->format('d-m-Y');
                $document->setValue('AGR_DATE', $tag_val);
            }
            if ($obj = Entity::findOne($dynModel->ent_id)) {
                $tag_val = $obj->entEntt->entt_name;
                $document->setValue('ORGANISATION_TYPE_FULL', $tag_val);
            }
            if ($obj = Entity::findOne($dynModel->ent_id)) {
                $tag_val = $obj->ent_name;
                $document->setValue('ORGANISATION_NAME_FULL', $tag_val);
            }
            if ($obj = Address::find()->where(['add_id'=>$model->aga_add_id])->one()) {
                $tag_val =
                    (!empty($obj->add_index) ? $obj->add_index.', ' : '') .
                    (!empty($obj->addCou) ? $obj->addCou->cou_name.', ' : '') .
                    (!empty($obj->addReg)? $obj->addReg->reg_name.', ' : '') .
                    (!empty($obj->addCity) ? $obj->addCity->city_name.', ' : '') .
                    (!empty($obj->add_data) ? $obj->add_data : '');
                $document->setValue('ORGANISATION_U_ADDRESS', $tag_val);
            }

            if ($obj = Entity::findOne($dynModel->ent_id)) {
                $tag_val = $obj->ent_inn;
                $document->setValue('ORGANISATION_INN', $tag_val);
            }

            if ($obj = Entity::findOne($dynModel->ent_id)) {
                $tag_val = $obj->ent_kpp;
                $document->setValue('ORGANISATION_KPP', $tag_val);
            }
            if ($obj = Account::findOne(['acc_id' => $model->aga_acc_id])) {
                $tag_val = $obj->acc_number;
                $document->setValue('ORGANISATION_R_SCHET', $tag_val);
            }
            if ($acc = Account::findOne(['acc_id' => $model->aga_acc_id])) {
                if ($obj = Bank::findOne($acc->acc_bank_id)) {
                    $tag_val = $obj->bank_name;
                    $document->setValue('ORGANISATION_BANK_NAME', $tag_val);
                }
            }
            if ($acc = Account::findOne(['acc_id' => $model->aga_acc_id])) {
                if ($obj = Bank::findOne($acc->acc_bank_id)) {
                    $tag_val = $obj->bank_account;
                }
            } else {
                $tag_val = '';
            }
            $document->setValue('ORGANISATION_BANK_K_SCHET', $tag_val);

            if ($acc = Account::findOne(['acc_id' => $model->aga_acc_id])) {
                if ($obj = Bank::findOne($acc->acc_bank_id)) {
                    $tag_val = $obj->bank_bic;
                }
            } else {
                $tag_val = '';
            }
            $document->setValue('ORGANISATION_BANK_BIK', $tag_val);

            if ($obj = Person::findOne($model->aga_prs_id)) {
                $tag_val = $obj->prs_full_name;
                $document->setValue('ORGANISATION_FIO', $tag_val);
            }

            if ($obj = Staff::find()->where(['stf_ent_id' => $dynModel->ent_id, 'stf_prs_id' => $model->aga_prs_id])->one()) {
                $tag_val = $obj->stf_position;
                $document->setValue('ORGANISATION_DOLGNOST', $tag_val);
            }

            $tag_val = number_format($model->aga_sum, 2);
            $document->setValue('DOC_SUM', $tag_val);

            $tag_val = Tools::num2str($model->aga_sum);
            $document->setValue('DOC_SUM_TEXT', $tag_val);

            $tag_val = number_format($model->aga_tax, 2);
            $document->setValue('DOC_TAX', $tag_val);

            $tag_val = Tools::num2str($model->aga_tax);
            $document->setValue('DOC_TAX_TEXT', $tag_val);


            $grid = Yii::$app->session['grid_agreement_acc_a'];

            $qty = 0;
            for ($i=1; $i <= count($grid); $i++) {
                $tax = $grid[$i-1]['tax']*$grid[$i-1]['qty'];
                $sum = $grid[$i-1]['price']*$grid[$i-1]['qty'];

                //$qty += $grid[$i-1]['qty'];
                $qty = $i;

                $document->setValue('num#'.$i, $i);
                $document->setValue('SVC#'.$i, $grid[$i-1]['ana_name']);

                $document->setValue('PRICE#'.$i, number_format($grid[$i-1]['price'], 2));
                $document->setValue('QTY#'.$i,  $grid[$i-1]['qty']);
                $document->setValue('SUM_T#'.$i, $tax==0 ? Yii::t('app', 'Without Tax') : number_format($tax, 2));
                $document->setValue('SUM#'.$i, number_format($sum, 2));
            }

            $document->setValue('DOC_QTY', $qty);

            $document->saveAs($file_name);

            $model->aga_data = file_get_contents($file_name);

            unlink($file_name);
            unlink($tmpl_name);

            if ($model->save()) {

                $grid = Yii::$app->session['grid_agreement_acc_a'];
                foreach ($grid as $key => $value) {
                    $ana = new AgreementAccA();
                    $ana->aca_aga_id = $model->aga_id;
                    $ana->aca_ana_id = $value['ana_id'];
                    $ana->aca_price = $value['price'];
                    $ana->aca_qty = $value['qty'];
                    $ana->aca_tax = $value['tax'];
                    $ana->save();
                }

                // TODO PaymentTo redirect

                return $this->redirect(['view', 'id' => $ent_id]);


            } else {

                $address = [];
                foreach (Address::find()->where(['add_ab_id'=>$dynModel->ent_id])->orderBy('add_id')->all() as $obj) {
                    $tag_val =
                        (!empty($obj->add_index) ? $obj->add_index.', ' : '') .
                        (!empty($obj->addCou) ? $obj->addCou->cou_name.', ' : '') .
                        (!empty($obj->addReg)? $obj->addReg->reg_name.', ' : '') .
                        (!empty($obj->addCity) ? $obj->addCity->city_name.', ' : '') .
                        (!empty($obj->add_data) ? $obj->add_data : '');
                    $address[$obj->add_id] = $tag_val;
                }

                $account = [];
                foreach (Account::find()->where(['acc_ab_id' => $ent_id])->all() as $acc) {
                    if ($obj = Bank::findOne($acc->acc_bank_id)) {
                        $account[$acc->acc_id] = $obj->bank_name.' '.$acc->acc_number;
                    }
                }

                return $this->render('create_agreement_acc', [
                    'model' => $dynModel,
                    'modelA' => $model,
                    'entity' => ArrayHelper::map(Entity::find()->select(['ent_id', 'ent_name_short'])->where(['ent_id' => $entity->ent_id])->all(), 'ent_id', 'ent_name_short'),
                    'company' => ArrayHelper::map(Company::find()->select(['comp_id', 'comp_name'])->where([])->all(), 'comp_id', 'comp_name'),
                    'agreement' => ['0' => '<>'] + ArrayHelper::map(Agreement::find()->select(['agr_id', 'agr_number'])->where(['agr_ab_id' => $entity->ent_id])->all(), 'agr_id', 'agr_number'),
                    //'agreement_annex' => ['0' => '<>'] + ArrayHelper::map(AgreementAnnex::find()->select(['agra_id', 'agra_number'])->where(['agra_ab_id' => $entity->ent_id])->all(), 'agra_id', 'agra_number'),
                    'annex_a' => ['0' => '<>'] + ArrayHelper::map(AgreementAnnex::find()->select(['agra_id', 'agra_number'])->where(['agra_ab_id' => $entity->ent_id])->all(), 'agra_id', 'agra_number'),
                    'pattern' => ArrayHelper::map(Pattern::find()->select(['pat_id', 'pat_name'])->where(['pat_patt_id'=>4])->all(), 'pat_id', 'pat_name'),
                    'dataProvider' => new ArrayDataProvider([
                        'allModels' => Yii::$app->session['grid_agreement_acc_a'],
                        'pagination' => [
                            'pageSize' => 0,
                        ],

                    ]),
                    'address' => $address,
                    'account' => $account,
                    'staff' => ArrayHelper::map(Person::find()->select(['prs_id', 'prs_full_name'])->innerJoin(Staff::tableName(), 'prs_id=stf_prs_id')->where(['stf_ent_id' => $entity->ent_id])->all(), 'prs_id', 'prs_full_name'),
                ]);
            }
        } else {
            Yii::$app->session['grid_agreement_acc_a'] = [];

            $dynModel->date = date('Y-m-d');

            $addr = Address::find()->where(['add_ab_id'=>$entity->ent_id, 'add_active' => 1])->one();
            $dynModel->add_id = $addr->add_id ?? null;
            $acc = Account::find()->where(['acc_ab_id' => $ent_id, 'acc_active' => 1])->one();
            $dynModel->acc_id = $acc->acc_id ?? null;
            $prs = Person::find()->innerJoin(Staff::tableName(), 'prs_id=stf_prs_id')->where(['stf_ent_id' => $entity->ent_id, 'stf_active' => 1])->one();
            $dynModel->prs_id = $prs->prs_id ?? null;

            $address = [];
            foreach (Address::find()->where(['add_ab_id'=>$dynModel->ent_id])->orderBy('add_id')->all() as $obj) {
                $tag_val =
                    (!empty($obj->add_index) ? $obj->add_index.', ' : '') .
                    (!empty($obj->addCou) ? $obj->addCou->cou_name.', ' : '') .
                    (!empty($obj->addReg)? $obj->addReg->reg_name.', ' : '') .
                    (!empty($obj->addCity) ? $obj->addCity->city_name.', ' : '') .
                    (!empty($obj->add_data) ? $obj->add_data : '');
                $address[$obj->add_id] = $tag_val;
            }

            $account = [];
            foreach (Account::find()->where(['acc_ab_id' => $ent_id])->all() as $acc) {
                if ($obj = Bank::findOne($acc->acc_bank_id)) {
                    $account[$acc->acc_id] = $obj->bank_name.' '.$acc->acc_number;
                }
            }

            return $this->render('create_agreement_acc', [
                'model' => $dynModel,
                'modelA' => $model,
                'company' => ArrayHelper::map(Company::find()->select(['comp_id', 'comp_name'])->all(), 'comp_id', 'comp_name'),
                'entity' => ArrayHelper::map(Entity::find()->select(['ent_id', 'ent_name_short'])->where(['ent_id' => $entity->ent_id])->all(), 'ent_id', 'ent_name_short'),
                'agreement' => ['0' => '<>'] + ArrayHelper::map(Agreement::find()->select(['agr_id', 'agr_number'])->where(['agr_ab_id' => $entity->ent_id])->all(), 'agr_id', 'agr_number'),
                'agreement_annex' => ['0' => '<>'] + ArrayHelper::map(AgreementAnnex::find()->select(['agra_id', 'agra_number'])->where(['agra_ab_id' => $entity->ent_id])->all(), 'agra_id', 'agra_number'),
                'pattern' => ArrayHelper::map(Pattern::find()->select(['pat_id', 'pat_name'])->where(['pat_patt_id'=>4])->all(), 'pat_id', 'pat_name'),
                'annex_a' => [0 => '<>'],
                'dataProvider' => new ArrayDataProvider([
                    'allModels' => Yii::$app->session['grid_agreement_acc_a'],
                    'pagination' => [
                        'pageSize' => 0,
                    ],

                ]),
                'address' => $address,
                'account' => $account,
                'staff' => ArrayHelper::map(Person::find()->select(['prs_id', 'prs_full_name'])->innerJoin(Staff::tableName(), 'prs_id=stf_prs_id')->where(['stf_ent_id' => $entity->ent_id])->all(), 'prs_id', 'prs_full_name'),
            ]);
        }
    }

    /**
     * Add row and return grid.
     * @param integer $ana_id
     * @param string $price
     * @param integer $qty
     * @return mixed
     */
    public function actionAjaxAgreementAccACreate($ana_id, $price, $qty) {
        $ana = AgreementAnnexA::findOne($ana_id);

        $grid = Yii::$app->session['grid_agreement_acc_a'];
        $sum = floatval($price) * intval($qty);
        foreach ($grid as $key => $value) {
            if ($value['ana_id'] == $ana_id) {
                $sum += $value['price'] * $value['qty'];
            }
        }

        if (($ana->ana_price*$ana->ana_qty) >= $sum)
        $grid[] = [
            'ana_id' => $ana_id,
            'ana_name' => (!is_null($ana->anaSvc) ? $ana->anaSvc->svc_name : '' ).' / '. (!is_null($ana->anaTrp) ? $ana->anaTrp->trp_name : ''),
            'price' => $price,
            'tax' =>  ($price / $ana->ana_price ) * $ana->ana_tax,
            'qty' => $qty,
        ];
        Yii::$app->session['grid_agreement_acc_a'] = $grid;

        $provider = new ArrayDataProvider([
            'allModels' => $grid,
            'pagination' => [
                'pageSize' => 0,
            ],

        ]);
        return $this->renderAjax('_ajax_agreement_acc_a_grid', [
                'dataProvider' => $provider,
            ]
        );
    }

    /**
     * Delete row and return grid.
     * @param integer $id
     * @return mixed
     */
    public function actionAjaxAgreementAccADelete($id) {
        $grid = Yii::$app->session['grid_agreement_acc_a'];
        $key = -1;
        foreach ($grid as $k => $value) {
            if ($value['ana_id'] == $id) {
                $key = $k;
                break;
            }
        }
        if ($key >= 0)
            unset($grid[$key]);

        Yii::$app->session['grid_agreement_acc_a'] = $grid;

        $provider = new ArrayDataProvider([
            'allModels' => $grid,
            'pagination' => [
                'pageSize' => 0,
            ],

        ]);

        return $this->renderAjax('_ajax_agreement_acc_a_grid', [
                'dataProvider' => $provider,
            ]
        );
    }


    /**
     * return AgreementAccA Sum.
     * @return mixed
     */
    public function actionAjaxAgreementAccASum() {
        $grid = Yii::$app->session['grid_agreement_acc_a'];
        $sum = 0.0;
        foreach ($grid as $k => $value) {
            $sum  += floatval($value['price']) * floatval($value['qty']);
        }

        return $this->renderAjax('_ajax_agreement_a_float', [
                'value' => $sum,
            ]
        );
    }

    /**
     * return AgreementAccA Tax.
     * @return mixed
     */
    public function actionAjaxAgreementAccATax() {
        $grid = Yii::$app->session['grid_agreement_acc_a'];
        $sum = 0.0;
        foreach ($grid as $k => $value) {
            $sum  += floatval($value['tax']) * floatval($value['qty']);
        }
        return $this->renderAjax('_ajax_agreement_a_float', [
                'value' => $sum,
            ]
        );
    }


    /**
     * Return list AgreementAnnexA array.
     * @param integer $agr_id
     * @return mixed
     */
    public function actionAjaxAgreementAccA($agr_id) {
        $data = [];

        foreach (AgreementAnnexA::find()
                     ->innerJoin(AgreementAnnex::tableName(), 'ana_agra_id = agra_id')
                     ->leftJoin(AgreementAccA::tableName(), 'ana_id = aca_ana_id')
                     ->where(['agra_agr_id' => $agr_id])
                     ->andWhere(['or', 'aca_ana_id IS NULL', 'ana_price * ana_qty > aca_price * aca_qty'])
                     ->all() as $key => $value) {

            $data[$value->ana_id] =
                (!is_null($value->anaAgra) ? $value->anaAgra->agra_number : '!!!')
                .' / '.
                (!is_null($value->anaSvc)  ? $value->anaSvc->svc_name : '!!!')
                .' / '.
                (!is_null($value->anaTrp)  ? $value->anaTrp->trp_name : '!!!')
            ;
        }

        return $this->renderAjax('_ajax_agreement_acc_a', [
                'annex_a' => [0 => '<>']+$data,
            ]
        );

    }

    /**
     * Return PRICE value.
     * @param integer $ana_id
     * @return mixed
     */
    public function actionAjaxAgreementAccAPrice($ana_id) {
        $data = AgreementAnnexA::find()->where(['ana_id' => $ana_id])->one();

        return $this->renderAjax('_ajax_agreement_a_float', [
                'value' => $data->ana_price,
            ]
        );
    }
    /**
     * Return QTY value.
     * @param integer $ana_id
     * @return mixed
     */
    public function actionAjaxAgreementAccAQty($ana_id) {
        $data = AgreementAnnexA::find()->where(['ana_id' => $ana_id])->one();

        return $this->renderAjax('_ajax_agreement_a_float', [
                'value' => $data->ana_qty,
            ]
        );

    }


    /**
     * Updates an existing AgreementAcc model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $ent_id
     * @param integer $id
     * @return mixed
     * @throws
     */
    public function actionUpdateAgreementAcc($ent_id, $id)
    {
        $this->getView()->registerJsFile('/js/UpdateAgreementAccController.js',  ['position' => yii\web\View::POS_END]);

        $entity = $this->findModel($ent_id);

        $model = AgreementAcc::findOne($id);

        $dataProvider = (new AgreementAccASearch())->search(['AgreementAccASearch' => ['aca_aga_id' => $id]]);

        if (Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->post());

            if (($file = UploadedFile::getInstance($model, 'aga_data')) != null) {
                $model->aga_fdata = $file->name;
                $model->aga_data = file_get_contents($file->tempName);
            } else {
                $model->aga_fdata = $model->oldAttributes['aga_fdata'];
                $model->aga_data = $model->oldAttributes['aga_data'];
            }

            if (($file = UploadedFile::getInstance($model, 'aga_data_sign')) != null) {
                $model->aga_fdata_sign = $file->name;
                $model->aga_data_sign = file_get_contents($file->tempName);
            } else {
                $model->aga_fdata_sign = $model->oldAttributes['aga_fdata_sign'];
                $model->aga_data_sign = $model->oldAttributes['aga_data_sign'];
            }

            if ($model->save()) {
                return $this->redirect(['view', 'id' => $entity->ent_id]);
            } else {
                return $this->render('update_agreement_acc', [
                    'model' => $model,
                    'company' => ArrayHelper::map(Company::find()->select(['comp_id', 'comp_name'])->all(), 'comp_id', 'comp_name'),
                    'entity' => ArrayHelper::map(Entity::find()->select(['ent_id', 'ent_name_short'])->where(['ent_id' => $entity->ent_id])->all(), 'ent_id', 'ent_name_short'),
                    'agreement' => ArrayHelper::map(Agreement::find()->select(['agr_id', 'agr_number'])->where(['agr_ab_id' => $entity->ent_id])->all(), 'agr_id', 'agr_number'),
                    'agreement_annex' => ['0' => '<>'] + ArrayHelper::map(AgreementAnnex::find()->select(['agra_id', 'agra_number'])->where(['agra_ab_id' => $entity->ent_id])->all(), 'agra_id', 'agra_number'),
                    'agreement_status' => ArrayHelper::map(AgreementStatus::find()->select(['ast_id', 'ast_name'])->orderBy('ast_id')->all(), 'ast_id', 'ast_name'),
                    'dataProvider' => $dataProvider,
                ]);
            }
        } else {
            return $this->render('update_agreement_acc', [
                'model' => $model,
                'company' => ArrayHelper::map(Company::find()->select(['comp_id', 'comp_name'])->all(), 'comp_id', 'comp_name'),
                'entity' => ArrayHelper::map(Entity::find()->select(['ent_id', 'ent_name_short'])->where(['ent_id' => $entity->ent_id])->all(), 'ent_id', 'ent_name_short'),
                'agreement' => ArrayHelper::map(Agreement::find()->select(['agr_id', 'agr_number'])->where(['agr_ab_id' => $entity->ent_id])->all(), 'agr_id', 'agr_number'),
                'agreement_annex' => ['0' => '<>'] + ArrayHelper::map(AgreementAnnex::find()->select(['agra_id', 'agra_number'])->where(['agra_ab_id' => $entity->ent_id])->all(), 'agra_id', 'agra_number'),
                'agreement_status' => ArrayHelper::map(AgreementStatus::find()->select(['ast_id', 'ast_name'])->orderBy('ast_id')->all(), 'ast_id', 'ast_name'),
                'dataProvider' => $dataProvider,
            ]);
        }

    }


    /**
     * Delete an existing AgreementAcc model.
     * If delete is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws
     */
    public function actionDeleteAgreementAcc($id)
    {
        $model = AgreementAcc::findOne($id);
        $entity = $this->findModel($model->aga_ab_id);
        $model->delete();
        return $this->redirect(['view', 'id' => $entity->ent_id]);
    }

    /**
     * Redirect to download page.
     * @param integer $id
     * @return mixed
     */
    public function actionDownloadAgreementAcc($id)
    {
        return $this->redirect(['download-agreement-acc-f', 'id' => $id]);
    }

    /**
     * Download file from existing AgreementAcc model.
     * @param integer $id
     * @return mixed
     * @throws
     */
    public function actionDownloadAgreementAccF($id)
    {
        $model = AgreementAcc::findOne($id);
        return Yii::$app->response->sendContentAsFile($model->aga_data, $model->aga_fdata);
    }

    /**
     * Redirect to download page.
     * @param integer $id
     * @return mixed
     */
    public function actionDownloadAgreementAccSign($id)
    {
        return $this->redirect(['download-agreement-acc-sign-f', 'id' => $id]);
    }

    /**
     * Download file from existing AgreementAcc model.
     * @param integer $id
     * @return mixed
     * @throws
     */
    public function actionDownloadAgreementAccSignF($id)
    {
        $model = AgreementAcc::findOne($id);
        return Yii::$app->response->sendContentAsFile($model->aga_data_sign, $model->aga_fdata_sign);
    }

    /***********************************************************************************
     *
     *                                  AgreementAct
     *
     ***********************************************************************************/


    /**
     * Creates a new AgreementAct model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @param integer $ent_id
     * @return mixed
     * @throws
     */
    public function actionCreateAgreementAct($ent_id)
    {
        $entity = $this->findModel($ent_id);

        $model = new AgreementAct();

        $dynModel = DynamicModel::validateData(
            ['ent_id', 'date', 'acc_id', 'add_id', 'prs_id', 'comp_id', 'agr_id', 'agra_id', 'pat_id', 'sum', 'tax'],
            [
                [['ent_id', 'agr_id', 'acc_id', 'add_id', 'prs_id', 'agra_id', 'comp_id', 'pat_id'], 'integer'],
                [['date'], 'string'],
                [['sum', 'tax'], 'number']
            ]
        );

        $dynModel->ent_id = $ent_id;

        if ($dynModel->load(Yii::$app->request->post())) {

            $model->act_ab_id   = $dynModel->ent_id;
            $model->act_date    = $dynModel->date;
            $model->act_agr_id  = $dynModel->agr_id;
            $model->act_agra_id = $dynModel->agra_id;
            $model->act_acc_id  = $dynModel->acc_id;
            $model->act_add_id  = $dynModel->add_id;
            $model->act_prs_id  = $dynModel->prs_id;
            $model->act_pat_id  = $dynModel->pat_id;
            $model->act_comp_id = $dynModel->comp_id;
            $model->act_sum     = $dynModel->sum;
            $model->act_tax     = $dynModel->tax;

            $model->act_ast_id  =  AgreementStatus::find()->orderBy('ast_id')->one()->ast_id;

            $pattern = Pattern::findOne($model->act_pat_id);

            /*
             * берем номер договора с годом и добавляем порядковый номер акта в рамках договора
             * т.е. для каждого договора свой счетчик актов
             */
            if (!is_null($model->actAgr)) {
                $date_id = AgreementAct::find()->where(['act_agr_id' => $model->act_agr_id])->count('act_agr_id') + 1;
                $number = $model->actAgr->agr_number . str_pad((string) $date_id, 2, '0', STR_PAD_LEFT);

                while (AgreementAct::find()->where(['act_agr_id' => $model->act_agr_id, 'act_number' => $number])->exists()) {
                    $date_id ++;
                    $number = $model->actAgr->agr_number . str_pad((string) $date_id, 2, '0', STR_PAD_LEFT);
                }
                $model->act_number = $number;
            }

            $grid = Yii::$app->session['grid_agreement_act_a'];


            // ------------- NEW -------------
            $tmpl_name = Yii::getAlias('@app') . '/storage/' . $pattern->pat_fname;
            $file_name = Yii::getAlias('@app') . '/storage/' . $model->act_date . '_' . $pattern->pat_fname;
            file_put_contents($tmpl_name, $pattern->pat_fdata);

            $document = new \PhpOffice\PhpWord\TemplateProcessor($tmpl_name);
            $vars = $document->getVariables();
            if (array_search('num', $vars) !== false) {
                $document->cloneRow('num', count($grid));
            }

            $tag_val = $model->act_number;
            $document->setValue('DOC_NUMBER', $tag_val);

            $tag_val = \DateTime::createFromFormat('Y-m-d', $model->act_date)->format('d-m-Y');
            $document->setValue('DOC_DATE', $tag_val);

            $dateVal = \DateTime::createFromFormat('Y-m-d', $model->act_date)->getTimestamp();
            $document->setValue('DOC_DATE_DD', strftime('%d', $dateVal));
            $document->setValue('DOC_DATE_MONTH', Constant::MONTHS[strftime('%B', $dateVal)]);
            $document->setValue('DOC_DATE_YYYY', strftime('%Y', $dateVal));


            $tag_val = '';
            if (!is_null($model->actAgr)) {
                $tag_val = $model->actAgr->agr_number;
            }
            $document->setValue('AGR_NUMBER', $tag_val);

            $tag_val = '';
            if (!is_null($model->actAgr)) {
                $tag_val = \DateTime::createFromFormat('Y-m-d', $model->actAgr->agr_date)->format('d-m-Y');
            }
            $document->setValue('AGR_DATE', $tag_val);

            $dateVal = \DateTime::createFromFormat('Y-m-d', $model->actAgr->agr_date)->getTimestamp();
            $document->setValue('AGR_DATE_DD', strftime('%d', $dateVal));
            $document->setValue('AGR_DATE_MONTH', Constant::MONTHS[strftime('%B', $dateVal)]);
            $document->setValue('AGR_DATE_YYYY', strftime('%Y', $dateVal));

            $tag_val = '';
            if ($obj = Entity::findOne($model->act_ab_id)) {
                $tag_val = $obj->entEntt->entt_name;
            }
            $document->setValue('ORGANISATION_TYPE_FULL', $tag_val);


            $tag_val = '';
            if ($obj = Entity::findOne($model->act_ab_id)) {
                $tag_val = $obj->ent_name;
            }
            $document->setValue('ORGANISATION_NAME_FULL', $tag_val);


            $tag_val = '';
            if ($obj = Address::find()->where(['add_id'=>$model->act_add_id])->one()) {
                $tag_val =
                    (!empty($obj->add_index) ? $obj->add_index.', ' : '') .
                    (!empty($obj->addCou) ? $obj->addCou->cou_name.', ' : '') .
                    (!empty($obj->addReg)? $obj->addReg->reg_name.', ' : '') .
                    (!empty($obj->addCity) ? $obj->addCity->city_name.', ' : '') .
                    (!empty($obj->add_data) ? $obj->add_data : '');
            }
            $document->setValue('ORGANISATION_U_ADDRESS', $tag_val);

            $tag_val = '';
            if ($obj = Entity::findOne($model->act_ab_id)) {
                $tag_val = $obj->ent_inn;
            }
            $document->setValue('ORGANISATION_INN', $tag_val);

            $tag_val = '';
            if ($obj = Entity::findOne($model->act_ab_id)) {
                $tag_val = $obj->ent_kpp;
            }
            $document->setValue('ORGANISATION_KPP', $tag_val);

            $tag_val = '';
            if ($obj = Account::findOne(['acc_id' => $model->actAgr->agr_acc_id])) {
                $tag_val = $obj->acc_number;
            }
            $document->setValue('ORGANISATION_R_SCHET', $tag_val);


            $tag_val = '';
            if ($acc = Account::findOne(['acc_id' => $model->act_acc_id])) {
                 if ($obj = Bank::findOne($acc->acc_bank_id)) {
                    $tag_val = $obj->bank_name;
                }
            }
            $document->setValue('ORGANISATION_BANK_NAME', $tag_val);

            $tag_val = '';
            if ($acc = Account::findOne(['acc_id' => $model->act_acc_id])) {
                if ($obj = Bank::findOne($acc->acc_bank_id)) {
                    $tag_val = $obj->bank_account;
                }
            }
            $document->setValue('ORGANISATION_BANK_K_SCHET', $tag_val);

            $tag_val = '';
            if ($acc = Account::findOne(['acc_id' => $model->act_acc_id])) {
                if ($obj = Bank::findOne($acc->acc_bank_id)) {
                    $tag_val = $obj->bank_bic;
                }
            }
            $document->setValue('ORGANISATION_BANK_BIK', $tag_val);

            $tag_val = '';
            if ($obj = Person::findOne($model->act_prs_id)) {
                $tag_val = $obj->prs_full_name;
            }
            $document->setValue('ORGANISATION_FIO', $tag_val);


            $tag_val = '';
            if ($obj = Staff::find()->where(['stf_ent_id' => $model->act_ab_id, 'stf_prs_id' => $model->act_prs_id])->one()) {
                $tag_val = $obj->stf_position;
            }
            $document->setValue('ORGANISATION_DOLGNOST', $tag_val);

            $qty = 0;
            foreach ($grid as $k => $value) {
                $qty += $value['qty'];
            }
            $tag_val = number_format($qty);
            $document->setValue('DOC_QTY', $tag_val);


            $tag_val = number_format($model->act_sum, 2);
            $document->setValue('DOC_SUM', $tag_val);

            $tag_val = Tools::num2str($model->act_sum);
            $document->setValue('DOC_SUM_TEXT', $tag_val);

            $tag_val = number_format($model->act_tax, 2);
            $document->setValue('DOC_TAX', $tag_val);

            $tag_val = Tools::num2str($model->act_tax);
            $document->setValue('DOC_TAX_TEXT', $tag_val);

            $i = 1;
            foreach ($grid as $k => $val) {
                $tax = 0;
                $sum = 0;
                if (isset($grid[$i-1]['qty'])) {
                    if (isset($grid[$i-1]['tax'])) {
                        $tax = $grid[$i-1]['tax']*$grid[$i-1]['qty'];
                    }
                    if (isset($grid[$i-1]['price'])) {
                        $sum = $grid[$i-1]['price']*$grid[$i-1]['qty'];
                    }
                }
                $document->setValue('num#' . $i, $i);
                $document->setValue('SVC#' . $i, isset($grid[$i-1]['ana_name']) ? $grid[$i-1]['ana_name'] : '');
                $document->setValue('PRICE#' . $i, number_format($grid[$i-1]['price'], 2));
                $document->setValue('QTY#' . $i, $grid[$i-1]['qty']);
                $document->setValue('SUM_T#' . $i, $tax==0 ? Yii::t('app', 'Without Tax') : number_format($tax, 2) );
                $document->setValue('SUM#' . $i, number_format($sum, 2));
                $i++;
            }
            $document->saveAs($file_name);

            $model->act_fdata = str_replace('/', '-', $model->act_number).' '.$pattern->pat_fname;
            $model->act_data = file_get_contents($file_name);

            unlink($file_name);
            unlink($tmpl_name);

            if ($model->save()) {
                $grid = Yii::$app->session['grid_agreement_act_a'];
                foreach ($grid as $key => $value) {
                    $ana = new AgreementActA();
                    $ana->acta_act_id = $model->act_id;
                    $ana->acta_ana_id = $value['ana_id'];
                    $ana->acta_price = $value['price'];
                    $ana->acta_qty = $value['qty'];
                    $ana->acta_tax = $value['tax'];
                    $ana->save();
                }

                return $this->redirect(['view', 'id' => $ent_id]);
            } else {
                $address = [];
                foreach (Address::find()->where(['add_ab_id'=>$dynModel->ent_id])->orderBy('add_id')->all() as $obj) {
                    $tag_val =
                        (!empty($obj->add_index) ? $obj->add_index.', ' : '') .
                        (!empty($obj->addCou) ? $obj->addCou->cou_name.', ' : '') .
                        (!empty($obj->addReg)? $obj->addReg->reg_name.', ' : '') .
                        (!empty($obj->addCity) ? $obj->addCity->city_name.', ' : '') .
                        (!empty($obj->add_data) ? $obj->add_data : '');
                    $address[$obj->add_id] = $tag_val;
                }

                $account = [];
                foreach (Account::find()->where(['acc_ab_id' => $ent_id])->all() as $acc) {
                    if ($obj = Bank::findOne($acc->acc_bank_id)) {
                        $account[$acc->acc_id] = $obj->bank_name.' '.$acc->acc_number;
                    }
                }

                return $this->render('create_agreement_act', [
                    'model' => $dynModel,
                    'modelA' => $model,
                    'entity' => ArrayHelper::map(Entity::find()->select(['ent_id', 'ent_name_short'])->where(['ent_id' => $entity->ent_id])->all(), 'ent_id', 'ent_name_short'),
                    'company' => ArrayHelper::map(Company::find()->select(['comp_id', 'comp_name'])->where([])->all(), 'comp_id', 'comp_name'),
                    'agreement' => [0 => '<>'] + ArrayHelper::map(Agreement::find()->select(['agr_id', 'agr_number'])->where(['agr_ab_id' => $entity->ent_id])->all(), 'agr_id', 'agr_number'),
                    'agreement_annex' => ['0' => '<>'] + ArrayHelper::map(AgreementAnnex::find()->select(['agra_id', 'agra_number'])->where(['agra_ab_id' => $entity->ent_id])->all(), 'agra_id', 'agra_number'),
                    'pattern' => ArrayHelper::map(Pattern::find()->select(['pat_id', 'pat_name'])->where(['pat_patt_id'=>5])->all(), 'pat_id', 'pat_name'),
                    'dataProvider' => new ArrayDataProvider([
                        'allModels' => Yii::$app->session['grid_agreement_act_a'],
                        'pagination' => [
                            'pageSize' => 0,
                        ],

                    ]),
                    'account' => $account,
                    'address' => $address,
                    'staff' => ArrayHelper::map(Person::find()->select(['prs_id', 'prs_full_name'])->innerJoin(Staff::tableName(), 'prs_id=stf_prs_id')->where(['stf_ent_id' => $entity->ent_id])->all(), 'prs_id', 'prs_full_name'),
                ]);

            }
        } else {
            Yii::$app->session['grid_agreement_act_a'] = [];

            $dynModel->date = date('Y-m-d');

            $addr = Address::find()->where(['add_ab_id'=>$entity->ent_id, 'add_active' => 1])->one();
            $dynModel->add_id = $addr->add_id ?? null;
            $acc = Account::find()->where(['acc_ab_id' => $ent_id, 'acc_active' => 1])->one();
            $dynModel->acc_id = $acc->acc_id ?? null;
            $prs = Person::find()->innerJoin(Staff::tableName(), 'prs_id=stf_prs_id')->where(['stf_ent_id' => $entity->ent_id, 'stf_active' => 1])->one();
            $dynModel->prs_id = $prs->prs_id ?? null;

            $address = [];
            foreach (Address::find()->where(['add_ab_id'=>$dynModel->ent_id])->orderBy('add_id')->all() as $obj) {
                $tag_val =
                    (!empty($obj->add_index) ? $obj->add_index.', ' : '') .
                    (!empty($obj->addCou) ? $obj->addCou->cou_name.', ' : '') .
                    (!empty($obj->addReg)? $obj->addReg->reg_name.', ' : '') .
                    (!empty($obj->addCity) ? $obj->addCity->city_name.', ' : '') .
                    (!empty($obj->add_data) ? $obj->add_data : '');
                $address[$obj->add_id] = $tag_val;
            }

            $account = [];
            foreach (Account::find()->where(['acc_ab_id' => $ent_id])->all() as $acc) {
                if ($obj = Bank::findOne($acc->acc_bank_id)) {
                    $account[$acc->acc_id] = $obj->bank_name.' '.$acc->acc_number;
                }
            }

            return $this->render('create_agreement_act', [
                'model' => $dynModel,
                'modelA' => $model,
                'entity' => ArrayHelper::map(Entity::find()->select(['ent_id', 'ent_name_short'])->where(['ent_id' => $entity->ent_id])->all(), 'ent_id', 'ent_name_short'),
                'company' => ArrayHelper::map(Company::find()->select(['comp_id', 'comp_name'])->where([])->all(), 'comp_id', 'comp_name'),
                'agreement' => [0 => '<>'] + ArrayHelper::map(Agreement::find()->select(['agr_id', 'agr_number'])->where(['agr_ab_id' => $entity->ent_id])->all(), 'agr_id', 'agr_number'),
                'agreement_annex' => ['0' => '<>'] + ArrayHelper::map(AgreementAnnex::find()->select(['agra_id', 'agra_number'])->where(['agra_ab_id' => $entity->ent_id])->all(), 'agra_id', 'agra_number'),
                'pattern' => ArrayHelper::map(Pattern::find()->select(['pat_id', 'pat_name'])->where(['pat_patt_id'=>5])->all(), 'pat_id', 'pat_name'),
                'dataProvider' => new ArrayDataProvider([
                    'allModels' => Yii::$app->session['grid_agreement_act_a'],
                    'pagination' => [
                        'pageSize' => 0,
                    ],

                ]),
                'account' => $account,
                'address' => $address,
                'staff' => ArrayHelper::map(Person::find()->select(['prs_id', 'prs_full_name'])->innerJoin(Staff::tableName(), 'prs_id=stf_prs_id')->where(['stf_ent_id' => $entity->ent_id])->all(), 'prs_id', 'prs_full_name'),
            ]);
        }
    }


    /**
     * Add row and return grid.
     * @param integer $ana_id
     * @param string $price
     * @param integer $qty
     * @return mixed
     */
    public function actionAjaxAgreementActACreate($ana_id, $price, $qty) {
        $ana = AgreementAnnexA::findOne($ana_id);

        $grid = Yii::$app->session['grid_agreement_act_a'];
        $sum = floatval($price) * intval($qty);
        foreach ($grid as $key => $value) {
            if ($value['ana_id'] == $ana_id) {
                $sum += $value['price'] * $value['qty'];
            }
        }

        if (($ana->ana_price*$ana->ana_qty) >= $sum)
            $grid[] = [
                'ana_id' => $ana_id,
                'ana_name' => (isset($ana->anaSvc) ? $ana->anaSvc->svc_name : '') .' / '. (isset($ana->anaTrp) ? $ana->anaTrp->trp_name : ''),
                'price' => $price,
                'tax' =>  ($price / $ana->ana_price ) * $ana->ana_tax,
                'qty' => $qty,
            ];
        Yii::$app->session['grid_agreement_act_a'] = $grid;

        $provider = new ArrayDataProvider([
            'allModels' => $grid,
            'pagination' => [
                'pageSize' => 0,
            ],

        ]);
        return $this->renderAjax('_ajax_agreement_act_a_grid', [
                'dataProvider' => $provider,
            ]
        );
    }

    /**
     * Delete row and return grid.
     * @param integer $id
     * @return mixed
     */
    public function actionAjaxAgreementActADelete($id) {
        $grid = Yii::$app->session['grid_agreement_act_a'];
        $key = -1;
        foreach ($grid as $k => $value) {
            if ($value['ana_id'] == $id) {
                $key = $k;
                break;
            }
        }
        if ($key >= 0)
            unset($grid[$key]);

        Yii::$app->session['grid_agreement_act_a'] = $grid;

        $provider = new ArrayDataProvider([
            'allModels' => $grid,
            'pagination' => [
                'pageSize' => 0,
            ],

        ]);

        return $this->renderAjax('_ajax_agreement_act_a_grid', [
                'dataProvider' => $provider,
            ]
        );
    }


    /**
     * return AgreementAccA Sum.
     * @return mixed
     */
    public function actionAjaxAgreementActASum() {
        $grid = Yii::$app->session['grid_agreement_act_a'];
        $sum = 0.0;
        foreach ($grid as $k => $value) {
            $sum  += floatval($value['price']) * floatval($value['qty']);
        }

        return $this->renderAjax('_ajax_agreement_a_float', [
                'value' => $sum,
            ]
        );
    }

    /**
     * return AgreementAccA Tax.
     * @return mixed
     */
    public function actionAjaxAgreementActATax() {
        $grid = Yii::$app->session['grid_agreement_act_a'];
        $sum = 0.0;
        foreach ($grid as $k => $value) {
            $sum  += floatval($value['tax']) * floatval($value['qty']);
        }
        return $this->renderAjax('_ajax_agreement_a_float', [
                'value' => $sum,
            ]
        );
    }


    /**
     * Return list AgreementAnnexA array.
     * @param integer $agr_id
     * @return mixed
     */
    public function actionAjaxAgreementActA($agr_id) {
        $data = [];

        foreach (AgreementAnnexA::find()
                     ->innerJoin(AgreementAnnex::tableName(), 'ana_agra_id = agra_id')
                     ->leftJoin(AgreementActA::tableName(), 'ana_id = acta_ana_id')
                     ->where(['agra_agr_id' => $agr_id])
                     ->andWhere(['or', 'acta_ana_id IS NULL', 'ana_price * ana_qty > acta_price * acta_qty'])
                     ->all() as $key => $value) {
            
            $acta = AgreementActA::find()->select(['acta_qty', 'acta_price'])->where('acta_ana_id = '.$value->ana_id)->all();

            $sum = 0;
            foreach ($acta as $actaItem) {
                $sum = $sum + $actaItem->acta_qty*$actaItem->acta_price;
            }

            if($sum < ($value->ana_price*$value->ana_qty)){
                $data[$value->ana_id] =
                    ($value->agreementAccAs[0]->acaAga->aga_number ?? 'N/A')
                    //($value->anaAgra->agreementAcc[0]->aga_number ?? 'N/A')
                    .' *** '.
                    ($value->anaAgra->agra_number  ?? ''). ' / ' .
                    ($value->anaSvc->svc_name ?? '') .' / '.
                    ($value->anaTrp->trp_name ?? '');
            }

        }

        return $this->renderAjax('_ajax_agreement_act_a', [
                'annex_a' => [0 => '<>']+$data,
            ]
        );

    }

    /**
     * Return PRICE value.
     * @param integer $ana_id
     * @return mixed
     */
    public function actionAjaxAgreementActAPrice($ana_id) {
        $data = AgreementAnnexA::find()->where(['ana_id' => $ana_id])->one();

        return $this->renderAjax('_ajax_agreement_a_float', [
                'value' => $data->ana_price,
            ]
        );
    }
    /**
     * Return QTY value.
     * @param integer $ana_id
     * @return mixed
     */
    public function actionAjaxAgreementActAQty($ana_id) {
        $data = AgreementAnnexA::find()->where(['ana_id' => $ana_id])->one();

        return $this->renderAjax('_ajax_agreement_a_float', [
                'value' => $data->ana_qty,
            ]
        );

    }


    /**
     * Updates an existing AgreementAct model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $ent_id
     * @param integer $id
     * @return mixed
     * @throws
     */
    public function actionUpdateAgreementAct($ent_id, $id)
    {

        $this->getView()->registerJsFile('/js/UpdateAgreementActController.js',  ['position' => yii\web\View::POS_END]);

        $entity = $this->findModel($ent_id);

        $model = AgreementAct::findOne($id);

        $dataProvider = (new AgreementActASearch())->search(['AgreementActASearch' => ['acta_act_id' => $id]]);

        if (Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->post());

            if (($file = UploadedFile::getInstance($model, 'act_data')) != null) {
                $model->act_fdata = $file->name;
                $model->act_data = file_get_contents($file->tempName);
            } else {
                $model->act_fdata = $model->oldAttributes['act_fdata'];
                $model->act_data = $model->oldAttributes['act_data'];
            }

            if (($file = UploadedFile::getInstance($model, 'act_data_sign')) != null) {
                $model->act_fdata_sign = $file->name;
                $model->act_data_sign = file_get_contents($file->tempName);
            } else {
                $model->act_fdata_sign = $model->oldAttributes['act_fdata_sign'];
                $model->act_data_sign = $model->oldAttributes['act_data_sign'];
            }

            if ($model->save()) {
                return $this->redirect(['view', 'id' => $entity->ent_id]);
            } else {
                return $this->render('update_agreement_act', [
                    'model' => $model,
                    'company' => ArrayHelper::map(Company::find()->select(['comp_id', 'comp_name'])->where([])->all(), 'comp_id', 'comp_name'),
                    'entity' => ArrayHelper::map(Entity::find()->select(['ent_id', 'ent_name_short'])->where(['ent_id' => $entity->ent_id])->all(), 'ent_id', 'ent_name_short'),
                    'agreement' => ArrayHelper::map(Agreement::find()->select(['agr_id', 'agr_number'])->where(['agr_ab_id' => $model->act_ab_id])->all(), 'agr_id', 'agr_number'),
                    'account' => ArrayHelper::map(Account::find()->select(['acc_id', 'acc_number'])->where(['acc_ab_id' => $entity->ent_id])->all(), 'acc_id', 'acc_number'),
                    'agreement_annex' => ArrayHelper::map(AgreementAnnex::find()->select(['agra_id', 'agra_number'])->where(['agra_ab_id' => $model->act_ab_id])->all(), 'agra_id', 'agra_number'),
                    'agreement_status' => ArrayHelper::map(AgreementStatus::find()->select(['ast_id', 'ast_name'])->orderBy('ast_id')->all(), 'ast_id', 'ast_name'),
                    'dataProvider' => $dataProvider,
                ]);
            }
        } else {
            return $this->render('update_agreement_act', [
                'model' => $model,
                'company' => ArrayHelper::map(Company::find()->select(['comp_id', 'comp_name'])->where([])->all(), 'comp_id', 'comp_name'),
                'entity' => ArrayHelper::map(Entity::find()->select(['ent_id', 'ent_name_short'])->where(['ent_id' => $entity->ent_id])->all(), 'ent_id', 'ent_name_short'),
                'agreement' => ArrayHelper::map(Agreement::find()->select(['agr_id', 'agr_number'])->where(['agr_ab_id' => $model->act_ab_id])->all(), 'agr_id', 'agr_number'),
                'account' => ArrayHelper::map(Account::find()->select(['acc_id', 'acc_number'])->where(['acc_ab_id' => $entity->ent_id])->all(), 'acc_id', 'acc_number'),
                'agreement_annex' => ArrayHelper::map(AgreementAnnex::find()->select(['agra_id', 'agra_number'])->where(['agra_ab_id' => $model->act_ab_id])->all(), 'agra_id', 'agra_number'),
                'agreement_status' => ArrayHelper::map(AgreementStatus::find()->select(['ast_id', 'ast_name'])->orderBy('ast_id')->all(), 'ast_id', 'ast_name'),
                'dataProvider' => $dataProvider,
            ]);
        }

    }

    /**
     * Delete an existing AgreementAct model.
     * If delete is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws
     */
    public function actionDeleteAgreementAct($id)
    {
        $model = AgreementAct::findOne($id);
        $entity = $this->findModel($model->act_ab_id);
        $model->delete();
        return $this->redirect(['view', 'id' => $entity->ent_id]);
    }

    /**
     * Redirect to download page.
     * @param integer $id
     * @return mixed
     */
    public function actionDownloadAgreementAct($id)
    {
        return $this->redirect(['download-agreement-act-f', 'id' => $id]);
    }

    /**
     * Download file from existing AgreementAct model.
     * @param integer $id
     * @return mixed
     * @throws
     */
    public function actionDownloadAgreementActF($id)
    {
        $model = AgreementAct::findOne($id);
        return Yii::$app->response->sendContentAsFile($model->act_data, $model->act_fdata);
    }

    /**
     * Redirect to download page.
     * @param integer $id
     * @return mixed
     */
    public function actionDownloadAgreementActSign($id)
    {
        return $this->redirect(['download-agreement-act-sign-f', 'id' => $id]);
    }

    /**
     * Download file from existing AgreementAct model.
     * @param integer $id
     * @return mixed
     * @throws
     */
    public function actionDownloadAgreementActSignF($id)
    {
        $model = AgreementAct::findOne($id);
        return Yii::$app->response->sendContentAsFile($model->act_data_sign, $model->act_fdata_sign);
    }



    /**
     * Lists FinanceBook models.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdatePay($id)
    {
        $model = AgreementAcc::findOne($id);

        $searchModel = new AgreementAccASearch();
        $dataProvider = $searchModel->search(['AgreementAccASearch' => ['aca_aga_id' => $id]]);

        $account = AgreementAcc::findOne($id);
        if (!$account) {
            return $this->redirect(['entity-frm/index']);
        }

        foreach ($line = AgreementAccA::find()->where(['aca_aga_id' => $account->aga_id])->all() as $key => $value) {
            $value->financeBookUpdate(false);
        }

        return $this->render('update_pay', [
            'dataProvider' => $dataProvider,
            'account' => $account,
            'model' => $model,
            'entity' => ArrayHelper::map(Entity::find()->select(['ent_id', 'ent_name_short'])->where(['ent_id' => $model->aga_ab_id])->all(), 'ent_id', 'ent_name_short'),
        ]);
    }

    /**
     * Check sum pay.
     * @param string $value
     * @return mixed
     */
    public function actionPayCheckSum($value)
    {
        $value = floatval($value);
        return $this->renderAjax('_ajax_pay_check_sum', [
            'value' => $value,
        ]);
    }

    /**
     * Calculate sum pay.
     * @param integer $id
     * @param string $value
     * @return mixed
     */
    public function actionPayCalculate($id, $value, $basis)
    {
        $payArray = [];

        $line = AgreementAccA::find()->innerJoin(FinanceBook::tableName(), 'fb_aca_id = aca_id')->where(['aca_aga_id' => $id])->andWhere(['fb_pay' => null])->all();
        $pay = floatval($value);

        foreach ($line as $key => $obj) {
            // Сумма оплаты больше необходимого
            if (($obj->getSum()-$obj->paySum) <= $pay) {
                $payArray[$obj->aca_id] = $obj->getSum()-$obj->paySum;
            }

            // Сумма оплаты меньше необходимого
            if (($obj->getSum()-$obj->paySum) > $pay) {
                $payArray[$obj->aca_id] = $pay;
            }

            $pay -= floatval($payArray[$obj->aca_id]);

            if ($pay <= 0.001)
                break;
        }

        $searchModel = new AgreementAccASearch();
        $dataProvider = $searchModel->search(['AgreementAccASearch' => ['aca_aga_id' => $id]]);

        Yii::$app->session['EntityFrmPay'] = $payArray;

        return $this->renderAjax('_grid_pay', [
            'dataProvider' => $dataProvider,
            'payArray' => Yii::$app->session['EntityFrmPay'],
        ]);


    }


    /**
     * Cancel pay.
     * @param integer $id
     * @return mixed
     * @throws
     */
    public function actionPayCancel($id)
    {
        $fb = FinanceBook::find()->where(['fb_aga_id' => $id])->all();
        foreach ($fb as $key => $obj) {
            $obj->delete();
        }

        $acc = AgreementAcc::find()->where(['aga_id' => $id])->one();
        $acc->aga_ast_id = 1;
        $acc->save(false);

        foreach (AgreementAccA::find()->where(['aca_aga_id' => $id])->all() as $key => $obj) {
            if (!$fb = FinanceBook::find()->where(['fb_aca_id' => $obj->aca_id])->one()) {
                $fb = new FinanceBook();
            }
            $fb->fb_svc_id = $obj->acaAna->ana_svc_id;
            $fb->fb_aca_id = $obj->aca_id;
            $fb->fb_fbt_id = 1;
            $fb->fb_ab_id = $obj->acaAga->aga_ab_id;
            $fb->fb_comp_id = $obj->acaAga->aga_comp_id;
            $fb->fb_sum = $obj->aca_price * $obj->aca_qty;
            $fb->fb_tax = $obj->aca_tax * $obj->aca_qty;
            $fb->fb_aga_id = $obj->aca_aga_id;
            $fb->fb_acc_id = $obj->acaAga->agaAgr->agr_acc_id;
            $fb->fb_fbs_id = 1;
            $fb->fb_date = $obj->acaAga->aga_date;
            $fb->save();
        }

        $searchModel = new AgreementAccASearch();
        $dataProvider = $searchModel->search(['AgreementAccASearch' => ['aca_aga_id' => $id]]);

        Yii::$app->session['EntityFrmPay'] = [];

        return $this->renderAjax('_grid_pay', [
            'dataProvider' => $dataProvider,
            'payArray' => Yii::$app->session['EntityFrmPay'],
        ]);


    }

    /**
     * Update pay Sum.
     * @param integer $id
     * @param string $value
     * @return mixed
     */
    public function actionUpdatePaySum($id, $value)
    {
        $payArray = Yii::$app->session['EntityFrmPay'];
        $payArray[$id] = floatval($value);
        Yii::$app->session['EntityFrmPay'] = $payArray;
        $value = Yii::$app->session['EntityFrmPay'][$id];

        return $this->renderAjax('_ajax_pay_check_sum', [
            'value' => $value,
        ]);
    }


    /**
     * Save sum pay.
     * @param integer $id
     * @param string $basis
     * @param string $date
     * @return mixed
     */
    public function actionSaveCalculate($id, $basis, $date)
    {

        $payArray = Yii::$app->session['EntityFrmPay'];

        foreach ($payArray as $k => $value) {
            $line = FinanceBook::find()->where(['fb_aca_id' => $k])->all();
            $pay = floatval($value);

            foreach ($line as $key => $obj) {
                if (!isset($obj->fb_pay)) {
                    // Сумма оплаты больше необходимого
                    if (floatval($obj->fb_sum) <= $pay) {
                        $obj->fb_pay = $obj->fb_sum;
                        $obj->fb_payment = $basis;
                        $obj->fb_date = $date;
                    }

                    // Сумма оплаты меньше необходимого
                    if (floatval($obj->fb_sum) > $pay) {
                        $obj->fb_pay = $pay;
                        $obj->fb_payment = $basis;
                        $obj->fb_date = $date;
                    }

                    if ((floatval($obj->fb_sum) > floatval($obj->fb_pay))) {

                        $p = $pay;
                        if ($obj->fb_tax > 0.001) {
                            if (strtotime($obj->fb_date) >= strtotime(date('2019-01-01'))) {
                                $p_tax = round($pay/1.2 * 0.2, 2);
                            } else {
                                $p_tax = round($pay/1.18 * 0.18, 2);
                            }
                        } else {
                            $p_tax = 0;
                        }

                        $fb = new FinanceBook();
                        $fb->fb_fb_id = $obj->fb_id;
                        $fb->fb_aca_id = $obj->fb_aca_id;
                        $fb->fb_svc_id = $obj->fb_svc_id;
                        $fb->fb_fbt_id = $obj->fb_fbt_id;
                        $fb->fb_ab_id = $obj->fb_ab_id;
                        $fb->fb_comp_id = $obj->fb_comp_id;
                        $fb->fb_sum = $obj->fb_sum - $p;
                        $fb->fb_tax = $obj->fb_tax - $p_tax;
                        $fb->fb_aga_id = $obj->fb_aga_id;
                        $fb->fb_acc_id = $obj->fb_acc_id;
                        $fb->fb_fbs_id = $obj->fb_fbs_id;

                        $obj->fb_sum = $p;
                        $obj->fb_tax = $p_tax;

                        if ($fb->save()) {
                            $obj->save(false);
                        }
                    }

                    $pay -= floatval($obj->fb_pay);
                }
                $obj->save(false);
                if ($pay <= 0.001)
                    break;
            }
        }

        AgreementAcc::findOne($id)->updateStatus();

        $searchModel = new AgreementAccASearch();
        $dataProvider = $searchModel->search(['AgreementAccASearch' => ['aca_aga_id' => $id]]);

        Yii::$app->session['EntityFrmPay'] = [];

        return $this->renderAjax('_grid_pay', [
            'dataProvider' => $dataProvider,
            'payArray' => Yii::$app->session['EntityFrmPay'],
        ]);

    }

    /**
     * Create EntityClassLnk.
     * @param integer $id
     * @param integer $value
     * @return mixed
     */
    public function actionCreateClassLink($id, $value)
    {
        $model = Entity::findOne($id);

        if (!$classLnk = EntityClassLnk::find()->where(['entcl_ent_id' => $id, 'entcl_entc_id' => $value])->one()) {
            $classLnk = new EntityClassLnk();
        }
        $classLnk->entcl_ent_id = $id;
        $classLnk->entcl_entc_id = $value;
        $classLnk->save();

        return $this->renderAjax('_ajax_class_link', [
            'model' => $model,
            'entityClass' => ArrayHelper::map(EntityClass::find()->select(['entc_id', 'entc_name'])->all(), 'entc_id', 'entc_name'),
        ]);
    }


    /**
     * Save sum pay.
     * @param integer $id
     * @return mixed
     * @throws
     */
    public function actionDeleteClassLink($id)
    {
        $class = EntityClassLnk::findOne($id);
        $model = Entity::findOne($class->entcl_ent_id);

        $class->delete();
        return $this->renderAjax('_ajax_class_link', [
            'model' => $model,
            'entityClass' => ArrayHelper::map(EntityClass::find()->all(), 'entc_id', 'entc_name'),
        ]);
    }


    /**
     * Create PaymentTo from AgreementAcc
     * @param int $id
     * @return mixed
     * @throws
     */
    public function actionCreatePaymentTo ($id) {
        PaymentTo::cacheClear();

        $acc = AgreementAcc::findOne($id);
        $entity = $this->findModel($acc->aga_ab_id);
        $accA = AgreementAccA::find()
            ->leftJoin(AgreementAnnexA::tableName(), 'aca_ana_id = ana_id')
            ->leftJoin(Svc::tableName(), 'ana_svc_id = svc_id')
            ->where(['aca_aga_id' => $id])
            ->andWhere(['svc_payment_flag' => 1])
            ->andWhere(['>', 'svc_ab_id', 0])
            ->all();

        $pmodel = null;

        foreach ($accA as $key => $obj) {
            $svc = Svc::findOne($obj->acaAna->ana_svc_id);

            if (!$pmodel = PaymentTo::find()->where(['payt_aca_id' => $obj->aca_id])->one()) {
                $pmodel = new PaymentTo();

                $pmodel->payt_svc_id = $svc->svc_id;
                $pmodel->payt_svc_price = $svc->svc_cost_ext;
                $pmodel->payt_svc_qty = $obj->aca_qty;

                $pmodel->payt_fbs_id = 1;
                $pmodel->payt_pt_id = 1;
                $pmodel->payt_date_pay = date('Y-m-d');
                $pmodel->payt_to_ab_id = $svc->svc_ab_id;
                $pmodel->payt_to_acc_id = Account::findOne(['acc_ab_id' => $svc->svc_ab_id])->acc_id;
                $pmodel->payt_from_ab_id = $obj->acaAga->aga_comp_id;
                $pmodel->payt_from_acc_id = Account::findOne($obj->acaAga->aga_comp_id)->acc_id;
            }
            $pmodel->payt_pt_id = 1;
            $pmodel->payt_aca_id = $obj->aca_id;
            $pmodel->payt_aga_id = $obj->aca_aga_id;
            if (!$pmodel->save()) {
                sleep(1);
            };
        }

        foreach (PaymentTo::find()->where(['payt_aga_id' => $acc->aga_id])->all() as $key => $obj) {
            PaymentTo::cacheUpdate($obj);
        }

        $searchModel = new PaymentToSearch();
        $dataProvider = $searchModel->search(['PaymentToSearch' => ['payt_aga_id' => $acc->aga_id]]);

        return $this->render('update_payment_to', [
            'pmodel' => $pmodel,
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'model' => $acc,
            'entity' => ArrayHelper::map(Entity::find()->select(['ent_id', 'ent_name_short'])->where(['ent_id' => $entity->ent_id])->all(), 'ent_id', 'ent_name_short'),
        ]);
    }


    /**
     * Pay PaymentTo
     * @param int $id
     * @return mixed
     * @throws
     */
    public function actionPayPaymentTo ($id) {
        PaymentTo::cacheClear();

        $pmodel = PaymentTo::findOne($id);

        $acc = AgreementAcc::findOne($pmodel->payt_aga_id);
        $entity = $this->findModel($acc->aga_ab_id);

        foreach (PaymentTo::find()->where(['payt_aga_id' => $acc->aga_id])->all() as $key => $obj) {
            PaymentTo::cacheUpdate($obj);
        }

        $searchModel = new PaymentToSearch();
        $dataProvider = $searchModel->search(['PaymentToSearch' => ['payt_id' => $id]]);

        return $this->render('update_payment_to_pay', [
            'pmodel' => $pmodel,
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'model' => $acc,
            'entity' => ArrayHelper::map(Entity::find()->select(['ent_id', 'ent_name_short'])->where(['ent_id' => $entity->ent_id])->all(), 'ent_id', 'ent_name_short'),
        ]);
    }


    /**
     * Delete PaymentTo
     * @param int $id
     * @return mixed
     * @throws
     */
    public function actionDeletePaymentTo ($id) {
        $obj =  PaymentTo::findOne($id);
        $id = $obj->paytAga->aga_ab_id;
        $obj->delete();

        return $this->redirect(['view', 'id' => $id]);

    }

    /**
     * Delete PaymentTo
     * @param int $id
     * @return mixed
     * @throws
     */
    public function actionDeletePaymentToCache ($id) {
        $obj =  PaymentTo::findOne($id);
        PaymentTo::cacheDelete($obj);
        $id = $obj->payt_aga_id;
        $obj->delete();

        return $this->redirect(['view-payment-to', 'id' => $id]);

    }

    /**
     * Create PaymentTo from AgreementAcc
     * @param int $id
     * @return mixed
     * @throws
     */
    public function actionViewPaymentTo ($id) {
        $acc = AgreementAcc::findOne($id);
        $entity = $this->findModel($acc->aga_ab_id);

        $searchModel = new PaymentToSearch();
        $dataProvider = $searchModel->search(['PaymentToSearch' => ['payt_aga_id' => $acc->aga_id]]);

        return $this->render('update_payment_to', [
            'pmodel' => null,
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'model' => $acc,
            'entity' => ArrayHelper::map(Entity::find()->select(['ent_id', 'ent_name_short'])->where(['ent_id' => $entity->ent_id])->all(), 'ent_id', 'ent_name_short'),
        ]);
    }

    /**
     * Save PaymentTo objects to database
     * @param int $id
     * @return mixed
     * @throws
     */
    public function actionSavePaymentTo ($id) {
        $acc = AgreementAcc::findOne($id);
        $entity = $this->findModel($acc->aga_ab_id);

        PaymentTo::cacheApply();

        return $this->redirect(['view', 'id' => $entity->ent_id]);
    }

    /**
     * Save PaymentTo Pay objects to database
     * @param int $id
     * @return mixed
     * @throws
     */
    public function actionSavePaymentToPay ($id) {
        $model = PaymentTo::findOne($id);

        $entity = $this->findModel($model->payt_to_ab_id);

        $model->updatePay();

        return $this->redirect(['view', 'id' => $entity->ent_id]);
    }

    /**
     *********************** PaymentRet ***********************
     */

    /**
     * Create PaymentRet from AgreementAcc
     * @param int $id
     * @return mixed
     * @throws
     */
    public function actionCreatePaymentRet ($id) {
        PaymentRet::cacheClear();

        $acc = AgreementAcc::findOne($id);
        $entity = $this->findModel($acc->aga_ab_id);
        $accA = AgreementAccA::find()
            ->leftJoin(AgreementAnnexA::tableName(), 'aca_ana_id = ana_id')
            ->leftJoin(Svc::tableName(), 'ana_svc_id = svc_id')
            ->where(['aca_aga_id' => $id])
            ->all();

        $pmodel = null;

        foreach ($accA as $key => $obj) {
            $svc = Svc::findOne($obj->acaAna->ana_svc_id);

            if (!$pmodel = PaymentRet::find()->where(['ptr_aca_id' => $obj->aca_id])->one()) {
                $pmodel = new PaymentRet();

                $pmodel->ptr_svc_id = $svc->svc_id;
                $pmodel->ptr_svc_sum = $obj->aca_price*$obj->aca_qty;
                $pmodel->ptr_svc_tax = $obj->aca_tax * $obj->aca_qty;

                $pmodel->ptr_fbs_id = 1;
                $pmodel->ptr_pt_id = 1;
                $pmodel->ptr_date_pay = date('Y-m-d');
                $pmodel->ptr_ab_id = $acc->aga_ab_id;
                $pmodel->ptr_acc_id = Account::findOne($acc->aga_ab_id)->acc_id ?? null;
                $pmodel->ptr_comp_id = $obj->acaAga->aga_comp_id;
            }
            $pmodel->ptr_pt_id = 1;
            $pmodel->ptr_aca_id = $obj->aca_id;
            $pmodel->ptr_aga_id = $obj->aca_aga_id;
            if (!$pmodel->save()) {
            };
        }

        foreach (PaymentRet::find()->where(['ptr_aga_id' => $acc->aga_id])->all() as $key => $obj) {
            PaymentRet::cacheUpdate($obj);
        }

        $searchModel = new PaymentRetSearch();
        $dataProvider = $searchModel->search(['PaymentRetSearch' => ['ptr_aga_id' => $acc->aga_id]]);

        return $this->render('update_payment_ret', [
            'pmodel' => $pmodel,
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'model' => $acc,
            'entity' => ArrayHelper::map(Entity::find()->select(['ent_id', 'ent_name_short'])->where(['ent_id' => $entity->ent_id])->all(), 'ent_id', 'ent_name_short'),
        ]);
    }

    /**
     * Pay PaymentRet
     * @param int $id
     * @return mixed
     * @throws
     */
    public function actionPayPaymentRet ($id) {
        PaymentRet::cacheClear();

        $pmodel = PaymentRet::findOne($id);

        $acc = AgreementAcc::findOne($pmodel->ptr_aga_id);
        $entity = $this->findModel($acc->aga_ab_id);

        foreach (PaymentRet::find()->where(['ptr_aga_id' => $acc->aga_id])->all() as $key => $obj) {
            Paymentret::cacheUpdate($obj);
        }

        $searchModel = new PaymentRetSearch();
        $dataProvider = $searchModel->search(['PaymentRetSearch' => ['ptr_id' => $id]]);

        return $this->render('update_payment_ret_pay', [
            'pmodel' => $pmodel,
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'model' => $acc,
            'entity' => ArrayHelper::map(Entity::find()->select(['ent_id', 'ent_name_short'])->where(['ent_id' => $entity->ent_id])->all(), 'ent_id', 'ent_name_short'),
        ]);
    }


    /**
     * Delete PaymentRet
     * @param int $id
     * @return mixed
     * @throws
     */
    public function actionDeletePaymentRet ($id) {
        $obj =  PaymentRet::findOne($id);
        $id = $obj->ptrAga->aga_ab_id;
        $obj->delete();

        return $this->redirect(['view', 'id' => $id]);

    }

    /**
     * Delete PaymentRet
     * @param int $id
     * @return mixed
     * @throws
     */
    public function actionDeletePaymentRetCache ($id) {
        $obj =  PaymentRet::findOne($id);
        PaymentRet::cacheDelete($obj);
        $id = $obj->ptr_aga_id;
        $obj->delete();

        return $this->redirect(['view-payment-ret', 'id' => $id]);

    }

    /**
     * View PaymentRet from AgreementAcc
     * @param int $id
     * @return mixed
     * @throws
     */
    public function actionViewPaymentRet ($id) {
        $acc = AgreementAcc::findOne($id);
        $entity = $this->findModel($acc->aga_ab_id);

        $searchModel = new PaymentRetSearch();
        $dataProvider = $searchModel->search(['PaymentRetSearch' => ['ptr_aga_id' => $acc->aga_id]]);

        return $this->render('update_payment_ret', [
            'pmodel' => null,
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'model' => $acc,
            'entity' => ArrayHelper::map(Entity::find()->select(['ent_id', 'ent_name_short'])->where(['ent_id' => $entity->ent_id])->all(), 'ent_id', 'ent_name_short'),
        ]);
    }

    /**
     * Save PaymentRet objects to database
     * @param int $id
     * @return mixed
     * @throws
     */
    public function actionSavePaymentRet ($id) {
        $acc = AgreementAcc::findOne($id);
        $entity = $this->findModel($acc->aga_ab_id);

        PaymentRet::cacheApply();

        return $this->redirect(['view', 'id' => $entity->ent_id]);
    }

    /**
     * Save PaymentRet Pay objects to database
     * @param int $id
     * @return mixed
     * @throws
     */
    public function actionSavePaymentRetPay ($id) {
        $model = PaymentRet::findOne($id);

        $entity = $this->findModel($model->ptr_ab_id);

        $model->updatePay();

        return $this->redirect(['view', 'id' => $entity->ent_id]);
    }

    /**
     * Create ApplFinal object
     * @param integer $ent_id
     * @param integer $id
     * @return mixed
     * @throws \yii\db\Exception
     */
    public function actionCreateApplFinal ($ent_id, $id) {
        if ($appl = ApplRequestContent::findOne($id)) {
            if ($main = ApplMain::find()->where(['applm_applrc_id' => $id])->one()) {
                $model = new ApplFinal();
                $model->applf_prs_id = $appl->applrcPrs->prs_id;
                $model->applf_name_first = $appl->applrcPrs->prs_first_name;
                $model->applf_name_last = $appl->applrcPrs->prs_last_name;
                $model->applf_name_middle = $appl->applrcPrs->prs_middle_name;
                $model->applf_name_full = $appl->applrcPrs->prs_full_name;
                $model->applf_end_date = $appl->applrc_date_upk;
                $model->applf_svdt_id = $appl->applrcSvc->svc_svdt_id;
                $model->applf_trt_id = $appl->applrcApplr->applr_trt_id;
                $model->applf_trp_id = $appl->applrc_trp_id;
                $model->applf_trp_hour = $appl->applrcTrp->trp_hour;
                $model->applf_cmd_date = $main->applm_applcmd_date ?? null;
                $model->applf_number = $main->applm_number_upk ?? null;
                $model->applf_applr_id = $appl->applrc_applr_id;
                $model->applf_applm_id = $main->applm_id;

                $applFinal = ApplFinal::findOne(['applf_number' => $main->applm_number_upk]);

//                if ($applFinal !== null){
//                    return $this->redirect(['appl-final/update', 'id' => $applFinal->applf_id, 'ent_id' => $ent_id]);
//                }

                Yii::$app->db->createCommand(
                    'delete from `appl_final` where `applf_number`=\''.$main->applm_number_upk.'\'')
                    ->execute();

                if ($model->save()) {
                    $appl->applrc_applf_id = $model->applf_id;
                    $appl->save(false);
                    return $this->redirect(['appl-final/update', 'id' => $model->applf_id, 'ent_id' => $ent_id]);
                } else {
                    return $this->render('_error_msg',
                        [
                            'msg' => 'Документ не записан. Проверьте ошибки',
                            'entity' => Entity::findOne($ent_id),
                            'model' => $model,
                        ]
                    );
                }
            } else {
                return $this->render('_error_msg',
                    [
                        'msg' => 'Нет записи в главной таблице.',
                        'entity' => Entity::findOne($ent_id),
                        'model' => null,
                    ]
                );
            }
        }
        return $this->render('_error_msg',
            [
                'msg' => 'Не найдена запись в заявках по этому сотруднику. Возможно она была удалена.',
                'entity' => Entity::findOne($ent_id),
                'model' => null,
            ]
        );

        //return $this->redirect(['view', 'id' => $ent_id]);
    }

    /**
     * Delete ApplFinal
     * @param int $id
     * @return mixed
     * @throws
     */
    public function actionDeleteApplFinal ($id) {
        $obj =  ApplFinal::findOne($id);
        $id = $obj->applfApplr->applr_ab_id;
        $obj->delete();

        return $this->redirect(['view', 'id' => $id]);
    }

    /**
     * Export to Excell
     * @param int $id
     * @return mixed
     * @throws \yii\web\RangeNotSatisfiableHttpException
     */
    public function actionApplShExportXml($id) {
        $searchModel = new ApplShSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $id);

        $data[] = [
            Yii::t('app', 'Appls ID'),
            Yii::t('app', 'Appls Applr ID'),
            Yii::t('app', 'Appls Prs ID'),
            Yii::t('app', 'Prs Connect User'),
            Yii::t('app', 'Appls Trp ID'),
            Yii::t('app', 'Appls Number'),
            Yii::t('app', 'Appls Date'),
            Yii::t('app', 'Appls Passed'),
            Yii::t('app', 'Appls Score Max'),
            Yii::t('app', 'Appls Score'),
            Yii::t('app', 'Appls Trt ID'),
            Yii::t('app', 'Applr Flag'),
            ];

        foreach ($dataProvider->query->all() as $model) {
            $data[] = [
                $model->appls_id,
                $model->applsApplr->applr_number ?? '',
                $model->applsPrs->prs_full_name,
                $model->applsPrs->prs_connect_user,
                $model->applsTrp->trp_name,
                $model->appls_number,
                $model->appls_date,
                \app\models\Constant::YES_NO[$model->appls_passed],
                $model->appls_score_max,
                $model->appls_score,
                $model->applsTrt->trt_name,
                \app\models\Constant::YES_NO[$model->applsApplr->applr_flag ?? 0],
            ];
            //unset($model);
        }

        $xls = new Excel_XML();
        $xls->WorksheetTitle = $id.'_ApplSh';
        $xls->addArray($data);
        //$xls->generateXML('AgreementAcc');

        return Yii::$app->response->sendContentAsFile($xls->generateXML0(), $id.'_ApplSh.xls');
    }

    /**
     * Export to Excell
     * @param int $id
     * @return mixed
     * @throws \yii\web\RangeNotSatisfiableHttpException
     */
    public function actionApplSheetExportXml($id) {
        $searchModel = new ApplSheetSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $id);

        $data[] = [
            Yii::t('app', 'Appls ID'),
            Yii::t('app', 'Appls Applr ID'),
            Yii::t('app', 'Appls Prs ID'),
            Yii::t('app', 'Prs Connect User'),
            Yii::t('app', 'Appls Trp ID'),
            Yii::t('app', 'Appls Number'),
            Yii::t('app', 'Appls Date'),
            Yii::t('app', 'Appls Passed'),
            Yii::t('app', 'Appls Score Max'),
            Yii::t('app', 'Appls Score'),
            Yii::t('app', 'Appls Trt ID'),
            Yii::t('app', 'Applr Flag'),
        ];

        foreach ($dataProvider->query->all() as $model) {
            $data[] = [
                $model->appls_id,
                $model->applsApplr->applr_number ?? '',
                $model->applsPrs->prs_full_name,
                $model->applsPrs->prs_connect_user,
                $model->applsTrp->trp_name,
                $model->appls_number,
                $model->appls_date,
                \app\models\Constant::YES_NO[$model->appls_passed],
                $model->appls_score_max,
                $model->appls_score,
                $model->applsTrt->trt_name,
                \app\models\Constant::YES_NO[$model->applsApplr->applr_flag ?? 0],
            ];
            //unset($model);
        }

        $xls = new Excel_XML();
        $xls->WorksheetTitle = $id.'_ApplSheet';
        $xls->addArray($data);
        //$xls->generateXML('AgreementAcc');

        return Yii::$app->response->sendContentAsFile($xls->generateXML0(), $id.'_ApplSheet.xls');
    }
}
