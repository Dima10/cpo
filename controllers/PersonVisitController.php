<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2018 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\controllers;

use app\models\TrainingProgSessionSearch;
use Yii;
use app\models\PersonVisit;
use app\models\PersonVisitSearch;
use yii\data\ActiveDataProvider;
use yii\debug\models\timeline\DataProvider;
use yii\web\Controller;
use yii\web\HttpException;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

/**
 * PersonVisitController implements the CRUD actions for PersonVisit model.
 */
class PersonVisitController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::class,
                'rules' => [
                    [
                        'allow' => false,
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' =>
                            function ($rule, $action) {
                                return \app\models\AcAccess::checkAction($action);
                            },
                    ],
                ],
            ],        
        ];
    }

    /**
     * Lists all PersonVisit models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PersonVisitSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $searchModelTPS = new TrainingProgSessionSearch();
        $dataProviderTPS = $searchModelTPS->search(['TrainingProgSessionSearch' => ['tps_id' => 0]]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'dataProviderTPS' => $dataProviderTPS,
            'searchModelTPS' => $searchModelTPS
        ]);
    }

    /**
     * Displays a single PersonVisit model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new PersonVisit model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new PersonVisit();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->pv_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing PersonVisit model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->pv_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing PersonVisit model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the PersonVisit model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PersonVisit the loaded model
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (($model = PersonVisit::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }

    /**
     * Ajax request return TrainingProgSession grid
     * @param string $id
     * @return string
     */
    public function actionAjaxSession($id)
    {
        $queryParams = Yii::$app->request->queryParams;
        $params = $queryParams['TrainingProgSessionSearch'] ?? [];
        $params['tps_session_id'] = empty($id) ? '-1' : $id;
        $searchModel = new TrainingProgSessionSearch();
        $dataProvider = $searchModel->search(['TrainingProgSessionSearch' => $params]);

        return $this->renderAjax('_grid_session', [
            'searchModelTPS' => $searchModel,
            'dataProviderTPS' => $dataProvider,
        ]);

    }
}
