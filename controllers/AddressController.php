<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\controllers;

use Yii;
use app\models\Address;
use app\models\AddressSearch;
use app\models\AddressType;
use app\models\Region;
use app\models\City;
use app\models\Country;
use app\models\Ab;

use yii\web\Controller;
use yii\web\HttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

/**
 * AddressController implements the CRUD actions for Address model.
 */
class AddressController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::class,
                'rules' => [
                    [
                        'allow' => false,
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' =>
                            function ($rule, $action) {
                                return \app\models\AcAccess::checkAction($action);
                            },
                    ],
                ],
            ],        
        ];
    }

    /**
     * Lists all Address models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AddressSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $country = isset(Yii::$app->request->queryParams['AddressSearch']['add_cou_id']) ? Yii::$app->request->queryParams['AddressSearch']['add_cou_id'] : 0;
        $region = isset(Yii::$app->request->queryParams['AddressSearch']['add_reg_id']) ? Yii::$app->request->queryParams['AddressSearch']['add_reg_id'] : 0;

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'addressType' => ArrayHelper::map(AddressType::find()->all(), 'addt_id', 'addt_name'),
            'addrbook' => ArrayHelper::map(Ab::find()->all(), 'ab_id', 'ab_name'),
            'country' => ArrayHelper::map(Country::find()->where('cou_id > 0')->all(), 'cou_id', 'cou_name'),
            'region' => ArrayHelper::map(Region::find()->where(['reg_cou_id' => $country ])->all(), 'reg_id', 'reg_name'),
            'city' => ArrayHelper::map(City::find()->where(['city_reg_id' => $region ])->all(), 'city_id', 'city_name'),
        ]);
    }

    /**
     * Displays a single Address model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Address model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Address();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->add_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'addrbook' => ArrayHelper::map(Ab::find()->all(), 'ab_id', 'ab_name'),
                'addressType' => ArrayHelper::map(AddressType::find()->all(), 'addt_id', 'addt_name'),
                'country' => ArrayHelper::map(Country::find()->all(), 'cou_id', 'cou_name'),
                'region' => [],
                'city' => [],
            ]);
        }
    }

    /**
     * Updates an existing Address model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->add_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'addrbook' => ArrayHelper::map(Ab::find()->all(), 'ab_id', 'ab_name'),
                'addressType' => ArrayHelper::map(AddressType::find()->all(), 'addt_id', 'addt_name'),
                'country' => ArrayHelper::map(Country::find()->all(), 'cou_id', 'cou_name'),
                'region' => ArrayHelper::map(Region::find()->where(['reg_cou_id' => $model->add_cou_id])->all(), 'reg_id', 'reg_name'),
                'city' => ['0' => '<>'] + ArrayHelper::map(City::find()->where(['city_reg_id' => $model->add_reg_id])->all(), 'city_id', 'city_name'),
            ]);
        }
    }

    /**
     * Deletes an existing Address model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }


    /**
     * Return Regions by Country.
     * @param integer $id
     * @return mixed
     */
    public function actionAjaxRegion($id)
    {
        return $this->renderAjax('_ajax_region', [
            'region' => ['0' => '<>'] + ArrayHelper::map(Region::find()->where(['reg_cou_id' => $id])->all(), 'reg_id', 'reg_name'),
        ]);
    }

    /**
     * Return City by Regions.
     * @param integer $id
     * @return mixed
     */
    public function actionAjaxCity($id)
    {
        return $this->renderAjax('_ajax_city', [
            'city' => ['0' => '<>'] + ArrayHelper::map(City::find()->where(['city_reg_id' => $id])->all(), 'city_id', 'city_name'),
        ]);
    }

    /**
     * Finds the Address model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Address the loaded model
     * @throws HttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Address::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }
}
