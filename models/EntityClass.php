<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%entity_class}}".
 *
 * @property integer $entc_id
 * @property string $entc_name
 * @property string $entc_sy
 * @property string $entc_create_user
 * @property string $entc_create_time
 * @property string $entc_create_ip
 * @property string $entc_update_user
 * @property string $entc_update_time
 * @property string $entc_update_ip
 *
 * @property EntityClassLnk[] $entityClassLnks
 */
class EntityClass extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%entity_class}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['entc_name'], 'required'],
            [['entc_create_time', 'entc_update_time'], 'safe'],
            [['entc_name'], 'string', 'max' => 128],
            [['entc_sy'], 'string', 'max' => 64],
            [['entc_create_user', 'entc_create_ip', 'entc_update_user', 'entc_update_ip'], 'string', 'max' => 64],
            [['entc_name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'entc_id' => Yii::t('app', 'Entc ID'),
            'entc_name' => Yii::t('app', 'Entc Name'),
            'entc_sy' => Yii::t('app', 'Entc Sy'),
            'entc_create_user' => Yii::t('app', 'Entc Create User'),
            'entc_create_time' => Yii::t('app', 'Entc Create Time'),
            'entc_create_ip' => Yii::t('app', 'Entc Create Ip'),
            'entc_update_user' => Yii::t('app', 'Entc Update User'),
            'entc_update_time' => Yii::t('app', 'Entc Update Time'),
            'entc_update_ip' => Yii::t('app', 'Entc Update Ip'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEntityClassLnks()
    {
        return $this->hasMany(EntityClassLnk::class, ['entcl_entc_id' => 'entc_id']);
    }

    /**
     * @inheritdoc
     * @return EntityClassQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new EntityClassQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) return false;
        if ($insert) {
            $this->entc_create_user = Yii::$app->user->identity->username;
            $this->entc_create_ip = Yii::$app->request->userIP;
            $this->entc_create_time = date('Y-m-d H:i:s');
            return true;
        } else {
            $this->entc_update_user = Yii::$app->user->identity->username;
            $this->entc_update_ip = Yii::$app->request->userIP;
            $this->entc_update_time = date('Y-m-d H:i:s');
            return true;
        }
    }

}
