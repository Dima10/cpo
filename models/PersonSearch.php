<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Person;

/**
 * PersonSearch represents the model behind the search form about `app\models\Person`.
 */
class PersonSearch extends Person
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['prs_id', 'prs_pass_sex', 'prs_connect_count'], 'integer'],
            [['prs_first_name', 'prs_last_name', 'prs_middle_name', 'prs_full_name', 'prs_inn', 'prs_birth_date', 'prs_pass_serial', 'prs_pass_number', 'prs_pass_issued_by', 'prs_pass_date', 'prs_create_user', 'prs_create_time', 'prs_create_ip', 'prs_update_user', 'prs_update_time', 'prs_update_ip'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Person::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'prs_id' => $this->prs_id,
            'prs_birth_date' => $this->prs_birth_date,
            'prs_pass_sex' => $this->prs_pass_sex,
            'prs_pass_date' => $this->prs_pass_date,
        ]);

        $query
            ->andFilterWhere(['like', 'prs_first_name', $this->prs_first_name])
            ->andFilterWhere(['like', 'prs_last_name', $this->prs_last_name])
            ->andFilterWhere(['like', 'prs_middle_name', $this->prs_middle_name])
            ->andFilterWhere(['like', 'prs_full_name', $this->prs_full_name])
            ->andFilterWhere(['like', 'prs_inn', $this->prs_inn])
            ->andFilterWhere(['like', 'prs_pass_serial', $this->prs_pass_serial])
            ->andFilterWhere(['like', 'prs_pass_number', $this->prs_pass_number])
            ->andFilterWhere(['like', 'prs_pass_issued_by', $this->prs_pass_issued_by])
            ->andFilterWhere(['like', 'prs_connect_count', $this->prs_connect_count])
            ->andFilterWhere(['like', 'prs_create_user', $this->prs_create_user])
            ->andFilterWhere(['like', 'prs_create_ip', $this->prs_create_ip])
            ->andFilterWhere(['like', 'prs_update_user', $this->prs_update_user])
            ->andFilterWhere(['like', 'prs_update_ip', $this->prs_update_ip])
            ->andFilterWhere(['like', 'prs_create_time', $this->prs_create_time])
            ->andFilterWhere(['like', 'prs_update_time', $this->prs_update_time])
        ;

        return $dataProvider;
    }
}
