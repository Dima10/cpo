<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%person}}".
 *
 * @property integer $prs_id
 * @property string $prs_first_name
 * @property string $prs_last_name
 * @property string $prs_middle_name
 * @property string $prs_full_name
 * @property string $prs_inn
 * @property string $prs_birth_date
 * @property integer $prs_pass_sex
 * @property string $prs_pass_serial
 * @property string $prs_pass_number
 * @property string $prs_pass_issued_by
 * @property string $prs_pass_date
 * @property string $prs_connect_link
 * @property string $prs_connect_user
 * @property string $prs_connect_pwd
 * @property string $prs_connect_count
 * @property string $prs_create_user
 * @property string $prs_create_time
 * @property string $prs_create_ip
 * @property string $prs_update_user
 * @property string $prs_update_time
 * @property string $prs_update_ip
 *
 * @property Ab $prs
 * @property Staff[] $staff
 * @property Contact[] $contacts
 */
class Person extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%person}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['prs_first_name', 'prs_last_name'], 'required'],
            [['prs_birth_date', 'prs_pass_date', 'prs_create_time', 'prs_update_time'], 'safe'],
            [['prs_pass_sex', 'prs_connect_count'], 'integer'],
            [['prs_first_name', 'prs_last_name', 'prs_middle_name', 'prs_create_user', 'prs_create_ip', 'prs_update_user', 'prs_update_ip'], 'string', 'max' => 64],
            [['prs_full_name', 'prs_pass_issued_by', 'prs_connect_user', 'prs_connect_pwd'], 'string', 'max' => 256],
            [['prs_inn'], 'string', 'max' => 16],
            [['prs_pass_serial'], 'string', 'max' => 4],
            [['prs_pass_number'], 'string', 'max' => 8],
            [['prs_inn'], 'unique'],
            [['prs_pass_serial', 'prs_pass_number'], 'unique', 'targetAttribute' => ['prs_pass_serial', 'prs_pass_number'], 'message' => Yii::t('app', 'The combination of Prs Pass Serial and Prs Pass Number has already been taken.')],
            [['prs_id'], 'exist', 'skipOnError' => true, 'targetClass' => Ab::class, 'targetAttribute' => ['prs_id' => 'ab_id']],

            [['prs_connect_link'], 'string', 'max' => 256],

        ];
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'prs_id' => Yii::t('app', 'Prs ID'),
            'prs_first_name' => Yii::t('app', 'Prs First Name'),
            'prs_last_name' => Yii::t('app', 'Prs Last Name'),
            'prs_middle_name' => Yii::t('app', 'Prs Middle Name'),
            'prs_full_name' => Yii::t('app', 'Prs Full Name'),
            'prs_inn' => Yii::t('app', 'Prs Inn'),
            'prs_birth_date' => Yii::t('app', 'Prs Birth Date'),
            'prs_pass_sex' => Yii::t('app', 'Prs Pass Sex'),
            'prs_pass_serial' => Yii::t('app', 'Prs Pass Serial'),
            'prs_pass_number' => Yii::t('app', 'Prs Pass Number'),
            'prs_pass_issued_by' => Yii::t('app', 'Prs Pass Issued By'),
            'prs_pass_date' => Yii::t('app', 'Prs Pass Date'),
            'prs_connect_link' => Yii::t('app', 'Prs Connect Link'),
            'prs_connect_user' => Yii::t('app', 'Prs Connect User'),
            'prs_connect_pwd' => Yii::t('app', 'Prs Connect Pwd'),
            'prs_connect_count' => Yii::t('app', 'Prs Connect Count'),
            'prs_create_user' => Yii::t('app', 'Create User'),
            'prs_create_time' => Yii::t('app', 'Create Time'),
            'prs_create_ip' => Yii::t('app', 'Create Ip'),
            'prs_update_user' => Yii::t('app', 'Update User'),
            'prs_update_time' => Yii::t('app', 'Update Time'),
            'prs_update_ip' => Yii::t('app', 'Update Ip'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrs()
    {
        return $this->hasOne(Ab::class, ['ab_id' => 'prs_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStaff()
    {
        return $this->hasMany(Staff::class, ['stf_prs_id' => 'prs_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContacts()
    {
        return $this->hasMany(Contact::class, ['con_ab_id' => 'prs_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVisit()
    {
        return $this->hasOne(PersonVisit::class, ['pv_prs_id' => 'prs_id']);
    }

    /**
     * @inheritdoc
     * @return PersonQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PersonQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) return false;
        if ($insert) {

            $ab = new Ab();
            $ab->ab_type = 2;
            $ab->ab_name = (isset($this->prs_last_name) ? $this->prs_last_name : '').' '.(isset($this->prs_first_name) ? $this->prs_first_name: '').' '.(isset($this->prs_middle_name) ? $this->prs_middle_name : '');
            $ab->save();
            $this->prs_id = $ab->ab_id;
            $this->prs_full_name = $ab->ab_name;

            $this->prs_create_user = Yii::$app->user->identity->username;
            $this->prs_create_ip = Yii::$app->request->userIP;
            $this->prs_create_time = date('Y-m-d H:i:s');

            $this->prs_connect_user = 'os-'.$this->prs_id;
            srand();
            $this->prs_connect_pwd = rand(10000, 99999);
            return true;
        } else {

            $ab = Ab::findOne($this->prs_id);
            $ab->ab_name = (isset($this->prs_last_name) ? $this->prs_last_name : '').' '.(isset($this->prs_first_name) ? $this->prs_first_name: '').' '.(isset($this->prs_middle_name) ? $this->prs_middle_name : '');
            $ab->save();
            $this->prs_full_name = $ab->ab_name;

            $this->prs_update_user = Yii::$app->user->identity->username;
            $this->prs_update_ip = Yii::$app->request->userIP;
            $this->prs_update_time = date('Y-m-d H:i:s');
            return true;
        }
    }

    /**
     * @inheritdoc
     */
    public function afterDelete()
    {
        $model = Ab::findOne($this->prs_id);
        $model->delete();
    }

    /**
     * @inheritdoc
     */
    public function genConnectLink()
    {
        $this->prs_connect_link = md5(uniqid(rand(), true));
    }


    /**
     * Return current object from session.
     * @return Person|boolean
     */
    public static function objectExists()
    {
        $class = get_called_class();
        Yii::$app->session->open();
        if (isset(Yii::$app->session[$class]) && is_a(Yii::$app->session[$class], $class)) {
            return Yii::$app->session[$class];
        }
        return false;
    }

    /**
     * Save current object in session.
     * @return mixed
     */
    public function objectSave()
    {
        Yii::$app->session->open();
        Yii::$app->session[get_class()] = $this;
    }

    /**
     * Clear current object from session.
     * @return boolean
     */
    public static function objectClear()
    {
        $class = get_called_class();
        Yii::$app->session->open();
        if (isset(Yii::$app->session[$class]) && is_a(Yii::$app->session[$class], $class)) {
            Yii::$app->session[$class] = null;
        }
    }

}
