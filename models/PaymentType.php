<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%payment_type}}".
 *
 * @property integer $pt_id
 * @property string $pt_name
 * @property string $pt_create_user
 * @property string $pt_create_ip
 * @property string $pt_create_time
 * @property string $pt_update_user
 * @property string $pt_update_ip
 * @property string $pt_update_time
 *
 */
class PaymentType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%payment_type}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pt_name'], 'required'],
            [['pt_create_time', 'pt_update_time'], 'safe'],
            [['pt_name', 'pt_create_user', 'pt_create_ip', 'pt_update_user', 'pt_update_ip'], 'string', 'max' => 64],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pt_id' => Yii::t('app', 'Pt ID'),
            'pt_name' => Yii::t('app', 'Pt Name'),
            'pt_create_user' => Yii::t('app', 'Pt Create User'),
            'pt_create_ip' => Yii::t('app', 'Pt Create Ip'),
            'pt_create_time' => Yii::t('app', 'Pt Create Time'),
            'pt_update_user' => Yii::t('app', 'Pt Update User'),
            'pt_update_ip' => Yii::t('app', 'Pt Update Ip'),
            'pt_update_time' => Yii::t('app', 'Pt Update Time'),
        ];
    }

    /**
     * @inheritdoc
     * @return FinanceBookTypeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new FinanceBookTypeQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) return false;
        if ($insert) {
            $this->pt_create_user = Yii::$app->user->identity->username;
            $this->pt_create_ip = Yii::$app->request->userIP;
            $this->pt_create_time = date('Y-m-d H:i:s');
            return true;
        } else {
            $this->pt_update_user = Yii::$app->user->identity->username;
            $this->pt_update_ip = Yii::$app->request->userIP;
            $this->pt_update_time = date('Y-m-d H:i:s');
            return true;
        }
    }

}
