<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2018-2019 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use app\exceptions\ImapErrorException;
use Yii;

/**
 * This is the model class for table "{{%user_mail}}".
 *
 * @property int $um_id
 * @property string $um_name
 * @property string $um_subj
 * @property string $um_text
 * @property int $um_sm_id
 * @property int $um_umt_id
 * @property string $um_to
 * @property int $um_reestr
 * @property int $um_flag
 * @property string $um_create_user
 * @property string $um_create_time
 * @property string $um_create_ip
 * @property string $um_update_user
 * @property string $um_update_time
 * @property string $um_update_ip
 *
 * @property SystemMail $umSm
 * @property UserMailType $umUmt
 */
class UserMail extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_mail}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['um_name', 'um_subj', 'um_text', 'um_sm_id', 'um_umt_id'], 'required'],
            [['um_text'], 'string'],
            [['um_sm_id', 'um_umt_id', 'um_reestr', 'um_flag'], 'integer'],
            [['um_create_time', 'um_update_time', 'um_to'], 'safe'],
            [['um_name'], 'string', 'max' => 128],
            [['um_subj', 'um_to'], 'string', 'max' => 255],
            [['um_create_user', 'um_create_ip', 'um_update_user', 'um_update_ip'], 'string', 'max' => 64],
            [['um_sm_id'], 'exist', 'skipOnError' => true, 'targetClass' => SystemMail::class, 'targetAttribute' => ['um_sm_id' => 'sm_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'um_id' => Yii::t('app', 'Um ID'),
            'um_name' => Yii::t('app', 'Um Name'),
            'um_subj' => Yii::t('app', 'Um Subj'),
            'um_text' => Yii::t('app', 'Um Text'),
            'um_sm_id' => Yii::t('app', 'Um Sm ID'),
            'um_umt_id' => Yii::t('app', 'Um Umt ID'),
            'um_reestr' => Yii::t('app', 'Um Reestr'),
            'um_flag' => Yii::t('app', 'Um Flag'),
            'um_to' => Yii::t('app', 'Um To'),
            'um_create_user' => Yii::t('app', 'Um Create User'),
            'um_create_time' => Yii::t('app', 'Um Create Time'),
            'um_create_ip' => Yii::t('app', 'Um Create Ip'),
            'um_update_user' => Yii::t('app', 'Um Update User'),
            'um_update_time' => Yii::t('app', 'Um Update Time'),
            'um_update_ip' => Yii::t('app', 'Um Update Ip'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUmSm()
    {
        return $this->hasOne(SystemMail::class, ['sm_id' => 'um_sm_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUmUmt()
    {
        return $this->hasOne(UserMailType::class, ['umt_id' => 'um_umt_id']);
    }

    /**
     * @inheritdoc
     * @return UserMailQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UserMailQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) return false;
        if ($insert) {
            $this->um_create_user = Yii::$app->user->identity->username;
            $this->um_create_ip = Yii::$app->request->userIP;
            $this->um_create_time = date('Y-m-d H:i:s');
            return true;
        } else {
            $this->um_update_user = Yii::$app->user->identity->username;
            $this->um_update_ip = Yii::$app->request->userIP;
            $this->um_update_time = date('Y-m-d H:i:s');
            return true;
        }
    }

    /**
     * Send mail
     * @param integer $id
     * @param string $to
     * @param array $param
     * @return null|string
     * @throws \Exception
     */
    public static function sendMail($id, $to = null, array $param = null)
    {
        $p = function (string $text, array $params): string {
            $t = $text;
            if (is_array($params)) {
                foreach ($params as $key => $value) {
                    $t = str_ireplace('${'.$key.'}', $value, $t);
                }
            }
            return $t;
        };

        if ($model = UserMail::find()->where(['um_umt_id' => $id, 'um_reestr' => ($param[':reestr'] ?? 0), 'um_flag' => ($param[':personal'] ?? 0)])->one()) {
            Yii::$app->mailer->setTransport(
                [
                    'class' => 'Swift_SmtpTransport',
                    'host' => $model->umSm->sm_server,
                    'username' => $model->umSm->sm_user,
                    'password' => $model->umSm->sm_password,
                    'port' => $model->umSm->sm_port,
                    'encryption' => $model->umSm->sm_encryption,
                ]
            );
            $send = explode(';', $model->um_to);
            foreach ($send as $addr) {
                if (trim($addr) == '') continue;
                try {
                    Yii::$app->mailer->compose(null, ['Received' => true])
                        ->setHtmlBody($p($model->um_text, $param))
                        ->setFrom(trim($model->umSm->sm_user))
                        ->setTo(trim($addr))
                        ->setSubject($p($model->um_subj, $param))
                        ->setReadReceiptTo(trim($model->umSm->sm_user))
                        ->send();
                } catch (\Exception $e) {
                    Yii::error($e->getMessage());
                    throw $e;
                }
            }
            if (!is_null($to)) {
                $send = explode(';', $to);
                foreach ($send as $addr) {
                    try {
                        if (trim($addr) == '') continue;
                        Yii::$app->mailer->compose(null, ['Received' => true])
                            ->setHtmlBody($p($model->um_text, $param))
                            ->setFrom(trim($model->umSm->sm_user))
                            ->setTo(trim($addr))
                            ->setSubject($p($model->um_subj, $param))
                            ->setReadReceiptTo(trim($model->umSm->sm_user))
                            ->send();
                    } catch (\Exception $e) {
                        Yii::error($e->getMessage());
                        throw $e;
                    }
                }
            }

            if (trim($model->umSm->sm_imap_server) != '') {
                $imap = \imap_open($model->umSm->sm_imap_server, $model->umSm->sm_imap_user, $model->umSm->sm_imap_password, 0, 1);
                try {
                    $send = explode(';', $model->um_to);
                    foreach ($send as $addr) {
                        if (trim($addr) == '') continue;
                        try {
                            $env = [];
                            $env["from"] = trim($model->umSm->sm_imap_user);
                            $env["to"]  = trim($addr);
                            $env["subject"]  = $p($model->um_subj, $param);
                            $env["Return-Receipt-To"]  = trim($model->umSm->sm_imap_user);
                            $env["Delivered-To"]  = trim($model->umSm->sm_imap_user);
                            $b1 = [];
                            $b1["type"] = 'TEXT';
                            $b1["subtype"] = 'html';
                            $b1["description"] = 'description1';
                            $b1["contents.data"] = $p($model->um_text, $param);
                            $body[] = $b1;
                            $msg = \imap_mail_compose($env, $body);
                            if (!\imap_append($imap, $model->umSm->sm_imap_server.'Sent', $msg)) {
                                if (!\imap_append($imap, $model->umSm->sm_imap_server.'&BB4EQgQ,BEAEMAQyBDsENQQ9BD0ESwQ1-', $msg)) {

                                    if (!\imap_append($imap, $model->umSm->sm_imap_server.'Sent', $msg)) {
                                        if (!\imap_append($imap, $model->umSm->sm_imap_server.'&BB4EQgQ,BEAEMAQyBDsENQQ9BD0ESwQ1-', $msg)) {
                                            throw new ImapErrorException(\imap_last_error());
                                        }
                                    }

                                }
                            }
                        } catch (\Exception $e) {
                            Yii::error($e->getMessage());
                            throw $e;
                        }
                    }
                    if (!is_null($to)) {
                        $send = explode(';', $to);
                        foreach ($send as $addr) {
                            if (trim($addr) == '') continue;
                            try {
                                $env = [];
                                $env["from"] = trim($model->umSm->sm_imap_user);
                                $env["to"]  = trim($addr);
                                $env["subject"]  = $p($model->um_subj, $param);
                                $env["Return-Receipt-To"]  = trim($model->umSm->sm_imap_user);
                                $env["Received"] = true;
                                $env["Delivered-To"]  = trim($model->umSm->sm_imap_user);
                                $b1 = [];
                                $b1["type"] = "TEXT";
                                $b1["subtype"] = "html";
                                $b1["description"] = 'description1';
                                $b1["contents.data"] = $p($model->um_text, $param);
                                $body[] = $b1;
                                $msg = \imap_mail_compose($env, $body);
                                if (!\imap_append($imap, $model->umSm->sm_imap_server.'Sent', $msg)) {
                                    if (!\imap_append($imap, $model->umSm->sm_imap_server.'&BB4EQgQ,BEAEMAQyBDsENQQ9BD0ESwQ1-', $msg)) {

                                        if (!\imap_append($imap, $model->umSm->sm_imap_server.'Sent', $msg)) {
                                            if (!\imap_append($imap, $model->umSm->sm_imap_server.'&BB4EQgQ,BEAEMAQyBDsENQQ9BD0ESwQ1-', $msg)) {
                                                throw new ImapErrorException(\imap_last_error());
                                            };
                                        };

                                    };
                                };
                            } catch (\Exception $e) {
                                Yii::error($e->getMessage());
                                throw $e;
                            }
                        }
                    }
                } finally {
                    imap_close($imap);
                }
            }
        }
        return true;
    }
}
