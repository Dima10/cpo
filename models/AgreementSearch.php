<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Agreement;

/**
 * AgreementSearch represents the model behind the search form about `app\models\Agreement`.
 */
class AgreementSearch extends Agreement
{

    public $yurLico;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['agr_id', 'agr_ab_id', 'agr_comp_id', 'agr_ast_id'], 'integer'],
            [['agr_sum', 'agr_tax'], 'number'],
            [['agr_number', 'agr_date', 'agr_create_time', 'agr_create_ip', 'agr_update_user', 'agr_update_time', 'agr_update_ip', 'agr_create_user', 'agr_comment'], 'safe'],
            [['yurLico'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @param integer $id
     *
     * @return ActiveDataProvider
     */
    public function search($params, $id = null)
    {
        if (isset($id)) {
            $query = Agreement::find()->where(['agr_ab_id' => $id]);
        } else {
            $query = Agreement::find()->joinWith(['agrAb' => function($query) { $query->from(['agrAb' => 'ab']);}]);
        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
            'sort' => [
                'defaultOrder' => ['agr_date' => SORT_DESC],
            ]
        ]);

        $dataProvider->sort->attributes['yurLico'] = [
            'asc' => ['agrAb.ab_name' => SORT_ASC],
            'desc' => ['agrAb.ab_name' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'agr_comp_id' => $this->agr_comp_id,
            'agr_ast_id' => $this->agr_ast_id,
        ]);

        $query
            ->andFilterWhere(['like', 'agr_id', $this->agr_id])
            ->andFilterWhere(['like', 'agr_number', $this->agr_number])
            ->andFilterWhere(['like', 'agr_date', $this->agr_date])
            ->andFilterWhere(['like', 'agr_sum', $this->agr_sum])
            ->andFilterWhere(['like', 'agr_tax', $this->agr_tax])
            ->andFilterWhere(['like', 'agrAb.ab_name', $this->yurLico])
            //->andFilterWhere(['like', 'agr_fdata', $this->agr_fdata])
            //->andFilterWhere(['like', 'agr_fdata_sign', $this->agr_fdata_sign])
            ->andFilterWhere(['like', 'agr_create_time', $this->agr_create_time])
            ->andFilterWhere(['like', 'agr_create_ip', $this->agr_create_ip])
            ->andFilterWhere(['like', 'agr_update_time', $this->agr_update_time])
            ->andFilterWhere(['like', 'agr_update_user', $this->agr_update_user])
            ->andFilterWhere(['like', 'agr_update_ip', $this->agr_update_ip])
            ->andFilterWhere(['like', 'agr_create_user', $this->agr_create_user])
            ->andFilterWhere(['like', 'agr_comment', $this->agr_comment])
            ;

        return $dataProvider;
    }
}
