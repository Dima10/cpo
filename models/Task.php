<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%task}}".
 *
 * @property integer $task_id
 * @property integer $task_ab_id
 * @property integer $task_taskt_id
 * @property string $task_date_end
 * @property integer $task_tasks_id
 * @property integer $task_user_id
 * @property string $task_note
 * @property integer $task_event_id
 * @property string $task_create_user
 * @property string $task_create_time
 * @property string $task_create_ip
 * @property string $task_update_user
 * @property string $task_update_time
 * @property string $task_update_ip
 *
 * @property TaskType $taskTaskt
 * @property Ab $taskAb
 * @property TaskStatus $taskTasks
 * @property User $taskUser
 * @property Event $taskEvent
 */
class Task extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%task}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['task_ab_id', 'task_taskt_id', 'task_date_end', 'task_tasks_id', 'task_user_id', 'task_note'], 'required'],
            [['task_ab_id', 'task_taskt_id', 'task_tasks_id', 'task_user_id', 'task_event_id'], 'integer'],
            [['task_date_end', 'task_create_time', 'task_update_time'], 'safe'],
            [['task_note'], 'string'],
            [['task_create_user', 'task_create_ip', 'task_update_user', 'task_update_ip'], 'string', 'max' => 64],
            [['task_taskt_id'], 'exist', 'skipOnError' => true, 'targetClass' => TaskType::class, 'targetAttribute' => ['task_taskt_id' => 'taskt_id']],
            [['task_ab_id'], 'exist', 'skipOnError' => true, 'targetClass' => Ab::class, 'targetAttribute' => ['task_ab_id' => 'ab_id']],
            [['task_tasks_id'], 'exist', 'skipOnError' => true, 'targetClass' => TaskStatus::class, 'targetAttribute' => ['task_tasks_id' => 'tasks_id']],
            [['task_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['task_user_id' => 'user_id']],
            [['task_event_id'], 'exist', 'skipOnError' => true, 'targetClass' => Event::class, 'targetAttribute' => ['task_event_id' => 'event_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'task_id' => Yii::t('app', 'Task ID'),
            'task_ab_id' => Yii::t('app', 'Task Ab ID'),
            'task_taskt_id' => Yii::t('app', 'Task Taskt ID'),
            'task_date_end' => Yii::t('app', 'Task Date End'),
            'task_tasks_id' => Yii::t('app', 'Task Tasks ID'),
            'task_user_id' => Yii::t('app', 'Task User ID'),
            'task_note' => Yii::t('app', 'Task Note'),
            'task_event_id' => Yii::t('app', 'Task Event ID'),
            'task_create_user' => Yii::t('app', 'Task Create User'),
            'task_create_time' => Yii::t('app', 'Task Create Time'),
            'task_create_ip' => Yii::t('app', 'Task Create Ip'),
            'task_update_user' => Yii::t('app', 'Task Update User'),
            'task_update_time' => Yii::t('app', 'Task Update Time'),
            'task_update_ip' => Yii::t('app', 'Task Update Ip'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaskTaskt()
    {
        return $this->hasOne(TaskType::class, ['taskt_id' => 'task_taskt_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaskAb()
    {
        return $this->hasOne(Ab::class, ['ab_id' => 'task_ab_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaskTasks()
    {
        return $this->hasOne(TaskStatus::class, ['tasks_id' => 'task_tasks_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaskUser()
    {
        return $this->hasOne(User::class, ['user_id' => 'task_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaskEvent()
    {
        return $this->hasOne(Event::class, ['event_id' => 'task_event_id']);
    }

    /**
     * @inheritdoc
     * @return TaskQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TaskQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) return false;
        if ($insert) {
            $this->task_create_user = Yii::$app->user->identity->username;
            $this->task_create_ip = Yii::$app->request->userIP;
            $this->task_create_time = date('Y-m-d H:i:s');
            return true;
        } else {
            $this->task_update_user = Yii::$app->user->identity->username;
            $this->task_update_ip = Yii::$app->request->userIP;
            $this->task_update_time = date('Y-m-d H:i:s');
            return true;
        }
    }

}
