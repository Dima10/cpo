<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2018 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use app\exceptions\ImapErrorException;
use Yii;
use yii\web\HttpException;

/**
 * This is the model class for table '{{%appl_request}}'.
 *
 * Заявка на обучение
 *
 * @property integer $applr_id
 * @property integer $applr_reestr
 * @property integer $applr_id_day
 * @property string $applr_number
 * @property string $applr_date
 * @property integer $applr_ab_id
 * @property integer $applr_comp_id

 * @property integer $applr_pat_id
 * @property string $applr_file_name
 * @property resource $applr_file_data
 * @property integer $applr_agra_id
 * @property string $applr_appls_date
 * @property string $applr_apple_date
 * @property string $applr_applsx_date
 * @property string $applr_applc_date
 * @property integer $applr_trt_id
 * @property integer $applr_ast_id
 * @property integer $applr_flag
 * @property integer $applr_flag_send
 * @property integer $applr_manager_id
 * @property string $applr_curator_name
 * @property string $applr_curator_addr

 * @property string $applr_create_user
 * @property string $applr_create_time
 * @property string $applr_create_ip
 * @property string $applr_update_user
 * @property string $applr_update_time
 * @property string $applr_update_ip
 *
 * @property Ab $applrAb
 * @property Ab $applrManager
 * @property Company $applrComp
 * @property TrainingType $applrTrt
 * @property AgreementStatus $applrAst
 * @property AgreementAnnex $applrAgra
 * @property ApplRequestContent[] $applRequestContents
 * @property Pattern $applrPat
 * @property ApplSh[] $applShes
 * @property ApplSheet[] $applSheets
 *
 */
class ApplRequest extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%appl_request}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['applr_date', 'applr_ab_id', 'applr_comp_id', 'applr_pat_id', 'applr_reestr'], 'required'],

            [['applr_file_name'], 'string', 'max' => 256],
            [['applr_file_data'], 'file', 'skipOnEmpty' => true, 'extensions' => 'docx'],


            [['applr_date', 'applr_appls_date', 'applr_apple_date', 'applr_applsx_date', 'applr_applc_date', 'applr_create_time', 'applr_update_time'], 'safe'],
            [['applr_reestr', 'applr_id_day', 'applr_ab_id', 'applr_comp_id', 'applr_agra_id', 'applr_trt_id',
                'applr_ast_id', 'applr_flag', 'applr_flag_send', 'applr_manager_id'], 'integer'],
            [['applr_curator_name', 'applr_curator_addr'], 'string', 'max' => 128],

            [['applr_number'], 'unique'],

            [['applr_number', 'applr_create_user', 'applr_create_ip', 'applr_update_user', 'applr_update_ip'], 'string', 'max' => 64],

            [['applr_appls_date', 'applr_apple_date', 'applr_applsx_date', 'applr_applc_date'], 'string'],


            [['applr_ab_id'], 'exist', 'skipOnError' => true, 'targetClass' => Ab::class, 'targetAttribute' => ['applr_ab_id' => 'ab_id']],
            [['applr_manager_id'], 'exist', 'skipOnError' => true, 'targetClass' => Ab::class, 'targetAttribute' => ['applr_manager_id' => 'ab_id']],
            [['applr_comp_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::class, 'targetAttribute' => ['applr_comp_id' => 'comp_id']],
            //[['applr_agra_id'], 'exist', 'skipOnError' => true, 'skipOnEmpty' => true, 'targetClass' => AgreementAnnex::class, 'targetAttribute' => ['applr_agra_id' => 'agra_id']],
            [['applr_trt_id'], 'exist', 'skipOnError' => true, 'targetClass' => TrainingType::class, 'targetAttribute' => ['applr_trt_id' => 'trt_id']],
            [['applr_ast_id'], 'exist', 'skipOnError' => true, 'targetClass' => AgreementStatus::class, 'targetAttribute' => ['applr_ast_id' => 'ast_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'applr_id' => Yii::t('app', 'Applr ID'),
            'applr_reestr' => Yii::t('app', 'Applr Reestr'),
            'applr_number' => Yii::t('app', 'Applr Number'),
            'applr_date' => Yii::t('app', 'Applr Date'),
            'applr_ab_id' => Yii::t('app', 'Applr Ab ID'),
            'applr_comp_id' => Yii::t('app', 'Applr Comp ID'),

            'applr_pat_id' => Yii::t('app', 'Applr Pat ID'),
            'applr_file_name' => Yii::t('app', 'Applr File Name'),
            'applr_file_data' => Yii::t('app', 'Applr File Data'),

            'applr_agra_id' => Yii::t('app', 'Applr Agra ID'),
            'applr_appls_date' => Yii::t('app', 'Applr Appls Date'),
            'applr_apple_date' => Yii::t('app', 'Applr Apple Date'),
            'applr_applsx_date' => Yii::t('app', 'Applr Applsx Date'),
            'applr_applc_date' => Yii::t('app', 'Applr Applc Date'),
            'applr_trt_id' => Yii::t('app', 'Applr Trt ID'),
            'applr_ast_id' => Yii::t('app', 'Applr Ast ID'),
            'applr_flag' => Yii::t('app', 'Applr Flag'),
            'applr_flag_send' => Yii::t('app', 'Applr Flag Send'),

            'applr_manager_id' => Yii::t('app', 'Applr Manager ID'),
            'applr_curator_name' => Yii::t('app', 'Applr Curator Name'),
            'applr_curator_addr' => Yii::t('app', 'Applr Curator Addr'),

            'applr_create_user' => Yii::t('app', 'Applr Create User'),
            'applr_create_time' => Yii::t('app', 'Applr Create Time'),
            'applr_create_ip' => Yii::t('app', 'Applr Create Ip'),
            'applr_update_user' => Yii::t('app', 'Applr Update User'),
            'applr_update_time' => Yii::t('app', 'Applr Update Time'),
            'applr_update_ip' => Yii::t('app', 'Applr Update Ip'),
        ];
    }

    const MAX_REQUEST_ID_BEFORE_CHANGE_LOGIC = 2578;

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplrTrt()
    {
        return $this->hasOne(TrainingType::class, ['trt_id' => 'applr_trt_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplrAst()
    {
        return $this->hasOne(AgreementStatus::class, ['ast_id' => 'applr_ast_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplrAb()
    {
        return $this->hasOne(Ab::class, ['ab_id' => 'applr_ab_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplrManager()
    {
        return $this->hasOne(Ab::class, ['ab_id' => 'applr_manager_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplrPat()
    {
        return $this->hasOne(Pattern::class, ['pat_id' => 'applr_pat_id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplrComp()
    {
        return $this->hasOne(Company::class, ['comp_id' => 'applr_comp_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplrAgra()
    {
        return $this->hasOne(AgreementAnnex::class, ['agra_id' => 'applr_agra_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplRequestContents()
    {
        return $this->hasMany(ApplRequestContent::class, ['applrc_applr_id' => 'applr_id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplShes()
    {
        return $this->hasMany(ApplSh::class, ['appls_applr_id' => 'applr_id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplSheets()
    {
        return $this->hasMany(ApplSheet::class, ['appls_applr_id' => 'applr_id']);
    }

    /**
     * @inheritdoc
     * @return ApplRequestQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ApplRequestQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) return false;

        $this->applr_agra_id = ($this->applr_agra_id == 0) ? null : $this->applr_agra_id;

        if ($insert) {
            $this->applr_create_user = Yii::$app->user->identity->username;
            $this->applr_create_ip = Yii::$app->request->userIP;
            $this->applr_create_time = date('Y-m-d H:i:s');
            return true;
        } else {
            $this->applr_update_user = Yii::$app->user->identity->username;
            $this->applr_update_ip = Yii::$app->request->userIP;
            $this->applr_update_time = date('Y-m-d H:i:s');
            return true;
        }
    }

    /**
     * @inheritdoc
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        $_SESSION['flash_appl_request_errors'] = [];
        if ($st = AgreementStatus::find()->where(['ast_id' => $this->applr_ast_id])->andWhere(['like', 'ast_sys', 'M'])->one()) {

            $this->updateMain(true);

            $table = [];

            try {
                foreach ($this->applRequestContents as $content) {
                    if (($content->applrc_notify ?? 1) == 1)
                    {
                        foreach ($content->applrcPrs->contacts as $contact) {
                            if ($contact->conCont->cont_type == 1) {
                                if(
                                UserMail::sendMail(1, $contact->con_text,
                                    [
                                        'link' => $content->applrcPrs->prs_connect_link,
                                        'login' => $content->applrcPrs->prs_connect_user,
                                        'password' => $content->applrcPrs->prs_connect_pwd,
                                        'programma' => $content->applrcTrp->trp_name,
                                        'fio' => $content->applrcPrs->prs_full_name,
                                        ':personal' => $content->applrcApplr->applr_flag,
                                        ':reestr' => $content->applrcApplr->applr_reestr,
                                    ])
                                ){
                                    $userMail = UserMail::find()->where(['um_umt_id' => 1, 'um_reestr' => $content->applrcApplr->applr_reestr, 'um_flag' => $content->applrcApplr->applr_flag])->one();

                                    if (trim($userMail->umSm->sm_imap_server) != '') {
                                        $fromMail = $userMail->umSm->sm_imap_user;
                                    } else {
                                        $fromMail = $userMail->umSm->sm_user;
                                    }

                                    $dateTime = new \DateTime('now');
                                    $sentEmailsFromRequest = new SentEmailsFromRequest();
                                    $sentEmailsFromRequest->applr_id = $content->applrc_applr_id;
                                    $sentEmailsFromRequest->applrc_prs_id = $content->applrc_prs_id;
                                    $sentEmailsFromRequest->fio = $content->applrcPrs->prs_full_name;
                                    $sentEmailsFromRequest->login = $content->applrcPrs->prs_connect_user;
                                    $sentEmailsFromRequest->applrc_trp_id = $content->applrcTrp->trp_id;
                                    $sentEmailsFromRequest->applr_ab_id = $content->applrcApplr->applr_ab_id;
                                    $sentEmailsFromRequest->applr_manager_id = $content->applrcApplr->applr_manager_id;
                                    $sentEmailsFromRequest->recipient_email = $contact->con_text;
                                    $sentEmailsFromRequest->sender_email = $fromMail;
                                    $sentEmailsFromRequest->send_date = $dateTime->format('Y-m-d H:i:s');
                                    $sentEmailsFromRequest->applr_reestr = $content->applrcApplr->applr_reestr;
                                    $sentEmailsFromRequest->applr_flag = $content->applrcApplr->applr_flag;
                                    $sentEmailsFromRequest->status = 1;
                                    $sentEmailsFromRequest->save(false);
                                }

                            }
                        }
                    }
                    $table[] = [
                        'login' => $content->applrcPrs->prs_connect_user,
                        'password' => $content->applrcPrs->prs_connect_pwd,
                        'programma' => $content->applrcTrp->trp_name,
                        'fio' => $content->applrcPrs->prs_full_name,
                    ];
                }

                $f = function ($s) {
                    $style = 'style="border: 1px solid black;"';
                    $r = '<table >';
                    $r .= "<th $style>ФИО</th>";
                    $r .= "<th $style>Логин</th>";
                    $r .= "<th $style>Пароль</th>";
                    $r .= "<th $style>Программа</th>";
                    foreach ($s as $item) {
                        $r .= '<tr>';
                        $r .= "<td $style>".$item['fio'].'</td>';
                        $r .= "<td $style>".$item['login'].'</td>';
                        $r .= "<td $style>".$item['password'].'</td>';
                        $r .= "<td $style>".$item['programma'].'</td>';
                        $r .= '</tr>';
                    }
                    $r .= '</table>';
                    return $r;
                };

                if ($this->applr_flag_send == 1) {
                    if (isset($this->applr_curator_addr)) {
                        UserMail::sendMail(7, $this->applr_curator_addr,
                            [
                                ':personal' => $this->applr_flag,
                                ':reestr' => $this->applr_reestr,
                                'TABLE_FIO_LOGIN_PASSWORD_PROGRAMMA' => $f($table),
                            ]);
                    }

                    foreach ($this->applrManager->contacts as $contact) {
                        Yii::debug('менеджер '.$contact->con_text);
                        if ($contact->conCont->cont_type==1) {
                            UserMail::sendMail(7, $contact->con_text,
                                [
                                    ':personal' => $this->applr_flag,
                                    ':reestr' => $this->applr_reestr,
                                    'TABLE_FIO_LOGIN_PASSWORD_PROGRAMMA' => $f($table),
                                ]);
                        }
                    }
                }
            } catch (ImapErrorException $exception){
                $_SESSION['flash_appl_request_errors'] = [
                    'Сбои в работе IMAP', // <=== эта строка - желание заказчика
                    'Пропало соединение с сервером',
                    'По техническим причинам сервер не смог обработать сообщение или время обработки превысило максимальное',
                    'Отправлено неверное или содержащее спецсимволы сообщение (реже всего)',
                ];
                header('location: '.$_SERVER['REQUEST_URI']);
                exit;
            }

        } else {
            $this->updateMain(false);
        }
    }

    /**
     * @inheritdoc
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            ApplSheet::updateAll(['appls_applr_id' => null], ['appls_applr_id' => $this->applr_id]);
            ApplSh::updateAll(['appls_applr_id' => null], ['appls_applr_id' => $this->applr_id]);

            $this->updateMain(false);
            ApplRequestContent::deleteAll(['applrc_applr_id' => $this->applr_id]);
            return true;
        }
        return false;
    }

    /**
     * create|delete ApplMain
     * @param boolean $flag
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function updateMainDocumentWithoutPersonalEducation($flag = true, $personId) {

        $translit = function ($str) {
            $conv = [
                'a' => 'а',
                'b' => 'б',
                'c' => 'c',
                'd' => 'д',
                'e' => 'е',
                'f' => 'ф',
                'g' => 'г',
                'h' => 'х',
                'i' => 'и',
                'j' => 'ж',
                'k' => 'к',
                'l' => 'л',
                'm' => 'м',
                'n' => 'н',
                'o' => 'о',
                'p' => 'п',
                'q' => 'ю',
                'r' => 'р',
                's' => 'с',
                't' => 'т',
                'u' => 'у',
                'v' => 'в',
            ];

            return mb_strtoupper(strtr(strtolower($str), $conv));
        };


        $cont = ApplRequestContent::find()->where(['applrc_applr_id' => $this->applr_id])->all();
        if ($flag) {

            $agrAnnex = AgreementAnnex::findOne($this->applr_agra_id);
            $agreement = Agreement::findOne($agrAnnex->agra_agr_id);

            foreach ($cont as $key => $value) {
                if($value->applrc_prs_id != $personId){
                    continue;
                }
                if (!$obj = ApplMain::find()->where(['applm_applrc_id' => $value->applrc_id])->one()) {
                    $obj = new ApplMain();
                }

                $obj->applm_applrc_id = $value->applrc_id;
                $obj->applm_ab_id = $this->applr_ab_id;
                $obj->applm_comp_id = $this->applr_comp_id;
                $obj->applm_prs_id = $value->applrc_prs_id;
                $obj->applm_position = $value->applrc_position;
                $obj->applm_svc_id = $value->applrc_svc_id;
                $obj->applm_trp_id = $value->applrc_trp_id;
                $obj->applm_trt_id = $this->applr_trt_id;
                $obj->applm_svdt_id = $value->applrcSvc->svc_svdt_id;
                $obj->applm_agr_number = $agreement->agr_number;
                $obj->applm_agr_date = $agreement->agr_date;
                $obj->applm_reestr = $this->applr_reestr;

                $obj->applm_date_upk = Calendar::workDate($value->applrc_date_upk) ?? date('Y-m-d');

                if (is_null($value->applrc_date_upk)) {
                    if ($rc = ApplRequestContent::findOne($value->applrc_id)) {
                        $rc->applrc_date_upk = $obj->applm_date_upk;
                        $rc->check = true;
                        if (!$rc->save()) {
                            Yii::debug($rc->errors);
                        }
                    }
                }

                $u = uniqid();
                $u = substr($u, strlen($u)-5, 5);

                $prefix = 'ПК';
                // 2578 - последний номер записи перед изменениями логики префикса
                if($this->applr_id > self::MAX_REQUEST_ID_BEFORE_CHANGE_LOGIC){
                    try{
                        $pattern = Pattern::findOne(['pat_patt_id' =>PatternType::FINAL_DOCUMENT_TYPE_ID, 'pat_svdt_id' => $obj->applm_svdt_id, 'pat_trt_id' => $obj->applm_trt_id]);
                        $prefix = $pattern->pat_code;
                    } catch (\Exception $exception){
                        $prefix = 'ПК';
                    }
                }

                $obj->applm_number_upk =
                    $prefix .
                    \DateTime::createFromFormat('Y-m-d', $obj->applm_date_upk)->format('y/m') .
                    '-' .
                    $translit(strtoupper($u)); // ПКмм/гг-ХХХХХ

                // костыль для документов "диплом о проф переподготовке" требование заказчика, и он в курсе ситуации
                if ($value->applrcSvc->svc_svdt_id == 3){
                    $command = Yii::$app->db->createCommand('Select applm_number_upk from appl_main where applm_svdt_id = 3 order by applm_id desc limit 1');
                    $number = $command->queryScalar();
                    $intNumberPart = substr($number, 0, -2);
                    $obj->applm_number_upk = ((int)$intNumberPart+1).date('y');
                }

                $obj->applm_apple_date = $obj->applm_date_upk;
                // • Дата приказа о зачислении = Дата итогового документа – Рабочие дни
                // { Округление вверх до целого { (Кол-во часов Программы / 8 ) } }
                $prog = TrainingProg::findOne($value->applrc_trp_id);
                $days = $prog->trp_hour/8.00;
                $days = round(empty($days) ? 1 : $days + 0.5, 0, PHP_ROUND_HALF_UP)-1;
                $obj->applm_applcmd_date = Calendar::workDatePeriod($obj->applm_date_upk, $days, false, false);

                // • Дата промежуточного тестирования = Сдвиг вперед на ближайший рабочий день { округление вверх до целого { ( Дата итогового документа + Дата приказа о зачислении ) / 2 } }
                $days = round(date_diff(\DateTime::createFromFormat('Y-m-d', $obj->applm_date_upk), \DateTime::createFromFormat('Y-m-d', $obj->applm_applcmd_date))->days / 2);
                $obj->applm_appls0_date = Calendar::workDate(\DateTime::createFromFormat('Y-m-d', $obj->applm_applcmd_date)->modify("+$days day")->format('Y-m-d'));
                // • Дата ведомости промежуточного тестирования = Дата промежуточного тестирования
                $obj->applm_appls0x_date = $obj->applm_appls0_date;


                $obj->applm_appls_date = $obj->applm_date_upk;
                $obj->applm_applsx_date = $obj->applm_appls_date;


                $obj->applm_applr_date = Calendar::workDate(\DateTime::createFromFormat('Y-m-d', $obj->applm_applcmd_date)->modify('-3 day')->format('Y-m-d'));

                if (!$obj->save()) {
                    throw new HttpException(500, json_encode($obj->errors, JSON_UNESCAPED_UNICODE));
                }

                //Create new end document
                if($value->applrcApplr->applr_flag ==0){
                    $applfId = $this->createApplFinal($value->applrc_id);
                    if(!empty($applfId)){
                        $applFinalFile = new ApplFinalFile();
                        $applFinalFile->createFinalDocumentFile($applfId);
                    }
                }
            }
        } else {
            foreach ($cont as $key => $value) {
                $objs = ApplMain::find()
                    ->where([
                        'applm_applrc_id' => $value->applrc_id
                    ])->all();
                foreach ($objs as $k => $v) {
                    $v->delete();
                }
            }
        }
    }

    private function createApplFinal ($applrc_id) {
        if ($appl = ApplRequestContent::findOne($applrc_id)) {
            if ($main = ApplMain::find()->where(['applm_applrc_id' => $applrc_id])->one()) {
                $applf = ApplFinal::find()->where(['applf_prs_id' => $appl->applrcPrs->prs_id])->one();
                if ($applf == NULL) {
                    $model = new ApplFinal();
                    $model->applf_prs_id = $appl->applrcPrs->prs_id;
                    $model->applf_name_first = $appl->applrcPrs->prs_first_name;
                    $model->applf_name_last = $appl->applrcPrs->prs_last_name;
                    $model->applf_name_middle = $appl->applrcPrs->prs_middle_name;
                    $model->applf_name_full = $appl->applrcPrs->prs_full_name;
                    $model->applf_end_date = $appl->applrc_date_upk;
                    $model->applf_svdt_id = $appl->applrcSvc->svc_svdt_id;
                    $model->applf_trt_id = $appl->applrcApplr->applr_trt_id;
                    $model->applf_trp_id = $appl->applrc_trp_id;
                    $model->applf_trp_hour = $appl->applrcTrp->trp_hour;
                    $model->applf_cmd_date = $main->applm_applcmd_date ?? null;
                    $model->applf_number = $main->applm_number_upk ?? null;
                    $model->applf_applr_id = $appl->applrc_applr_id;
                    $model->applf_applm_id = $main->applm_id;

                    if ($model->save()) {
                        $appl->applrc_applf_id = $model->applf_id;
                        $appl->save(false);
                        return $model->applf_id;
                    }
                }
            }
        }
    }

    /**
     * create|delete ApplMain
     * @param boolean $flag
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    private function updateMain($flag) {
        $translit = function ($str) {
            $conv = [
                'a' => 'а',
                'b' => 'б',
                'c' => 'c',
                'd' => 'д',
                'e' => 'е',
                'f' => 'ф',
                'g' => 'г',
                'h' => 'х',
                'i' => 'и',
                'j' => 'ж',
                'k' => 'к',
                'l' => 'л',
                'm' => 'м',
                'n' => 'н',
                'o' => 'о',
                'p' => 'п',
                'q' => 'ю',
                'r' => 'р',
                's' => 'с',
                't' => 'т',
                'u' => 'у',
                'v' => 'в',
            ];
            
            return mb_strtoupper(strtr(strtolower($str), $conv));
        };

        if ($this->applr_flag == 0) return;

        $cont = ApplRequestContent::find()->where(['applrc_applr_id' => $this->applr_id])->all();
        if ($flag) {

            $agrAnnex = AgreementAnnex::findOne($this->applr_agra_id);
            $agreement = Agreement::findOne($agrAnnex->agra_agr_id);

            foreach ($cont as $key => $value) {
                if (!$obj = ApplMain::find()->where(['applm_applrc_id' => $value->applrc_id])->one()) {
                    $obj = new ApplMain();
                }
                $obj->applm_applrc_id = $value->applrc_id;
                $obj->applm_ab_id = $this->applr_ab_id;
                $obj->applm_comp_id = $this->applr_comp_id;
                $obj->applm_prs_id = $value->applrc_prs_id;
                $obj->applm_position = $value->applrc_position;
                $obj->applm_svc_id = $value->applrc_svc_id;
                $obj->applm_trp_id = $value->applrc_trp_id;
                $obj->applm_trt_id = $this->applr_trt_id;
                $obj->applm_svdt_id = $value->applrcSvc->svc_svdt_id;
                $obj->applm_agr_number = $agreement->agr_number;
                $obj->applm_agr_date = $agreement->agr_date;
                $obj->applm_reestr = $this->applr_reestr;

                $obj->applm_date_upk = Calendar::workDate($value->applrc_date_upk) ?? date('Y-m-d');

                if (is_null($value->applrc_date_upk)) {
                    if ($rc = ApplRequestContent::findOne($value->applrc_id)) {
                        $rc->applrc_date_upk = $obj->applm_date_upk;
                        $rc->check = true;
                        if (!$rc->save()) {
                            Yii::debug($rc->errors);
                        }
                    }
                }

                if ($this->applr_flag) {
                    $u = uniqid();
                    $u = substr($u, strlen($u)-5, 5);

                    $prefix = 'ПК';
                    // 2578 - последний номер записи перед изменениями логики префикса
                    if($this->applr_id > self::MAX_REQUEST_ID_BEFORE_CHANGE_LOGIC){
                        try{
                            $pattern = Pattern::findOne(['pat_patt_id' =>PatternType::FINAL_DOCUMENT_TYPE_ID, 'pat_svdt_id' => $obj->applm_svdt_id, 'pat_trt_id' => $obj->applm_trt_id]);
                            $prefix = $pattern->pat_code;
                        } catch (\Exception $exception){
                            $prefix = 'ПК';
                        }
                    }

                    $obj->applm_number_upk =
                        $prefix .
                        \DateTime::createFromFormat('Y-m-d', $obj->applm_date_upk)->format('y/m') .
                        '-' .
                            $translit(strtoupper($u)); // ПКмм/гг-ХХХХХ

                    // костыль для документов "диплом о проф переподготовке" требование заказчика, и он в курсе ситуации
                    if ($value->applrcSvc->svc_svdt_id == 3){
                        $command = Yii::$app->db->createCommand('Select applm_number_upk from appl_main where applm_svdt_id = 3 order by applm_id desc limit 1');
                        $number = $command->queryScalar();
                        $intNumberPart = substr($number, 0, -2);
                        $obj->applm_number_upk = ((int)$intNumberPart+1).date('y');
                    }
                }

                $obj->applm_apple_date = $obj->applm_date_upk;
                // • Дата приказа о зачислении = Дата итогового документа – Рабочие дни
                // { Округление вверх до целого { (Кол-во часов Программы / 8 ) } }
                $prog = TrainingProg::findOne($value->applrc_trp_id);
                $days = $prog->trp_hour/8.00;
                $days = round(empty($days) ? 1 : $days + 0.5, 0, PHP_ROUND_HALF_UP)-1;
                $obj->applm_applcmd_date = Calendar::workDatePeriod($obj->applm_date_upk, $days, false, false);

                // • Дата промежуточного тестирования = Сдвиг вперед на ближайший рабочий день { округление вверх до целого { ( Дата итогового документа + Дата приказа о зачислении ) / 2 } }
                $days = round(date_diff(\DateTime::createFromFormat('Y-m-d', $obj->applm_date_upk), \DateTime::createFromFormat('Y-m-d', $obj->applm_applcmd_date))->days / 2);
                $obj->applm_appls0_date = Calendar::workDate(\DateTime::createFromFormat('Y-m-d', $obj->applm_applcmd_date)->modify("+$days day")->format('Y-m-d'));
                // • Дата ведомости промежуточного тестирования = Дата промежуточного тестирования
                $obj->applm_appls0x_date = $obj->applm_appls0_date;


                $obj->applm_appls_date = $obj->applm_date_upk;
                $obj->applm_applsx_date = $obj->applm_appls_date;


                $obj->applm_applr_date = Calendar::workDate(\DateTime::createFromFormat('Y-m-d', $obj->applm_applcmd_date)->modify('-3 day')->format('Y-m-d'));

                if (!$obj->save()) {
                    throw new HttpException(500, json_encode($obj->errors, JSON_UNESCAPED_UNICODE));
                }
                //Yii::debug(json_encode($obj->errors, JSON_UNESCAPED_UNICODE));
            }
        } else {
            foreach ($cont as $key => $value) {
                $objs = ApplMain::find()
                    ->where([
                        'applm_applrc_id' => $value->applrc_id
                        ])->all();
                foreach ($objs as $k => $v) {
                    $v->delete();
                }
            }
        }
    }
}
