<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TrainingModule;

/**
 * TrainingModuleSearch represents the model behind the search form about `app\models\TrainingModule`.
 */
class TrainingModuleSearch extends TrainingModule
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['trm_id', 'trm_trm_id'], 'integer'],
            [['trm_name', 'trm_code', 'trm_dataA_name', 'trm_dataB_name', 'trm_create_user', 'trm_create_time', 'trm_create_ip', 'trm_update_user', 'trm_update_time', 'trm_update_ip', 'trm_test_question'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TrainingModule::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'trm_id' => $this->trm_id,
            'trm_trm_id' => $this->trm_trm_id,
        ]);

        $query
            ->andFilterWhere(['like', 'trm_name', $this->trm_name])
            ->andFilterWhere(['like', 'trm_code', $this->trm_code])
            ->andFilterWhere(['like', 'trm_dataA_name', $this->trm_dataA_name])
            ->andFilterWhere(['like', 'trm_dataB_name', $this->trm_dataB_name])
            ->andFilterWhere(['like', 'trm_create_user', $this->trm_create_user])
            ->andFilterWhere(['like', 'trm_create_time', $this->trm_create_time])
            ->andFilterWhere(['like', 'trm_create_ip', $this->trm_create_ip])
            ->andFilterWhere(['like', 'trm_update_user', $this->trm_update_user])
            ->andFilterWhere(['like', 'trm_update_time', $this->trm_update_time])
            ->andFilterWhere(['like', 'trm_update_ip', $this->trm_update_ip])
            ->andFilterWhere(['like', 'trm_test_question', $this->trm_test_question])
        ;

        return $dataProvider;
    }
}
