<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sent_emails_from_request".
 *
 * @property int $id
 * @property int $applr_id
 * @property int $applrc_prs_id
 * @property string $fio
 * @property string $login
 * @property int $applrc_trp_id
 * @property int $applr_ab_id
 * @property int $applr_manager_id
 * @property string $recipient_email
 * @property string $sender_email
 * @property int $status
 * @property string $send_date
 * @property int $applr_reestr
 * @property int $applr_flag
 * @property string $portal
 */
class SentEmailsFromRequest extends \yii\db\ActiveRecord
{


    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sent_emails_from_request';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['applr_id', 'applrc_prs_id', 'applrc_trp_id', 'applr_ab_id', 'applr_manager_id', 'status', 'applr_reestr', 'applr_flag'], 'integer'],
//            [['send_date', 'portal'], 'required'],
            [['send_date', 'portal'], 'safe'],
            [['fio', 'login', 'recipient_email', 'sender_email'], 'string', 'max' => 256],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'applr_id' => 'Applr ID',
            'applrc_prs_id' => 'Applrc Prs ID',
            'fio' => 'Fio',
            'login' => 'Login',
            'applrc_trp_id' => 'Applrc Trp ID',
            'applr_ab_id' => 'Applr Ab ID',
            'applr_manager_id' => 'Applr Manager ID',
            'recipient_email' => 'Recipient Email',
            'sender_email' => 'Sender Email',
            'status' => 'Status',
            'send_date' => 'Send Date',
            'applr_reestr' => 'Applr Reestr',
            'applr_flag' => 'Applr Flag',
            'portal' => 'Portal',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplrcTrp()
    {
        return $this->hasOne(TrainingProg::class, ['trp_id' => 'applrc_trp_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAb()
    {
        return $this->hasOne(Ab::class, ['ab_id' => 'applr_ab_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getManager()
    {
        return $this->hasOne(Ab::class, ['ab_id' => 'applr_manager_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplSh()
    {
        return $this->hasOne(ApplSh::class, ['appls_prs_id' => 'applrc_prs_id', 'appls_trp_id' => 'applrc_trp_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplSheet()
    {
        return $this->hasOne(ApplSheet::class, ['appls_prs_id' => 'applrc_prs_id', 'appls_trp_id' => 'applrc_trp_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplFinal()
    {
        return $this->hasOne(ApplFinal::class, ['applf_prs_id' => 'applrc_prs_id', 'applf_trp_id' => 'applrc_trp_id']);
    }
}
