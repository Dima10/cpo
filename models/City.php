<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%city}}".
 *
 * @property integer $city_id
 * @property integer $city_reg_id
 * @property string $city_name
 * @property string $city_create_user
 * @property string $city_create_time
 * @property string $city_create_ip
 * @property string $city_update_user
 * @property string $city_update_time
 * @property string $city_update_ip
 *
 * @property Address[] $addresses
 * @property Country $cityCou
 * @property Region $cityReg
 */
class City extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%city}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['city_cou_id', 'city_reg_id', 'city_name'], 'required'],
            [['city_reg_id', 'city_cou_id'], 'integer'],
            [['city_create_time', 'city_update_time'], 'safe'],
            [['city_name'], 'string', 'max' => 128],
            [['city_create_user', 'city_create_ip', 'city_update_user', 'city_update_ip'], 'string', 'max' => 64],
            [['city_reg_id', 'city_name'], 'unique', 'targetAttribute' => ['city_reg_id', 'city_name'], 'message' => 'The combination of City Reg ID and City Name has already been taken.'],
            [['city_reg_id'], 'exist', 'skipOnError' => true, 'targetClass' => Region::class, 'targetAttribute' => ['city_reg_id' => 'reg_id']],
            [['city_cou_id'], 'exist', 'skipOnError' => true, 'targetClass' => Country::class, 'targetAttribute' => ['city_cou_id' => 'cou_id']],
        ];
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'city_id' => Yii::t('app', 'City ID'),
            'city_cou_id' => Yii::t('app', 'City Cou ID'),
            'city_reg_id' => Yii::t('app', 'City Reg ID'),
            'city_name' => Yii::t('app', 'City Name'),
            'city_create_user' => Yii::t('app', 'Create User'),
            'city_create_time' => Yii::t('app', 'Create Time'),
            'city_create_ip' => Yii::t('app', 'Create Ip'),
            'city_update_user' => Yii::t('app', 'Update User'),
            'city_update_time' => Yii::t('app', 'Update Time'),
            'city_update_ip' => Yii::t('app', 'Update Ip'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddresses()
    {
        return $this->hasMany(Address::class, ['add_city_id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCityCou()
    {
        return $this->hasOne(Country::class, ['cou_id' => 'city_cou_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCityReg()
    {
        return $this->hasOne(Region::class, ['reg_id' => 'city_reg_id']);
    }

    /**
     * @return array
     */
    public function getRegions($id)
    {
        return ArrayHelper::map(Region::find()->where(['reg_cou_id' => $id])->all(), 'reg_id', 'reg_name');
    }

    /**
     * @inheritdoc
     * @return CityQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CityQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) return false;
        if ($insert) {
            $this->city_create_user = Yii::$app->user->identity->username;
            $this->city_create_ip = Yii::$app->request->userIP;
            $this->city_create_time = date('Y-m-d H:i:s');
            return true;
        } else {
            $this->city_update_user = Yii::$app->user->identity->username;
            $this->city_update_ip = Yii::$app->request->userIP;
            $this->city_update_time = date('Y-m-d H:i:s');
            return true;
        }
    }

}
