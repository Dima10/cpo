<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PaymentTo;

/**
 * PaymentToSearch represents the model behind the search form about `app\models\PaymentTo`.
 */
class PaymentToSearch extends PaymentTo
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['payt_id', 'payt_from_ab_id', 'payt_from_acc_id', 'payt_to_ab_id', 'payt_to_acc_id', 'payt_fbs_id', 'payt_svc_id', 'payt_svc_qty'], 'integer'],
            [['payt_svc_price', 'payt_svc_sum'], 'number'],
            [['payt_date_pay', 'payt_create_user', 'payt_create_time', 'payt_create_ip', 'payt_update_user', 'payt_update_time', 'payt_update_ip'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PaymentTo::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'payt_id' => $this->payt_id,
            'payt_from_ab_id' => $this->payt_from_ab_id,
            'payt_from_acc_id' => $this->payt_from_acc_id,
            'payt_to_ab_id' => $this->payt_to_ab_id,
            'payt_to_acc_id' => $this->payt_to_acc_id,
            'payt_fbs_id' => $this->payt_fbs_id,
            'payt_svc_id' => $this->payt_svc_id,
            'payt_svc_price' => $this->payt_svc_price,
            'payt_svc_qty' => $this->payt_svc_qty,
            'payt_svc_sum' => $this->payt_svc_sum,
            'payt_date_pay' => $this->payt_date_pay,
            'payt_create_time' => $this->payt_create_time,
            'payt_update_time' => $this->payt_update_time,
        ]);

        $query->andFilterWhere(['like', 'payt_create_user', $this->payt_create_user])
            ->andFilterWhere(['like', 'payt_create_ip', $this->payt_create_ip])
            ->andFilterWhere(['like', 'payt_update_user', $this->payt_update_user])
            ->andFilterWhere(['like', 'payt_update_ip', $this->payt_update_ip]);

        return $dataProvider;
    }
}
