<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TrainingAnswer;

/**
 * TrainingAnswerSearch represents the model behind the search form about `app\models\TrainingAnswer`.
 */
class TrainingAnswerSearch extends TrainingAnswer
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tra_id', 'tra_trq_id', 'tra_variant'], 'integer'],
            [['tra_answer', 'tra_create_user', 'tra_create_time', 'tra_create_ip', 'tra_update_user', 'tra_update_time', 'tra_update_ip'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TrainingAnswer::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'tra_id' => $this->tra_id,
            'tra_trq_id' => $this->tra_trq_id,
            'tra_variant' => $this->tra_variant,
            'tra_create_time' => $this->tra_create_time,
            'tra_update_time' => $this->tra_update_time,
        ]);

        $query->andFilterWhere(['like', 'tra_answer', $this->tra_answer])
            ->andFilterWhere(['like', 'tra_create_user', $this->tra_create_user])
            ->andFilterWhere(['like', 'tra_create_ip', $this->tra_create_ip])
            ->andFilterWhere(['like', 'tra_update_user', $this->tra_update_user])
            ->andFilterWhere(['like', 'tra_update_ip', $this->tra_update_ip]);

        return $dataProvider;
    }
}
