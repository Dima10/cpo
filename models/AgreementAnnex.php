<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%agreement_annex}}".
 *
 * Приложения
 *
 * @property integer $agra_id
 * @property integer $agra_ab_id
 * @property integer $agra_comp_id
 * @property string $agra_number
 * @property string $agra_date
 * @property integer $agra_agr_id
 * @property integer $agra_add_id
 * @property integer $agra_acc_id
 * @property integer $agra_prs_id 
 * @property string $agra_sum
 * @property string $agra_tax
 * @property integer $agra_pat_id
 * @property integer $agra_ast_id
 * @property string $agra_comment
 * @property string $agra_fdata
 * @property string $agra_fdata_sign
 * @property resource $agra_data
 * @property resource $agra_data_sign
 * @property string $agra_create_user
 * @property string $agra_create_time
 * @property string $agra_create_ip
 * @property string $agra_update_user
 * @property string $agra_update_time
 * @property string $agra_update_ip
 *
 *
 * @property AgreementAcc[] $agreementAccs 
 * @property AgreementAct[] $agreementActs
 * @property AgreementAnnexA[] $agreementAnnexAs
 * @property Agreement $agraAgr
 * @property Ab $agraAb
 * @property Pattern $agraPat
 * @property Account $agraAcc
 * @property Address $agraAdd
 * @property Company $agraComp
 * @property Person $agraPrs
 * @property AgreementStatus $agraAst 
 */

class AgreementAnnex extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%agreement_annex}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['agra_number', 'agra_date', 'agra_agr_id'], 'required'],
            [['agra_ab_id', 'agra_comp_id', 'agra_agr_id', 'agra_acc_id', 'agra_prs_id', 'agra_ast_id', 'agra_pat_id', 'agra_add_id'], 'integer'],
            [['agra_sum', 'agra_tax'], 'number'],
            [['agra_date'], 'string'],

            [['agra_comment'], 'string', 'max' => 1024],

            //[['agra_data', 'agra_data_sign'], 'string'],
            [['agra_data', 'agra_data_sign'], 'file', 'skipOnEmpty' => true, 'extensions' => 'rtf, pdf, docx'],


            [['agra_fdata', 'agra_fdata_sign'], 'string', 'max' => 256],
            [['agra_create_time', 'agra_update_time'], 'safe'],
            [['agra_number', 'agra_create_user', 'agra_create_ip', 'agra_update_user', 'agra_update_ip'], 'string', 'max' => 64],
            //[['agra_ab_id'], 'exist', 'skipOnError' => true, 'targetClass' => Ab::class, 'targetAttribute' => ['agra_ab_id' => 'ab_id']],

            [['agra_agr_id'], 'exist', 'skipOnError' => true, 'targetClass' => Agreement::class, 'targetAttribute' => ['agra_agr_id' => 'agr_id']],
            [['agra_acc_id'], 'exist', 'skipOnError' => true, 'targetClass' => Account::class, 'targetAttribute' => ['agra_acc_id' => 'acc_id']],
            [['agra_ab_id'], 'exist', 'skipOnError' => true, 'targetClass' => Ab::class, 'targetAttribute' => ['agra_ab_id' => 'ab_id']],
            [['agra_comp_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::class, 'targetAttribute' => ['agra_comp_id' => 'comp_id']],
            [['agra_pat_id'], 'exist', 'skipOnError' => true, 'targetClass' => Pattern::class, 'targetAttribute' => ['agra_pat_id' => 'pat_id']],
            [['agra_prs_id'], 'exist', 'skipOnError' => true, 'targetClass' => Person::class, 'targetAttribute' => ['agra_prs_id' => 'prs_id']],
            [['agra_ast_id'], 'exist', 'skipOnError' => true, 'targetClass' => AgreementStatus::class, 'targetAttribute' => ['agra_ast_id' => 'ast_id']],

            [['agra_number', 'agra_agr_id', 'agra_comp_id'], 'unique', 'targetAttribute' => ['agra_number', 'agra_agr_id', 'agra_comp_id'], 'message' => Yii::t('app', 'The combination of Agra Number and Agra Agr ID and Agra Comp ID has already been taken.')],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'agra_id' => Yii::t('app', 'Agra ID'),
            'agra_ab_id' => Yii::t('app', 'Agra Ab ID'),
            'agra_comp_id' => Yii::t('app', 'Agra Comp ID'),
            'agra_acc_id' => Yii::t('app', 'Agra Acc ID'),
            'agra_number' => Yii::t('app', 'Agra Number'),
            'agra_date' => Yii::t('app', 'Agra Date'),
            'agra_agr_id' => Yii::t('app', 'Agra Agr ID'),
            'agra_prs_id' => Yii::t('app', 'Agra Prs ID'),
            'agra_add_id' => Yii::t('app', 'Agra Add ID'),
            'agra_comment' => Yii::t('app', 'Agra Comment'),
            'agra_sum' => Yii::t('app', 'Agra Sum'),
            'agra_tax' => Yii::t('app', 'Agra Tax'),
            'agra_ast_id' => Yii::t('app', 'Agra Ast ID'), 
            'agra_pat_id' => Yii::t('app', 'Agra Pat ID'),
            'agra_data' => Yii::t('app', 'Agra Data'),
            'agra_data_sign' => Yii::t('app', 'Agra Data Sign'),
            'agra_fdata' => Yii::t('app', 'Agra Fdata'),
            'agra_fdata_sign' => Yii::t('app', 'Agra Fdata Sign'),
            'agra_create_user' => Yii::t('app', 'Agra Create User'),
            'agra_create_time' => Yii::t('app', 'Agra Create Time'),
            'agra_create_ip' => Yii::t('app', 'Agra Create Ip'),
            'agra_update_user' => Yii::t('app', 'Agra Update User'),
            'agra_update_time' => Yii::t('app', 'Agra Update Time'),
            'agra_update_ip' => Yii::t('app', 'Agra Update Ip'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgreementAnnexAs()
    {
        return $this->hasMany(AgreementAnnexA::class, ['ana_agra_id' => 'agra_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgreementAccs()
    {
        return $this->hasMany(AgreementAcc::class, ['aga_agra_id' => 'agra_id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgreementActs()
    {
        return $this->hasMany(AgreementAct::class, ['act_agra_id' => 'agra_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgraAgr()
    {
        return $this->hasOne(Agreement::class, ['agr_id' => 'agra_agr_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgraAdd()
    {
        return $this->hasOne(Address::class, ['add_id' => 'agra_add_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgraAb()
    {
        return $this->hasOne(Ab::class, ['ab_id' => 'agra_ab_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgraEnt()
    {
        return $this->hasOne(Entity::class, ['ent_id' => 'agra_ab_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgraAcc()
    {
        return $this->hasOne(Account::class, ['acc_id' => 'agra_acc_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgraPat()
    {
        return $this->hasOne(Pattern::class, ['pat_id' => 'agra_pat_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgraPrs()
    {
        return $this->hasOne(Person::class, ['prs_id' => 'agra_prs_id']);
    }

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getAgraComp()
    {
        return $this->hasOne(Company::class, ['comp_id' => 'agra_comp_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgraAst()
    {
        return $this->hasOne(AgreementStatus::class, ['ast_id' => 'agra_ast_id']);
    }


    /**
    * @inheritdoc
    * @return AgreementAnnexQuery the active query used by this AR class.
    */
    public static function find()
    {
        return new AgreementAnnexQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) return false;
        if ($insert) {
            $agr = Agreement::findOne($this->agra_agr_id);
            $this->agra_ab_id = $agr->agr_ab_id;
            $this->agra_comp_id = $agr->agr_comp_id;
            $this->agra_acc_id = $agr->agr_acc_id;
            $this->agra_prs_id = $agr->agr_prs_id;

            $this->agra_create_user = Yii::$app->user->identity->username;
            $this->agra_create_ip = Yii::$app->request->userIP;
            $this->agra_create_time = date('Y-m-d H:i:s');
            return true;
        } else {
            $this->agra_ab_id = Agreement::findOne($this->agra_agr_id)->agr_ab_id;

            $this->agra_update_user = Yii::$app->user->identity->username;
            $this->agra_update_ip = Yii::$app->request->userIP;
            $this->agra_update_time = date('Y-m-d H:i:s');
            return true;
        }
    }

    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            foreach (AgreementAnnexA::find()->where(['ana_agra_id' => $this->agra_id])->all() as $key => $value) {
                $value->delete();
            }
            return true;
        } else {
            return false;
        }
    }

}
