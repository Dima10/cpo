<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PaymentRet;

/**
 * PaymentRetSearch represents the model behind the search form about `app\models\PaymentRet`.
 */
class PaymentRetSearch extends PaymentRet
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ptr_id', 'ptr_ab_id', 'ptr_comp_id', 'ptr_aga_id', 'ptr_svc_id', 'ptr_aca_id', 'ptr_fb_id', 'ptr_fbs_id', 'ptr_pt_id'], 'integer'],
            [['ptr_svc_sum', 'ptr_svc_tax'], 'number'],
            [['ptr_comment', 'ptr_create_user', 'ptr_create_time', 'ptr_create_ip', 'ptr_update_user', 'ptr_update_time', 'ptr_update_ip'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PaymentRet::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ptr_id' => $this->ptr_id,
            'ptr_ab_id' => $this->ptr_ab_id,
            'ptr_comp_id' => $this->ptr_comp_id,
            'ptr_aga_id' => $this->ptr_aga_id,
            'ptr_aca_id' => $this->ptr_aca_id,
            'ptr_fb_id' => $this->ptr_fb_id,
            'ptr_fbs_id' => $this->ptr_fbs_id,
            'ptr_pt_id' => $this->ptr_pt_id,
            'ptr_svc_sum' => $this->ptr_svc_sum,
            'ptr_svc_tax' => $this->ptr_svc_tax,
            'ptr_create_time' => $this->ptr_create_time,
            'ptr_update_time' => $this->ptr_update_time,
        ]);

        $query
            ->andFilterWhere(['like', 'ptr_svc_sum', $this->ptr_svc_sum])
            ->andFilterWhere(['like', 'ptr_svc_tax', $this->ptr_svc_tax])
            ->andFilterWhere(['like', 'ptr_comment', $this->ptr_comment])
            ->andFilterWhere(['like', 'ptr_create_user', $this->ptr_create_user])
            ->andFilterWhere(['like', 'ptr_create_ip', $this->ptr_create_ip])
            ->andFilterWhere(['like', 'ptr_update_user', $this->ptr_update_user])
            ->andFilterWhere(['like', 'ptr_update_ip', $this->ptr_update_ip])
        ;

        return $dataProvider;
    }
}
