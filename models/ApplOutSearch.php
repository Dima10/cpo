<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ApplOut;

/**
 * ApplOutSearch represents the model behind the search form about `app\models\ApplOut`.
 */
class ApplOutSearch extends ApplOut
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['applout_id', 'applout_svdt_id', 'applout_trt_id'], 'integer'],
            [['applout_number', 'applout_date', 'applout_file_name', 'applout_file_data', 'applout_create_user', 'applout_create_time', 'applout_create_ip', 'applout_update_user', 'applout_update_time', 'applout_update_ip'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ApplOut::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
            'sort' => [
                'defaultOrder' => ['applout_date' => SORT_DESC],
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'applout_id' => $this->applout_id,
            'applout_trt_id' => $this->applout_trt_id,
            'applout_svdt_id' => $this->applout_svdt_id,
        ]);

        $query
            ->andFilterWhere(['like', 'applout_date', $this->applout_date])
            ->andFilterWhere(['like', 'applout_create_time', $this->applout_create_time])
            ->andFilterWhere(['like', 'applout_update_time', $this->applout_update_time])
            ->andFilterWhere(['like', 'applout_number', $this->applout_number])
            ->andFilterWhere(['like', 'applout_file_name', $this->applout_file_name])
            ->andFilterWhere(['like', 'applout_file_data', $this->applout_file_data])
            ->andFilterWhere(['like', 'applout_create_user', $this->applout_create_user])
            ->andFilterWhere(['like', 'applout_create_ip', $this->applout_create_ip])
            ->andFilterWhere(['like', 'applout_update_user', $this->applout_update_user])
            ->andFilterWhere(['like', 'applout_update_ip', $this->applout_update_ip])
        ;

        return $dataProvider;
    }
}
