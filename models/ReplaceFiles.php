<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;

class ReplaceFiles extends \yii\db\ActiveRecord
{

    public $replaceTypeId;
    public $idDocument;
    public $fileData;

    const PRIKAZ__O_ZACHISLENII = 1;
    const PRIKAZ__OB_OKONCHANII = 2;
    const PROTOKOL__KOMISII = 3;

    public static $_replaceTypes = [
        self::PRIKAZ__O_ZACHISLENII => 'Приказ о зачислении',
        self::PRIKAZ__OB_OKONCHANII => 'Приказ об окончании',
        self::PROTOKOL__KOMISII => 'Протокол комиссии',
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'replaceTypeId' => 'Тип заменяемого документа',
            'idDocument' => 'Код строки заменяемого документа',
            'fileData' => 'Файл',
        ];
    }

}
