<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2018-2019 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%system_mail}}".
 *
 * @property int $sm_id
 * @property string $sm_name
 * @property string $sm_server
 * @property string $sm_port
 * @property string $sm_user
 * @property string $sm_password
 * @property string $sm_encryption
 * @property string $sm_imap_server
 * @property string $sm_imap_user
 * @property string $sm_imap_password
 * @property string $sm_create_user
 * @property string $sm_create_time
 * @property string $sm_create_ip
 * @property string $sm_update_user
 * @property string $sm_update_time
 * @property string $sm_update_ip
 *
 * @property UserMail[] $userMails
 *
 */
class SystemMail extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%system_mail}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sm_name', 'sm_server', 'sm_port', 'sm_user'], 'required'],
            [['sm_port'], 'integer'],
            [['sm_create_time', 'sm_update_time'], 'safe'],
            [['sm_name', 'sm_server', 'sm_user', 'sm_password'], 'string', 'max' => 128],
            [['sm_imap_server', 'sm_imap_user', 'sm_imap_password'], 'string', 'max' => 128],
            [['sm_encryption'], 'string', 'max' => 32],
            [['sm_create_user', 'sm_create_ip', 'sm_update_user', 'sm_update_ip'], 'string', 'max' => 64],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'sm_id' => Yii::t('app', 'Sm ID'),
            'sm_name' => Yii::t('app', 'Sm Name'),
            'sm_server' => Yii::t('app', 'Sm Server'),
            'sm_port' => Yii::t('app', 'Sm Port'),
            'sm_user' => Yii::t('app', 'Sm User'),
            'sm_password' => Yii::t('app', 'Sm Password'),
            'sm_encryption' => Yii::t('app', 'Sm Encryption'),
            'sm_imap_server' => Yii::t('app', 'Sm Imap Server'),
            'sm_imap_user' => Yii::t('app', 'Sm Imap User'),
            'sm_imap_password' => Yii::t('app', 'Sm Imap Password'),
            'sm_create_user' => Yii::t('app', 'Sm Create User'),
            'sm_create_time' => Yii::t('app', 'Sm Create Time'),
            'sm_create_ip' => Yii::t('app', 'Sm Create Ip'),
            'sm_update_user' => Yii::t('app', 'Sm Update User'),
            'sm_update_time' => Yii::t('app', 'Sm Update Time'),
            'sm_update_ip' => Yii::t('app', 'Sm Update Ip'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserMails()
    {
        return $this->hasMany(UserMail::class, ['um_sm_id' => 'sm_id']);
    }

    /**
     * @inheritdoc
     * @return SystemMailQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SystemMailQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) return false;
        if ($insert) {
            $this->sm_create_user = Yii::$app->user->identity->username;
            $this->sm_create_ip = Yii::$app->request->userIP;
            $this->sm_create_time = date('Y-m-d H:i:s');
            return true;
        } else {
            $this->sm_update_user = Yii::$app->user->identity->username;
            $this->sm_update_ip = Yii::$app->request->userIP;
            $this->sm_update_time = date('Y-m-d H:i:s');
            return true;
        }
    }
}
