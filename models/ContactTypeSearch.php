<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ContactType;

/**
 * ContactTypeSearch represents the model behind the search form about `app\models\ContactType`.
 */
class ContactTypeSearch extends ContactType
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cont_id'], 'integer'],
            [['cont_name', 'cont_create_user', 'cont_create_time', 'cont_create_ip', 'cont_update_user', 'cont_update_time', 'cont_update_ip'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ContactType::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'cont_id' => $this->cont_id,
        ]);

        $query->andFilterWhere(['like', 'cont_name', $this->cont_name])
            ->andFilterWhere(['like', 'cont_create_user', $this->cont_create_user])
            ->andFilterWhere(['like', 'cont_create_ip', $this->cont_create_ip])
            ->andFilterWhere(['like', 'cont_update_user', $this->cont_update_user])
            ->andFilterWhere(['like', 'cont_create_time', $this->cont_create_time])
            ->andFilterWhere(['like', 'cont_update_time', $this->cont_update_time])
            ->andFilterWhere(['like', 'cont_update_ip', $this->cont_update_ip]);

        return $dataProvider;
    }
}
