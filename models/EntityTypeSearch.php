<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\EntityType;

/**
 * EntityTypeSearch represents the model behind the search form about `app\models\EntityType`.
 */
class EntityTypeSearch extends EntityType
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['entt_id'], 'integer'],
            [['entt_name', 'entt_name_short', 'entt_create_user', 'entt_create_time', 'entt_create_ip', 'entt_update_user', 'entt_update_time', 'entt_update_ip'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = EntityType::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'entt_id' => $this->entt_id,
        ]);

        $query->andFilterWhere(['like', 'entt_name', $this->entt_name])
            ->andFilterWhere(['like', 'entt_name_short', $this->entt_name_short])
            ->andFilterWhere(['like', 'entt_create_user', $this->entt_create_user])
            ->andFilterWhere(['like', 'entt_create_ip', $this->entt_create_ip])
            ->andFilterWhere(['like', 'entt_update_user', $this->entt_update_user])
            ->andFilterWhere(['like', 'entt_create_time', $this->entt_create_time])
            ->andFilterWhere(['like', 'entt_update_time', $this->entt_update_time])
            ->andFilterWhere(['like', 'entt_update_ip', $this->entt_update_ip]);

        return $dataProvider;
    }
}
