<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TrainingProg;

/**
 * TrainingProgSearch represents the model behind the search form about `app\models\TrainingProg`.
 */
class TrainingProgSearch extends TrainingProg
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['trp_id', 'trp_hour', 'trp_test_question', 'trp_reestr'], 'integer'],
            [['trp_name', 'trp_comment', 'trp_code', 'trp_dataA1', 'trp_dataA2', 'trp_dataA1_name', 'trp_dataA2_name', 'trp_dataB1', 'trp_dataB2', 'trp_dataB1_name', 'trp_dataB2_name', 'trp_create_user', 'trp_create_time', 'trp_create_ip', 'trp_update_user', 'trp_update_time', 'trp_update_ip'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TrainingProg::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'trp_id' => $this->trp_id,
            'trp_reestr' => (User::isSpecAdmin()) ? 0 : $this->trp_reestr,
        ]);

        $query
            ->andFilterWhere(['like', 'trp_name', $this->trp_name])
            ->andFilterWhere(['like', 'trp_code', $this->trp_code])
            ->andFilterWhere(['like', 'trp_hour', $this->trp_hour])
            ->andFilterWhere(['like', 'trp_comment', $this->trp_comment])
            ->andFilterWhere(['like', 'trp_dataA1_name', $this->trp_dataA1_name])
            ->andFilterWhere(['like', 'trp_dataA2_name', $this->trp_dataA2_name])
            ->andFilterWhere(['like', 'trp_dataB1_name', $this->trp_dataB1_name])
            ->andFilterWhere(['like', 'trp_dataB2_name', $this->trp_dataB2_name])
            ->andFilterWhere(['like', 'trp_create_user', $this->trp_create_user])
            ->andFilterWhere(['like', 'trp_create_ip', $this->trp_create_ip])
            ->andFilterWhere(['like', 'trp_create_time', $this->trp_create_time])
            ->andFilterWhere(['like', 'trp_update_user', $this->trp_update_user])
            ->andFilterWhere(['like', 'trp_update_ip', $this->trp_update_ip])
            ->andFilterWhere(['like', 'trp_update_time', $this->trp_update_time])
            ->andFilterWhere(['like', 'trp_test_question', $this->trp_test_question])
        ;

        return $dataProvider;
    }
}
