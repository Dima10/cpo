<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2018 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%training_prog_session}}".
 *
 * @property int $tps_id
 * @property string $tps_session_id
 * @property int $tps_trp_id
 * @property int $tps_prs_id
 * @property string $tps_create_user
 * @property string $tps_create_time
 * @property string $tps_create_ip
 *
 * @property Person $tpsPrs
 * @property TrainingProg $tpsTrp
 */
class TrainingProgSession extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%training_prog_session}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tps_session_id'], 'required'],
            [['tps_trp_id', 'tps_prs_id'], 'integer'],
            [['tps_create_time'], 'safe'],
            [['tps_session_id', 'tps_create_user', 'tps_create_ip'], 'string', 'max' => 64],
            [['tps_prs_id'], 'exist', 'skipOnError' => true, 'targetClass' => Person::class, 'targetAttribute' => ['tps_prs_id' => 'prs_id']],
            [['tps_trp_id'], 'exist', 'skipOnError' => true, 'targetClass' => TrainingProg::class, 'targetAttribute' => ['tps_trp_id' => 'trp_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'tps_id' => Yii::t('app', 'Tps ID'),
            'tps_session_id' => Yii::t('app', 'Tps Session ID'),
            'tps_trp_id' => Yii::t('app', 'Tps Trp ID'),
            'tps_prs_id' => Yii::t('app', 'Tps Prs ID'),
            'tps_create_user' => Yii::t('app', 'Tps Create User'),
            'tps_create_time' => Yii::t('app', 'Tps Create Time'),
            'tps_create_ip' => Yii::t('app', 'Tps Create Ip'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTpsPrs()
    {
        return $this->hasOne(Person::class, ['prs_id' => 'tps_prs_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTpsTrp()
    {
        return $this->hasOne(TrainingProg::class, ['trp_id' => 'tps_trp_id']);
    }

    /**
     * {@inheritdoc}
     * @return TrainingProgSessionQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TrainingProgSessionQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) return false;
        if ($insert) {
            $this->tps_create_user = Yii::$app->user->identity->username;
            $this->tps_create_ip = Yii::$app->request->userIP;
            $this->tps_create_time = date('Y-m-d H:i:s');
            return true;
        }
        return true;
    }

    /**
     * @param int $trp_id программа
     *
     */
    public static function saveData($trp_id)
    {
        if ($prs = Person::findOne(['prs_connect_user' => Yii::$app->user->identity->username]))
        {
            if (!$tps = TrainingProgSession::find()->where(['tps_prs_id' => $prs->prs_id, 'tps_trp_id' => $trp_id, 'tps_session_id' => yii::$app->session->id])->one()) {
                $tps = new TrainingProgSession();
                $tps->tps_prs_id = $prs->prs_id;
                $tps->tps_trp_id  = $trp_id;
                $tps->tps_session_id = yii::$app->session->id;
                $tps->save();
            }
        }

    }
}
