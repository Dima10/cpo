<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2019 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ApplRequest;

/**
 * ApplRequestSearch represents the model behind the search form about `app\models\ApplRequest`.
 */
class ApplRequestSearch extends ApplRequest
{

    public $comp_name;
    public $yurLico;
    public $agra_number;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['applr_id', 'applr_reestr', 'applr_ab_id', 'applr_comp_id', 'applr_agra_id', 'applr_flag', 'applr_trt_id', 'applr_ast_id'], 'integer'],
            [['applr_number', 'applr_date', 'applr_create_user', 'applr_create_time', 'applr_create_ip', 'applr_update_user', 'applr_update_time', 'applr_update_ip'], 'safe'],
            [['comp_name', 'agra_number', 'yurLico'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @param integer $id
     *
     * @return ActiveDataProvider
     */
    public function search($params, $id = null)
    {
        $query = ApplRequestSearch::find()
            ->leftJoin(Company::tableName().' as comp', 'applr_comp_id = comp_id')
            ->leftJoin(AgreementAnnex::tableName().' as agra', 'applr_agra_id = agra_id')
            ->leftJoin(Ab::tableName().'as ab', 'applr_ab_id = ab_id');
        if (!is_null($id)) {
            $query = $query->where(['applr_ab_id' => $id]);
        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['applr_date' => SORT_DESC],
            ]

        ]);

        $dataProvider->sort->attributes['applr_date'] = [
            'asc' => ['applr_date' => SORT_ASC],
            'desc' => ['applr_date' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['agra_number'] = [
            'asc' => ['agra_number' => SORT_ASC],
            'desc' => ['agra_number' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['yurLico'] = [
            'asc' => ['ab.ab_name' => SORT_ASC],
            'desc' => ['ab.ab_name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['comp_name'] = [
            'asc' => ['comp.comp_name' => SORT_ASC],
            'desc' => ['comp.comp_name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['agra_number'] = [
            'asc' => ['agra.agra_number' => SORT_ASC],
            'desc' => ['agra.agra_number' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $reestr = $this->applr_reestr;
        if (User::isSpecAdmin()) {
            $reestr = 0;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'applr_id' => $this->applr_id,
            'applr_ab_id' => $this->applr_ab_id,
            'applr_comp_id' => $this->applr_comp_id,
            'applr_agra_id' => $this->applr_agra_id,
            'applr_flag' => $this->applr_flag,
            'applr_reestr' => $reestr,
            'applr_trt_id' => $this->applr_trt_id,
            'applr_ast_id' => $this->applr_ast_id,
        ]);

        $query
            ->andFilterWhere(['like', 'comp_name', $this->comp_name])
            ->andFilterWhere(['like', 'agra_number', $this->agra_number])
            ->andFilterWhere(['like', 'applr_date', $this->applr_date])
            ->andFilterWhere(['like', 'applr_create_time', $this->applr_create_time])
            ->andFilterWhere(['like', 'applr_update_time', $this->applr_update_time])
            ->andFilterWhere(['like', 'applr_number', $this->applr_number])
            ->andFilterWhere(['like', 'applr_create_user', $this->applr_create_user])
            ->andFilterWhere(['like', 'applr_create_ip', $this->applr_create_ip])
            ->andFilterWhere(['like', 'applr_update_user', $this->applr_update_user])
            ->andFilterWhere(['like', 'applr_update_ip', $this->applr_update_ip])
            ->andFilterWhere(['like', 'ab.ab_name', $this->yurLico])
        ;

        return $dataProvider;
    }
}
