<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%appl_blank}}".
 *
 * @property integer $applblk_id
 * @property string $applblk_number
 * @property string $applblk_date
 * @property integer $applblk_qty_1
 * @property integer $applblk_qty_2
 * @property integer $applblk_qty_3
 * @property integer $applblk_qty_4
 * @property integer $applblk_qty_5
 * @property string $applblk_file_name
 * @property resource $applblk_file_data
 * @property string $applblk_create_user
 * @property string $applblk_create_time
 * @property string $applblk_create_ip
 * @property string $applblk_update_user
 * @property string $applblk_update_time
 * @property string $applblk_update_ip
 *
 * @property Pattern $applblkPat
 */
class ApplBlank extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%appl_blank}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['applblk_number', 'applblk_date'], 'required'],
            [['applblk_date', 'applblk_create_time', 'applblk_update_time'], 'safe'],
            [['applblk_qty_1', 'applblk_qty_2', 'applblk_qty_3', 'applblk_qty_4', 'applblk_qty_5'], 'integer'],
            [['applblk_file_data'], 'string'],
            [['applblk_number', 'applblk_create_user', 'applblk_create_ip', 'applblk_update_user', 'applblk_update_ip'], 'string', 'max' => 64],
            [['applblk_file_name'], 'string', 'max' => 256],
            [['applblk_pat_id'], 'exist', 'skipOnError' => true, 'targetClass' => Pattern::class, 'targetAttribute' => ['applblk_pat_id' => 'pat_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'applblk_id' => Yii::t('app', 'Applblk ID'),
            'applblk_number' => Yii::t('app', 'Applblk Number'),
            'applblk_date' => Yii::t('app', 'Applblk Date'),
            'applblk_qty_1' => Yii::t('app', 'Applblk Qty 1'),
            'applblk_qty_2' => Yii::t('app', 'Applblk Qty 2'),
            'applblk_qty_3' => Yii::t('app', 'Applblk Qty 3'),
            'applblk_qty_4' => Yii::t('app', 'Applblk Qty 4'),
            'applblk_qty_5' => Yii::t('app', 'Applblk Qty 5'),
            'applblk_pat_id' => Yii::t('app', 'Applblk Pat Id'),
            'applblk_file_name' => Yii::t('app', 'Applblk File Name'),
            'applblk_file_data' => Yii::t('app', 'Applblk File Data'),
            'applblk_create_user' => Yii::t('app', 'Applblk Create User'),
            'applblk_create_time' => Yii::t('app', 'Applblk Create Time'),
            'applblk_create_ip' => Yii::t('app', 'Applblk Create Ip'),
            'applblk_update_user' => Yii::t('app', 'Applblk Update User'),
            'applblk_update_time' => Yii::t('app', 'Applblk Update Time'),
            'applblk_update_ip' => Yii::t('app', 'Applblk Update Ip'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplblkPat()
    {
        return $this->hasOne(Pattern::class, ['pat_id' => 'applblk_pat_id']);
    }

    /**
     * @inheritdoc
     * @return ApplBlankQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ApplBlankQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) return false;
        if ($insert) {
            $this->applblk_create_user = Yii::$app->user->identity->username;
            $this->applblk_create_ip = Yii::$app->request->userIP;
            $this->applblk_create_time = date('Y-m-d H:i:s');
            return true;
        } else {
            $this->applblk_update_user = Yii::$app->user->identity->username;
            $this->applblk_update_ip = Yii::$app->request->userIP;
            $this->applblk_update_time = date('Y-m-d H:i:s');
            return true;
        }
    }
}
