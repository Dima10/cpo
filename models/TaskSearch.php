<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Task;

/**
 * TaskSearch represents the model behind the search form about `app\models\Task`.
 */
class TaskSearch extends Task
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['task_id', 'task_ab_id', 'task_taskt_id', 'task_tasks_id', 'task_user_id', 'task_event_id'], 'integer'],
            [['task_date_end', 'task_note', 'task_create_user', 'task_create_time', 'task_create_ip', 'task_update_user', 'task_update_time', 'task_update_ip'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Task::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'task_id' => $this->task_id,
            'task_ab_id' => $this->task_ab_id,
            'task_taskt_id' => $this->task_taskt_id,
            'task_date_end' => $this->task_date_end,
            'task_tasks_id' => $this->task_tasks_id,
            'task_user_id' => $this->task_user_id,
            'task_event_id' => $this->task_event_id,
            'task_create_time' => $this->task_create_time,
            'task_update_time' => $this->task_update_time,
        ]);

        $query->andFilterWhere(['like', 'task_note', $this->task_note])
            ->andFilterWhere(['like', 'task_create_user', $this->task_create_user])
            ->andFilterWhere(['like', 'task_create_ip', $this->task_create_ip])
            ->andFilterWhere(['like', 'task_update_user', $this->task_update_user])
            ->andFilterWhere(['like', 'task_update_ip', $this->task_update_ip]);

        return $dataProvider;
    }
}
