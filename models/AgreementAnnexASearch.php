<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\AgreementAnnexA;

/**
 * AgreementAnnexASearch represents the model behind the search form about `app\models\AgreementAnnexA`.
 */
class AgreementAnnexASearch extends AgreementAnnexA
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ana_id', 'ana_agra_id', 'ana_svc_id', 'ana_qty'], 'integer'],
            [['ana_cost_a', 'ana_price_a', 'ana_price', 'ana_tax'], 'number'],
            [['ana_create_user', 'ana_create_time', 'ana_create_ip', 'ana_update_user', 'ana_update_time', 'ana_update_ip'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AgreementAnnexA::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 0,
            ],

        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ana_id' => $this->ana_id,
            'ana_agra_id' => $this->ana_agra_id,
            'ana_svc_id' => $this->ana_svc_id,
            'ana_cost_a' => $this->ana_cost_a,
            'ana_price_a' => $this->ana_price_a,
            'ana_qty' => $this->ana_qty,
            'ana_price' => $this->ana_price,
            'ana_tax' => $this->ana_tax,
            'ana_create_time' => $this->ana_create_time,
            'ana_update_time' => $this->ana_update_time,
        ]);

        $query->andFilterWhere(['like', 'ana_create_user', $this->ana_create_user])
            ->andFilterWhere(['like', 'ana_create_ip', $this->ana_create_ip])
            ->andFilterWhere(['like', 'ana_update_user', $this->ana_update_user])
            ->andFilterWhere(['like', 'ana_update_ip', $this->ana_update_ip]);

        return $dataProvider;
    }
}
