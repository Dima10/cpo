<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Tag;

/**
 * TagSearch represents the model behind the search form about `app\models\Tag`.
 */
class TagSearch extends Tag
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tag_id'], 'integer'],
            [['tag_name', 'tag_comment', 'tag_create_user', 'tag_create_time', 'tag_create_ip', 'tag_update_user', 'tag_update_time', 'tag_update_ip'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Tag::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'tag_id' => $this->tag_id,
            'tag_create_time' => $this->tag_create_time,
            'tag_update_time' => $this->tag_update_time,
        ]);

        $query->andFilterWhere(['like', 'tag_name', $this->tag_name])
            ->andFilterWhere(['like', 'tag_comment', $this->tag_comment])
            ->andFilterWhere(['like', 'tag_create_user', $this->tag_create_user])
            ->andFilterWhere(['like', 'tag_create_ip', $this->tag_create_ip])
            ->andFilterWhere(['like', 'tag_update_user', $this->tag_update_user])
            ->andFilterWhere(['like', 'tag_update_ip', $this->tag_update_ip]);

        return $dataProvider;
    }
}
