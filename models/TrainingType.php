<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%training_type}}".
 *
 * @property integer $trt_id
 * @property string $trt_name
 * @property string $trt_create_user
 * @property string $trt_create_time
 * @property string $trt_create_ip
 * @property string $trt_update_user
 * @property string $trt_update_time
 * @property string $trt_update_ip
 *
 * @property ApplRequest[] $applRequests
 */
class TrainingType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%training_type}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['trt_name'], 'required'],
            [['trt_create_time', 'trt_update_time'], 'safe'],
            [['trt_name', 'trt_create_user', 'trt_create_ip', 'trt_update_user', 'trt_update_ip'], 'string', 'max' => 64],
            [['trt_name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'trt_id' => Yii::t('app', 'Trt ID'),
            'trt_name' => Yii::t('app', 'Trt Name'),
            'trt_create_user' => Yii::t('app', 'Trt Create User'),
            'trt_create_time' => Yii::t('app', 'Trt Create Time'),
            'trt_create_ip' => Yii::t('app', 'Trt Create Ip'),
            'trt_update_user' => Yii::t('app', 'Trt Update User'),
            'trt_update_time' => Yii::t('app', 'Trt Update Time'),
            'trt_update_ip' => Yii::t('app', 'Trt Update Ip'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplRequests()
    {
        return $this->hasMany(ApplRequest::class, ['applr_trt_id' => 'trt_id']);
    }

    /**
     * @inheritdoc
     * @return TrainingTypeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TrainingTypeQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) return false;
        if ($insert) {
            $this->trt_create_user = Yii::$app->user->identity->username;
            $this->trt_create_ip = Yii::$app->request->userIP;
            $this->trt_create_time = date('Y-m-d H:i:s');
            return true;
        } else {
            $this->trt_update_user = Yii::$app->user->identity->username;
            $this->trt_update_ip = Yii::$app->request->userIP;
            $this->trt_update_time = date('Y-m-d H:i:s');
            return true;
        }
    }
}
