<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Bank;

/**
 * BankSearch represents the model behind the search form about `app\models\Bank`.
 */
class BankSearch extends Bank
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bank_id'], 'integer'],
            [['bank_name', 'bank_bic', 'bank_account', 'bank_create_user', 'bank_create_time', 'bank_create_ip', 'bank_update_user', 'bank_update_time', 'bank_update_ip'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Bank::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'bank_id' => $this->bank_id,
        ]);

        $query->andFilterWhere(['like', 'bank_name', $this->bank_name])
            ->andFilterWhere(['like', 'bank_bic', $this->bank_bic])
            ->andFilterWhere(['like', 'bank_account', $this->bank_account])
            ->andFilterWhere(['like', 'bank_create_user', $this->bank_create_user])
            ->andFilterWhere(['like', 'bank_create_ip', $this->bank_create_ip])
            ->andFilterWhere(['like', 'bank_update_user', $this->bank_update_user])
            ->andFilterWhere(['like', 'bank_create_time', $this->bank_create_time])
            ->andFilterWhere(['like', 'bank_update_time', $this->bank_update_time])
            ->andFilterWhere(['like', 'bank_update_ip', $this->bank_update_ip]);

        return $dataProvider;
    }
}
