<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%payment_to}}".
 *
 * @property integer $payt_id
 * @property integer $payt_aca_id
 * @property integer payt_aga_id
 * @property integer $payt_from_ab_id
 * @property integer $payt_from_acc_id
 * @property integer $payt_to_ab_id
 * @property integer $payt_to_acc_id
 * @property integer $payt_fbs_id
 * @property integer $payt_pt_id
 * @property integer $payt_fb_id
 * @property integer $payt_svc_id
 * @property string $payt_svc_price
 * @property integer $payt_svc_qty
 * @property string $payt_svc_sum
 * @property string $payt_date_pay
 * @property string $payt_create_user
 * @property string $payt_create_time
 * @property string $payt_create_ip
 * @property string $payt_update_user
 * @property string $payt_update_time
 * @property string $payt_update_ip
 *
 * @property AgreementAccA $paytAca
 * @property AgreementAcc $paytAga
 * @property Ab $paytFromAb
 * @property Ab $paytToAb
 * @property Account $paytFromAcc
 * @property Account $paytToAcc
 * @property FinanceBookStatus $paytFbs
 * @property PaymentType $paytPt
 * @property FinanceBook $financeBook
 * @property Svc $paytSvc
 * @property FinanceBook $paytFb
 * @property Ab[] $toAbs
 * @property Account[] $fromAccounts
 * @property Account[] $toAccounts
 */
class PaymentTo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%payment_to}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['payt_aca_id', 'payt_aga_id', 'payt_from_ab_id', 'payt_to_ab_id', 'payt_fbs_id', 'payt_svc_id', 'payt_svc_price', 'payt_svc_qty', 'payt_date_pay'], 'required'],
            [['payt_aca_id', 'payt_aga_id', 'payt_from_ab_id', 'payt_from_acc_id', 'payt_to_ab_id', 'payt_to_acc_id', 'payt_fbs_id', 'payt_pt_id', 'payt_fb_id', 'payt_svc_id', 'payt_svc_qty'], 'integer'],
            [['payt_svc_price', 'payt_svc_sum'], 'number'],
            [['payt_date_pay', 'payt_create_time', 'payt_update_time'], 'safe'],
            [['payt_create_user', 'payt_create_ip', 'payt_update_user', 'payt_update_ip'], 'string', 'max' => 64],
            [['payt_from_ab_id'], 'exist', 'skipOnError' => true, 'targetClass' => Ab::class, 'targetAttribute' => ['payt_from_ab_id' => 'ab_id']],
            [['payt_to_ab_id'], 'exist', 'skipOnError' => true, 'targetClass' => Ab::class, 'targetAttribute' => ['payt_to_ab_id' => 'ab_id']],
            [['payt_from_acc_id'], 'exist', 'skipOnError' => true, 'targetClass' => Account::class, 'targetAttribute' => ['payt_from_acc_id' => 'acc_id']],
            [['payt_to_acc_id'], 'exist', 'skipOnError' => true, 'targetClass' => Account::class, 'targetAttribute' => ['payt_to_acc_id' => 'acc_id']],
            [['payt_fbs_id'], 'exist', 'skipOnError' => true, 'targetClass' => FinanceBookStatus::class, 'targetAttribute' => ['payt_fbs_id' => 'fbs_id']],
            [['payt_pt_id'], 'exist', 'skipOnError' => true, 'targetClass' => PaymentType::class, 'targetAttribute' => ['payt_pt_id' => 'pt_id']],
            [['payt_svc_id'], 'exist', 'skipOnError' => true, 'targetClass' => Svc::class, 'targetAttribute' => ['payt_svc_id' => 'svc_id']],
            [['payt_fb_id'], 'exist', 'skipOnError' => true, 'targetClass' => FinanceBook::class, 'targetAttribute' => ['payt_fb_id' => 'fb_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'payt_id' => Yii::t('app', 'Payt ID'),
            'payt_aca_id' => Yii::t('app', 'Payt Aca ID'),
            'payt_aga_id' => Yii::t('app', 'Payt Aga ID'),
            'payt_from_ab_id' => Yii::t('app', 'Payt From Ab ID'),
            'payt_from_acc_id' => Yii::t('app', 'Payt From Acc ID'),
            'payt_to_ab_id' => Yii::t('app', 'Payt To Ab ID'),
            'payt_to_acc_id' => Yii::t('app', 'Payt To Acc ID'),
            'payt_fbs_id' => Yii::t('app', 'Payt Fbs ID'),
            'payt_pt_id' => Yii::t('app', 'Payt Pt ID'),
            'payt_fb_id' => Yii::t('app', 'Payt Fb ID'),
            'payt_svc_id' => Yii::t('app', 'Payt Svc ID'),
            'payt_svc_price' => Yii::t('app', 'Payt Svc Price'),
            'payt_svc_qty' => Yii::t('app', 'Payt Svc Qty'),
            'payt_svc_sum' => Yii::t('app', 'Payt Svc Sum'),
            'payt_date_pay' => Yii::t('app', 'Payt Date Pay'),
            'payt_create_user' => Yii::t('app', 'Payt Create User'),
            'payt_create_time' => Yii::t('app', 'Payt Create Time'),
            'payt_create_ip' => Yii::t('app', 'Payt Create Ip'),
            'payt_update_user' => Yii::t('app', 'Payt Update User'),
            'payt_update_time' => Yii::t('app', 'Payt Update Time'),
            'payt_update_ip' => Yii::t('app', 'Payt Update Ip'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaytAca()
    {
        return $this->hasOne(AgreementAccA::class, ['aca_id' => 'payt_aca_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaytAga()
    {
        return $this->hasOne(AgreementAcc::class, ['aga_id' => 'payt_aga_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceBook()
    {
        return $this->hasOne(FinanceBook::class, ['fb_payt_id' => 'payt_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaytFb()
    {
        return $this->hasOne(FinanceBook::class, ['fb_id' => 'payt_fb_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaytFromAb()
    {
        return $this->hasOne(Ab::class, ['ab_id' => 'payt_from_ab_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaytToAb()
    {
        return $this->hasOne(Ab::class, ['ab_id' => 'payt_to_ab_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaytFromAcc()
    {
        return $this->hasOne(Account::class, ['acc_id' => 'payt_from_acc_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaytToAcc()
    {
        return $this->hasOne(Account::class, ['acc_id' => 'payt_to_acc_id']);
    }

    /**
     * @return array app\models\Ab
     */
    public function getToAbs()
    {
        return
            Ab::find()
                ->leftJoin(EntityClassLnk::tableName(),'entcl_ent_id = ab_id')
                ->leftJoin(EntityClass::tableName(), 'entc_id =entcl_entc_id')
                ->where(['like', 'entc_sy', 'A'])
                ->all();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFromAccounts()
    {
        return $this->hasMany(Account::class, ['acc_ab_id' => 'payt_from_ab_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getToAccounts()
    {
        return $this->hasMany(Account::class, ['acc_ab_id' => 'payt_to_ab_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaytFbs()
    {
        return $this->hasOne(FinanceBookStatus::class, ['fbs_id' => 'payt_fbs_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaytPt()
    {
        return $this->hasOne(PaymentType::class, ['pt_id' => 'payt_pt_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaytSvc()
    {
        return $this->hasOne(Svc::class, ['svc_id' => 'payt_svc_id']);
    }

    /**
     * @inheritdoc
     * @return PaymentToQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PaymentToQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) return false;

        $this->payt_svc_sum = round($this->payt_svc_qty * $this->payt_svc_price, 2);

        if ($insert) {
            $this->payt_create_user = Yii::$app->user->identity->username;
            $this->payt_create_ip = Yii::$app->request->userIP;
            $this->payt_create_time = date('Y-m-d H:i:s');
            return true;
        } else {
            $this->payt_update_user = Yii::$app->user->identity->username;
            $this->payt_update_ip = Yii::$app->request->userIP;
            $this->payt_update_time = date('Y-m-d H:i:s');
            return true;
        }
    }

    /**
     * @inheritdoc
     */
    public function financeBookUpdate()
    {
        // Insert|Update a linked FinanceBook record
        if (!$fb = FinanceBook::find()->where(['fb_payt_id' => $this->payt_id])->one()) {
            $fb = new FinanceBook();
            //$fb->fb_fbs_id = 1;
        }
        $fb->fb_fbs_id = $this->payt_fbs_id;
        $fb->fb_pt_id = $this->payt_pt_id;
        $fb->fb_svc_id = $this->paytSvc->svc_id;
        $fb->fb_payt_id = $this->payt_id;
        $fb->fb_aca_id = $this->payt_aca_id;
        $fb->fb_fbt_id = 2;
        $fb->fb_ab_id = $this->payt_to_ab_id;
        $fb->fb_comp_id = $this->payt_from_ab_id;
        $fb->fb_sum = $this->payt_svc_sum;
        $fb->fb_tax = 0;
        $fb->fb_aga_id = $this->payt_aga_id;
        $fb->fb_acc_id = $this->payt_to_acc_id;
        $fb->fb_date = $this->payt_date_pay;
        if ($fb->save()) {
            $this->payt_fb_id = $fb->fb_id;
            $this->save(true);
        }
        /*
        $fb->fb_payment
        $fb->fb_comment
        */

    }

    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            foreach (FinanceBook::find()->where(['fb_payt_id' => $this->payt_id])->all() as $key => $value) {
                $value->delete();
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * Cache modifying object
     * Read all objects
     * @return array
     */
    public static function cacheObjects(): array
    {
        if (Yii::$app->session->has('PaymentToCacheObject')) {
            return Yii::$app->session['PaymentToCacheObject'];
        }
        return [];
    }

    /**
     * Cache modifying object
     * Read object
     * @param integer $id
     * @return PaymentTo|null
     */
    public static function cacheObject($id)
    {
        if (isset(Yii::$app->session['PaymentToCacheObject'][$id]) && Yii::$app->session['PaymentToCacheObject'][$id] instanceof PaymentTo) {
            return  Yii::$app->session['PaymentToCacheObject'][$id];
        }
        return null;
    }

    /**
     * Cache modifying object
     * Update object in cache
     * @param PaymentTo $obj
     * @return boolean
     */
    public static function cacheUpdate(PaymentTo $obj)
    {
        $ob = Yii::$app->session['PaymentToCacheObject'];
        $ob[$obj->payt_id] = $obj;
        Yii::$app->session['PaymentToCacheObject'] = $ob;
        return true;
    }

    /**
     * Cache modifying object
     * Delete object from cache
     * @param PaymentTo $obj
     * @return boolean
     */
    public static function cacheDelete(PaymentTo $obj)
    {
        if ($obj instanceof PaymentTo) {
            $ob = Yii::$app->session['PaymentToCacheObject'];
            if (isset($ob[$obj->payt_id]))
                unset($ob[$obj->payt_id]);
            Yii::$app->session['PaymentToCacheObject'] = $ob;
            return true;
        }
        return false;
    }

    /**
     * Cache modifying object
     * Clear cache
     */
    public static function cacheClear()
    {
        Yii::$app->session['PaymentToCacheObject'] = [];
    }

    /**
     * Cache modifying object
     * Apply cache to database
     */
    public static function cacheApply()
    {
        foreach (PaymentTo::cacheObjects() as $key => $obj) {
            if ($obj instanceof PaymentTo) {
                if (PaymentTo::findOne($obj->payt_id)) {
                    $obj->save();
                    $obj->financeBookUpdate();
                } else {
                    $payt = new PaymentTo();
                    $payt->payt_aca_id = $obj-> payt_aca_id;
                    $payt->payt_aga_id = $obj-> payt_aga_id;
                    $payt->payt_from_ab_id = $obj-> payt_from_ab_id;
                    $payt->payt_from_acc_id = $obj-> payt_from_acc_id;
                    $payt->payt_to_ab_id = $obj-> payt_to_ab_id;
                    $payt->payt_to_acc_id = $obj-> payt_to_acc_id;
                    $payt->payt_fbs_id = $obj->payt_fbs_id;
                    $payt->payt_pt_id = $obj->payt_pt_id;
                    $payt->payt_svc_id = $obj->payt_svc_id;
                    $payt->payt_svc_price = $obj->payt_svc_price;
                    $payt->payt_svc_qty = $obj->payt_svc_qty;
                    $payt->payt_svc_sum = $obj->payt_svc_sum;
                    $payt->payt_date_pay = $obj->payt_date_pay;
                    if ($payt->save()) {
                        $payt->financeBookUpdate();
                    }
                }
            }
        }
        PaymentTo::cacheClear();
    }

    /**
     * Set next status
     */
    public function updatePay()
    {
        $this->cacheApply();
        if (!$fb = FinanceBook::find()->where(['fb_payt_id' => $this->payt_id])->one()) {
            die();
        }
        $fb->fb_fbs_id = 2;
        $fb->fb_pay = $this->payt_svc_sum;
        if ($fb->save()) {
            $this->payt_fbs_id = $fb->fb_fbs_id;
            $this->save();
        }
    }

}
