<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%appl_xxx_content}}".
 *
 * Протокол
 *
 * @property integer $applxxxc_id
 * @property integer $applxxxc_applxxx_id
 * @property integer $applxxxc_position
 * @property integer $applxxxc_prs_id
 * @property integer $applxxxc_ab_id
 * @property integer $applxxxc_passed
 * @property string $applxxxc_create_user
 * @property string $applxxxc_create_time
 * @property string $applxxxc_create_ip
 * @property string $applxxxc_update_user
 * @property string $applxxxc_update_time
 * @property string $applxxxc_update_ip
 *
 * @property Ab $applxxxcAb
 * @property Person $applxxxcPrs
 * @property ApplXxx $applxxxcApplxxx
 * @property TrainingProg $applxxxcTrp
 */
class ApplXxxContent extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%appl_xxx_content}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['applxxxc_applxxx_id', 'applxxxc_prs_id', 'applxxxc_passed', 'applxxxc_trp_id'], 'required'],
            [['applxxxc_applxxx_id', 'applxxxc_prs_id', 'applxxxc_trp_id', 'applxxxc_ab_id', 'applxxxc_passed'], 'integer'],
            [['applxxxc_create_time', 'applxxxc_update_time'], 'safe'],
            [['applxxxc_position'], 'string'],
            [['applxxxc_create_user', 'applxxxc_create_ip', 'applxxxc_update_user', 'applxxxc_update_ip'], 'string', 'max' => 64],
            [['applxxxc_prs_id'], 'exist', 'skipOnError' => true, 'targetClass' => Person::class, 'targetAttribute' => ['applxxxc_prs_id' => 'prs_id']],
            [['applxxxc_applxxx_id'], 'exist', 'skipOnError' => true, 'targetClass' => ApplXxx::class, 'targetAttribute' => ['applxxxc_applxxx_id' => 'applxxx_id']],
            [['applxxxc_ab_id'], 'exist', 'skipOnError' => true, 'targetClass' => Ab::class, 'targetAttribute' => ['applxxxc_ab_id' => 'ab_id']],
            [['applxxxc_trp_id'], 'exist', 'skipOnError' => true, 'targetClass' => TrainingProg::class, 'targetAttribute' => ['applxxxc_trp_id' => 'trp_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'applxxxc_id' => Yii::t('app', 'Applxxxc ID'),
            'applxxxc_applxxx_id' => Yii::t('app', 'Applxxxc applxxx ID'),
            'applxxxc_prs_id' => Yii::t('app', 'Applxxxc Prs ID'),
            'applxxxc_ab_id' => Yii::t('app', 'Applxxxc Ab ID'),
            'applxxxc_trp_id' => Yii::t('app', 'Applxxxc Trp ID'),
            'applxxxc_position' => Yii::t('app', 'Applxxxc Position'),
            'applxxxc_passed' => Yii::t('app', 'Applxxxc Passed'),
            'applxxxc_create_user' => Yii::t('app', 'Applxxxc Create User'),
            'applxxxc_create_time' => Yii::t('app', 'Applxxxc Create Time'),
            'applxxxc_create_ip' => Yii::t('app', 'Applxxxc Create Ip'),
            'applxxxc_update_user' => Yii::t('app', 'Applxxxc Update User'),
            'applxxxc_update_time' => Yii::t('app', 'Applxxxc Update Time'),
            'applxxxc_update_ip' => Yii::t('app', 'Applxxxc Update Ip'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplxxxcAb()
    {
        return $this->hasOne(Ab::class, ['ab_id' => 'applxxxc_ab_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplxxxcPrs()
    {
        return $this->hasOne(Person::class, ['prs_id' => 'applxxxc_prs_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplxxxcTrp()
    {
        return $this->hasOne(TrainingProg::class, ['trp_id' => 'applxxxc_trp_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplxxxcApplxxx()
    {
        return $this->hasOne(ApplXxx::class, ['applxxx_id' => 'applxxxc_applxxx_id']);
    }

    /**
     * @inheritdoc
     * @return ApplXxxContentQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ApplXxxContentQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) return false;
        if ($insert) {
            $this->applxxxc_create_user = Yii::$app->user->identity->username;
            $this->applxxxc_create_ip = Yii::$app->request->userIP;
            $this->applxxxc_create_time = date('Y-m-d H:i:s');
            return true;
        } else {
            $this->applxxxc_update_user = Yii::$app->user->identity->username;
            $this->applxxxc_update_ip = Yii::$app->request->userIP;
            $this->applxxxc_update_time = date('Y-m-d H:i:s');
            return true;
        }
    }
}
