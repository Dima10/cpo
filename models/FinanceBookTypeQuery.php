<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[FinanceBookType]].
 *
 * @see FinanceBookType
 */
class FinanceBookTypeQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return FinanceBookType[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return FinanceBookType|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
