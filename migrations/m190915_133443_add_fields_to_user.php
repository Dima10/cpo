<?php

use yii\db\Migration;

/**
 * Class m190915_133443_add_fields_to_user
 */
class m190915_133443_add_fields_to_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("ALTER TABLE `user`
            ADD COLUMN `user_create_user` VARCHAR(64),
            ADD COLUMN `user_create_time` DATETIME,
            ADD COLUMN `user_create_ip` VARCHAR(64),
            ADD COLUMN `user_update_user` VARCHAR(64),
            ADD COLUMN `user_update_time` DATETIME,
            ADD COLUMN `user_update_ip` VARCHAR(64);
        ");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->execute("ALTER TABLE `user`
            DROP COLUMN `user_create_user`,
            DROP COLUMN `user_create_time`,
            DROP COLUMN `user_create_ip`,
            DROP COLUMN `user_update_user`,
            DROP COLUMN `user_update_time`,
            DROP COLUMN `user_update_ip`;
        ");
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190915_133443_add_fields_to_user cannot be reverted.\n";

        return false;
    }
    */
}
